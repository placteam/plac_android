import React, { Component } from 'react';
import {StyleSheet,AppRegistry, Text, View,TouchableOpacity} from 'react-native';

var params;
var plac;
export default class FirstComponent extends Component {


  constructor(props){
    super(props);
    this.state={
      number:0,
    }


  }

  render() {
  return  <View  style={[styles.container]}>
             <TouchableOpacity onPress={()=>this.manageAction('plus')} style={styles.buttonActionPlus} >
                <Text>+</Text>
             </TouchableOpacity>


             <TouchableOpacity onPress={()=>this.manageAction('minus')} style={styles.buttonActionMinus} >
                <Text>-</Text>
             </TouchableOpacity>

             <Text style={{color: 'white',fontSize: 40,marginTop:50}}>
               {this.state.number}
             </Text>
          </View>
  }


  manageAction=(type)=>{
        let number=this.state.number;
        if(type=='plus'){
           number++;
        }else{
           number--;
        }

        this.setState({number});

  }


  onPressButton=()=>{
    var data={
      name:"Carlos",
      "phone":"3503429563",
    }
    this.props.onPressButtonCallBack(data);
  }

}

const styles = StyleSheet.create({
  container: {

    justifyContent: 'center',
    alignItems: 'center',
    width: 200,
    height: 200,
    backgroundColor: 'blue'

  },
  customStyleCircle:{
    borderRadius: 20,
    width: 40,
    height:40,
  },
  buttonActionPlus:{
     justifyContent: 'center',
     alignItems: 'center',
     width: 100,
     height: 36,
     backgroundColor: 'yellow',
  },
  buttonActionMinus:{
     justifyContent: 'center',
     alignItems: 'center',
     width: 100,
     height: 36,
     backgroundColor: 'red',
  }

});
