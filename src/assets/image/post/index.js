import React from 'react';
import Svg,{
    Circle,
    Ellipse,
    G,
    Lineargradient,
    Radialgradient,
    Line,
    Path,
    Polygon,
    Polyline,
    Rect,
    Symbol,
    Use,
    Defs,
    Stop
} from 'react-native-svg';



//post



export function getIconComment(){
return  <Svg width="24" height="24" viewBox="0 0 24 24">
            <Path  fill="#9E9E9E" d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z"/>
            <Path d="M0 0h24v24H0z" fill="none"/>
        </Svg>
}

export function getIconCommentPress(){
return  <Svg width="24" height="24" viewBox="0 0 24 24">
            <Path fill="#15d2bb" d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z"/>
            <Path d="M0 0h24v24H0z" fill="none"/>
        </Svg>
}


export function getMenuDots(){
  return <Svg width="24" height="24" viewBox="0 0 24 24">
            <Path fill="#15d2bb" d="M0 0h24v24H0z" fill="none"/>
            <Path d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/>
        </Svg>
}
