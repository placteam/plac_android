import React,{ Component }from 'react'
import {View,Text,TouchableOpacity,  BackHandler,StatusBar,SafeAreaView,Platform,Linking} from 'react-native'

//React navigation
import { createSwitchNavigator, createStackNavigator} from 'react-navigation';

//STACKS
import MainStack from './components/config/navigation/main'
import AuthStack from './components/config/navigation/auth'
import AuthLoadingScreen from './components/auth/AuthLoadingScreen'
import Profile from './components/main/profile/Profile';




export default class App extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    const prefix = Platform.OS == 'android' ? 'placapp://placapp/' : 'placapp://';

    return   <View style={{flex:1,backgroundColor: '#6a51ae' }}>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor="#15D2BB"
                  />
                  <Navigation uriPrefix={prefix} />
              </View>
  }




}


const Navigation =  createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    main: {screen:MainStack,path:'main'},
    auth: AuthStack,
  }

);

const goForward = () => {
  const { navigation } = this.props
  const screenNumber = navigation.state.params ? navigation.state.params.screenNumber : 0
  const params = { screenNumber: screenNumber + 1 }
  if (Math.random() > .75) params.plain = true // Include a 'plain' param 25% of the time
   navigation.navigate('Go', params)
}



const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps

      const thisSceneIndex = scene.index
      const width = layout.initWidth

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0],
      })

      return { transform: [ { translateX } ] }
    },
  }
}
