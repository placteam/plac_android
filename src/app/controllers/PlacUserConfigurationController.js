
import realm from "../../config/realm"
import * as Constants from "../constants"


export function index(){
  return realm.objects(Constants.MODEL_PLAC_USER_CONFIGURATION_NAME);
}


export function store(object){
   realm.write(() => {
        realm.create(Constants.MODEL_PLAC_USER_CONFIGURATION_NAME,object);
   });
   return true;
}



export function manage(placUserId,profileId){
    let pucs = realm.objects(Constants.MODEL_PLAC_USER_CONFIGURATION_NAME);
    let placUserConfiguration=pucs.filtered('plac_user_id = "'+placUserId+'"');
    let  object={
      profile_id_selected:profileId,
      plac_user_id:placUserId
    }

    if(placUserConfiguration.length>0){
       update(object);
    }else{
       store(object);
    }

}


export function show(id){
  let users = realm.objects(Constants.MODEL_PLAC_USER_CONFIGURATION_NAME);
  return users.filtered('plac_user_id = "'+id+'"')[0];
}


export function update(object){
   let pucRealm={
    ...object,
   }
   realm.write(() => {
        realm.create(Constants.MODEL_PLAC_USER_CONFIGURATION_NAME,pucRealm,true);
   });
}

export function destroy(){
      realm.write(() => {
          let users = realm.objects(Constants.MODEL_PLAC_USER_CONFIGURATION_NAME);
        // Delete the book
        realm.delete(users);
    });
}
