
import realm from "../../config/realm"
export function store(product, place, quantity) {

    let total = parseFloat(product.price_final) * quantity;
    let productJSON = JSON.stringify(product);
    let placeJSON = JSON.stringify(place);
    let orderDetail = {
        order_detail_id: generateUniqueID(),
        product_id: product.product_id,
        productJSON,
        place_id: place.place_id,
        placeJSON,
        price: parseFloat(product.product_price),
        quantity,
        discount: parseFloat(product.product_discount_rate),
        total,
    }

    realm.write(() => {
        realm.create('order_details', orderDetail);
    });

    return { type: "order_details", success: true, object: orderDetail };
}


export function update(product, orderDetail, quantity) {

    if (quantity == 0) {
        return deleteOrderDetailByProductId(product.product_id);
    }

    realm.write(() => {
        productPrice = parseFloat(product.price_final);
        orderDetail.quantity = quantity;
        total = productPrice * quantity;
        orderDetail.total = total;
        realm.create('order_details', orderDetail, true);
    });
}

export function manageQuantities(product, place, quantity) {
    placeId = place.place_id;
    productId = product.product_id;
    let orderDetail = getOrderDetail(placeId, productId);
    if (orderDetail.length > 0) { 
        update(product, orderDetail[0], quantity);
    } else {
        store(product, place, quantity);
    }

    return true;
}


export function getOrderDetail(placeId, productId) {
    var orderDetailRealm = realm.objects('order_details');
    var orderDetail = orderDetailRealm.filtered('place_id = "' + placeId + '" AND product_id = "' + productId + '"');
    return orderDetail;
}

export function getQuantityProductsInOrder(){
    var orderDetails = realm.objects('order_details');
    
    let quantities=0;
    orderDetails.forEach(orderDetail => {
        quantities+=orderDetail.quantity;
    });
    return quantities;
}


export function getOrderDetailsByPlace(){
   let places=[];
   let orderDetails=realm.objects('order_details');
   orderDetails.forEach(orderDetail => {
       placeJSON=orderDetail.placeJSON;
       if(places.indexOf(placeJSON)==-1){
        places.push(placeJSON);
      }     
       
   });
   var i=0;
   places.forEach(place => {
    place=JSON.parse(place);
    let orderDetails = realm.objects('order_details').filtered('place_id = "'+place.place_id+'"' );
    let total=0;
    orderDetails.forEach(orderDetail => {
        total+=orderDetail.total;
    });
    place.total=total;
    place.products=orderDetails;
    places[i]=place;
    i++;
       
   });
  return places;

}

export function getResumedCheckoutByPlaceId(placeId){
   let total=0;
   let orderDetails = realm.objects('order_details').filtered('place_id = "'+placeId+'"');

   orderDetails.forEach(orderDetail => {
       total+=orderDetail.total;
   });

   return [orderDetails,total];
 
 }



function deleteOrderDetailByProductId(productId) {
    realm.write(() => {
        let orders = realm.objects('order_details').filtered('product_id = "' + productId + '"');
        realm.delete(orders);
    });
}



export function deleteOrderDetailsByPlaceId(placeId) {
    realm.write(() => {
        let orders = realm.objects('order_details').filtered('place_id= "' + placeId + '"');
        realm.delete(orders);
    });
}


export function destroy(){
    realm.write(() => {
        let  orders = realm.objects('order_details');
      // Delete the book
        realm.delete(orders);
    });
}




function generateUniqueID() {
    let id = Math.random().toString(36).substr(2, 9);
    let orders = realm.objects('order_details').filtered('order_detail_id = "' + id + '"');
    if (orders.length > 0) {
       generateUniqueID();
    } else {
        return id;
    }
}
