import realm from "../../config/realm"
import * as Constants from "../../config/constants"
import  * as ProfileController from './ProfileController';
import * as PlacUserConfigurationController from './PlacUserConfigurationController'

export function store(request){
  user=request.user;
  deviceToken=request.deviceToken;
  var placUser={
    plac_user_id:user.plac_user_id,
    plac_user_name:user.plac_user_name,
    plac_user_image:user.plac_user_image,
    plac_user_gender:user.plac_user_gender,
    plac_user_work:user.plac_user_work,
    plac_user_email:user.plac_user_email,
    plac_user_instagram_link:user.plac_user_instagram_link,
    plac_user_twitter_link:user.plac_user_twiter_link,
    plac_user_facebook_link:user.plac_user_facebook_link,
    plac_user_device_token:deviceToken,
    plac_user_cellphone:(user.plac_user_cellphone)?user.plac_user_cellphone:"",
    plac_user_city:(user.plac_user_city)?user.plac_user_city:"",
    plac_user_accepted_terms:user.plac_user_accepted_terms
  }

 realm.write(() => {
      realm.create(Constants.MODEL_PLAC_USER_NAME, placUser);
 });

 profiles=user.profiles;

 if(profiles.length>0){
     for(i=0;i<profiles.length;i++){
       ProfileController.store(profiles[i]);
     }
     PlacUserConfigurationController.manage(user.plac_user_id,profiles[profiles.length-1].profile_id);
 }


 return true;
}

export function update(userChanges){
   let userRealm={
    ...userChanges
   }

  
   
   realm.write(() => {
        realm.create(Constants.MODEL_PLAC_USER_NAME,userRealm,true);
   });
}

export function getCurrentUser(){
    let configuration = realm.objects(Constants.MODEL_APP_CONFIGURATION_NAME)[0];
    let user=null;
    if(configuration){
      user= show(configuration.plac_user_id_selected);
    }

    return user;
}

export function getCurrentUserOld(){
  let users = realm.objects(Constants.MODEL_PLAC_USER_NAME);
  return users;
}

export function show(id){
  let users = realm.objects(Constants.MODEL_PLAC_USER_NAME);
  return users.filtered('plac_user_id = "'+id+'"')[0];
}


export function destroy(){
    realm.write(() => {
        let users = realm.objects(Constants.MODEL_PLAC_USER_NAME);
      // Delete the book
      realm.delete(users);
});
}
