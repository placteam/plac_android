
import realm from "../../config/realm"
import * as Constants from "../../config/constants"

export function index(){
  return realm.objects(Constants.MODEL_APP_CONFIGURATION_NAME);
}


export function store(appConfiguration){
   realm.write(() => {
        realm.create(Constants.MODEL_APP_CONFIGURATION_NAME,appConfiguration);
   });
   return true;
}


export function show(){

}


export function manage(deviceToken,placUserId){
   let appConfigurations=index();
    if(appConfigurations.length==0){
      store({app_configuration_id:deviceToken,plac_user_id_selected: placUserId});
    }else{
      update({plac_user_id_selected:placUserId},appConfigurations[0].app_configuration_id);
    }
}

export function update(object,app_configuration_id){
   let acRealm={
     app_configuration_id,
    ...object,
   }
   realm.write(() => {
        realm.create(Constants.MODEL_APP_CONFIGURATION_NAME,acRealm,true);
   });
}



export function destroy(){
  realm.write(() => {
   appConfigurations=realm.objects(Constants.MODEL_APP_CONFIGURATION_NAME);
     realm.delete(appConfigurations);
   });
}
