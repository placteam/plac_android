import realm from "../../config/realm"
import * as Constants from "../constants"

export function store(profile){

  var profileRealm={
    profile_id:profile.profile_id,
    profile_name:profile.profile_name,
    profile_image:profile.profile_image,
    profile_unique_name:profile.profile_unique_name,
    profile_type:profile.profile_type,
    plac_user_id:profile.plac_user_id,
    isSelected:1,
    is_private:0,
  };

  if(profile.profile_type=='company'){
      profileRealm.company_categories=profile.company_categories;
      profileRealm.company_address=profile.company_address;
      profileRealm.company_email=profile.company_email;
      profileRealm.company_hour_office_start=profile.company_hour_office_start;
      profileRealm.company_hour_office_end=profile.company_hour_office_end;
      profileRealm.company_categories_name=profile.company_categories_name;
  }else{
      profileRealm.pet_type=profile.pet_type;
      profileRealm.pet_gender=profile.pet_gender;
      profileRealm.pet_birthday=profile.pet_birthday;
      profileRealm.breed_id=profile.breed.breed_id;
      profileRealm.breed_name=profile.breed.breed_name;
  }
   realm.write(() => {
        realm.create(Constants.MODEL_PROFILE_NAME, profileRealm);
   });
}



export function getProfilesSelected(placUserId){
  let profiles = realm.objects(Constants.MODEL_PROFILE_NAME);
  return profiles.filtered('plac_user_id = "'+placUserId+'"');
}

export function getCurrentProfileSelectedByUser(placUserId){
    let pucs = realm.objects(Constants.MODEL_PLAC_USER_CONFIGURATION_NAME);
    var puc=  pucs.filtered('plac_user_id = "'+placUserId+'"')[0];
    profile=null;
    if(puc){
     profile= show(puc.profile_id_selected);
    }

    return profile;


}

export function show(id){
  let users = realm.objects(Constants.MODEL_PROFILE_NAME);
  return users.filtered('profile_id = "'+id+'"')[0];
}

export function update(profile){
   let profileRealm={
    ...profile
   }
   
   realm.write(() => {
        realm.create(Constants.MODEL_PROFILE_NAME, profileRealm,true);
   });
}


export function getProfilesByUser(placUserId){
  let profiles = realm.objects(Constants.MODEL_PROFILE_NAME);
  return profiles.filtered('plac_user_id = "'+placUserId+'"');
}



export function destroy(){
    realm.write(() => {
        let users = realm.objects(Constants.MODEL_PROFILE_NAME);
      // Delete the book
      realm.delete(users);
});
}
