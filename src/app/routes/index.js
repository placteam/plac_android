//Authentication
export const  URL_PLAC_USERS_MANAGE_LOGIN='authentication/manage/login';
export const  URL_PLAC_USER_LINK_ACCOUNT="authentication/account/link/phone"
export const  URL_VERIFY_LINK_CODE="authentication/account/verify/link/code"



//PLAC USERS
export const URL_PLAC_USER_RESOURCE="placusers";



//PROFILES
export const URL_GET_SEARCH_BREEDS="profiles/pet/breed/search/";
export const URL_PROFILE_VERIFY_UNIQUE_NAME="profiles/uniquename/verify/";
export const URL_GET_PROFILE_DATA="profiles/show/";
export const URL_PROFILES_BYUSER="profiles/byUser/";
export const URL_PROFILE_FOLLOWING_MANAGE="profiles/following/manage";
export const URL_GET_PROFILES_RECENTS="profiles/recents/";
export const URL_GET_PROFILES_SUGGEST="profiles/suggest/filters";
export const URL_SEARCH_PROFILES="profiles/search";
export const URL_PROFILES_RESOURCE="profiles";



//PROFILES FOLLOWINGS
export const URL_PROFILES_FOLLOWING_COUNT="profiles/followings/count/";
export const URL_GET_PROFILE_FOLLOWING_FROM="profiles/followings/from/get/";
export const URL_GET_PROFILE_FOLLOWING_TO="profiles/followings/to/get/";

//POSTS

export const URL_GET_POST_FOLLOWING="profiles/posts/following"
export const URL_POST_RESOURCE="profiles/posts";
export const URL_MANAGE_POST_LIKES="profiles/posts/likes/manage"
export const URL_GET_POST_LIKES="profiles/posts/likes/get"
export const URL_GET_POST_COMMENTS="profiles/posts/comments/get"
export const URL_POST_COMMENTS_RESOURCE="profiles/posts/comments"
export const URL_GET_POST_SINGLE="profiles/posts/single"

export const URL_PROFILE_POSTS="profiles/posts/get/";

export const URL_GET_POST_COMPLAINTS="profiles/posts/complaints/all";
export const URL_POST_COMPLAINTS_RESOURCE="profiles/posts/reports";



//NOTIFICATIONS 
export const URL_GET_NOTIFICATIONS_POSTS='notifications/posts/me';
export const URL_GET_NOTIFICATIONS_FOLLOWERS='notifications/followers/me'
export const URL_GET_NOTIFICATIONS_GENERAL='notifications/user'
export const URL_RESOURCE_NOTIFICATIONS='notifications'

export const URL_GET_NOTIFICATION_SETTINGS_USER="notifications/settings/users/"
export const URL_RESOURCE_NOTIFICATION_SETTINGS="notifications/settings"
/*STORE*/

export const URL_RESOURCE_CITIES="cities";
//PLACES
export const URL_GET_PLACES_RATING_BRIEF="places/rating/brief"
export const URL_GET_PLACES_RATING_REVIEWS="places/rating/reviews"
export const URL_SHOW_PLACE_NAME="places/show/name";
export const URL_RESOURCE_PLACES="places";


//PRODUCTS CATEGEORIES 
export const URL_RESOURCE_PRODUCT_CATEGORIES="products/categories";
export const URL_GET_PRODUCT_SUBCATEGORIES_BY_FILTER="products/categories/subcategories/filter";
//PORUDCT BRANDS
export const URL_GET_PRODUCT_BRANDS_BY_FILTER="products/brands/filter";

//PRODUCTS
export const URL_GET_PRODUCTS_PLACES_BY_FILTER="products/places/filters";
export const URL_GET_PRODUCTS_BY_FILTER="products/filters";
export const URL_RESOURCE_PRODUCT="products";

// PRODUCT QUESTIONS
export const URL_GET_PRODUCT_QUESTIONS_BY_PRODUCT="products/questions/";
export const URL_GET_PRODUCT_QUESTIONS_BY_ME="products/questions/user/";
export const URL_RESOURCE_PRODUCT_QUESTIONS="products/questions";

//PLAC USER ADDRESS SHIPPING 
export const URL_GET_PLACUSER_SHIPPING_ADDRESS="placusers/shippingaddresses/";
export const URL_RESOURCE_PLACUSER_SHIPPING_ADDRESS="placusers/shippingaddresses";
//coupons
export const URL_CHECK_COUPON="coupons/check";

//orders

export const URL_GET_ORDER_USERS="orders/user";
export const URL_RESOURCE_ORDERS="orders";
export const URL_GET_STORE_CONFIGURATION="storeconfiguration/place";
export const URL_ORDER_ASSESSMENT="orders/assessment";
/*ADS*/

export const URL_GET_STORE_PROMOTIONS_ADS="advertisements/promotions";