'use strict';

import Realm from 'realm';
import * as Constants from "../../config/constants"

export default class Profiles extends Realm.Object {}
Profiles.schema = {
  name: Constants.MODEL_PROFILE_NAME,
  primaryKey: 'profile_id',
  properties: {
    profile_id: 'string',
    profile_name: 'string',
    profile_unique_name:'string',
    profile_type: 'string',
    profile_image:{type: 'string',optional:true},
    profile_selected:{type: 'string',optional:true},
    pet_birthday: {type: 'string',optional:true},
    pet_image_vaccine: {type: 'string',optional:true},
    pet_image_pedigree: {type: 'string',optional:true},
    pet_gender:  {type: 'string', optional: true},
    pet_type:  {type: 'string', optional: true},
    company_categories:  {type: 'string', optional: true}, // optional property
    company_categories_name:  {type: 'string', optional: true}, // optional property
    company_address:  {type: 'string', optional: true}, // optional property
    company_email:  {type: 'string', optional: true}, // optional property
    company_hour_office_start:   {type: 'string', optional: true},  // optional property
    company_hour_office_end:   {type: 'string', optional: true}, // optional property
    company_description:{type:'string',optional:true},
    breed_id:   {type: 'string', optional: true},  // optional property
    breed_name:   {type: 'string', optional: true},  // optional property
    plac_user_id: 'string', // optional property
    isSelected:{type: 'int', default:0},
    is_private:{type: 'int', default:0},
    isBirthDayNotified:{type: 'int', default:0}
  }
};
