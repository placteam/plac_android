'use strict';

import Realm from 'realm';
import * as Constants from "../../config/constants"

export default class PlacUserConfiguration extends Realm.Object {}
PlacUserConfiguration.schema = {
  name: Constants.MODEL_PLAC_USER_CONFIGURATION_NAME,
  primaryKey: 'plac_user_id',
  properties: {
    profile_id_selected:{type:'string',optional:false},
    plac_user_id:{type:'string',optional:false}
  }
};
