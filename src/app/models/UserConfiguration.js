'use strict';

import Realm from 'realm';
import * as Constants from "../../config/constants"

export default class UserConfiguration extends Realm.Object {}
UserConfiguration.schema = {
  name: Constants.MODEL_USER_CONFIGURATION_NAME,
  primaryKey: 'user_configuration_id',
  properties: {
    user_configuration_id:{type: 'string',optional:false},
    user_uid:{type:'string',optional:true},
  }
};
