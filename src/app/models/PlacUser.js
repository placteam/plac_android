'use strict';

import Realm from 'realm';
import * as Constants from "../../config/constants"



export default class PlacUser extends Realm.Object {}
PlacUser.schema = {
  name: Constants.MODEL_PLAC_USER_NAME,
  primaryKey: 'plac_user_id',
  properties: {
    plac_user_id:{type: 'string',optional:false},
    plac_user_name: {type: 'string',optional:false},
    plac_user_image: {type: 'string',optional:true},
    plac_user_cellphone:{type: 'string',optional:true},
    plac_user_image2: {type: 'string',optional:true},
    plac_user_image3: {type: 'string',optional:true},
    plac_user_birthday: {type: 'string',optional:true},
    plac_user_gender: {type: 'string',optional:true},
    plac_user_work: {type: 'string',optional:true},
    plac_user_hometown: {type: 'string',optional:true},
    plac_user_location: {type: 'string',optional:true},
    plac_user_city:{type: 'string',optional:true},
    plac_user_email: {type: 'string', optional:false},
    plac_user_instagram_link: {type: 'string',optional:true},
    plac_user_twitter_link: {type: 'string',optional:true},
    plac_user_facebook_link: {type: 'string',optional:true},
    plac_user_movies: {type: 'string',optional:true},
    plac_user_books: {type: 'string',optional:true},
    plac_user_device_token:{type: 'string',optional:true},
    plac_user_accepted_terms:{type:'int',optional:false},
    account_type: {type: 'string',optional:true},
    sign_up_type:{type: 'string',optional:true},
  }
};
