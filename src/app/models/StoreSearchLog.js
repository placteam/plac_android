'use strict';

import Realm from 'realm';
import * as Constants from "../../config/constants"

export default class StoreSearchLogs extends Realm.Object {}
StoreSearchLogs.schema = {
    name: Constants.MODEL_STORE_SEARCH_LOG_NAME,
    properties:{
      text_searched: 'string',
      place_id:'string',
      created_at:'date' // optional property
    }
}
