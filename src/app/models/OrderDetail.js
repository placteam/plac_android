'use strict';

import Realm from 'realm';

export default class   OrderDetail extends Realm.Object{}
OrderDetail.schema={
  name: 'order_details',
  primaryKey: 'order_detail_id',
  properties: {
    order_detail_id: 'string',
    product_id: 'string',
    productJSON: 'string',
    place_id:'string',
    placeJSON:'string',
    price:'double',
    quantity:'int',
    discount:'int',
    total:'double',
  }
}