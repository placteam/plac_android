'use strict';

import Realm from 'realm';
import * as Constants from "../../config/constants"

export default class AppConfiguration extends Realm.Object {}
AppConfiguration.schema = {
  name: Constants.MODEL_APP_CONFIGURATION_NAME,
  primaryKey: 'app_configuration_id',
  properties: {
    app_configuration_id:{type: 'string',optional:false},
    plac_user_id_selected:{type:'string',optional:false},
    plac_app_version_installed:{type:'string',optional:true},
  }
};
