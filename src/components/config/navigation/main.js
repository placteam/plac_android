import React, { Component } from 'react'
import { createDrawerNavigator } from 'react-navigation';
import {
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from 'react-native';

import { getIconAddPet, getIconLookFor, getIconCart, getIconQuestion, getIconSetting} from '../../../assets/image/svg'


import * as PlacUserController from '../../../app/controllers/PlacUserController'
import * as ProfileController from '../../../app/controllers/ProfileController'
import * as PlacUserConfigurationController from '../../../app/controllers/PlacUserConfigurationController'

import TabsNavigator from './tabs'

import { getUrlImageCompress } from '../../../lib/image';
import { CachedImage } from 'react-native-img-cache';

const stackMainRoutes = {
  Tabs: { screen: TabsNavigator, path: 'tabs' },


}



/*<TouchableOpacity  style={{flexDirection: 'row',width: '100%'}}>
     <View style={{width: 48,height: 48,justifyContent: 'center',alignItems: 'center'}}>
       {getIconLookFor()}
     </View>
     <View  style={{marginLeft: 10,width: '100%',marginRight: 11.5,justifyContent: 'center',borderBottomWidth: 1,borderColor: "#EDEDED",}}>
       <Text style={{fontSize: 16,color: '#212121'}}>Buscar amigo</Text>
     </View>
 </TouchableOpacity>*/
_keyExtractor = (item, index) => item.profile_id;
const CustomDrawerContentComponent = (props) => {
  let user = PlacUserController.getCurrentUser();
  let profiles = ProfileController.getProfilesByUser(user.plac_user_id);

  return <ScrollView style={{}}>
    <SafeAreaView>
      <View style={{ marginRight: 11.5, marginLeft: 11.5, borderColor: '#15D2BB', borderBottomWidth: 2, backgroundColor: 'white', alignItems: 'center', height: 144 }}>
          <CachedImage source={{ uri: getUrlImageCompress(user.plac_user_image, 20) }} style={{ borderRadius: 36, width: 72, height: 72, marginTop: 24 }} />
          <View style={{ height: 48, justifyContent: 'center', alignItems: 'center', }}>
            <Text style={{ fontSize: 20, color: '#555555' }}>{user.plac_user_name}</Text>
          </View>
      </View>


      <View style={{ marginRight: 11.5, marginLeft: 11.5, height: 36, marginBottom: 5, marginTop: 12, justifyContent: 'center', }}>
         <Text style={{ fontSize: 16, color: 'black' }}>Mis mascotas</Text>
      </View>

      <FlatList
        data={profiles}
        renderItem={(object) => renderProfileRow(object, props, user)}
        keyExtractor={_keyExtractor}
        ItemSeparatorComponent={() => <View
          style={{
            height: 1,
            width: "100%",
            marginLeft: 58,
            backgroundColor: "#EDEDED",
          }}
        />}
        ListFooterComponent={() =>
          <View>
            <TouchableOpacity onPress={() => addNewPet(user, props)} style={{ flexDirection: 'row', width: '100%' }}>
              <View style={{ width: 48, height: 48, justifyContent: 'center', alignItems: 'center' }}>
                {getIconAddPet()}
              </View>
              <View style={{ marginLeft: 10, width: '100%', marginRight: 11.5, justifyContent: 'center', borderBottomWidth: 1, borderTopWidth: 1, borderColor: "#EDEDED", }}>
                <Text style={{ fontSize: 16, color: '#212121' }}>Nueva mascota</Text>
              </View>
            </TouchableOpacity>

          </View>
        }
      />

      <View style={{ marginRight: 11.5, marginLeft: 11.5, height: 36, marginBottom: 0, marginTop: 20 }}>
        <Text style={{ fontSize: 16, color: 'black' }}>Tienda</Text>
      </View>

      <View>
        <TouchableOpacity onPress={() => goOrdersList(props)} style={{ flexDirection: 'row', width: '100%' }}>
          <View style={{ width: 48, height: 48, justifyContent: 'center', alignItems: 'center' }}>
            {getIconCart()}
          </View>
          <View style={{ marginLeft: 10, width: '100%', marginRight: 11.5, justifyContent: 'center', borderBottomWidth: 1, borderColor: "#EDEDED", }}>
            <Text style={{ fontSize: 16, color: '#212121' }}>Ordenes de compra</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => goProductQuestionsByMe(user, props)} style={{ flexDirection: 'row', width: '100%' }}>
          <View style={{ width: 48, height: 48, justifyContent: 'center', alignItems: 'center' }}>
            {getIconQuestion()}
          </View>
          <View style={{ marginLeft: 10, width: '100%', marginRight: 11.5, justifyContent: 'center', borderBottomWidth: 1, borderColor: "#EDEDED", }}>
            <Text style={{ fontSize: 16, color: '#212121' }}>Preguntas realizadas</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={{ marginRight: 11.5, marginLeft: 11.5, height: 36, marginBottom: 0, marginTop: 20 }}>
        <Text style={{ fontSize: 16, color: 'black' }}>Ajustes y ayuda</Text>
      </View>
      <View>
        <TouchableOpacity onPress={() => goSettings(props)} style={{ flexDirection: 'row', width: '100%' }}>
          <View style={{ width: 48, height: 48, justifyContent: 'center', alignItems: 'center' }}>
            {getIconSetting()}
          </View>
          <View style={{ marginLeft: 10, width: '100%', marginRight: 11.5, justifyContent: 'center', borderBottomWidth: 1, borderColor: "#EDEDED", }}>
            <Text style={{ fontSize: 16, color: '#212121' }}>Configuración</Text>
          </View>
        </TouchableOpacity>


      </View>
    </SafeAreaView>
  </ScrollView>
}

/*
   <TouchableOpacity style={{ flexDirection: 'row', width: '100%' }}>
        <View style={{ width: 48, height: 48, justifyContent: 'center', alignItems: 'center' }}>
          {getIconSetting()}
        </View>
        <View style={{ marginLeft: 10, width: '100%', marginRight: 11.5, justifyContent: 'center', borderBottomWidth: 1, borderColor: "#EDEDED", }}>
          <Text style={{ fontSize: 16, color: '#212121' }}>Configuración</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={{ flexDirection: 'row', width: '100%' }}>
        <View style={{ width: 48, height: 48, justifyContent: 'center', alignItems: 'center' }}>
          {getIconHelpCenter()}
        </View>
        <View style={{ marginLeft: 10, width: '100%', marginRight: 11.5, justifyContent: 'center', borderBottomWidth: 1, borderColor: "#EDEDED", }}>
          <Text style={{ fontSize: 16, color: '#212121' }}>Centro de ayuda</Text>
        </View>
      </TouchableOpacity>
*/


const renderProfileRow = (object, props, user) => {
  let item = object.item;
  let puc = PlacUserConfigurationController.show(user.plac_user_id);
  let profileIdSelected = puc.profile_id_selected;
  let backgroundColor = {}
  let color = '#212121';
  if (item.profile_id == profileIdSelected) {
    backgroundColor = 'rgba(21, 210, 187, 1)';
    color = "white";
  }

  return <TouchableOpacity onPress={() => changeProfile(object.item, props)} style={{ flexDirection: 'row', height: 48, flexDirection: 'row', backgroundColor }}>
            <View style={{ width: 48, height: 48, justifyContent: 'center', alignItems: 'center' }}>
              <CachedImage source={{ uri: getUrlImageCompress(item.profile_image, 20) }} style={{ width: 24, height: 24, borderRadius: 12 }} />
            </View>
            <View style={{ marginLeft: 10, justifyContent: 'center', alignItems: 'flex-start' }}>
              <Text style={{ fontSize: 16, color }}>{item.profile_name}</Text>
              <Text style={{ fontSize: 14, color }}>@{item.profile_unique_name}</Text>
            </View>
         </TouchableOpacity >
}

const goProductQuestionsByMe = (user, props) => {
  let params = {
    plac_user: user,
  }
  props.navigation.navigate('ProductQuestionsByMe', params);

}
const goOrdersList = (props) => {
  props.navigation.navigate('OrdersList', {});

}

const changeProfile = (profile, props) => {
  PlacUserConfigurationController.manage(profile.plac_user_id, profile.profile_id);
  props.navigation.navigate('AuthLoading', {});
}

const addNewPet = (user, props) => {
  let params = {
    plac_user: user,
  }
  props.navigation.navigate('RegisterPetStep1', params);
}
const goSettings = (props) => {
  props.navigation.navigate('Settings', {});
}



export default AppStack = createDrawerNavigator(
  stackMainRoutes, {
    mode: 'modal',
    headerMode: 'screen',
    contentComponent: CustomDrawerContentComponent
  });


class Main extends React.Component {



  render() {
    return <TabsMain />;
  }

  componentWillMount() {
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
  }

  onReceived = (notification) => {
    console.warn("Notification received: ", notification);
  }

  onOpened = (openResult) => {
    this.props.navigation.navigate('AuthLoading');
  }

}







