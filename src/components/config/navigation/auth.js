import {createStackNavigator} from 'react-navigation';
import  LogIn from '../../auth/LogIn'
import  RequestPhone from '../../auth/RequestPhone'
import ContinueSocialMedia from '../../auth/ContinueSocialMedia'
import PlacUserData from '../../register/PlacUserData'
import WelcomePlac from '../../register/WelcomePlac'
import RegisterPetStep1 from '../../register/pet/RegisterPetStep1'
import RegisterPetStep2 from '../../register/pet/RegisterPetStep2'
import RegisterPetStep3 from '../../register/pet/RegisterPetStep3'
import PickBreed from '../../views/pet/ModalPickBreed'

//contacts
import RequestPermissionContacts from '../../main/contacts/RequestPermissionContacts'
import ShowContactsToInvite  from '../../main/contacts/ShowContactsToInvite'
import ShowContactsSuggestInPlac  from '../../main/contacts/ShowContactsSuggestInPlac'
import ShowProfilesSuggest  from '../../main/contacts/ShowProfilesSuggest'

import FollowingsFrom from '../../main/profile/followings/FollowingsFrom'
import FollowingsTo from '../../main/profile/followings/FollowingsTo'
import ProfileInvite from '../../main/profile/ProfileInvite'
import PlacUserInvite from '../../main/placuser/PlacUserInvite'

import PostSingle from '../../main/timeline/post/PostSingle'
import PostLikes from '../../main/timeline/post/Likes'
import PostComments from '../../main/timeline/post/Comments'


const Routes={
    LogIn:{screen:LogIn},
    RegisterPetStep1:{screen:RegisterPetStep1},
    RegisterPetStep2:{screen:RegisterPetStep2},
    RegisterPetStep3:{screen:RegisterPetStep3},
    WelcomePlac:{screen:WelcomePlac},
    RequestPhone:{screen:RequestPhone},
    ContinueSocialMedia:{screen:ContinueSocialMedia},
    PlacUserData:{screen:PlacUserData},
    PickBreed:{screen:PickBreed},
    RequestPermissionContacts:{screen:RequestPermissionContacts},
    ShowContactsToInvite:{screen:ShowContactsToInvite},
    ShowContactsSuggestInPlac:{screen:ShowContactsSuggestInPlac},
    ShowProfilesSuggest:{screen:ShowProfilesSuggest},
    PlacUserInvite: { screen: PlacUserInvite },
    ProfileInvite: { screen: ProfileInvite },
    PostSingle: { screen: PostSingle },
    FollowingsFrom: { screen: FollowingsFrom },
    FollowingsTo: { screen: FollowingsTo },
    PostLikes: { screen: PostLikes },
    PostComments: { screen: PostComments },
}



export default AppStack = createStackNavigator(Routes);
