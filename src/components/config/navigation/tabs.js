import { Image, } from 'react-native'
import React, { Component } from 'react'

import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import icons from '../../../assets/image/tabs';


import OneSignal from 'react-native-onesignal'; // Import package from node modules
import RegisterPetStep1 from '../../register/pet/RegisterPetStep1'
import RegisterPetStep2 from '../../register/pet/RegisterPetStep2'
import RegisterPetStep3 from '../../register/pet/RegisterPetStep3'
import PickBreed from '../../views/pet/ModalPickBreed'
import TimeLine from '../../main/timeline/'
import PostLikes from '../../main/timeline/post/Likes'
import PostComments from '../../main/timeline/post/Comments'
import NewPost from '../../main/timeline/post/NewPost'
import PostSingle from '../../main/timeline/post/PostSingle'
import Report from '../../main/timeline/post/Report'


//STORE
import Store from '../../main/store/'
//PRODUCT
import Product from '../../main/store/Product'
import ProductsSearch from '../../main/store/ProductsSearch'
import PlacesByCategory from '../../main/store/PlacesByCategory'
//PRODUCT QUESTIONS
import ProductQuestions from '../../main/store/ProductQuestions'
import ProductQuestionsByMe from '../../main/store/ProductQuestionsByMe';

//PLACE
import Place from '../../main/store/Place'
import PlaceRating from '../../main/store/rating/'

//CHECKOUT 
import Checkout from '../../main/store/Checkout'
import PaymentGateway from '../../main/store/PaymentGateway'
import UserAddress from '../../main/store/address/UserAddress'
import CreateAddress from '../../main/store/address/CreateAddress'

//ORDERS
import OrdersList from '../../main/store/orders/OrdersList'
import OrderDetail from '../../main/store/orders/OrderDetail'
import Assessment from '../../main/store/assessment'

import Notifications from '../../main/notifications/'

import FollowingsFrom from '../../main/profile/followings/FollowingsFrom'
import FollowingsTo from '../../main/profile/followings/FollowingsTo'
import ProfileInvite from '../../main/profile/ProfileInvite'
import ProfileMe from '../../main/profile/Profile'
import ProfileEdit from '../../main/profile/ProfileEdit'
import ProfilesSearch from '../../main/profile/ProfilesSearch'

import PlacUserMe from '../../main/placuser/PlacUser'
import PlacUserInvite from '../../main/placuser/PlacUserInvite'

//Settings
import Settings from '../../main/settings'
import NotificationsPush from '../../main/settings/NotificationsPush'
import NotificationsMail from '../../main/settings/NotificationsMail';

const HomeRoutes = {
  TimeLine: { screen: TimeLine },
  PostLikes: { screen: PostLikes },
  PostComments: { screen: PostComments },
  NewPost: { screen: NewPost },
  PostComplaint: { screen: Report },
  RegisterPetStep1: { screen: RegisterPetStep1 },
  RegisterPetStep2: { screen: RegisterPetStep2 },
  RegisterPetStep3: { screen: RegisterPetStep3 },
  PickBreed: { screen: PickBreed },
  PlacUserInvite: { screen: PlacUserInvite },
  ProfileInvite: { screen: ProfileInvite },
  FollowingsFrom: { screen: FollowingsFrom },
  FollowingsTo: { screen: FollowingsTo },
  PostSingle: { screen: PostSingle },
  ProfilesSearch: { screen: ProfilesSearch }
}


const StoreRoutes = {
  Store: { screen: Store },
  Product: { screen: Product },
  ProductsSearch: { screen: ProductsSearch },
  ProductQuestions: { screen: ProductQuestions },
  ProductQuestionsByMe: { screen: ProductQuestionsByMe },
  PlacesByCategory: { screen: PlacesByCategory },
  Place: { screen: Place },
  PlaceRating: { screen: PlaceRating },
  Checkout: { screen: Checkout },
  UserAddress: { screen: UserAddress },
  CreateAddress: { screen: CreateAddress },
  OrdersList: { screen: OrdersList },
  OrderDetail: { screen: OrderDetail},
  PaymentGateway: { screen: PaymentGateway }
}



const NotificationsRoutes = {
  Notifications: { screen: Notifications },
  ...HomeRoutes,
  OrderDetail: { screen: OrderDetail},
  Assessment: { screen: Assessment }
}

const ProfileRoutes = {
  ProfileMe: { screen: ProfileMe },
  ProfileEdit: { screen: ProfileEdit },
  PlacUserMe: { screen: PlacUserMe },
  PickBreed: { screen: PickBreed },
  ...HomeRoutes,
  Settings: { screen: Settings },
  NotificationsPush: { screen: NotificationsPush },
  NotificationsMail: { screen: NotificationsMail }
}

const HomeStack = createStackNavigator(HomeRoutes);
const StoreStack = createStackNavigator(StoreRoutes);
const NotificationsStack = createStackNavigator(NotificationsRoutes);
const ProfileStack = createStackNavigator(ProfileRoutes);



HomeStack.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName

  if (routeName == "PostComments" || routeName == "ProfilesSearch" || routeName == "RequestPermissionContacts" || routeName == "ShowContactsToInvite" || routeName == "ShowContactsSuggestInPlac" || routeName == "ShowProfilesSuggest") {
    tabBarVisible = false
  }

  return {
    tabBarVisible,
  }
}

StoreStack.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName

  if (routeName == "PaymentGateway" || routeName == "CreateAddress" || routeName == 'UserAddress' || routeName == 'Product' || routeName == 'ProductQuestions' || routeName == 'PlacesByCategory' || routeName == 'Place' || routeName == 'Checkout') {
    tabBarVisible = false
  }

  return {
    tabBarVisible,
  }
}

NotificationsStack.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName

  if (routeName == "PostComments" || routeName == "Assessment") {
    tabBarVisible = false
  }

  return {
    tabBarVisible,
  }
}


const TabsRoutes = {
  HomeStack: { screen: HomeStack, path: 'home' },
  StoreStack: { screen: StoreStack, path: 'store' },
  NotificationsStack: { screen: NotificationsStack },
  ProfileStack: { screen: ProfileStack, path: 'profileme' },
}

export default TabsMain = createBottomTabNavigator(TabsRoutes, {
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;
      let icon;
      if (routeName === 'HomeStack') {
        icon = (!focused) ? icons.getHomeIcon() : icons.getHomeIconPress();
      } else if (routeName === 'StoreStack') {
        icon = (!focused) ? icons.getStoreIcon() : icons.getStoreIconPress();
      } else if (routeName === 'NotificationsStack') {
        icon = (!focused) ? icons.getNotificationsIcon() : icons.getNotificationsIconPress();
      } else if (routeName === 'ProfileStack') {
        icon = (!focused) ? icons.getProfileIcon() : icons.getProfileIconPress();
      }

      // You can return any component that you like here! We usually use an
      // icon component from react-native-vector-icons
      return <Image style={{ width: 42, height: 42 }} source={{ uri: icon }} />
    },

  }),
  tabBarOptions: {
    activeTintColor: '#e91e63',
    inactiveTintColor: 'gray',
    showLabel: false,
    style: { backgroundColor: 'white', borderColor: 'blue', height: 45, }
  },

});



class TabNavigator extends React.Component {



  render() {
    return <TabsMain />;
  }

  componentWillMount() {
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
  }

  onReceived = (notification) => {
    console.warn("Notification received: ", notification);
  }

  onOpened = (openResult) => {
    this.props.navigation.navigate('AuthLoading');
  }

}
