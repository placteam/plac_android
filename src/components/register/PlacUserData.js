import React from 'react';
import {

  StyleSheet,
  Text,
  Dimensions,
  View,
  Platform,
  Image,
  TextInput,
  TouchableOpacity,
  Modal,
} from 'react-native';

const { width,height} = Dimensions.get('window');
import {validateEmail,validateSpecialCharacters} from '../../lib/validations'
import * as PlacUserController from '../../app/controllers/PlacUserController'
import * as AppConfigurationController from '../../app/controllers/AppConfigurationController'


//API REQUEST
import Api from '../../lib/http_request/'
import * as Routes from '../../app/routes'

var params;
var profile={};
var providerId="";

const MESSAGE_ASSOCIATE_EMAIL="El correo ingresado tiene una cuenta asociada, para continuar debes vincular el télefono con este";


export default class PlacUserData extends React.Component {

  constructor(props){
    super(props)
    params=this.props.navigation.state.params;
    console.log(params);
    this.state={
       phoneNumber:'',
       isValidData:false,
       isValidName:false,
       isValidEmail:false,
       userName:"",
       userEmail:"",
       placUser:{},
       modalVinculationCode:false,
       transactionType:"store_user",
    }

  }

  static navigationOptions = {
      title: '',
      headerTransparent:true,
  };

  render() {
    var  buttonStyleNotPress={};
     if(!this.state.isValidData){
       buttonStyleNotPress.backgroundColor="#ededed";
     }
    return (
      <View style={styles.container}>
          <View style={{marginBottom: 4,marginTop: 37,padding: 20,}} >
              <Text style={{fontSize: 18,color: 'black'}}>Tus datos</Text>
              <View style={{marginTop: 20,}}>
                  <Text style={styles.textLabel}>Nombre</Text>
                    <TextInput
                     ref={(input) => { this.userName = input; }}
                     returnKeyType={"next"}
                     onSubmitEditing={()=>this.userEmail.focus()}
                     style={styles.textInputData}
                     onChangeText={this.onChangeName}
                     autoCapitalize = {"words"}

                     placeholder='Ingresa tu nombre'
                     value={this.state.userName}
                 />
                  {this.getViewErrorMessage(0)}
              </View>
              <View style={{marginTop: 20}}>
                  <Text style={styles.textLabel}>Email</Text>
                    <TextInput
                     ref={(input) => { this.userEmail = input; }}
                     returnKeyType={"done"}
                     keyboardType={'email-address'}
                     style={styles.textInputData}
                     onBlur={this.onBlurEmail}
                     onChangeText={this.onChangeEmail}
                     autoCapitalize = {"none"}
                     placeholder='Ingresa tu email'
                     value={this.state.userEmail}
                 />
                  {this.getViewErrorMessage(1)}
                  {this.getViewLinkAccount()}
             </View>
             {this.getViewTransaction('store_user')}


            {(!this.state.isActiveTransaction)?<TouchableOpacity  onPress={this.storePlacUser} disabled={!this.state.isValidData} style={[styles.buttonContinue,buttonStyleNotPress]}>
               <Text style={{color: 'white',fontSize: 16}}>Continuar</Text>
            </TouchableOpacity>:null}
          </View>


          {this.getViewFillCodeModal()}


      </View>
    );
  }

  getViewTransaction(transactionType){

    if(this.state.isActiveTransaction && transactionType==this.state.transactionType){
      return  <View style={{justifyContent: 'center',marginVertical:25,alignItems: 'center'}}>
                <Image  style={{width: 70,height: 26,}}   source={require('../../assets/image/general/loader.gif')}/>
                <Text style={{fontSize: 12}}>{this.state.messageTransaction}</Text>               
             </View>
    }
  }

  getViewErrorMessage(indexInput){
    indexError= this.state.indexError;
    if(indexInput==indexError){
      return <Text style={[this.state.messageStyle,{marginTop: 10,}]}>{this.state.message}</Text>;
    }
  }

  getViewLinkAccount(){
    var showFormLinkAccount=  this.state.showFormLinkAccount;
    if(showFormLinkAccount){
      return <View style={{flexDirection:'row'}}>
                    <TouchableOpacity onPress={this.sendCodeForLinkingAccount} style={{marginTop: 10,borderRadius: 8,justifyContent: 'center',alignItems: 'center',width:width-200,height: 36,backgroundColor: 'orange'}}>
                    <Text style={{fontSize: 14,color: 'white'}}>Vincular correo</Text>
                  </TouchableOpacity>
            </View>
    }
  }

  getViewFillCodeModal(){
    return     <Modal
               animationType="fade"
               transparent={true}
               visible={this.state.modalVinculationCode}
               onRequestClose={() =>{this.setState({modalVinculationCode:false})}}  >
              <View style={styles.viewModalContainer}>
                    <View style={styles.viewModalContent}>
                          <Text style={styles.textModalTitle}>Por favor ingresa el código para vincular  el correo al télefono</Text>
                          <Text style={styles.textModalSubtitle}>Un código de vinculación ha sido enviado a {this.state.userEmail} </Text>
                          <TextInput
                            style={styles.textConfirmCode}
                            placeholder='Ingresa el código'
                            value={this.state.vinculationCode}
                            onChangeText={(vinculationCode)=>this.setState({vinculationCode})}
                          />
                           {this.getViewTransaction('validate_code')}
                          <View style={styles.viewButtonBox}>
                              <Text onPress={()=>{this.setState({modalVinculationCode:false})}} style={styles.textBauttonCancell}>Cancelar</Text>
                              <TouchableOpacity onPress={this.validateLinkAccountCode} style={styles.buttonValidateCode}>
                                <Text style={{color:'white'}}>Validar código</Text>
                              </TouchableOpacity>
                          </View>
                    </View>
              </View>
      </Modal>
  }

  onChangeName=(userName)=>{
     this.setState({userName});
       if(!validateSpecialCharacters(userName)){
           this.setState({indexError:0,isValidName:false,isValidData:false,message:"Nombre no valido "+userName,messageStyle:styles.messageWarning});
           return false;
       }else{
         if(userName.length==0){
           this.setState({indexError:0,message:"Campo vacio",isValidData:false,messageStyle:styles.messageWarning});
         }else{
          this.setState({indexError:-1,isValidName:true});
          this.manageValidData();
         }
         return true;
       }
  }

  onChangeEmail=(userEmail)=>{
      if(userEmail.length>9){
        if(this.state.email_duplicate!=userEmail){
          this.setState({isValidData:true,showFormLinkAccount:false,message:""});
        }else{
          this.setState({isValidData:false,showFormLinkAccount:true,message:MESSAGE_ASSOCIATE_EMAIL});
        }
    
      }
      this.setState({userEmail});
  }

  onBlurEmail=()=>{

      let {userEmail,userEmailToLink}=  this.state;

      if(userEmail!=userEmailToLink){
        this.setState({
          showFormLinkAccount:false,
        });
      }
      if(!validateEmail(userEmail)){
       this.setState({indexError:1,isValidEmail:false,isValidData:false,message:"Correo no valido "+userEmail,messageStyle:styles.messageWarning});
       return false;
      }else{
        this.setState({indexError:-1,isValidEmail:true});
        this.manageValidData();
        return true;
      }
  }



  manageValidData(){
    setTimeout(()=>{
      if(this.state.isValidName && this.state.isValidEmail){
        this.setState({isValidData:true});
      }
    },100)
  }


 sendCodeForLinkingAccount=()=>{
   this.setState({isActiveTransaction:true,messageTransaction:"Enviando código a tu correo..."});

   let placUser=this.state.placUser;
   var body={
     plac_user:placUser
   }
   Api.post(Routes.URL_PLAC_USER_LINK_ACCOUNT,body)
      .then((response)=>{
        console.warn(response);
        if(response.status=='success'){

          this.setState({
            transactionType:'validation_code',
            isActiveTransaction:false,
            modalVinculationCode:true,
          });
        }
    }).catch((ex)=>{
       console.log(ex);
    });

 }

 validateLinkAccountCode=()=>{
   var {placUser,vinculationCode,userEmail}=this.state;

   if(!vinculationCode){
     return;
   }

   body={
     code:vinculationCode,
     email:userEmail,
     plac_user_id:placUser.plac_user_id,
     ...params
   }
   this.setState({userEmailToLink:userEmail,isActiveTransaction:true, transactionType:'validate_code',messageTransaction:'Validando codigo...'});

   Api.post(Routes.URL_VERIFY_LINK_CODE,body)
      .then((response)=>{
        const status=response.status;
        if(status=='success'){
            this.storePlacUserLocal(response);
        }else if(status=="error"){
          this.setState({messageStyle:styles.messageError,indexError:1,message:"El código no es correcto o ha expirado por favor solicita uno nuevo para continuar"});
        }

        this.setState({
          modalVinculationCode:false,
        });
    }).catch((ex)=>{
       console.log(ex);
    });




 }


  storePlacUser=()=>{

    if(!this.onBlurEmail()){
     return;
    }

    if(!this.onChangeName(this.state.userName)){
      return;
    }
     


    plac_user={
      plac_user_name:this.state.userName,
      plac_user_email:this.state.userEmail,
      plac_user_image:'empty',
    };

    if(providerId=='facebook.com'){
      plac_user.plac_user_image=profile.picture.data.url;
    }else if(providerId=='google.com'){
      plac_user.plac_user_image=profile.picture;
    }

    URL_REQUEST=Routes.URL_PLAC_USER_RESOURCE;
    var body={
      plac_user,
      ...params
    }
    this.setState({
      isActiveTransaction:true,
      messageTransaction:"Tu usuario se esta creando...",
    });

    Api.post(URL_REQUEST,body)
       .then((response)=>{
             console.warn(response);
         const status=response.status;
         if(status=='success'){

             this.storePlacUserLocal(response);
         }else if(status=='error'){
           if(response.message=='23000'){
               placUser=response.data;
               this.setState({
                 placUser:{
                   plac_user_id:placUser.plac_user_id,
                   plac_user_phone:params.authentication.authentication_identificator,
                   ...plac_user,
                 },
                 email_duplicate:this.state.userEmail,
                 isActiveTransaction:false,
                 isValidEmail:false,
                 indexError:1,
                 isValidData:false,
                 message:MESSAGE_ASSOCIATE_EMAIL,
                 messageStyle:styles.messageWarning,
                 showFormLinkAccount:true,
               });

               setTimeout(()=>{
                  console.log(this.state.placUser);
               },200);
           }
         }
        console.log(response);
     }).catch((ex)=>{
        console.log(ex);
     });
  }


  storePlacUserLocal(response){
    deviceToken=params.installation.device_token
    data=response.data;
    placUserId=data.plac_user_id;
    const request={
      user:data,
      deviceToken,
    }
    PlacUserController.store(request);
    AppConfigurationController.manage(deviceToken,placUserId);
    this.goAuthLoading();
  }

  goAuthLoading(){
    setTimeout(()=>{
      this.props.navigation.navigate('AuthLoading',null);
    },200);
  }


  componentDidMount(){
   additionalUserInfo=params.additionalUserInfo;
   providerId=additionalUserInfo.providerId;
    if(providerId!='phone'){
        profile=params.additionalUserInfo.profile;
        if(profile.name!="" &&  profile.email!=""){
          this.setState({
            userName:profile.name,
            userEmail:profile.email,
            isValidData:true,
          });
        }
    }

  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textLabel:{
    fontWeight: '500',
    color: '#15d2bb',
    fontSize: 12,
  },
  textInputData:{
    height: 40,
    borderColor: '#00B99B',
    borderBottomWidth: 1,
    fontSize: 16
  },
  buttonContinue:{
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
    marginTop: 40,
    backgroundColor: '#00B99B',
    borderRadius: 8
  },
  viewModalContainer:{
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,
    backgroundColor: 'rgba(0,0,0,.4)'
  },
  viewModalContent:{
    width: "80%",
    borderRadius: 8,
    borderWidth: .5,
    padding: 25,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textModalTitle:{
    fontWeight: 'bold',
    fontSize: 16,
    color: 'black'
  },
  textModalSubtitle:{
    fontSize: 12,
    color: 'grey',
    marginTop: 20
  },
  textConfirmCode:{
    borderWidth: .6,
    height: 40,
    marginTop: 10,
    width: "100%",
    textAlign: 'center',
    borderRadius: 4,
    borderColor: 'grey'
  },
  viewButtonBox:{
    width: "100%",
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    justifyContent: 'center'
  },
  textBauttonCancell:{
    color: 'grey',
    fontSize: 12,
    marginTop: 20,
    marginRight: 10
  },
  buttonValidateCode:{
    width: "60%",
    height: 36,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#15d2bb',
    borderRadius: 8
  },
  messageSuccess:{
    fontSize: 14,
    color: 'green',
  },
  messageWarning:{
    fontSize: 14,
    color: '#f0ad4e',
  },
  messageError:{
    fontSize: 14,
    color: '#d2152b',
  }


});
