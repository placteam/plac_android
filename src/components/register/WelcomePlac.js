import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  Button,
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const { width,height} = Dimensions.get('window');
import {validateEmail,validateSpecialCharacters} from '../../lib/validations'
import {getProportionalImageHeight} from '../../lib/image'

var imageHeight=getProportionalImageHeight({width: 720,height: 433},width-30);

export default class WelcomePlac extends React.Component {

  constructor(props){
    super(props)




  }

  static navigationOptions = {
      title: '',
      header:null,

      headerTransparent:true,
  };

  render() {

    return (
      <View style={styles.container}>
        <View style={{margin:15}}>
            <Image style={{borderRadius: 8,width: width-30,height:imageHeight,}} source={require('../../assets/image/general/welcome_plac.jpg')}/>
            <View style={{width:width-30,top:20,position: 'absolute',alignItems: 'center',justifyContent: 'center'}}>
              <Text style={{fontSize: 20,color:'#15d2bb'}}>Bienvenido a <Text style={{fontWeight: 'bold'}}>plac</Text></Text>
              <Text style={{marginLeft: 42,marginRight: 42,marginTop: 10,fontSize: 14,color:'black',textAlign: 'center'}}>Encuentra miles de productos para tu mascota y comparte sus fotos con otros usuarios en nuestra comunidad.</Text>
            </View>
            <View style={{alignItems:'center',marginTop: 20}}>
                    <TouchableOpacity onPress={this.goAddPet} style={styles.buttonAddPet }>
                        <Text style={{fontSize: 16,color: 'white'}}>Agregar mascota</Text>
                    </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }


  goAddPet=()=>{
    this.props.navigation.navigate('RegisterPetStep1',this.props.navigation.state.params);
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  buttonAddPet:{
    justifyContent: 'center',
    alignItems: 'center',
    width: width-100,
    height: 36,
    backgroundColor: '#00B99B',
    borderRadius: 8
  },
  buttonContinueMain:{
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    width: width-100,
    height: 36,
    borderWidth: 1,
    borderColor: '#00B99B',
    borderRadius: 8
  },

  messageSuccess:{
    fontSize: 14,
    color: 'green',
  },
  messageWarning:{
    fontSize: 14,
    color: '#f0ad4e',
  },
  messageError:{
    fontSize: 14,
    color: '#d2152b',
  }


});
