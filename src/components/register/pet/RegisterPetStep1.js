import React, { Component } from 'react';
const PropTypes = require('prop-types');
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  StatusBar,
  Platform,
  Modal,
  TextInput,
  ListView,
  Image,
  FlatList,
  Dimensions,
  View } from 'react-native';

  //COMPONENTS
  import PetTypeListView from '../../views/pet/PetTypeListView'
  import ModalPickBreed from '../../views/pet/ModalPickBreed'



  var params;
  export default class RegisterPetStep1 extends React.Component {

    static navigationOptions = {
      headerTransparent:true,
      headerTitle: <Text style={{fontSize: 20,fontWeight: 'bold',color: 'black'}}>Crear perfil de mascota</Text>,
    };


   constructor(props) {
     super(props);
     params=props.navigation.state.params;
     this.state={
       textValueSearch:"",
       breeds:[],
       isDisabledPickBreed:false,
       isVisibleModalPickBreed:true,
       breedSelected:null,

     }
   }



    render(){

      isDisabledPickBreed=this.state.isDisabledPickBreed;
      breedSelected=this.state.breedSelected;
      var  buttonStyleNotPress={};
      if(!isDisabledPickBreed){
        buttonStyleNotPress.backgroundColor="#ededed";
      }

      return (
         <View style={styles.container}>
               <View style={{marginTop: 48,marginLeft: 24,marginRight: 24,flex:1}}>
                      <Text style={{marginTop: 28,color: "#555555",fontSize: 16}}>¿Qué tipo de mascota es?</Text>
                      <View style={{width: "100%"}}>
                            <PetTypeListView petTypeSelected={this.petTypeSelected} />
                      </View>
                      <Text style={{marginTop: 28,color: "#555555",fontSize: 16}}>¿Es de alguna raza?</Text>
                      {(breedSelected==null)?
                        <TouchableOpacity disabled={!isDisabledPickBreed} onPress={this.goPickBreed}  style={[styles.buttonPickBreed,buttonStyleNotPress]}>
                          <Text style={{color:'white',fontSize: 16}}>Seleccionar raza</Text>
                       </TouchableOpacity>:
                       <View style={{height: 48,width: "100%",justifyContent: 'center',alignItems:'center',flexDirection: 'row'}}>
                         <View style={{width: "70%",padding:5,paddingLeft: 0,borderBottomWidth: 1,borderBottomColor: "#00B99B",}}>
                           <Text style={{fontSize: 16,color: 'black'}}>{breedSelected.breed_name}</Text>
                         </View>
                         <View style={{width: "30%",justifyContent: 'center',alignItems: 'center'}}>
                           <TouchableOpacity  onPress={this.goPickBreed} style={{borderWidth: 1,width: "80%",borderColor: "#00B99B",justifyContent: 'center',alignItems:'center',height: 36,borderRadius: 8,}}>
                             <Text  style={{color:"#00B99B",fontSize: 14}}>Cambiar</Text>
                           </TouchableOpacity>
                         </View>
                       </View>
                    }

                  {(breedSelected!=null)?
                    <TouchableOpacity  onPress={this.goRegisterPetStep2}  style={styles.buttonContinue}>
                       <Text style={{color: 'white',fontSize: 16}}>Continuar</Text>
                    </TouchableOpacity>:null}
               </View>
         </View>
      );
    }

    petTypeSelected=(petType)=>{
      params.petType=petType;
      this.setState({isDisabledPickBreed:true});
    }

    goPickBreed=()=>{
      params.breedSelectedCallBack=(this.breedSelected);
      this.props.navigation.navigate('PickBreed',params)
    }

    breedSelected=(breed)=>{
      params.breed=breed;
      this.setState({breedSelected:breed});
    }

    goRegisterPetStep2=()=>{
      this.props.navigation.navigate('RegisterPetStep2',params)
    }


  }



  const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:'white',
    },

    textSearch:{
      height:40,
      fontSize:18,
      borderBottomWidth: 1,
    },
    buttonPickBreed:{
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 8,
      marginTop: 10,
      height: 36,
      backgroundColor: '#00B99B'
    },
    buttonContinue:{
      justifyContent: 'center',
      alignItems: 'center',
      height: 36,
      marginTop: 40,
      backgroundColor: '#00B99B',
      borderRadius: 8
    },

  });
