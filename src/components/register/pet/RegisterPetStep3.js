import React, { Component } from 'react';
const PropTypes = require('prop-types');
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  Animated,
  View } from 'react-native';


 import  * as ProfileController from '../../../app/controllers/ProfileController'
 import  * as PlacUserConfigurationController from '../../../app/controllers/PlacUserConfigurationController'
  import PickImage from '../../views/pet/PickImage'
  import ButtonContinue from '../../views/ButtonContinue'

  //API REQUEST
  import Api from '../../../lib/http_request/'
  import * as Routes from '../../../app/routes'

  const { width,height} = Dimensions.get('window');

  var params;
  export default class RegisterPetStep3 extends React.Component {

    static navigationOptions = {
      headerTransparent:true,
      headerTitle: <Text style={{fontSize: 20,fontWeight: 'bold',color: 'black'}}>Crear perfil de mascota</Text>,
    };


   constructor(props) {
     super(props);
     params=props.navigation.state.params;
     console.warn(params);
     this.state={
       isValidData:false,
       message:"Creando perfil de mascota."
     }

   }



    render(){

      return (
         <View style={styles.container}>
               <View style={{marginTop: 48,marginLeft: 24,marginRight: 24,flex:1}}>
                    <Text style={{marginTop: 24,color: "#555555",fontSize: 16}}>Hace falta una foto para terminar!</Text>
                    <PickImage       pickerImageConfig={{ width: 600, height: 600 }} onImageSelected={(image)=>this.onImageSelected(image)} styleContainer={{marginTop:20}}  />
                    <View
                      style={{justifyContent: 'center',alignItems: 'center',marginTop: 40}}>
                      {(this.state.isLoading)?
                          <View style={{justifyContent: 'center',alignItems: 'center'}}>
                               <Image  style={{width: 70,height: 26,}}   source={require('../../../assets/image/general/loader.gif')}/>
                               <Text style={{fontSize: 12}}>{this.state.message}</Text>
                           </View>:
                           <ButtonContinue  onPress={()=>this.finishRegister()}  customStyle={{width:"50%",}} isValidData={this.state.isValidData} text={"Terminar registro"} />
                         }

                    </View>
               </View>
         </View>
      );
    }

  onImageSelected(image){
      this.setState({
        isValidData:true,
        imageSelected:image.imageSelected,
      });
  }

  finishRegister(){
        this.setState({isLoading:true});
        var url= Routes.URL_PROFILES_RESOURCE;
        placUserId=params.plac_user.plac_user_id;
        var body={
           profile:{
             pet_type: params.petType.id,
             breed_id:params.breed.breed_id,
             profile_name:params.profile_name,
             profile_unique_name:params.petUniqueName,
             profile_birthday:params.petBirthday,
             plac_user_id:placUserId,
             profile_image:this.state.imageSelected
           }
        }
      
        
    
        Api.post(url,body)
           .then((response)=>{
             var {status,message,data} =response;
             if(status=="success"){
              
               let profile=data;
               ProfileController.store(profile);
               PlacUserConfigurationController.manage(placUserId,profile.profile_id);
               this.goAuthLoading();
             }else{
               console.warn(message);
             }
         }).catch((ex)=>{
           console.warn(ex.message);
            this.setState({isLoading:false});
         });
    }

    goAuthLoading(){
        setTimeout(()=>{
          this.setState({isLoading:false});
          let placUserId=params.plac_user.plac_user_id;
          let profiles= ProfileController.getProfilesByUser(placUserId);          
          if(profiles.length>1){
            this.props.navigation.navigate('AuthLoading',null);
          }else{
            params.profileFrom=profiles[0];
            this.props.navigation.navigate('RequestPermissionContacts',params);
          }


      
        },300);
    }

}




  const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:'white',
    },

    textSearch:{
      height:40,
      fontSize:18,
      borderBottomWidth: 1,
    },
    buttonPickBreed:{
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 8,
      marginTop: 10,
      height: 36,
      backgroundColor: '#00B99B'
    },
    buttonContinue:{
      justifyContent: 'center',
      alignItems: 'center',
      height: 36,
      marginTop: 40,
      backgroundColor: '#00B99B',
      borderRadius: 8
    },


  });
