import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  Button,
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  Animated,
} from 'react-native';


//API REQUEST
import Api from '../../../lib/http_request/'
import * as Routes from '../../../app/routes'

import {validateSpecialCharacters,cleanString} from '../../../lib/validations'

const { width,height} = Dimensions.get('window');

import DatePicker from '../../views/DatePicker'

import ButtonContinue from '../../views/ButtonContinue'


export default class RegisterPet extends React.Component {

  static navigationOptions = {
    headerTransparent:true,
    headerTitle: <Text style={{fontSize: 20,fontWeight: 'bold',color: 'black'}}>Crear perfil de mascota</Text>,
  };

  constructor(props){
    super(props)
    params=this.props.navigation.state.params;
    this.state={
      petName:"",
      isValidName:false,
      isValidPetUniqueName:true,
      petBirthday:"Selecciona la fecha de cumpleaños",
      isPickBirthday:false,
      isEditablePetUniqueName:false,
      indexError:-1,
      isValidData:false,
    }

  }


  render() {


    return (
      <View style={styles.container}>
        <View style={{marginLeft:24,marginRight: 24,marginTop: 80}}>
          <View >
              <Text style={styles.textLabel}>Nombre de tu mascota</Text>
                <TextInput
                 ref={(input) => { this.petName = input; }}
                 returnKeyType={"next"}
                 onSubmitEditing={()=>{
                   this.verifyUniqueNameBefore(this.state.petName);
                   this.petUniqueName.focus()
                 }}
                 onEndEditing={()=>{
                   this.verifyUniqueNameBefore(this.state.petName);
                 }}
                 style={styles.textInputData}
                 onChangeText={this.onChangePetName}
                 placeholder='Ingresa el nombre de tu mascota'
                 placeholderTextColor="#C0C0C0"
                 value={this.state.petName}
             />
                {this.getViewErrorMessage(0)}

          </View>

          <View style={{marginTop: 20,}}>
              <Text style={styles.textLabel}>Nombre unico para tu mascota</Text>
                 <View style={{flexDirection: 'row'}}>
                     <View style={{width: "70%"}}>
                          <TextInput
                           ref={(input) => { this.petUniqueName = input; }}
                           returnKeyType={"next"}
                           editable={this.state.isEditablePetUniqueName}
                           onEndEditing={this.onSubmitUniqueName}
                           onSubmitEditing={this.onSubmitUniqueName}
                           style={[styles.textInputData,{width: "100%"}]}
                           onChangeText={(petUniqueName)=>this.setState({petUniqueName:cleanString(petUniqueName)})}
                           placeholder='Ingresa el nombre de usuario'
                           placeholderTextColor="#C0C0C0"
                           value={this.state.petUniqueName}/>
                           {this.getViewErrorMessage(1)}
                       </View>

                       <View style={{width: "30%",justifyContent: 'center',alignItems: 'flex-end'}}>
                         <TouchableOpacity
                           onPress={this.manageButtonVerifyPetUniqueName}
                           style={[styles.buttonVerifyPetUniqueName,(this.state.isEditablePetUniqueName)?{backgroundColor:'orange',borderWidth: 0}:{}]}>
                           <Text  style={{color:(this.state.isEditablePetUniqueName)?"white":"#00B99B",fontSize: 14}}>{(this.state.isEditablePetUniqueName)?'Verificar':"Modificar"}</Text>
                         </TouchableOpacity>
                       </View>
                 </View>
          </View>

           <View style={{marginTop: 20}}>
                <Text style={[styles.textLabel]}>Cumpleaños</Text>
                <DatePicker    onDateChange={this.onDateChange}    customComponent={
                                  <View style={styles.viewContainerDate}>
                                      <Text style={{fontSize: 16,color: (this.state.isPickBirthday)?"black":'#C0C0C0'}}>{this.state.petBirthday}</Text>
                                  </View> }/>
            </View>

            <ButtonContinue  onPress={this.goRegisterPetStep3} customStyle={{width:"100%",marginTop: 30,}} isValidData={this.state.isValidData} text={"Listo"} />
        </View>



      </View>
    );
  }


  onChangePetName=(petName)=>{
       isValidName=true;
        if(!validateSpecialCharacters(petName)){
            this.setState({
              indexError:0,
              message:"Nombre no valido "+petName,
              messageStyle:styles.messageWarning
            });
            isValidName=false;
        }

        this.setState({
          isValidName,
          petName,
          petUniqueName:this.getPetUniqueName(petName)
        });

        this.validateData();
  }

  onDateChange=(date)=>{
      this.setState({petBirthday:date,isPickBirthday:true,});
      this.validateData();
  }


  onSubmitUniqueName=()=>{
     this.verifyUniqueNameBefore(this.state.petUniqueName);
  }

  manageButtonVerifyPetUniqueName=()=>{
    if(this.state.isEditablePetUniqueName){
       this.onSubmitUniqueName();
    }else{
       this.petUniqueName.focus();
       this.setState({isEditablePetUniqueName:true,isValidPetUniqueName:false,});
    }
  }


  verifyUniqueNameBefore(petName){
    petUniqueName=this.getPetUniqueName(petName);
    if(petUniqueName.length>=3 && petUniqueName.length<=19){
      this.verifyUniqueName(petUniqueName);
    }else{
       this.setState({
         indexError:1,
         isValidPetUniqueName:false,
         isEditablePetUniqueName:true,
         message:"Nombre de perfil unico muy largo, debe ser menor a 20",
         messageStyle:styles.messageError});
    }
  }

  getPetUniqueName(petName){
    return cleanString((petName.toLowerCase()).trim());
  }




  getViewErrorMessage(indexInput){
    indexError= this.state.indexError;
    if(indexInput==indexError){
      return <Text style={[this.state.messageStyle,{marginTop: 10,}]}>{this.state.message}</Text>;
    }
  }

  verifyUniqueName(petUniqueName){

    Api.get(Routes.URL_PROFILE_VERIFY_UNIQUE_NAME+petUniqueName)
       .then((response)=>{
         if(response.status=="error"){
           this.setState({indexError:1,isEditablePetUniqueName:true,isValidPetUniqueName:false,message:response.message,messageStyle:styles.messageError});
         }else{
           this.validateData();
           this.setState({indexError:1,isValidPetUniqueName:true,isEditablePetUniqueName:false,message:response.message,messageStyle:styles.messageSuccess});
         }

     }).catch((ex)=>{
        console.log(ex);
     });
  }


  validateData(){
    setTimeout(()=>{
        var {isValidPetUniqueName,petBirthday,isValidName} =this.state;
        if(isValidPetUniqueName && petBirthday!='Selecciona la fecha de cumpleaños' && isValidName){
          this.setState({isValidData:true});
        }else{
          this.setState({isValidData:false});
        }

    },200);
  }


  goRegisterPetStep3=()=>{
    console.warn("sresd");
      var {petUniqueName,petBirthday,petName} =this.state;
      params.petUniqueName=petUniqueName;
      params.petBirthday=petBirthday
      params.profile_name=petName;

       this.props.navigation.navigate('RegisterPetStep3',params);
  }





}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textLabel:{
    fontWeight: '500',
    color: '#15d2bb',
    fontSize: 12,
  },
  textInputData:{
    height: 40,
    borderColor: '#00B99B',
    borderBottomWidth: 1,
    fontSize: 16,
    padding: 0,
  },
  viewContainerDate:{
    paddingBottom: 8,
    padding: 0,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    height: 40,
    borderBottomWidth: 1,
    borderColor: '#00B99B'
  },
  buttonVerifyPetUniqueName:{
    borderWidth: 1,
    width: "85%",
    borderColor: "#00B99B",
    justifyContent: 'center',
    alignItems:'center',
    height: 36,
    borderRadius: 8,
  },
  messageSuccess:{
    fontSize: 12,
    color: 'green',
  },
  messageWarning:{
    fontSize: 12,
    color: '#f0ad4e',
  },
  messageError:{
    fontSize: 12,
    color: '#d2152b',
  }



});
