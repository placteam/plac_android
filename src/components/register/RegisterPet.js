import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  Button,
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  Animated,
} from 'react-native';


const { width,height} = Dimensions.get('window');

import DatePicker from '../views/DatePicker'

import ButtonContinue from '../views/ButtonContinue'

var petTypeList=[{id:"DOG",name:"Perro",isSelected:0},{id:"CAT",name:"Gato",isSelected:0},{id:"RABBIT",name:"Conejo",isSelected:0},{id:"HAMSTER",name:"Hamster",isSelected:0},{id:'MINIPIG',name:"Mini pig",isSelected:0}];
var widthPetTypeList=(width-48)/5;

var heightImagePet=144;
var widthImagePet=width-24;


var params;

export default class RegisterPet extends React.Component {

  constructor(props){
    super(props)
    params=this.props.navigation.state.params;
    this.state={
      profile_name:"",
      petBirthday:"",
      petTypeList,
      petTypeSelected:null,
      isValidData:false,
    }

  }

  static navigationOptions = {
      title: 'Agregar mascota',
  };

  render() {


    return (
      <View style={styles.container}>

        <View style={{margin: 12,marginBottom: 0}}>
            <View style={{width:widthImagePet,height:168}}>
              <Image  resizeMode={"cover"} style={{width:widthImagePet,height:heightImagePet,borderRadius: 8}} source={require('../../assets/image/general/add_pet.png')}/>
  <PickImage/>
                <View style={{top:(heightImagePet/2)-54,left:(widthImagePet/2)-54,position: 'absolute',width: 108,height: 108,borderRadius: 54,borderWidth: 2,borderColor:'#AAAAAA',backgroundColor: 'white',justifyContent: 'center',alignItems: 'center'}}>
                  {getImageFingerStepPet()}
                </View>




            </View>
        </View>


        <View style={{marginLeft:24,marginRight: 24,marginTop: 20}}>
          <View >
              <Text style={styles.textLabel}>Nombre</Text>
                <TextInput
                 ref={(input) => { this.profile_name = input; }}
                 returnKeyType={"next"}
                 onSubmitEditing={()=>{}}
                 style={styles.textInputData}
                 onChangeText={this.onChangeName}
                 placeholder='Ingresa el nombre de tu mascota'
                 placeholderTextColor="#C0C0C0"
                 value={this.state.profile_name}
             />

          </View>
        </View>
        <View style={{marginLeft:24,marginRight: 24,marginTop: 12}}>
            <Text style={styles.textLabel}>Cumpleaños</Text>
            <DatePicker customComponent={
                            <View style={{paddingBottom: 8,padding: 0,justifyContent: 'flex-end',alignItems: 'flex-start',height: 40,borderBottomWidth: 1,borderColor: '#00B99B'}}>
                                <Text style={{fontSize: 16,color: '#C0C0C0'}}>Selecciona la fecha de cumpleaños</Text>
                            </View> }/>
        </View>

        <View style={{marginLeft:24,marginRight: 24,marginTop: 12}}>
            <Text style={{fontSize: 15,color:'#555555',}}>¿Qué tipo de mascota es?</Text>
            {this.getViewPetTypeList()}
        </View>

        {(this.state.petTypeSelected!=null)?<View style={{marginLeft:24,marginRight: 24,marginTop: 12}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('PickBreed',)}  style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center',borderRadius: 8,height: 36,backgroundColor: '#00B99B'}}>
              <Text style={{color:'white',fontSize: 16}}>Seleccionar raza</Text>

            </TouchableOpacity>
        </View>:null}




        <View style={{position: 'absolute',bottom: 20,alignItems: 'center',justifyContent: 'center',height: 40,width:width-48,left: 24}}>
           <ButtonContinue customStyle={{width: 100}} isValidData={this.state.isValidData} text={"Listo"} onPress={()=>this.props.navigation.navigate('PickBreed',{})}/>
        </View>





      </View>
    );
  }



_keyExtractor = (item, index) => item.name;
 getViewPetTypeList(){
    return <FlatList
      style={{marginTop: 10}}
       horizontal={true}
       data={this.state.petTypeList}
       keyExtractor={this._keyExtractor}
       renderItem={this.renderPetTypeItem}
     />
 }

 renderPetTypeItem=(object)=>{
   item=object.item;
   index=object.index;
   styleRoundeLeft={};
   styleRoundedRight={};
   styleViewPress={};
   styleTextPress={};
   if(index==0){
     styleRoundeLeft={borderTopLeftRadius: 8,borderBottomLeftRadius: 8}
   }

   if(index==4){
     styleRoundedRight={borderTopRightRadius: 8,borderBottomRightRadius: 8,borderRightWidth: 1}
   }

   if(item.isSelected){
     styleViewPress={borderTopWidth:2,borderBottomWidth:2,borderLeftWidth:2,borderRightWidth:1};
     styleTextPress={fontSize:14,fontWeight:'bold'}
   }

   return <TouchableOpacity onPress={()=>this.onSelectPetType(object.item) } style={[styles.buttonPetTypeItem,styleRoundeLeft,styleRoundedRight,styleViewPress]}>
                    <Text style={[{fontSize: 14,color:'black'},styleTextPress]}>{item.name}</Text>
          </TouchableOpacity>

 }

 onSelectPetType(item){
   var petTypeList=this.state.petTypeList;
   for(i=0;i<petTypeList.length;i++){

     if(item.name==petTypeList[i].name){
        petTypeList[i].isSelected=true;
     }else{
       petTypeList[i].isSelected=false;
     }
   }
   this.setState({petTypeList,petTypeSelected:item});
 }



}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textLabel:{
    fontWeight: '500',
    color: '#15d2bb',
    fontSize: 12,
  },
  textInputData:{
    height: 40,
    borderColor: '#00B99B',
    borderBottomWidth: 1,
    fontSize: 16,
    padding: 0,
  },
  buttonPetTypeItem:{
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#00B99B',
    width: widthPetTypeList,
    height: 48
  },
  messageSuccess:{
    fontSize: 14,
    color: 'green',
  },
  messageWarning:{
    fontSize: 14,
    color: '#f0ad4e',
  },
  messageError:{
    fontSize: 14,
    color: '#d2152b',
  },
  box:{
    position:'absolute',

    height: 100,
//   width: 100,
    left:0,
    top:250,
    backgroundColor: 'tomato',
  }


});
