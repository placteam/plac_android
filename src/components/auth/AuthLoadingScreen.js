import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';


//API REQUEST
import Api from '../../lib/http_request/'
import * as Routes from '../../app/routes'

import * as PlacUserController from '../../app/controllers/PlacUserController'
import * as AppConfigurationController from '../../app/controllers/AppConfigurationController'
import * as PlacUserConfigurationController from '../../app/controllers/PlacUserConfigurationController'
import * as ProfileController from '../../app/controllers/ProfileController'
import * as OrderDetailController from '../../app/controllers/OrderDetailController';
import firebase from 'react-native-firebase';
import * as Constanst from '../../app/constants'

import {getDeviceInfo} from '../../lib/device_info'
// PUSH NOTIFICATIONS
import OneSignal from 'react-native-onesignal';


export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);

  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {

    this.props.navigation.navigate(userToken ? 'main' : 'auth', params);
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar
          barStyle="light-content"
          backgroundColor="#15D2BB"
        />
      </View>
    );
  }



  componentWillMount() {
    OneSignal.init(Constanst.ONE_SIGNAL_APIKEY);
    OneSignal.configure({});
    OneSignal.inFocusDisplaying(2);
  }

  componentDidMount() {

    setTimeout(() => {
  
      if (AppConfigurationController.index().length>0) {
        let ac= AppConfigurationController.index()[0];
          if(ac.plac_app_version_installed==null){
            OrderDetailController.destroy();
            let object={
              plac_app_version_installed:getDeviceInfo().app_version,
            }
            AppConfigurationController.update(object,ac.app_configuration_id);
          }
          this.redirectNewUser();
      } else {
          let users=PlacUserController.getCurrentUserOld();
          if(users.length>0){
            let user=users[0];

            let placUserId=user.plac_user_id;

            if(user.sign_up_type=="guest"){
                PlacUserController.destroy();
                ProfileController.destroy();
                this.redirectNewUser();
                return;
            }

            AppConfigurationController.manage('newplac',placUserId);
            OrderDetailController.destroy();
            let profiles=ProfileController.getProfilesByUser(placUserId);
            if(profiles.length>0){
              let profile= profiles[0];
              let profileId=profile.profile_id;
              PlacUserConfigurationController.manage(placUserId,profileId);
              this.updateProfiles(placUserId);
            }else{
              this.redirectNewUser();
            }
          }else{
            this.redirectNewUser();
          }
      }
    },1000);
  }


  updateProfiles(placUserId){

    Api.get(Routes.URL_PROFILES_BYUSER+placUserId)
         .then((response)=>{
           var {status,data,message}=response;
           if(status=="success"){
              let profiles=data;
              for(i=0;i<profiles.length;i++){
                  let profile= profiles[i];
                  let profileRealm={
                    profile_id:profile.profile_id,
                    profile_name:profile.profile_name,
                    profile_unique_name:profile.profile_unique_name
                  }
                  ProfileController.update(profileRealm); 
              }
              this.redirectNewUser();
           }

        
       }).catch((ex)=>{
          console.log(ex);
       });

  }
  

  redirectNewUser(){
     let placUser = PlacUserController.getCurrentUser();
      if (placUser != null) {
        var params = {
          plac_user: placUser
        };
        profiles = ProfileController.getProfilesByUser(placUser.plac_user_id);
        if (profiles.length == 0) {
            this.props.navigation.navigate("WelcomePlac", params);
        } else {
          params.profiles = profiles;
          params.profileSelected = ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id);
          console.log(params);
          this.props.navigation.navigate("main", params);
        }
      } else {
        this.props.navigation.navigate("auth");
      }

  }


  resetObjects() {
    AppConfigurationController.destroy();
    PlacUserController.destroy();
    ProfileController.destroy();
    PlacUserConfigurationController.destroy();

    firebase.auth().signOut();

    console.warn("reset objects");

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#15d2bb',
  },

});
