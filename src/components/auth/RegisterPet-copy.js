import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  Button,
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  Animated,
} from 'react-native';


const { width,height} = Dimensions.get('window');
import {getImageFingerStepPet} from '../../assets/image/svg'
import DatePicker from '../views/DatePicker'

import {validateEmail,validateSpecialCharacters} from '../../lib/validations'
var petTypeList=["Perro","Gato","Conejo","Hamster","Mini pig"];
var widthPetTypeList=(width-40)/5;




export default class RegisterPet extends React.Component {
  static navigationOptions = {
      title: 'Agregar mascota',
  };

  constructor(props){
    super(props)
    this.state={
      profile_name:"",
      petBirthday:"",
      petTypeList,
    }

  }



  render() {

    return (
      <View style={styles.container}>
        <View style={{margin: 15,marginBottom: 0}}>
            <Image  resizeMode={"cover"} style={{width:width-30,height:144,borderRadius: 8}} source={require('../../assets/image/general/add_pet.png')}/>
            <View style={{top:0,height: 144,position: 'absolute',justifyContent: 'center',alignItems: 'center',width: width-30}}>
                <View style={{width: 108,height: 108,borderRadius: 54,borderWidth: 2,borderColor:'#AAAAAA',backgroundColor: 'white',justifyContent: 'center',alignItems: 'center'}}>
                  {getImageFingerStepPet()}
                </View>
            </View>
        </View>
        <View style={{margin:10}}>
          <View >
                
          </View>
        </View>
        <View style={{margin:20,marginTop: 0}}>
            <Text style={styles.textLabel}>Cumpleaños</Text>
            <DatePicker customComponent={
                            <View style={{paddingBottom: 8,padding: 3,justifyContent: 'flex-end',alignItems: 'flex-start',height: 40,borderBottomWidth: 1,borderColor: '#00B99B'}}>
                                <Text style={{fontSize: 16,color: 'grey'}}>Selecciona la fecha de cumpleaños</Text>
                            </View> }/>
        </View>

        <View style={{margin:20,marginTop: 0}}>
            <Text style={{fontSize: 16,color:'#555555',}}>¿Qué tipo de mascota es?</Text>
            {this.getViewPetTypeList()}


        </View>

        <TouchableOpacity  activeOpacity={.5} style={styles.buttonPickImage}>
          <Text  style={{fontSize: 22,color:'white',fontWeight: '400'}}>+</Text>
        </TouchableOpacity>


      </View>
    );
  }

_keyExtractor = (item, index) => item;
 getViewPetTypeList(){
    return <FlatList
      style={{marginTop: 10}}
       horizontal={true}
       data={this.state.petTypeList}
       keyExtractor={this._keyExtractor}
       renderItem={this.renderPetTypeItem}
     />
 }

 renderPetTypeItem=(object)=>{
   item=object.item;
   index=object.index;
   styleRoundeLeft={};
   styleRoundedRight={};
   if(index==0){
     styleRoundeLeft={borderTopLeftRadius: 10,borderBottomLeftRadius: 10}
   }

   if(index==4){
     styleRoundedRight={borderTopRightRadius: 10,borderBottomRightRadius: 10,borderRightWidth: 1}
   }


   return <TouchableOpacity style={[styles.buttonPetTypeItem,styleRoundeLeft,styleRoundedRight]}>
                    <Text style={{fontSize: 14,color:'black',}}>{item}</Text>
          </TouchableOpacity>

 }





}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  buttonPickImage:{
    position: 'absolute',
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    width: 48,
    borderRadius: 24,
    right: 30,
    backgroundColor: "#00B99B",
    top:136,
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowOpacity: 0.8,
    shadowRadius: 4,
    elevation: 3,
  },
  textLabel:{
    fontWeight: '500',
    color: '#15d2bb',
    fontSize: 12,
  },
  textInputData:{
    height: 40,
    borderColor: '#00B99B',
    borderBottomWidth: 1,
    fontSize: 16
  },
  buttonPetTypeItem:{
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#00B99B',
    width: widthPetTypeList,
    height: 48,
  },
  messageSuccess:{
    fontSize: 14,
    color: 'green',
  },

  messageWarning:{
    fontSize: 14,
    color: '#f0ad4e',
  },
  messageError:{
    fontSize: 14,
    color: '#d2152b',
  },
  labelInput: {
    color: '#555555',
 },
 formInput: {
   borderBottomWidth: 1,
   borderColor: '#00B99B',
   padding: 0,
 },
 input: {
   borderWidth: 0,
   padding:0,
     fontSize: 16,
 }


});
