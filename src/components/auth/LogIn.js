import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  Button,
  StyleSheet,
  Alert,
  Text,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const { width, height } = Dimensions.get('window');

import Swiper from 'react-native-swiper';
import { getProportionalImageHeight } from '../../lib/image';

// PUSH NOTIFICATIONS
import OneSignal from 'react-native-onesignal';



export default class LogIn extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isVisibleTerms: true,
      isAcceptedTerms: false,
      isLoading: false,
      device: null,
    }

  }
  static navigationOptions = {
    title: 'Please sign in',
    header: null,

  };

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.swiper}>
          <Swiper
            autoplay={false}
            autoplayTimeout={2}
            dotStyle={{ margin: 0, width: 6, height: 6, borderRadius: 3 }}
            activeDotStyle={{ margin: 0 }}
            activeDotColor="#15d2bb"
          >
            <View style={styles.slide1}>
              <Image style={{ width, height: getProportionalImageHeight({ width: 3466, height: 2575 }, width) }} source={require('../../assets/image/general/banner_login.jpg')} />
            </View>
          </Swiper>
        </View>
        <View style={styles.viewPhoneNumber} >
          <View style={{ flexDirection: 'row', alignItems: 'center', }}>
            <Image style={styles.imagePhoneNumberCountry} source={require('../../assets/image/flag.png')} />
            <Text style={styles.textPhoneNumberCountryCode}>+57</Text>
          </View>
          <View style={{ flex: 1, marginLeft: 5, }}>
            <TouchableOpacity onPress={() => this.goPhone()} style={{ marginLeft: 10, justifyContent: 'center' }}>
              <Text style={styles.textPhoneNumberPlaceHolder}>Ingresar con tu número de teléfono móvil</Text>
            </TouchableOpacity>
          </View>
        </View>


        <View style={styles.viewIntroWithOtherOptions}>

          <TouchableOpacity disabled={(this.state.device == null) ? true : false} onPress={() => this.goSocialMedia()} style={styles.buttonBorderColor}>
            <Text style={{ fontSize: 16, color: '#00B99B' }}>Ingresar con redes sociales</Text>
          </TouchableOpacity>
          {(this.state.isLoading) ?
            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
              <Image style={{ width: 70, height: 26, }} source={require('../../assets/image/general/loader.gif')} />
              <Text style={{ fontSize: 12, color: '#15d2bb' }}>Espere un momento por favor</Text>
            </View> : null
          }


          { /* <TouchableOpacity style={styles.buttonIntroWithOutSignUp}>
                    <Text style={{fontSize: 14,color: '#00B99B'}}>Ingresar sin registro</Text>
                </TouchableOpacity>*/
          }
        </View>
        {this.getViewTerms()}
      </View>
    );
  }

  goPhone() {
    if (this.state.isAcceptedTerms) {
      let params = {
        device: this.state.device
      }
      this.props.navigation.navigate('RequestPhone', params);
    } else {
      Alert.alert("Términos y condiciones", "Debes aceptar los términos y condiciones antes de continuar");
    }
  }

  goSocialMedia() {
    if (this.state.isAcceptedTerms) {
      let params = {
        device: this.state.device
      }
      console.warn(params);
      this.props.navigation.navigate('ContinueSocialMedia', params);
    } else {
      Alert.alert("Términos y condiciones", "Debes aceptar los términos y condiciones antes de continuar");
    }
  }

  getViewTerms() {

    if (this.state.isVisibleTerms) {
      let isAcceptedTerms = this.state.isAcceptedTerms;
      let style = styles.chekBoxAcceptTerms;
      if (isAcceptedTerms) {
        style = styles.chekBoxAcceptTermsPress;
      }
      return <View style={styles.viewTerms}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={() => this.onChangeAcceptedTerms()} style={style} />
          <Text style={{ fontSize: 12, marginLeft: 5, color: 'black' }}>Acepto <Text style={{ textDecorationLine: 'underline', color: '#15d2bb' }}>términos</Text> y <Text style={{ textDecorationLine: 'underline', color: '#15d2bb' }}>políticas</Text> de privacidad</Text>
        </View>
      </View>
    }


  }

  onChangeAcceptedTerms() {
    let isAcceptedTerms = this.state.isAcceptedTerms;
    if (isAcceptedTerms) {
      isAcceptedTerms = false;
    } else {
      isAcceptedTerms = true;
    }
    this.setState({ isAcceptedTerms });

  }

  componentWillUnmount() {
    OneSignal.removeEventListener('ids', this.onIds);
    this.setState({ isVisibleTerms: false });
  }



  componentWillMount() {
    this.setState({ isVisibleTerms: true });
    OneSignal.addEventListener('ids', this.onIds);
  }


  onIds = (device) => {
    this.setState({ device, isLoading: false });
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  swiper: {
    backgroundColor: '#15d2bb',
    width,
    height: '50%'
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  viewPhoneNumber: {
    marginBottom: 4,
    marginLeft: 24,
    marginRight: 24,
    padding: 8,
    paddingLeft: 14,
    paddingRight: 14,
    borderRadius: 8,
    alignItems: 'center',
    flexDirection: 'row',
    height: 72,
    backgroundColor: '#00B99B',
  },
  imagePhoneNumberCountry: {
    width: 36,
    height: 24,

    borderRadius: 4
  },
  textPhoneNumberCountryCode: {
    marginLeft: 5,
    color: 'white',
    fontSize: 16,
  },
  textPhoneNumberPlaceHolder: {
    fontSize: 16,
    color: 'white',
    lineHeight: 19,
    fontFamily: 'Roboto',
    alignSelf: 'flex-start'
  },
  viewIntroWithOtherOptions: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 12,
  },
  buttonBorderColor: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 100,
    height: 36,
    borderWidth: 1,
    borderColor: '#00B99B',
    borderRadius: 8
  },
  buttonCompany: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 100,
    height: 36,
    backgroundColor: '#00B99B',
    borderRadius: 8
  },
  buttonIntroWithOutSignUp: {
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
    width: width - 100,
    height: 36,
  },
  viewTerms: {
    position: 'absolute',
    bottom: 20, width,
    justifyContent: 'center',
    alignItems: 'center'
  },
  chekBoxAcceptTerms: {
    width: 18,
    height: 18,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: "#555555"
  },
  chekBoxAcceptTermsPress: {
    width: 18,
    height: 18,
    borderRadius: 2,
    backgroundColor: "#00B99B",

  }

});
