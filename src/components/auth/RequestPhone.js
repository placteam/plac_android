import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  Button,
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Animated,
} from 'react-native';


//Plac user controller REALM
import * as PlacUserController from '../../app/controllers/PlacUserController'
import * as AppConfigurationController from '../../app/controllers/AppConfigurationController'

import firebase from 'react-native-firebase';


//API REQUEST
import Api from '../../lib/http_request/'
import * as Routes from '../../app/routes'

//INFO DEVICE
var {height, width} = Dimensions.get('window');
import {getDeviceInfo} from '../../lib/device_info'
var params;
var device;


export default class RequestPhone extends React.Component {
  static navigationOptions = {
      title: '',
      headerTransparent:true,
  };

  constructor(props){
    super(props)
    params=props.navigation.state.params;
    device=params.device;
    this.state={
      phoneNumber:'',
      isValidPhoneNumber:false,
      continueTo:'send_code',
      animationPhoneNumber:new Animated.Value(1),
      animationCode:new Animated.Value(0),
      device:null,
      message:"",
      code1:"",
      code2:"",
      code3:"",
      code4:"",
      code5:"",
      code6:"",
    }

  }

  render() {

   var  buttonStyleNotPress={};
    if(!this.state.isValidPhoneNumber){
      buttonStyleNotPress.backgroundColor="#ededed";
    }

    return (
      <View style={styles.container}>
        <View style={{marginLeft:20,marginRight: 20}}>
          {this.getViewPhoneNumber()}
          {this.getViewCodeConfirmation()}

          <TouchableOpacity  disabled={!this.state.isValidPhoneNumber} onPress={()=>this.onPressContinueTo(this.state.continueTo)} style={[styles.buttonContinue,buttonStyleNotPress]}>
             <Text style={{color: 'white',fontSize: 16}}>Continuar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }



  getViewPhoneNumber(){
    let  animatedStyles1={
      opacity:this.state.animationPhoneNumber
    }

        return  <Animated.View  style={animatedStyles1}>
                   <View style={styles.viewPhoneNumber}>
                          <View style={{flexDirection: 'row',alignItems: 'center',}}>
                            <Image  style={styles.imageFlag} source={require('../../assets/image/flag.png')}/>
                            <Text style={styles.textCountryCode}>+57</Text>
                          </View>
                          <View style={styles.textInputPhoneNumber}>
                              <TextInput
                               ref={(input) => { this.phoneNumber = input; }}
                               keyboardType={'number-pad'}
                               style={{height: 40,fontSize: 18,}}
                               onChangeText={this.onChangePhoneNumber}
                               placeholder='Ingresa tu número de celular'
                               value={this.state.phoneNumber} />
                          </View>
                    </View>
                      {this.getViewMessage()}
                </Animated.View>
  }



  getViewCodeConfirmation(){
    let  animatedStyles2={
      opacity:this.state.animationCode
    }
    if(this.state.continueTo=="validate_code"){

  
    return    <Animated.View  style={[styles.animatedViewCodeConfirmation,animatedStyles2]} >
               <View style={styles.viewCodeConfirmation}>
                   <TextInput
                    ref={(input) => { this.code1 = input; }}
                    returnKeyType ="next"
                    keyboardType={'number-pad'}
                    style={styles.textInputCode}
                    onChangeText={(code1) => {
                      this.setState({code1});
                      if(code1.length==1){
                        this.code2.focus();
                      }
                    }}
                    value={this.state.code1}
                    onKeyPress={(e)=>this.onKeyPress(e,2)}
                  
                />

                  <TextInput
                    ref={(input) => { this.code2 = input; }}
                    returnKeyType ="next"
                   keyboardType={'number-pad'}
                   style={styles.textInputCode}
                   onChangeText={(code2) => {
                       this.setState({code2});
                     if(code2.length==1){
                       this.code3.focus();
                     }
                   }}
                   value={this.state.code2}
                   onKeyPress={(e)=>this.onKeyPress(e,2)}

                 />

               <TextInput
                 ref={(input) => { this.code3 = input; }}
                 returnKeyType ="next"
                 keyboardType={'number-pad'}
                 style={styles.textInputCode}
                 onChangeText={(code3) => {
                        this.setState({code3});
                   if(code3.length==1){
                     this.code4.focus();
                   }

                 }}
                 value={this.state.code3}
                 onKeyPress={(e)=>this.onKeyPress(e,3)}

              />

                <TextInput
                  ref={(input) => { this.code4 = input; }}
                  returnKeyType ="next"
                  keyboardType={'number-pad'}
                  style={styles.textInputCode}
                  onChangeText={(code4) => {
                      this.setState({code4});
                    if(code4.length==1){
                      this.code5.focus();
                    }
                  }}
                  value={this.state.code4}
                  onKeyPress={(e)=>this.onKeyPress(e,4)}

             />

                 <TextInput
                   ref={(input) => { this.code5 = input; }}
                   returnKeyType ="next"
                   keyboardType={'number-pad'}
                   style={styles.textInputCode}
                   onChangeText={(code5) => {
                        this.setState({code5});
                     if(code5.length==1){
                       this.code6.focus();
                     }

                   }}
                   value={this.state.code5}
                   onKeyPress={(e)=>this.onKeyPress(e,5)}
              />
                <TextInput
                  ref={(input) => { this.code6 = input; }}
                  returnKeyType ="next"
                  keyboardType={'number-pad'}
                  style={styles.textInputCode}
                  onChangeText={(code6) => {
                        this.setState({code6},()=>{
                           this.confirmCode();
                        });
                     
                     }
                  }
                  value={this.state.code6}
                  onKeyPress={(e)=>this.onKeyPress(e,6)}
                 />
          </View>
            {this.getViewMessage()}
          <View style={styles.viewResendConfirmationCode}>
            <Text style={styles.textQuestionRCC}>¿ Recibiste el código de confirmación ?</Text>
            <Text  onPress={()=>alert("Reenviando código")} style={styles.textBtnResendCode}>Reenviar codigo</Text>
          </View>
       </Animated.View>
      }

  }

  getViewMessage(){
    return <View style={{justifyContent: 'center',marginTop: 10}}>
                <Text style={[{fontSize: 14},this.state.messageStyle]}>{this.state.message}</Text>
           </View>
  }

  onKeyPress(e,position){

    if (e.nativeEvent.key === 'Backspace') {
            switch (position) {
              case 1:
                
              break;
              case 2:
                this.code1.focus();
                this.code1.clear();
              break;
              case 3:
                this.code2.focus();
                this.code2.clear();
                
              break;
              case 4:
                this.code3.focus();
                this.code3.clear();
              break;
              case 5:
                this.code4.focus();
                this.code4.clear();
              break;
              case 6:
                this.code5.clear();
                this.code5.focus();
                this.code6.clear();
              break;
        
          }
    }
   

  }

  onChangePhoneNumber=(phoneNumber)=>{
       this.setState({phoneNumber});
        if(phoneNumber.length>=10 && phoneNumber.length<=10){
          this.setState({isValidPhoneNumber:true});
        }else{
         this.setState({isValidPhoneNumber:false});
        }
  }

  onPressContinueTo(to){
    if(to=='send_code'){
        this.startAnimation();
        let phoneNumber="+57"+this.state.phoneNumber;
        this.signIn(phoneNumber);
    }else{
      this.confirmCode();
    }

  

  }

  startAnimation=()=>{
      Animated.parallel([
        Animated.timing(this.state.animationCode,{
          toValue:1,
          duration:500,
        }),
        Animated.timing(
          this.state.animationPhoneNumber,{
          toValue:0,
          duration:600
        })
      ]).start();
  }

  confirmCode(){
     this.code6.blur();
     let  {code1,code2,code3,code4,code5,code6,confirmResult} =this.state;
     
     if(!code1 || !code2 || !code3 || !code4  || !code5 || ! code6){
        this.setState({ message: 'Ingresa el código',messageStyle:styles.messageError});
        return ;
     }

     let codeFinal=code1.concat(code2).concat(code3).concat(code4).concat(code5).concat(code6)
    
     if(codeFinal.length<6){
        this.setState({ message: 'Completa el código  de confirmación',messageStyle:styles.messageError});
        return ;
    }  
     
   
     this.setState({ message: 'Verificando codigo...',messageStyle:styles.messageSuccess});
     if (confirmResult && codeFinal.length) {
       confirmResult.confirm(codeFinal)
         .then((user) => {
           this.setState({ message: 'Codigo confirmado!',messageStyle:styles.messageSuccess});
           this.checkUserExist(user);
         })
         .catch(error => this.setState({ message: `Error: ${error.message}` ,messageStyle:styles.messageError}));
     }else{
        this.setState({message:'Ingresa el código',messageStyle:styles.messageError});
     }

  }


  signIn = (phoneNumber) => {
     firebase.auth().signInWithPhoneNumber(phoneNumber)
       .then((confirmResult) =>{
           this.setState({ confirmResult, message: 'Codigo enviado!' ,continueTo:'validate_code',messageStyle:styles.messageSuccess});
        })
       .catch(error => this.setState({ message: `Sign In With Phone Number Error: ${error.message}` }));
   };

   checkUserExist=(user)=>{

       let providerData=user.providerData[0];
       const URL_REQUEST=Routes.URL_PLAC_USERS_MANAGE_LOGIN;
       let installation=getDeviceInfo();
       installation.device_token=device.userId;
       let params={
           authentication:{
              authentication_type:providerData.providerId,
           		authentication_identificator:providerData.phoneNumber,
           		user_uid:user.uid,
           },
           installation
       }

  
       
       Api.post(URL_REQUEST,params)
          .then((response)=>{
            console.warn(response);

             if(response.status=="error"){
                 if(response.message=="user_not_exist"){
                   params.additionalUserInfo={providerId:providerData.providerId};
                   this.props.navigation.navigate('PlacUserData',params);
                 }
             }else if(response.status=='success'){
                  deviceToken=params.installation.device_token;
                  let  placUser=response.data;
                  placUserId=placUser.plac_user_id;
                  const request={
                    user:placUser,
                    deviceToken,
                  }
                  PlacUserController.store(request);
                  AppConfigurationController.manage(deviceToken,placUserId);
                  this.props.navigation.navigate('AuthLoading',null);
             }
        }).catch((ex)=>{
           console.warn(ex);
        });
  }


  componentWillMount(){
       this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
         if (user) {
               this.setState({ message: 'Codigo confirmado!',messageStyle:styles.messageSuccess});
               this.checkUserExist(user);
         }
       });

   }

  componentWillUnmount(){
      this.phoneNumber.clear();
      if (this.unsubscribe) this.unsubscribe();
  }

  componentDidMount(){
      this.phoneNumber.focus();
  }




}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  viewPhoneNumber:{
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: 'gray',
    borderBottomWidth: .5,
    height:65,
    marginTop: 50
  },
  imageFlag:{
    width:36,
    height: 24,
    borderRadius: 4
  },
  textCountryCode:{
    marginLeft: 5,
    color: 'black',
    fontSize: 18,
  },
  animatedViewCodeConfirmation:{
    position: 'absolute',
    width: width-40,
    marginTop: 50,
  },
  viewCodeConfirmation:{
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  textInputCode:{
    height: 40,
    textAlign: 'center',
    width: 45,
    borderRadius: 4,
    borderColor: 'grey',
    borderWidth: 1,
    fontSize: 17
  },
  textInputPhoneNumber:{
    flex:1,
    marginLeft: 5,
    paddingTop: 6.5,
    justifyContent: 'center'
  },
  viewResendConfirmationCode:{
    marginTop: 20,
    width: "80%"
  },
  textQuestionRCC:{
    fontSize: 14,
    color:'black',
    alignItems: 'stretch'
  },
  textBtnResendCode:{
    fontSize: 12,
    color: 'black',
    color:'#00b99b'
  },
  buttonContinue:{
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
    marginTop: 60,
    backgroundColor: '#00B99B',
    borderRadius: 8
  },
  messageSuccess:{
    fontSize: 14,
    color: 'green',
  },
  messageError:{
    fontSize: 14,
    color: 'red',
  }


});
