import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Image,
} from 'react-native';

//Plac user controller REALM
import * as PlacUserController from '../../app/controllers/PlacUserController'
import * as AppConfigurationController from '../../app/controllers/AppConfigurationController'



//Manage login
import { AccessToken, LoginManager,GraphRequest, GraphRequestManager} from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import firebase from 'react-native-firebase'


//API REQUEST
import Api from '../../lib/http_request/'
import * as Routes from '../../app/routes'

import {getDeviceInfo} from '../../lib/device_info'
import {getIconFacebook,getIconGoogle} from  '../../assets/image/svg'
import {getGoogleClientIdByEnvironment} from '../../../config/credentials'
const  requestDataToGraphApiFacebook='/me?fields=id,name,picture.width(400).height(300),email';
var params;
var device;

export default class ContinueSocialMedia extends React.Component {

  constructor(props){
    super(props)
    params=props.navigation.state.params;
    device=params.device;
    this.state={
      phoneNumber:'',
      facebookUser:{},
      googleUser:null,
      providerToLink:"",
      isLoading:false,
      message:"Espera un momento por favor",
    }

  }

  static navigationOptions = {
      title: '',
      headerTransparent:true,
  };

  render() {
    return (
      <View style={styles.container}>
          <View style={styles.content} >
                <Text style={styles.textHeader}>Ingresa con tu cuenta de:</Text>
                <TouchableOpacity  onPress={this.facebookLogin}  style={styles.buttonPickSocialMedia}>
                     {getIconFacebook()}
                      <Text style={styles.textTitle}>Facebook</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.googleLogin} style={[styles.buttonPickSocialMedia,{marginTop: 12}]}>
                    {getIconGoogle()}
                      <Text style={styles.textTitle}>Google</Text>
                </TouchableOpacity>


                {(this.state.isLoading)?
                    <View style={{justifyContent: 'center',alignItems: 'center',marginTop: 70}}>
                         <Image  style={{width: 70,height: 26,}}   source={require('../../assets/image/general/loader.gif')}/>
                         <Text style={{fontSize: 12}}>{this.state.message}</Text>
                     </View>:null
                   }
          </View>
      </View>
    );
  }


  // Calling the following function will open the FB login dialogue:
  facebookLogin = async () => {
    try {
      const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
       if(!result.isCancelled) {
          // get the access token
          const data = await AccessToken.getCurrentAccessToken();
          if (data) {
              const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
              // login with credential
              const currentUser = await firebase.auth().signInAndRetrieveDataWithCredential(credential);
              this.manageLogin(currentUser);
          }
      }
    } catch (e) {
      console.log(e);
    }
  }

  //Create response callback.
_responseInfoCallback=(error: ?Object, result: ?Object)=> {
  if (error) {
    console.log('Error fetching data: ' + error.toString());
  } else {
    this.setState({facebookUser:result});
    console.warn(JSON.stringify(result));
  }
}


  // Calling this function will open Google for login.
 googleLogin = async () => {
    try {
      // Add any configuration settings here:
      const data = await GoogleSignin.signIn();
      // create a new firebase credential with the token
      const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
      // login with credential
      var  currentUser = await firebase.auth().signInAndRetrieveDataWithCredential(credential);
     this.manageLogin(currentUser);
    } catch (e) {
      console.warn(e);
    }
  }


  async _setupGoogleSignin() {
    var configureGoogleSignin={
      scopes: ["https://www.googleapis.com/auth/user.birthday.read","https://www.googleapis.com/auth/plus.me","https://www.googleapis.com/auth/userinfo.email","https://www.googleapis.com/auth/userinfo.profile"],
      offlineAccess: true
    }
      configureGoogleSignin.webClientId=getGoogleClientIdByEnvironment().web_client_id;
      if(Platform.OS === 'ios'){
         configureGoogleSignin.iosClientId= getGoogleClientIdByEnvironment().ios_client_id;
      }
        try {
          await GoogleSignin.hasPlayServices({ autoResolve: true });
          await GoogleSignin.configure(configureGoogleSignin);
        }
        catch(err) {
          console.warn ("Play services error", err.code, err.message);
        }
  }




  manageLogin=(userObject)=>{
      this.setState({isLoading:true});
      let user=userObject.user;
      let additionalUserInfo=userObject.additionalUserInfo;
      let profile=additionalUserInfo.profile;
      const URL_REQUEST=Routes.URL_PLAC_USERS_MANAGE_LOGIN;
      let installation=getDeviceInfo();
      installation.device_token=device.userId;
      let params={
          authentication:{
             authentication_type: additionalUserInfo.providerId,
             authentication_identificator:profile.email,
             user_uid:user.uid,
          },
          installation
      }
      Api.post(URL_REQUEST,params)
         .then((response)=>{
            params.additionalUserInfo=additionalUserInfo;
            this.manageResponse(response,params);  
       }).catch((ex)=>{
          console.log(ex);
       });
 }


 manageResponse(response,params){
  let status=response.status;
  let message=response.message;

   if(status=="error"){
     if(message=='user_not_exist'){
       this.props.navigation.navigate('PlacUserData',params);
     }
   }else if(status=="success"){
       if(message=='user_exist'){
          let deviceToken=params.installation.device_token;
          let placUser=response.data;
          let placUserId=placUser.plac_user_id;
           const request={
             user:placUser,
             deviceToken,
           }
           try {
            PlacUserController.store(request);
            AppConfigurationController.manage(deviceToken,placUserId);
            this.props.navigation.navigate('AuthLoading',null);
           } catch (error) {
             console.warn(error.message);
             
           }
 
       }
   }



 }


  componentDidMount(){
    this._setupGoogleSignin();
  }




}

/*<TouchableOpacity style={{marginTop: 10,flexDirection: 'row',alignItems: 'center'}}>
      <Svg  width="36"  height="36" version="1.1" id="Layer_1"  x="0px" y="0px"
         viewBox="0 0 36 36" enable-background="new 0 0 36 36" >
         <Path fill="#1B9AF7" d="M34,36H2c-1.1,0-2-0.9-2-2V2c0-1.1,0.9-2,2-2h32c1.1,0,2,0.9,2,2v32C36,35.1,35.1,36,34,36z"/>

         <G>
            <Path fill="#FFFFFF" d="M7.9,10.6l8.6,6.8c0.4,0.3,0.9,0.4,1.5,0.4c0.5,0,1.1-0.1,1.5-0.4l8.6-6.8c0.7-0.5,0.5-1-0.3-1H8.2
              C7.4,9.6,7.2,10,7.9,10.6L7.9,10.6z M7.9,10.6"/>
            <Path fill="#FFFFFF" d="M28.7,12.4l-9.4,7.2C18.9,19.8,18.5,20,18,20c-0.5,0-0.9-0.1-1.3-0.4l-9.4-7.2C6.6,11.9,6,12.2,6,13v11.8
              c0,0.9,0.7,1.6,1.6,1.6h20.8c0.9,0,1.6-0.7,1.6-1.6V13C30,12.2,29.4,11.9,28.7,12.4L28.7,12.4z M28.7,12.4"/>
         </G>
      </Svg>
      <Text style={styles.textTitle}>Correo electrónico</Text>
</TouchableOpacity>*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content:{
    marginTop: 61,
    padding: 24,
    paddingTop: 0
  },
  textHeader:{
    color:'black',
    fontSize: 16,
  },
  textTitle:{
    marginLeft: 13,
    fontSize: 16,
    color:'black'
  },
  buttonPickSocialMedia:{
    marginTop: 21,
    flexDirection: 'row',
    alignItems: 'center'
  }

});
