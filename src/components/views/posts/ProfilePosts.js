import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  RefreshControl,
  Dimensions,
  FlatList,
} from 'react-native';

// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'
//Image
import {getUrlImageCompress} from  '../../../lib/image';

import { CachedImage } from 'react-native-img-cache';
const { width,height} = Dimensions.get('window');
var widthImage=((width-30)/3)- 7.5;

var post;
var urlProfilePosts;
export default class ButtonFollower extends React.Component{




   constructor(props){
     super(props)
     urlProfilePosts=Routes.URL_PROFILE_POSTS+this.props.profileFrom.profile_id+"/"+this.props.profileTo.profile_id;
     this.state={
       posts:[],
       isLoading:true,
       urlProfilePosts,
       arrayUrls:[],
     }
   }



 _keyExtractor = (item, index) => item.post_id;
  render() {
     return <FlatList
                style={{marginTop: 5,marginBottom:20}}
                initialNumToRender={21}
                data={this.state.posts}
                numColumns={3}
                removeClippedSubviews={true}
                refreshControl={
                  <RefreshControl
                      refreshing={this.state.isLoading}
                      onRefresh={()=>this.onRefresh()}
                      title="Cargando..."
                      colors={["#15d2bb","#15d2bb","#15d2bb"]}
                      tintColor="#15d2bb"
                      titleColor="#15d2bb"
                   />
                }
                showsVerticalScrollIndicator={false}
                removeClippedSubviews={true}
                ListHeaderComponent={this.props.viewHeader}
                ListFooterComponent={()=>this.footerComponent()}
                onEndReached={()=>this.handleLoadMore()}
                onEndReachedThreshold={1}
                keyExtractor={this._keyExtractor}
                renderItem={this.renderRow}
             />
  }


  renderRow=(object)=>{
    post=object.item;
    return <TouchableOpacity onPress={()=>this.goPostSingle(object)}>
               <CachedImage  style={{borderRadius: 8,marginRight:10,marginTop: 10,width: widthImage,height:widthImage }} source={{uri:getUrlImageCompress(post.post_path_image,20)}}/>
           </TouchableOpacity>
  }

  goPostSingle(object){
   let  post=object.item;
   let params={
      postId:post.post_id,
      deleteCallBack:this.deleteCallBack
    };
    
    this.props.navigation.push('PostSingle',params);
  }

  deleteCallBack=(post)=>{
      var posts=this.state.posts;
      for(i=0;i<posts.length;i++){
        if(post.post_id==posts[i].post_id){
          posts.splice(i, 1);
        }
      }
      this.setState({
        posts,
      });

  }

  footerComponent(){
    return (this.state.loadMore)?<View style={{justifyContent: 'center',height: 70,alignItems: 'center'}}>
                                 <Image style={{width: 60,height: 23}} source={require('../../../assets/image/general/loader.gif')}/>
                               </View>:null
  }

  handleLoadMore () {
      paginate=  this.state.paginate;
      nextPageURL=paginate.next_page_url;
      if(paginate.current_page!=paginate.last_page){
          if(nextPageURL){
            nextPage=nextPageURL.split("?");
            arrayUrls=this.state.arrayUrls;
                if(arrayUrls.indexOf(nextPageURL)==-1){
                   arrayUrls.push(nextPageURL);
                        this.setState({
                          loadMore:true,
                          arrayUrls,
                          urlProfilePosts:urlProfilePosts+"?"+nextPage[1]
                        }, () => {
                              this.getProfilePosts()
                        });
                }
          }
     }
  };

  onRefresh(){
      this.setState({
        posts:[],
        urlProfilePosts
      },()=>{
        this.getProfilePosts();
      });
  }




  getProfilePosts(){
    Api.get(this.state.urlProfilePosts)
       .then((response)=>{
         const {data}=response;
         let posts= this.state.posts;

         for(i=0;i<data.length;i++){
             posts.push(data[i]);
         }
         this.setState({
           posts,
           paginate:response,
           isLoading:false,
           loadMore:false,
         });
         this.props.postsNumber(response.total);
       }).catch((ex)=>{
          console.warn(ex);
       });
  }



  componentDidMount(){
    this.getProfilePosts();
  }







}



const styles = StyleSheet.create({
  buttonFollowing:{
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    height: 36,width:"50%",
    borderColor: '#00B99B',
    borderRadius: 8,
  },
  textFollowing:{
    fontSize: 14,
    color:'#00B99B'
  }

});
