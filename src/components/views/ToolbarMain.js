import React from 'react';
import DatePickerComponent from 'react-native-datepicker'
import {
  StyleSheet,
  View,
  TouchableOpacity
} from 'react-native';
import { getIconPlacHome, getIconMenu, search } from '../../assets/image/svg'

export default class ToolbarMain extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return <View style={{ flexDirection: 'row', backgroundColor: 'white', height: 50, width: "100%", }}>
      <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={{ flex: 1, marginLeft: 10, justifyContent: 'center', }}>
        {getIconMenu()}
      </TouchableOpacity>
      <TouchableOpacity disabled={this.props.isDisabled} onPress={() => this.props.onPressMiddle()} style={[{ flex: 1, justifyContent: 'center', alignItems: 'center' }, (this.props.viewMiddleStyle != undefined) ? this.props.viewMiddleStyle : {}]}>
        {(this.props.viewMiddle == undefined) ? getIconPlacHome() : this.props.viewMiddle}
      </TouchableOpacity>
      <TouchableOpacity disabled={this.props.btnLeftDisabled} onPress={() => this.props.navigation.navigate('ProfilesSearch', {})} style={styles.btnLeft} >
        {(!this.props.btnLeftDisabled) ? search() : null}
      </TouchableOpacity>
    </View>
  }
}

const styles = StyleSheet.create({
  buttonContinue: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
    backgroundColor: '#00B99B',
    borderRadius: 8
  },
  btnLeft: {
    flex: 1,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'flex-end'
  }
});
