import React, { Component } from 'react';
const PropTypes = require('prop-types');
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  StatusBar,
  Platform,
  Modal,
  TextInput,
  ListView,
  Image,
  FlatList,
  Dimensions,
  View } from 'react-native';

  // API
  import Api from  '../../../lib/http_request/';
  import * as Routes from '../../../app/routes'
  const { width,height} = Dimensions.get('window');

  import {getIconDog,getIconCat,getIconRabbit,getIconHamster,getIconHamsterPress,getIconMiniPig}  from '../../../assets/image/svg';


  widthImage=27;
  heightImage=27;

  var petTypeList=[
    {id:"DOG",name:"Perro",isSelected:0,image:getIconDog(widthImage,heightImage)},
    {id:"CAT",name:"Gato",isSelected:0,image:getIconCat(widthImage,heightImage)},
    {id:"RABBIT",name:"Conejo",isSelected:0,image:getIconRabbit(widthImage,heightImage)},
    {id:"HAMSTER",name:"Hamster",isSelected:0,image:getIconHamster(widthImage,heightImage)},
    {id:'MINIPIG',name:"Mini pig",isSelected:0,image:getIconMiniPig(widthImage,heightImage)}
  ];


  export default class PetTypeListView extends React.Component {

   constructor(props) {
     super(props);
     this.state={
       textValueSearch:"",
       petTypeList,
     }
   }




    render(){
      return (
           <FlatList
             contentContainerStyle={{justifyContent: 'space-between'}}
             style={{marginTop: 10}}
             data={this.state.petTypeList}
             horizontal={true}
             keyExtractor={this._keyExtractor}
             renderItem={this.renderRow}
           />
      );
    }


  _keyExtractor = (item, index) => item.id;
    renderRow=(object)=>{

      item=object.item;
      var image=item.image;
      boxImagePress={};
      if(item.isSelected){
          boxImagePress={borderWidth:2,borderColor:"#15d2bb"};
      }

      marginHorizontalBox=48;
      if(this.props.marginBoxHorizontal!=undefined){
       marginHorizontalBox=this.props.marginBoxHorizontal* 2;
      }

      return <TouchableOpacity onPress={()=>this.onPressPetType(object)} style={[styles.buttonPetTypeItem,{ 
        width:(width- marginHorizontalBox)/5 ,
        height:(width- marginHorizontalBox)/5+10 ,}]}>
                <View style={[styles.boxImage,boxImagePress]}>
                       {image}
                </View>
               <Text style={{fontSize: 14,marginTop: 2,color: 'black'}}>{item.name}</Text>
            </TouchableOpacity>
    }


    onPressPetType=(object)=>{
      item=object.item;
      var petTypeList=this.state.petTypeList;
      for(i=0;i<petTypeList.length;i++){
        if(item.name==petTypeList[i].name){
           petTypeList[i].isSelected=true;
        }else{
          petTypeList[i].isSelected=false;
        }
      }
      this.setState({petTypeList,petTypeSelected:item});
      this.props.petTypeSelected(item);
    }


    componentDidMount(){
     let petType="NA";
      if(this.props.profile!=undefined){
        petType=this.props.profile.pet_type;
      }
    
      let petTypeList=this.state.petTypeList;
      
      for(i=0;i<petTypeList.length;i++){
        if(petType==petTypeList[i].id){

           petTypeList[i].isSelected=true;
           this.props.petTypeSelected(petTypeList[i]);
           break;
        }
      }
      this.setState({petTypeList});
    }

  }



  const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:'white',
    },

    textSearch:{
      height:40,
      fontSize:18,
      borderBottomWidth: 1,
    },
    buttonPetTypeItem:{
      alignItems: 'center',
      borderColor: '#00B99B',
    },
    boxImage:{
      width: 48,
      borderRadius: 8,
      height: 48,
      justifyContent: 'center',
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: { width: 1, height: 2 },
      shadowOpacity: 0.1,
      shadowRadius: 8,
      elevation: 1,
    }
  });
