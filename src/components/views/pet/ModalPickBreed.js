import React, { Component } from 'react';
const PropTypes = require('prop-types');
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  StatusBar,
  Platform,
  Modal,
  TextInput,
  ListView,
  Image,
  FlatList,
  Dimensions,
  RefreshControl,
  View } from 'react-native';

  // API
  import Api from  '../../../lib/http_request/';
  import * as Routes from '../../../app/routes'
  const { width,height} = Dimensions.get('window');

  var params;

  export default class ModalPickBreeed extends React.Component {

   constructor(props) {
     super(props);
     params=props.navigation.state.params;
     this.state={
       textValueSearch:"",
       breeds:[],
     }
   }

   static navigationOptions = {
     headerTransparent:true,
     headerTitle: <Text style={{fontSize: 20,fontWeight: 'bold',color: 'black'}}>Seleccionar raza</Text>,
   };




    render(){
      return (
         <View style={styles.container}>

             <View style={{marginLeft: 24,marginRight: 24,marginTop: 60,marginBottom: 5,height:60,justifyContent:'center',}}>

                 <TextInput
                  ref={(input) => { this.breed = input; }}
                  placeholderTextColor="grey"
                  placeholder={"De qué raza es tu "+params.petType.name+"?"}
                  style={styles.textSearch}
                  returnKeyType={'search'}
                  underlineColorAndroid={"white"}
                  editable = {true}
                  value={this.state.textValueSearch}
                  onChangeText={(text)=>this.manageSearch(text)}
                  />


              </View>



              {this.getViewPetTypeList()}


         </View>
      );
    }

    _keyExtractor = (item, index) => item.breed_id;
     getViewPetTypeList(){
        return  <FlatList
                   style={{margin: 24,marginTop: 5}}
                   data={this.state.breeds}
                   refreshControl={
                     <RefreshControl
                         refreshing={this.state.isLoading}
                         onRefresh={()=>this.fetchBreeds("null",params.petType.id)}
                         title="Cargando..."
                         colors={["#15d2bb","#15d2bb","#15d2bb"]}
                         tintColor="#15d2bb"
                         titleColor="#15d2bb"
                      />
                   }
                   ListEmptyComponent={this.renderHeader}
                   keyExtractor={this._keyExtractor}
                   ItemSeparatorComponent={()=> <View
                          style={{
                            height: 1,
                            width: "100%",
                            backgroundColor: "#CED0CE",
                          }}
                        />}
                   renderItem={this.renderRow}
                />
  }

    renderRow=(object)=>{
       breed=object.item;
          return(
               <TouchableOpacity  onPress={()=>this.onBreedSelected(object) } style={{height:40,justifyContent: 'center',}}>
                    <View style={{flexDirection:'row',justifyContent: 'space-between',alignItems: 'center',paddingTop: 5,paddingBottom: 5,}}>
                            <Text allowFontScaling={false} style={{color:'black',fontSize:14}}>
                               {breed.breed_name}
                            </Text>
                         <View style={{borderWidth: 1,backgroundColor: 'white',width: 16,height: 16,borderRadius: 12}}/>
                   </View>
               </TouchableOpacity>
          );
    }


    renderHeader= () => {
      if(!this.state.isLoading){
          return (
              <View   style={{justifyContent: 'center',alignItems: 'center',paddingVertical: 5}}  >
                 <Image  style={{width:width/2.5,height: width/2.5,marginTop: 10}} source={require('../../../assets/image/general/empty.png')}/>
                 <Text style={{fontSize: 14,marginLeft: 50,marginRight: 50,color: 'black',marginTop: 5,textAlign: 'center'}}>{this.state.message} </Text>
              </View>
            );
      }else{
        return null;
      }
 }

    onBreedSelected(object){
      params.breedSelectedCallBack(object.item);
      this.props.navigation.goBack();


    }




    manageSearch(textValueSearch){
      this.setState({textValueSearch,isLoading:true});
      this.fetchBreeds(textValueSearch,params.petType.id);

    }

    fetchBreeds(value,petType){
      console.warn();
      if(value.length==0){
          value="null";
      }

      var url=Routes.URL_GET_SEARCH_BREEDS+value+'/'+petType;
        Api.get(url)
           .then((response)=>{
             const {status,data,message}=response;
              if(status=='empty'){
                   this.setState({
                     message,
                   });
               }

             this.setState({
                isLoading:false,
                  breeds:data,
             });

           }).catch((ex)=>{
              console.warn(ex);
           });

    }

    componentDidMount(){
      this.breed.focus();

      this.fetchBreeds("null",params.petType.id);
      this.setState({isLoading:true});

    }


  }



  const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:'white',
    },

    textSearch:{
      height:40,
      fontSize:18,
      padding:1 ,
      borderBottomWidth: 1,
    },
    buttonContinue:{
      justifyContent: 'center',
      alignItems: 'center',
      height: 36,
      marginTop: 40,
      backgroundColor: '#00B99B',
      borderRadius: 8
    },
  });
