import React, { Component } from 'react';
const PropTypes = require('prop-types');
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  Animated,
  View } from 'react-native';

  import ImagePicker from 'react-native-image-crop-picker';

  // API
  import Api from  '../../../lib/http_request/';
  import * as Routes from '../../../app/routes'
  const { width,height} = Dimensions.get('window');

  import {getIconCameraBlack,getIconCamera,getIconGallery,getIconAdd} from '../../../assets/image/svg'

  var widthView=width-48;
  var btnPickPositionRight=(widthView/2)-74;
  var btnPickPositionBottom=14;
  var increase=70;

  var pickerImageConfig={
       width: 600,
       height: 400,
       cropping: true,
       includeBase64:true,
       compressImageQuality:.8,
       multiple:false,
  }

  export default class PickImage extends React.Component {

   constructor(props) {
     super(props);
     console.warn(props);
     this.state={
       animationPickImageGallery:new Animated.Value(btnPickPositionRight),
       animationPickImageCamera:new Animated.Value(btnPickPositionBottom),
       animationOpacity:new Animated.Value(0),
       animationRotate:new Animated.Value(0),
       isOpenPickImage:false,
       petImage:(this.props.image!=undefined)?this.props.image:null,
     }
     pickerImageConfig={
       ...pickerImageConfig,
       ...this.props.pickerImageConfig
     }
   }




    render(){

      const animatedGallery={
        right:this.state.animationPickImageGallery,
        opacity:this.state.animationOpacity
      }

      const animatedCamera={
        bottom:this.state.animationPickImageCamera,
        opacity:this.state.animationOpacity
      }

      const rotation = this.state.animationRotate.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '45deg']
    });


      return (
             <View style={this.props.styleContainer}>
                      <View style={{justifyContent: 'flex-start',alignItems: 'center',height: 144}}>

                              {(this.state.petImage==null)?
                                <TouchableOpacity style={styles.imageButton}>
                                    {getIconCameraBlack()}
                                </TouchableOpacity>:
                               <Image style={styles.imageButton} source={this.state.petImage}/>
                                }
                              <Animated.View    style={[styles.buttonPickImage,animatedCamera]}>
                                     <TouchableOpacity onPress={this.takePhoto} style={{width: 48,height: 48,borderRadius: 24,justifyContent: 'center',alignItems: 'center'}}>
                                        {getIconCamera()}
                                    </TouchableOpacity>
                             </Animated.View>

                             <Animated.View  style={[styles.buttonPickImage,animatedGallery]}>
                                   <TouchableOpacity  onPress={this.pickGallery} style={{width: 48,height: 48,borderRadius: 24,justifyContent: 'center',alignItems: 'center'}}>
                                        {getIconGallery()}
                                      </TouchableOpacity>
                             </Animated.View>

                             <Animated.View  style={[styles.buttonPickImage,{transform: [{rotate: rotation}] }]}>
                               <TouchableOpacity  onPress={this.openPickImage} style={{width: 48,height: 48,borderRadius: 24,justifyContent: 'center',alignItems: 'center'}}>
                                  {getIconAdd()}
                               </TouchableOpacity>
                            </Animated.View>

                      </View>


                </View>
      );
    }


    takePhoto=()=>{
      ImagePicker.openCamera(pickerImageConfig).then(this.manageImagePicked);
    }

    pickGallery=()=>{

      ImagePicker.openPicker(pickerImageConfig).then(this.manageImagePicked);
    }

    manageImagePicked=(image)=>{
      this.openPickImage();
      var imageBase64='data:image/png;base64,'+image.data;
      var image= {imageView:{uri:imageBase64},imageSelected:imageBase64}
      this.setState({petImage:image.imageView});
       setTimeout(()=>{
        this.props.onImageSelected(image);
      },200);


    }

    openPickImage=()=>{
      isOpenPickImage= this.state.isOpenPickImage;
      if(!isOpenPickImage){
        isOpenPickImage=true;
      }else{
        isOpenPickImage=false;
      }
         this.setState({isOpenPickImage})
         this.startAnimation(isOpenPickImage)
    }



    startAnimation=(state)=>{
          valuePickGallery=btnPickPositionRight;
          valuePickCamera= btnPickPositionBottom;
          console.warn();
          opacity=0;
          rotate=0;
          if(state){
             valuePickGallery=btnPickPositionRight+increase;
             valuePickCamera=btnPickPositionBottom+increase;
             opacity=1;
             rotate=1;
          }
          Animated.parallel([
            Animated.timing(this.state.animationRotate,{
              toValue: rotate,
              duration: 200
            }),
            Animated.timing(this.state.animationPickImageCamera,{
            toValue:valuePickCamera,
            duration:300,
          }),
          Animated.timing(this.state.animationPickImageGallery,{
               toValue:valuePickGallery,
               duration:300,
             }),
           Animated.timing(this.state.animationOpacity,{
             toValue: opacity,
             duration:500,
           }),

           ]).start()
   }







  }



  const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:'white',
    },
     imageButton:{
      backgroundColor: '#EDEDED',
      width: 120,
      height: 120,
      borderRadius: 60,
      justifyContent: 'center',
      alignItems: 'center',
    },
    buttonPickImage:{
      position: 'absolute',
      bottom: 14,
      right: btnPickPositionRight,
      backgroundColor: '#00B99B',
      width: 48,
      height: 48,
      borderRadius: 24,
      shadowColor: '#000',
      shadowOffset: { width: 1, height: 2 },
      shadowOpacity: 0.1,
      shadowRadius: 8,
      elevation: 2,
    },

  });
