import React, { Component } from 'react';
import {

  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,

} from 'react-native';
import { CachedImage } from "react-native-img-cache";
const { width } = Dimensions.get('window');
import { getUrlImageCompress } from '../../../lib/image'
import { getMaleGender, getFemaleGender } from '../../../assets/image/svg'
import ButtonFollower from '../../views/profile/ButtonFollower'

export default class ListRow extends React.Component {

  constructor(props) {
    super(props);
  }


  render() {
    let params = this.props.navigation.state.params;
    let profileTo = this.props.profile;
    let profileFromId=params.profileFromId || this.props.profileFromId;
    let placUser = profileTo.plac_user;

    return <TouchableOpacity disabled={(params.profileFromId == profileTo.profile_id) ? true : false} onPress={() => this.goProfile(profileTo)} style={{ height: 96, marginLeft: 15, marginRight: 15, flexDirection: 'row' }}>
      {this.getViewProfileImage(profileTo, placUser)}
      {this.getProfileInfo(profileTo)}
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
        <ButtonFollower customStyle={{ width: "80%" }} profileFromId={profileFromId} profileToId={profileTo.profile_id} isFollowing={this.props.isFollowing} />
      </View>
    </TouchableOpacity>
  }




  getViewProfileImage(profileTo, placUser) {

    return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      {(profileTo.profile_type == 'pet') ?
        <View style={{ height: 96, width: 96, alignItems: 'flex-start' }}>
          {(placUser.plac_user_image) ? <CachedImage resizeMethod="resize" style={{ height: 48, width: 48, borderRadius: 24, position: 'absolute', borderRadius: 24, left: 0, top: (96 / 2) - 24 }} source={{ uri: getUrlImageCompress(placUser.plac_user_image, 20) }} /> : null}
          {(profileTo.profile_image) ? <CachedImage resizeMethod="resize" style={{ height: 48, width: 48, borderRadius: 24, position: 'absolute', borderRadius: 24, right: 20, top: (96 / 2) - 24 }} source={{ uri: getUrlImageCompress(profileTo.profile_image, 20) }} /> : null}
        </View> :
        (profileTo.profile_image) ? <CachedImage resizeMethod="resize" style={{ height: 48, width: 48, borderRadius: 24, borderRadius: 24 }} source={{ uri: getUrlImageCompress(profileTo.profile_image, 20) }} /> : null
      }
    </View>
  }

  getProfileInfo(profileTo) {
    placUser = profileTo.plac_user;
    if (profileTo.profile_type == 'pet') {
      return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text ellipsizeMode={'tail'} numberOfLines={1} style={{ color: 'black', fontSize: 14, marginRight: 2, }}>{profileTo.profile_name}</Text>
          {this.getPetGender(profileTo)}
        </View>
        <Text ellipsizeMode={'tail'} numberOfLines={1} style={{ color: '#757575', fontSize: 12 }}>{profileTo.breed.breed_name}</Text>
        <View style={{ height: 1, backgroundColor: '#a6a6a6', marginTop: 1, marginBottom: 1, width: '60%' }} />
        <Text ellipsizeMode={'tail'} numberOfLines={1} style={{ textAlign: 'center', color: 'black', fontSize: 12 }}>{placUser.plac_user_name}</Text>
      </View>
    } else {
      return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text ellipsizeMode={'tail'} numberOfLines={1} style={{ color: 'black', fontSize: 14 }}>{profileTo.profile_name}</Text>
        <Text ellipsizeMode={'tail'} numberOfLines={1} style={{ color: '#757575', fontSize: 12 }}>Tienda</Text>
      </View>

    }
  }

  getPetGender(profileTo) {
    petGender = profileTo.pet_gender;
    if (petGender) {
      if (petGender == "MALE") {
        return getMaleGender(12, 12);
      } else {
        return getFemaleGender(12, 12);
      }
    }
  }




  goProfile(profile) {
    console.warn("going to profile");

    params.profileTo = profile;
    if (profile.profile_id != params.profileFromId) {
      this.props.navigation.push('ProfileInvite', params);
    } else {
      this.props.navigation.push('ProfileMe', params);
    }
  }



}




const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
  },

});
