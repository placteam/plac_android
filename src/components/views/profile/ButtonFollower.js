import React, { Component } from 'react';
import {
   AppRegistry,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  RefreshControl,
  Dimensions,
  FlatList,
} from 'react-native';

// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'



const { width,height} = Dimensions.get('window');
var params;
var post;

export default class ButtonFollower extends React.Component{



   constructor(props){
     super(props)
     this.state={
       isFollowing:this.props.isFollowing,
       isLoading:false,
     }
   }




  render() {
    buttonStyleNoFollowing={};
    textStyleNoFollowing={};
    textFollowing="Seguir";
    if(this.state.isFollowing){
      textFollowing="Siguiendo";
      buttonStyleNoFollowing={borderColor:"grey",backgroundColor:'white'};
      textStyleNoFollowing={color:'grey'};
    }

      if(this.props.profileFromId!=this.props.profileToId){
        return (<TouchableOpacity  disabled={this.state.isLoading}  onPress={()=>this.manageFollowing()}  style={[styles.buttonFollowing,buttonStyleNoFollowing,this.props.customStyle]}>
                      
                  {(this.state.isLoading)?<Image style={{width:30,height:11}} source={require('../../../assets/image/general/loader.gif')}/>:<Text style={[styles.textFollowing,textStyleNoFollowing,this.props.customTextStyle]}>{textFollowing}</Text>}
               </TouchableOpacity>)
      }else{
        return null;
      }
    }




  manageFollowing(){
    this.setState({
      isLoading:true,
    });
    url=Routes.URL_PROFILE_FOLLOWING_MANAGE;
    var body={
      profile_from_id:this.props.profileFromId,
      profile_to_id:this.props.profileToId,
    }
    Api.post(url,body)
       .then((response)=>{
         console.warn(response);
         
         var {status,data,message} =response;
          isFollowing=this.state.isFollowing;
         if(message=='follower_added'){
           this.manageFollower("add");
           isFollowing=1;   
         }else{
         this.manageFollower("minus");
           isFollowing=0;
         }
         setTimeout(()=>{
          this.setState({isFollowing,isLoading:false});
          if(this.props.onFollowProfile!=undefined){
            this.props.onFollowProfile(this.props.profileToId);
          }
         
         },1000);
       
       }).catch((ex)=>{
          console.warn(ex);
       });
  }

  manageFollower(action){
    if(this.props.manageFollower!=undefined){
      this.props.manageFollower(action);
    }

  }





}



const styles = StyleSheet.create({
  buttonFollowing:{
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    height: 36,
    backgroundColor:'#00B99B' ,
    width:"50%",
    flexDirection:'row',
    borderColor: '#00B99B',
    borderRadius: 8,
  },
  textFollowing:{
    fontSize: 14,
    color:'white'
  }

});
