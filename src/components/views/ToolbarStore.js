import React from 'react';

import {
  StyleSheet,
  Text,
  Dimensions,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Picker,
} from 'react-native';

import *  as ProfileController from '../../app/controllers/ProfileController'
import * as PlacUserController from '../../app/controllers/PlacUserController'


import {getIconPlacHome, getIconMenu, getIconCart} from '../../assets/image/svg'
import icons from '../../assets/image/tabs';
import { getQuantityProductsInOrder } from '../../app/controllers/OrderDetailController';





export default class ToolbarStore extends React.Component {

  constructor(props){
     super(props)     
   }



  render() {
    var placUser=this.props.placUser;
 


    return     <View style={{flexDirection: 'row',backgroundColor: 'white',height: 50,width: "100%",}}>
                    <TouchableOpacity  onPress={()=>this.props.navigation.toggleDrawer()} style={{marginLeft:10,flex:1,justifyContent: 'center',}}>
                    { getIconMenu()}
                    </TouchableOpacity>
                    <TouchableOpacity disabled={true} onPress={()=>this.props.onPressMiddle()} style={[{flex:1,justifyContent: 'center',alignItems: 'center'},(this.props.viewMiddleStyle!=undefined)?this.props.viewMiddleStyle:{}]}>
                      {(this.props.viewMiddle==undefined)?getIconPlacHome():this.props.viewMiddle}
                    </TouchableOpacity>
                    <View style={{flex:1,justifyContent: 'center',alignItems:'flex-end'}}>
                   
                    
                    </View>
              </View>
  }










}

const styles = StyleSheet.create({
  buttonContinue:{
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
    backgroundColor: '#00B99B',
    borderRadius: 8
  },

});
