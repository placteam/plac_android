import React from 'react';
import DatePickerComponent from 'react-native-datepicker'
import {
  StyleSheet,
  Text,
  Dimensions,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';


export default class ButtonContinue extends React.Component {

  constructor(props){
     super(props)
   }



  render() {
    var  buttonStyleNotPress={};
     if(!this.props.isValidData){
       buttonStyleNotPress.backgroundColor="#ededed";
     }

    return   <TouchableOpacity  onPress={this.onPress} disabled={!this.props.isValidData} style={[styles.buttonContinue,this.props.customStyle,buttonStyleNotPress]}>
               <Text style={[{color: 'white',fontSize: 16},this.props.customStyleText]}>{this.props.text}</Text>
            </TouchableOpacity>;
  }

  onPress=()=>{
    this.props.onPress();
  }



}

const styles = StyleSheet.create({
  buttonContinue:{
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
    backgroundColor: '#00B99B',
    borderRadius: 8
  },

});
