import React from 'react';
import DatePickerComponent from 'react-native-datepicker'
import {
  StyleSheet,
  RefreshControl,
  FlatList,
  View,
  Text,
  Image,
} from 'react-native';



export default class ListViewPagination extends React.Component {

  constructor(props){
     super(props);
     this.state={
        url_request:this.props.urlRequest,
        paginate:null,
        arrayUrls:[],
        loadMore:false,
        isLoading:true,
     }


   }


 
  render() {
    return    <View   style={[{flex:1}]}>
                          <FlatList 
                                    style={this.props.style}
                                    data={this.props.data}
                                    removeClippedSubviews={true}
                                    showsVerticalScrollIndicator={false}
                                    refreshControl={
                                    <RefreshControl
                                        refreshing={this.props.isLoading}
                                        onRefresh={()=>this.onRefresh()}
                                        title="Cargando..."
                                        colors={["#15d2bb","#15d2bb","#15d2bb"]}
                                        tintColor="#15d2bb"
                                        titleColor="#15d2bb"
                                    />
                                    }

                                    numColumns={this.props.numColumns}
                                    initialNumToRender={this.props.initialNumToRender}
                                    ListHeaderComponent={this.props.ListHeaderComponent}
                                    ListFooterComponent={()=>this.footerComponent()}
                                    ListEmptyComponent={()=>this.postsEmpty()}
                                    onEndReached={()=>this.handleLoadMore()}
                                    ItemSeparatorComponent={this.ItemSeparatorComponent}
                                    onEndReachedThreshold={1}
                                    keyExtractor={this.props.keyExtractor}
                                    renderItem={this.props.renderRow}
                                />
           </View>
  }

  ItemSeparatorComponent=()=>{
    if(this.props.isSeparatorActive==undefined){
      return <View style={{height:.5,width:'100%',backgroundColor:'#a6a6a6'}}/>
    }
    return null;
    
  }

  onRefresh(){
      this.props.onRefresh();
  }

  postsEmpty(){
    return (!this.props.isLoading)?<View style={{justifyContent: 'center',alignItems: 'center',flex:1}}>
                                      
                                  </View>:null
  }

  footerComponent(){
    return (this.props.loadMore)?<View style={{justifyContent: 'center',height: 70,alignItems: 'center'}}>
                                 <Image style={{width: 60,height: 23}} source={require('../../assets/image/general/loader.gif')}/>
                               </View>:null
  }

  handleLoadMore () {
    let paginate=  this.props.paginate;
    let nextPageURL=paginate.next_page_url;

    if(paginate.current_page!=paginate.last_page){
        if(nextPageURL){
          let nextPage=nextPageURL.split("?");
          let arrayUrls=this.props.arrayUrls;
              if(arrayUrls.indexOf(nextPageURL)==-1){
                 arrayUrls.push(nextPageURL);
                 let np="?"+nextPage[1];
                 this.props.handleLoadMore(np,arrayUrls);
              }
        }
   }
  }
   

   componentWillReceiveProps(nextProps, nextContext){
  

   }

 





}

const styles = StyleSheet.create({
  buttonContinue:{
    justifyContent: 'center',
    alignItems: 'center',
    height: 36,
    backgroundColor: '#00B99B',
    borderRadius: 8
  },

});
