const React = require('react');
const { ViewPropTypes } = ReactNative = require('react-native');
const PropTypes = require('prop-types');
const createReactClass = require('create-react-class');
const {
  StyleSheet,
  Text,
  View,
  Image,
  Animated,
} = ReactNative;
const Button = require('./Button');
import icons from '../../../image/tabs';

const TabIconText =createReactClass({
  propTypes: {
    goToPage: PropTypes.func,
    activeTab: PropTypes.number,
    tabs: PropTypes.array,
    backgroundColor: PropTypes.string,
    activeTextColor: PropTypes.string,
    inactiveTextColor: PropTypes.string,
    textStyle: Text.propTypes.style,
    tabStyle: ViewPropTypes.style,
    renderTab: PropTypes.func,
    underlineStyle: ViewPropTypes.style,
  },

  getDefaultProps() {
    return {
      activeTextColor: 'navy',
      inactiveTextColor: 'black',
      backgroundColor: null,
    };
  },

  renderTabOption(name, page) {
  },

  renderTab(name, page, isTabActive, onPressHandler) {
    const { activeTextColor, inactiveTextColor, textStyle, } = this.props;
    viewAdditional=null;

    var icon;
    switch (name) {
      case "Publicaciones":

        if(!isTabActive){
             icon=icons.getHomeIcon();
        }else{
             icon=icons.getHomeIconPress();
        }

      break;
      case "Notifications":
        if(!isTabActive){
        icon=icons.getNotificationsIcon();
        }else{
           icon=icons.getNotificationsIconPress();
        }
      break;
      case "Lugares":
        if(!isTabActive){
        icon=icons.getPlacesIcon();
        }else{
          icon=icons.getPlacesIconPress();
        }
      break;
      case "Tienda":
        if(!isTabActive){
          icon=icons.getStoreIcon();
        }else{
              icon=icons.getStoreIconPress();
        }
      break;
      case "TiendaNotification":
          if(!isTabActive){
            icon=icons.getStoreIcon();
          }else{
                icon=icons.getStoreIconPress();
          }
          viewAdditional= (<View style={styles.notification}>
                            <Text style={{fontSize:10,color:'white'}}>1</Text>
                           </View>);


      break;
      case "Perfil":
        if(!isTabActive){
        icon=icons.getProfileIcon();
        }else{
        icon=icons.getProfileIconPress();
        }

      break;


    }


    return <Button
      style={{flex: 1, }}
      key={name}
      accessible={true}
      accessibilityLabel={name}
      accessibilityTraits='button'
      onPress={() => onPressHandler(page)}>
      <View style={[styles.tab, this.props.tabStyle, ]}>


        <View style={{flex:2,flexDirection:"column",justifyContent:"center",}}>
          <Image  source={{uri:icon,width:45,height:45}} />
        </View>

        {viewAdditional}


      </View>


    </Button>;
  },

  render() {
    const containerWidth = this.props.containerWidth;
    const numberOfTabs = this.props.tabs.length;
    const tabUnderlineStyle = {
      position: 'absolute',
      width: containerWidth / numberOfTabs,
      height: 4,
      backgroundColor: 'navy',
      bottom: 0,
    };


    const translateX = this.props.scrollValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0,  containerWidth / numberOfTabs],
    });
    var widthTab={};
    if(this.props.heightTab==0){
       widthTab={width:0,}
    }
    return (
      <View style={[styles.tabs, {backgroundColor: this.props.backgroundColor,height:this.props.heightTab},widthTab, this.props.style, ]}>

        {this.props.tabs.map((name, page) => {
          const isTabActive = this.props.activeTab === page;
          const renderTab = this.props.renderTab || this.renderTab;
          return renderTab(name, page, isTabActive, this.props.goToPage);
        })}

        <Animated.View
          style={[
            tabUnderlineStyle,
            {
              transform: [
                { translateX },
              ]
            },
            this.props.underlineStyle,
            {backgroundColor:"#15d2bb",height:1,}
          ]}
        />
      </View>
    );
  },
});

const styles = StyleSheet.create({
  tab: {
    flex: 2,
    flexDirection:"column",
    backgroundColor:"#fff",
    justifyContent:"center",
    alignItems:"center",
    borderWidth: 0,
    borderColor: "rgba(113,113,113,.1)",
  },
  tabs: {
    flexDirection: 'row',
    borderWidth: 1,
    borderTopWidth: 1,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderColor: 'rgba(113,113,113,.1)'
  },
  notification:{
    alignItems:"center",
    right:5,top:5,
    position: 'absolute',
    width:15,height:15,
    borderRadius:7.5,
    borderWidth:1,
    borderColor:'#fb5b58',
    backgroundColor:'#fb5b58'
 }
});

module.exports = TabIconText;
