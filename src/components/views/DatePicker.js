
import React from 'react';
import DatePickerComponent from 'react-native-datepicker'
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  Button,
  StyleSheet,
  Text,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import {getCurrentDate} from '../../lib/date'
export default class DatePicker extends React.Component {

  constructor(props){
     super(props)
     this.state = {date:getCurrentDate()}
   }



  render() {

    return   <DatePickerComponent
             ref={(input) => { this.datePicker = input; }}

        date={this.state.date}
        mode="date"
        androidMode={"spinner"}
        placeholder="select date"
        format="YYYY-MM-DD"
        minDate="1990-05-01"
        maxDate={getCurrentDate()}
        confirmBtnText="Confirmar"
        cancelBtnText="Cancelar"
        customStyles={{

        }}
        TouchableComponent={()=>{return <TouchableOpacity onPress={()=>this.datePicker.onPressDate()}>{this.props.customComponent}</TouchableOpacity>}}
        //TouchableComponent={()=>{return <TouchableOpacity onPress={()=>this.datePicker.onPressDate()}><Text>{this.state.date}</Text></TouchableOpacity>}}
        showIcon={false}
        onDateChange={this.onDateChange}
      />;
  }


  onDateChange=(date)=>{
    console.warn(date);
    this.props.onDateChange(date);
  }






}
