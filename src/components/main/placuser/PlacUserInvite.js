import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  Dimensions,
  FlatList,
  TouchableOpacity,
} from "react-native";

import ToolbarMain from "../../views/ToolbarMain";
import Button from "../../views/ButtonContinue";

import * as ProfileController from "../../../app/controllers/ProfileController";
import * as PlacUserController from "../../../app/controllers/PlacUserController";
// API
import Api from "../../../lib/http_request/";
import * as Routes from "../../../app/routes";
var { width } = Dimensions.get("window");

var widthViewProfile = width / 3;

export default class PlacUserInvite extends React.Component {

  static navigationOptions = {
    title: "dasds",
    headerTitle: (<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginRight: 40 }}>
      <Text style={{ fontSize: 20, color: 'black' }}>Mi perfil</Text>
    </View>)
  };


  constructor(props) {
    super(props);
    this.state = {
      countFollowers: 0,
      countFollowing: 0,
      postsNumber: 0,
      profiles: [],
      placUser: null,
    };
  }

  _keyExtractor = (item, index) => item.profile_id;
  render() {
    placUser = this.state.placUser;
    if (placUser == null) {
      return null;
    }
    return (
      <View style={styles.container}>

        <View style={{ flex: 1, marginLeft: 15, marginRight: 15 }}>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 20
            }}
          >
            <Image
              style={{ width: 120, height: 120, borderRadius: 60 }}
              source={{ uri: placUser.plac_user_image }}
            />
            <Text style={{ fontSize: 20, color: "black", marginTop: 10 }}>
              {placUser.plac_user_name}
            </Text>
            <View style={{ flexDirection: "row", marginTop: 10 }}>
              <View style={{ flex: 1 }} />
              <View style={{ flex: 1.5,justifyContent:'center',alignItems:'center'}}>
                <Text style={{ fontSize: 12, color: "black" }}>
                {this.getCity(placUser)}
               
                </Text>
                <Text style={{ fontSize: 12, color: "black" }}>
                  {placUser.plac_user_ }
                </Text>
              </View>

              <View style={{ flex: 1 }} />


            </View>
          </View>
          <FlatList
            style={{ marginTop: 20, flex: 1 }}
            contentContainerStyle={{ justifyContent: "center", flex: 1 }}
            data={this.state.profiles}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            keyExtractor={this._keyExtractor}
            renderItem={this.renderRow}
          />
        </View>
      </View>
    );
  }

  getCity(placUser){
     cityName="";
     if(placUser.city){
       cityName= placUser.city.city_name;
     }
     return cityName;        
  }

  renderRow = object => {
    profile = object.item;
    return (
      <TouchableOpacity onPress={()=>this.goProfile(object)} style={{ width: widthViewProfile, alignItems: "center" }}>
        <Image
          source={{ uri: profile.profile_image }}
          style={{ width: 56, height: 56, borderRadius: 28 }}
        />
        <Text style={{ fontSize: 16, color: "black" }}>
          {profile.profile_name}
        </Text>
        <Text style={{ fontSize: 14, color: "#717171" }}>
          @{profile.profile_unique_name}
        </Text>
        <View
          style={{
            padding: 10,
            marginTop: 10,
            backgroundColor: "#ededed",
            borderRadius: 20,
            height: 36,
            borderWidth: 0,
            borderColor: "#d1d1d1",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text style={{ fontSize: 14 }}>
            historias
            <Text style={{ fontWeight: "bold" }}> {profile.post_count}</Text>
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  goProfile(profile){
    console.warn(profile.item.profile_id);
    
    params={
      profileTo:{
      ...profile.item
    }}
    this.props.navigation.push('ProfileInvite',params);
  }

  getProfilesByUser(plac) {
    url = Routes.URL_PROFILES_BYUSER + this.state.placUser.plac_user_id;
    console.warn(url);

    Api.get(url)
      .then(response => {
        var { data, status } = response;
        this.setState({ profiles: data });
      })
      .catch(ex => {
        console.warn(ex);
      });
  }

  componentDidMount() {
    placUser = this.props.navigation.state.params.placUser;
    this.setState({
      placUser
    }, () => {
      this.getProfilesByUser();
    });

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  imageProfile: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 20
  },
  viewProfileName: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonEditProfile: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#00b99b",
    width: "80%"
  },
  textEditProfile: {
    color: "#00b99b",
    fontSize: 14
  },
  viewInfoAboutProfile: {
    height: 36,
    flexDirection: "row",
    borderRadius: 20,
    justifyContent: "space-around",
    marginTop: 20,
    backgroundColor: "#ededed"
  },
  textInfo: {
    fontSize: 14,
    color: "#555555"
  },
  viewInfoBox: {
    justifyContent: "center",
    alignItems: "center"
  }
});
