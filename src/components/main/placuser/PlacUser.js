import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  Dimensions,
  FlatList,
  TextInput,
  ScrollView,
  Picker,
 TouchableOpacity,
 ToastAndroid,
} from "react-native";

import * as PlacUserController from "../../../app/controllers/PlacUserController";
import  PickImage from '../../views/pet/PickImage'

// API
import Api from "../../../lib/http_request/";
import * as Routes from "../../../app/routes";
var { width } = Dimensions.get("window");

export default class PlacUserMe extends React.Component {

  static navigationOptions = ({ navigation, navigationOptions }) => {
    
    params=navigation.state.params;
    return {
          headerRight:params.viewRight,
          headerTitle:(<View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:20,color:'black'}}>Editar usuario</Text>
                        </View>)
        }
    };

    



      
     

  constructor(props) {
    super(props);
    let placUser = PlacUserController.getCurrentUser();
    this.state = {
      ...placUser,
      isLoading:false,
      plac_user_image_new:"empty",
      cities:[],
    };
  }

  _keyExtractor = (item, index) => item.profile_id;
  render() {
    profile = this.state.profile;
    return (
      <View style={styles.container}>
     
        <ScrollView style={{ flex: 1, marginLeft: 24, marginRight: 24,marginBottom:20,}}>
        
             <PickImage    pickerImageConfig={{ width: 600, height: 600 }} onImageSelected={this.onImageSelected} image={{uri:this.state.plac_user_image}} styleContainer={{marginTop:20}}/>
       
             <View  style={{marginTop:20}}>
                    <Text style={styles.textLabel}>Nombre</Text>
                        <TextInput
                        ref={(input) => { this.placUserName = input; }}
                        returnKeyType={"next"}
                        onSubmitEditing={()=>this.placUserPhone.focus()}
                        style={styles.textInputData}
                        autoCapitalize = {"words"}
                        onChangeText={(plac_user_name)=>this.setState({plac_user_name,indexError:-1})}
                        placeholder='Ingresa tu nombre'
                        placeholderTextColor="#C0C0C0"
                        value={this.state.plac_user_name}
                    />
                      {this.getViewErrorMessage(0)}

              </View>

                <View  style={{marginTop:20}}>
                    <Text style={styles.textLabel}>Télefono</Text>
                        <TextInput
                        ref={(input) => { this.placUserPhone = input; }}
                        returnKeyType={"next"}
                        onSubmitEditing={()=>{}}
                        style={styles.textInputData}
                        keyboardType={"number-pad"}
                        onChangeText={(plac_user_cellphone)=>this.setState({plac_user_cellphone,indexError:-1})}
                        placeholder='Ingresa tu celular'
                        placeholderTextColor="#C0C0C0"
                        value={this.state.plac_user_cellphone}
                    />
                      {this.getViewErrorMessage(1)}

              </View>

             <View  style={{marginTop:20}}>
                    <Text style={styles.textLabel}>Sexo</Text>
                    <Picker
                        mode={'dropdown'}
                        selectedValue={this.state.plac_user_gender}
                        style={{ height: 50,width:'100%'}}
                        onValueChange={(gender, itemIndex) => this.setState({plac_user_gender:gender})}>
                        <Picker.Item  key={"-1"} label={"Genero"} value={""} />
                        <Picker.Item  key={"Hombre"} label="Hombre" value="male" />
                        <Picker.Item  key={"Mujer"}  label="Mujer" value="female" />
                   </Picker>

                     {this.getViewErrorMessage(2)}
              

              </View>
            
             <View  style={{marginTop:20}}> 
                    <Text style={styles.textLabel}>Ciudad</Text>
                    <Picker
                        mode={'dialog'}
                        selectedValue={this.state.plac_user_city}
                        style={{ height: 50,width:'100%',    borderColor: '#00B99B',
                        borderBottomWidth: 1,}}
                        itemStyle={{ backgroundColor: "grey", color: "blue", fontFamily:"Ebrima", fontSize:17 }}

                        onValueChange={(city, itemIndex) => {
                          console.warn(city);
                          this.setState({plac_user_city:city})
                      
                        }}>
                        <Picker.Item key={"-1"} label={"Ciudad"} value={""} />
                      { this.state.cities.map((city) => {
                            return (<Picker.Item key={city.city_id} label={city.city_name} value={city.city_id} /> )
                            })}
                   </Picker>
                   {this.getViewErrorMessage(3)}
                   {this.getViewErrorMessage(4)}
              </View>
        </ScrollView>
      </View>
    );
  }

  getViewErrorMessage(indexInput){
    indexError= this.state.indexError;
    if(indexInput==indexError){
      return <Text style={[this.state.messageStyle,{marginTop: 10,}]}>{this.state.message}</Text>;
    }
  }

  onImageSelected=(image)=>{
    this.setState({
      plac_user_image:image.imageView,
      plac_user_image_new:image.imageSelected,
    });
  }


  getCities(){
      Api.get("cities")
      .then((response)=>{
          var{data}=response;
          this.setState({cities:data});
      }).catch((ex)=>{
        console.warn(ex);
      });
  }

  update=()=>{
        var {plac_user_name,plac_user_cellphone,plac_user_city,plac_user_gender,plac_user_image,plac_user_image_new}=this.state;
        if(!plac_user_name){
         return  this.setState({indexError:0,message:"Te hace falta el nombre",messageStyle:styles.messageError});
        }

        if(!plac_user_cellphone){
         return  this.setState({indexError:1,message:"Te hace falta el celular",messageStyle:styles.messageError});
        }
        if(plac_user_gender==""){
         return  this.setState({indexError:2,message:"Te hace falta el sexo",messageStyle:styles.messageError});
        }

        if(plac_user_city==''){
          return this.setState({indexError:3,message:"Te hace falta la ciudad",messageStyle:styles.messageError});
        }
      
        if(plac_user_image_new=='empty'){
          plac_user_image='not_modified';
        }else{
          plac_user_image_new="";
          plac_user_image=plac_user_image_new;
        }

        this.showLoading();

        var body={
          _method:'PUT',
          plac_user:{
            ...this.state,
            cities:[],
            plac_user_image,
            plac_user_image_new,
          }
        }
        url=Routes.URL_PLAC_USER_RESOURCE+"/"+this.state.plac_user_id;
        Api.post(url,body)
        .then((response)=>{
          if(response.status=="success"){
              PlacUserController.update(response.data);
              ToastAndroid.show('Usuario actualizado', ToastAndroid.SHORT);
              this.goBack();

          }else{
            this.showSave();
            this.setState({indexError:4,message:"Ha ocurrido el siguiente error:"+ response.message +" .Intenta otra vez",messageStyle:styles.messageError});
          }
      
        }).catch((ex)=>{
          this.showSave();
          this.setState({indexError:4,message:"Ha ocurrido el siguiente error: "+ex+" .Intenta otra vez",messageStyle:styles.messageError});

        });
        
  }


   showLoading(){
      params.viewRight=(<Image style={{width:45,height:17,marginRight:20}} source={require('../../../assets/image/general/loader.gif')} />);
      this.props.navigation.setParams(params);
   }

  showSave(){
      params.viewRight=(<TouchableOpacity onPress={this.update} style={{fontSize:20,color:'#15d2bb',paddingRight:20}}>
                                                  <Text  style={{fontSize:14,color:'#15d2bb'}}>Guardar</Text>
                                              </TouchableOpacity>);
      this.props.navigation.setParams(params);
  }

  goBack(){
       params=this.props.navigation.state.params;
       params.editUserCallBack();
       this.props.navigation.goBack();
  }
  


  componentDidMount(){ 
    this.showSave();
  
    this.getCities();
  }





 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  textLabel:{
    fontWeight: '500',
    color: '#555555',
    fontSize: 12,
  },
  textInputData:{
    height: 40,
    borderColor: '#00B99B',
    borderBottomWidth: 1,
    fontSize: 16,
    color:'#212121',
    padding: 0,
  },
  messageSuccess:{
    fontSize: 12,
    color: 'green',
  },
  messageWarning:{
    fontSize: 12,
    color: '#f0ad4e',
  },
  messageError:{
    fontSize: 12,
    color: '#d2152b',
  }
});
