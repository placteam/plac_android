import React from 'react';
import { Text, View,TouchableOpacity,  StyleSheet,Alert,} from 'react-native';

// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'



import { getUrlImageCompress } from '../../../lib/image';

import {CachedImage}  from "react-native-img-cache";

export default class PostRow extends React.Component {
    constructor(props){
        super(props);

    }

    _keyExtractor = (item) => item.follower_id;
    render() {
        let object=this.props.object;
        let index=object.index;
        let notification=object.item;
        let profileFrom=notification.profile_from;
        let post=notification.post;
        let viewActionType=null;
      
        if(notification.type=="LIKE"){
            viewActionType=<TouchableOpacity onPress={()=>this.goPost(notification)} style={styles.notificationContent}>
                              <Text style={{fontSize:14,color:'black',fontWeight:'bold'}}>{profileFrom.profile_name} <Text style={{fontWeight:'normal'}}>le ha gustado tu publicación</Text>  </Text>
                              <Text style={{fontSize:10}}>{notification.notification_time_ago}</Text>
                          </TouchableOpacity>;
        }else if(notification.type=="COMMENT"){
  
            let postComment=notification.post_comment;    
            let comment=postComment.message;     
            if(comment.length>40){
               comment=comment.substring(0, 40)+'...';
            }
            viewActionType=(<TouchableOpacity onPress={()=>this.goPost(notification)}  style={styles.notificationContent}>
                              <Text style={{fontSize:14,fontWeight:'bold',color:'black'}}>{profileFrom.profile_name} <Text style={{fontWeight:'normal'}}>ha comentado tu publicación</Text>  </Text>
                              <Text style={{fontSize:12,}}>{comment}</Text>
                              <Text style={{fontSize:10}}>{notification.notification_time_ago}</Text>
                          </TouchableOpacity>);
        }
  
      
        let marginBottom=0;
        if(index==(this.props.notificationsLength)-1){
          marginBottom=15
        }

        let styleHighlight={};

        if(!notification.is_read){
            styleHighlight={backgroundColor:'rgba(21, 210, 187, .1)',marginHorizontal:0,paddingHorizontal:15,}
        }
        
        return(<View style={[styles.notificationContainer,styleHighlight,{marginBottom}]}>
                    <TouchableOpacity  onPress={()=>this.goProfile(profileFrom)}  style={{flex:.5,justifyContent:'center',alignItems:'flex-start'}}>
                        <CachedImage  style={{width:48,height:48,borderRadius:24,}} source={{uri:getUrlImageCompress(profileFrom.profile_image,20)  }}/>
                    </TouchableOpacity>
                    {viewActionType}
                  
                    <TouchableOpacity onPress={()=>this.goPost(notification)} style={{flex:.5,justifyContent:'center',alignItems:'flex-end'}}>
                       <CachedImage   style={{width:40,height:40}} source={{uri:post.post_path_image}}/>
                    </TouchableOpacity>
              </View>);
    }


    goProfile(profile){
        let params={
            profileTo:profile,
        }
        this.props.navigation.push('ProfileInvite',params);
        
      }
    
      goPost(notification){
          console.warn(notification);
                  
          if(!notification.is_read){
              this.props.updateNotification(notification);
          }

         let post=notification.post;

         let params={
            notification,
            postId:post.post_id,
            deleteCallBack:this.deleteCallBack

          };
          this.props.navigation.push('PostSingle',params);
      }

   

      

  }


const styles = StyleSheet.create({
    buttonContinue:{
      justifyContent: 'center',
      alignItems: 'center',
      height: 36,
      backgroundColor: '#00B99B',
      borderRadius: 8
    },
    notificationContainer:{
        height:60,
        flexDirection:'row',
        marginTop:10,
        marginHorizontal:15,
        alignItems:'center'
    },
    notificationContent:{
        flex:2,
        paddingHorizontal:5,
        justifyContent:'center',
        alignItems:'flex-start'
    }
  
  });