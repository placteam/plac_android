import React from 'react';
import { Text, View } from 'react-native';


import OneSignal from 'react-native-onesignal';
// API
import Api from '../../../lib/http_request/';
import * as Routes from '../../../app/routes'


//CUSTOM VIEWS
import ToolbarMain from '../../views/ToolbarMain'
import PostRow from './PostRow'
import FollowerRow from './FollowerRow';
import OrderRow from './OrderRow';
import ListViewPagination from '../../views/ListViewPagination';

//CONTROLLERS
import { getCurrentUser } from '../../../app/controllers/PlacUserController';
import { getCurrentProfileSelectedByUser } from '../../../app/controllers/ProfileController';


var placUser = null;
var profile = null;
var params;
export default class Notifications extends React.Component {
  static navigationOptions = {
    header: null
  }

  constructor(props) {
    super(props);
    params = props.navigation.state.params;
    this.state = {
      notifications: [],
      arrayUrls: [],
      isLoading: true,
      loadMore: false,
      paginate: null,
      url_query: Routes.URL_GET_NOTIFICATIONS_GENERAL
    }

    placUser = getCurrentUser();
    profile = getCurrentProfileSelectedByUser(placUser.plac_user_id);

  }
  _keyExtractor = (item) => item.notification_id;
  render() {
    return <View style={{ flex: 1, backgroundColor: 'white' }}>
      <ToolbarMain btnLeftDisabled={true} isDisabled={true} navigation={this.props.navigation} />
      <Text style={{ marginLeft: 15, fontSize: 16, color: 'black', }}>Notificaciones</Text>
      <View style={{ flex: 1, marginTop: 5 }}>
        <ListViewPagination
          initialNumToRender={20}
          data={this.state.notifications}
          isLoading={this.state.isLoading}
          loadMore={this.state.loadMore}
          isSeparatorActive={true}
          paginate={this.state.paginate}
          onRefresh={this.onRefresh}
          keyExtractor={this._keyExtractor}
          renderRow={this.renderItem}
          handleLoadMore={this.handleLoadMore}
          arrayUrls={this.state.arrayUrls}
        />

      </View>
    </View>
  }

  renderItem = (object) => {
    let item = object.item;
    if (item.type == 'LIKE' || item.type == 'COMMENT') {
      return <PostRow updateNotification={this.updateNotification} notificationsLength={this.state.notifications.length} object={object} navigation={this.props.navigation} />
    } else if (item.type == 'NEW_FOLLOWER' || item.type == 'REQUEST_FOLLOWER') {
      return <FollowerRow updateNotification={this.updateNotification} notificationsLength={this.state.notifications.length} profileFrom={profile} object={object} navigation={this.props.navigation} />
    } else if (item.type == "ORDER_DELIVERY_STATUS") {
      return <OrderRow updateNotification={this.updateNotification} object={object} navigation={this.props.navigation} notificationsLength={this.state.notifications.length} />
    }
  }

  onRefresh = () => {
    this.setState({
      url_query: Routes.URL_GET_NOTIFICATIONS_GENERAL,
      isLoading: true,
      notifications: [],
      arrayUrls: [],
    }, () => {
      this.fetchNotifications();
    });
  }

  handleLoadMore = (np, arrayUrls) => {
    this.setState({
      url_query: Routes.URL_GET_NOTIFICATIONS_GENERAL + np,
      loadMore: true,
      arrayUrls,
    }, () => {
      if (params == undefined) {
        this.fetchNotifications();
      }
    });
  }

  updateNotification = (notificationIn) => {
    var body = {
      is_read: 1,
      _method: "PUT"
    }
    OneSignal.clearOneSignalNotifications();

    Api.post(Routes.URL_RESOURCE_NOTIFICATIONS + "/" + notificationIn.notification_id, body)
      .then((response) => {
        let { status, data, message } = response;
        if (status == 'success') {
          let notifications = this.state.notifications;
          for (i = 0; i < notifications.length; i++) {
            let notification = notifications[i];
            if (notification.notification_id == notificationIn.notification_id) {
              notifications[i].is_read = 1;
              this.setState({
                notifications
              });
              break;
            }
          }
        } else {
          Alert.alert('Ha ocurrido un error', " Intenta otra vez por favor." + message);
        }

      }).catch((ex) => {
        Alert.alert('Ha ocurrido un error', " Intenta otra vez por favor. " + ex.message);
      });
  }

  manageNotificationComingPush() {

    if (params != undefined) {
      let notifAdditionalData = params.additionalData;
      let data = notifAdditionalData.data;
      let type = notifAdditionalData.notification_type;

      if (type == 'follower') {
        let params = {
          profileTo: { profile_id: data.profile_from_id }
        }

        this.props.navigation.push('ProfileInvite', params);
      }

    }
  }

  fetchNotifications() {
    var body = {
      plac_user_id: placUser.plac_user_id,
      profile_id: profile.profile_id,
    }
    Api.post(this.state.url_query, body)
      .then((response) => {
        this.manageResponse(response);
      }).catch((ex) => {
        console.warn(ex.message);
      });
  }

  manageResponse(response) {

    let { data } = response;
    let paginate = data;
    let notifications = this.state.notifications.slice();
    data = paginate.data;
    for (i = 0; i < data.length; i++) {
      notifications.push(data[i]);
    }

    this.setState({
      notifications,
      isSearching: false,
      paginate,
      isLoading: false,
      loadMore: false,
    }, () => {
      this.manageNotificationComingPush()
    });
  }

  componentDidMount() {
    this.fetchNotifications();
  }


}

