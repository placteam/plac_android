import React from 'react';
import { Text, View,TouchableOpacity} from 'react-native';

// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'
import { getCurrentProfileSelectedByUser } from '../../../app/controllers/ProfileController';
import { getCurrentUser } from '../../../app/controllers/PlacUserController';
import ListViewPagination from '../../views/ListViewPagination';

import { getUrlImageCompress } from '../../../lib/image';
import ButtonFollower from '../../views/profile/ButtonFollower';
import {CachedImage}  from "react-native-img-cache";

var placUser= null;
var profile=null;

export default class Followers extends React.Component {
    constructor(props){
        super(props);
        this.state={
            arrayUrls:[],
            followers:[],
            isLoading:true,
            url_query:Routes.URL_GET_NOTIFICATIONS_FOLLOWERS,
            loadMore:false,
            paginate:null,
        }
        placUser=getCurrentUser();
        profile=getCurrentProfileSelectedByUser(placUser.plac_user_id);
    }

    _keyExtractor = (item) => item.follower_id;
    render() {
      return (
        <View style={{flex: 1,marginHorizontal:15,backgroundColor:'white'}}>
            
              <ListViewPagination
                    initialNumToRender={5}
                    data={this.state.followers}
                    isLoading={this.state.isLoading}
                    loadMore={this.state.loadMore}
                    isSeparatorActive={true}
                    paginate={this.state.paginate}
                    onRefresh={this.onRefresh}
                    keyExtractor={this._keyExtractor}
                    renderRow={this.renderItem}
                    handleLoadMore={this.handleLoadMore}
                    arrayUrls={this.state.arrayUrls}
              />
        </View>
      );
    }


    renderItem=(object)=>{
       let follower=object.item;
       let isFollowing=follower.isFollowing;
       let profileTo=follower.profile_from;
  

       return <TouchableOpacity   onPress={()=>this.goProfile(profileTo)} style={{height:60,flexDirection:'row',marginTop:10,alignItems:'center',}}>
                    <TouchableOpacity  onPress={()=>this.goProfile(profileTo)}  style={{flex:.6,justifyContent:'center',alignItems:'flex-start'}}>
                        <CachedImage  style={{width:48,height:48,borderRadius:24,}} source={{uri:getUrlImageCompress(profileTo.profile_image,20)  }}/>
                    </TouchableOpacity>

                    <View style={{flex:1.4,justifyContent:'center',alignItems:'flex-start'}}>
                                <Text style={{fontSize:14,color:'black',fontWeight:'bold'}}>{profileTo.profile_name} <Text style={{fontWeight:'normal'}}>ha empezado a seguirte</Text>  </Text>
                                <Text style={{fontSize:10}}>{follower.time_ago}</Text>
                    </View>
                  
                 
                  <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
                     <ButtonFollower  customStyle={{width:"80%"}} profileFromId={profile.profile_id} profileToId={profileTo.profile_id}  isFollowing={isFollowing}/>
                  </View>
              </TouchableOpacity>
    }

    getViewProfileImage(profile){

      return  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
               {(profile.profile_type=='pet')?
                <View style={{height:96,width:96,alignItems:'flex-start'}}>
                    {(profile.profile_image)?<CachedImage   resizeMethod="resize"  style={{height:48,width:48,borderRadius:24,position:'absolute',borderRadius:24, right:20,top:(96/2)-24}} source={{uri:getUrlImageCompress(profile.profile_image,20)}} />:null}
                </View>:
                   (profile.profile_image)?<CachedImage  resizeMethod="resize"  style={{height:48,width:48,borderRadius:24,borderRadius:24}} source={{uri:getUrlImageCompress(profile.profile_image,20)}} />:null
               }       
              </View>
    }
  
    getProfileInfo(profileTo){ 
      placUser=profileTo.plac_user;
      if(profileTo.profile_type=='pet'){
        return  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{color:'black',fontSize:14,marginRight:2,}}>{profileTo.profile_name}</Text>
                    </View>
      
         
                </View>
      }else{
        return <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{color:'black',fontSize:14}}>{ profileTo.profile_name}</Text>
                <Text   ellipsizeMode={'tail'} numberOfLines={1} style={{color:'#757575',fontSize:12}}>Tienda</Text>
              </View>
  
      }
  }

    goProfile(profile){
        let params={
            profileTo:profile,
        }
        this.props.navigation.push('ProfileInvite',params);
    }

    goPost(notification){
        post=notification.post;
        params={
          postId:post.post_id,
          deleteCallBack:this.deleteCallBack
        };
        this.props.navigation.push('PostSingle',params);
    }

    deleteCallBack=()=>{

    }

      onRefresh = () => {
        this.setState({
          url_query: Routes.URL_GET_NOTIFICATIONS_FOLLOWERS,
          isLoading: false,
          followers: [],
          arrayUrls:[],
        }, () => {
          this.fetchNotificationsFollowers()
        });
      }
    
      handleLoadMore = (np,arrayUrls) => {
        this.setState({
          url_query: Routes.URL_GET_NOTIFICATIONS_FOLLOWERS+np,
          loadMore: true,
          arrayUrls,
        }, () => {
            this.fetchNotificationsFollowers()
        });
      }

    fetchNotificationsFollowers(){

        var body={
            plac_user_id:placUser.plac_user_id,
            profile_id:profile.profile_id,
        }

         Api.post(this.state.url_query,body)
          .then((response)=>{
            
            this.manageResponse(response);
          }).catch((ex)=>{
              console.warn(ex.message);
          });
    }

    manageResponse(response){
  
        let {data}=response;
        let paginate=data;
        let followers= this.state.followers;
        data=paginate.data;
        for(i=0;i<data.length;i++){
            followers.push(data[i]);
        }
    
        this.setState({
          followers,
          isSearching:false,
          paginate,
          isLoading:false,
          loadMore:false,
        });
    }

    componentDidMount(){
        this.fetchNotificationsFollowers();

    }

  }