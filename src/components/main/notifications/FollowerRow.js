import React from 'react';
import { Text, View,TouchableOpacity,  StyleSheet,Alert,} from 'react-native';

import { getUrlImageCompress } from '../../../lib/image';

import {CachedImage}  from "react-native-img-cache";
import ButtonFollower from '../../views/profile/ButtonFollower';

export default class FollowerRow extends React.Component {
    constructor(props){
        super(props);

    }

    _keyExtractor = (item) => item.notification_id;
    render() {
        let object=this.props.object;
        let index=object.index;
        let notification=object.item;
        let isFollowing=notification.isFollowing;
        let profileTo=notification.profile_from;
        let marginBottom=0;
        if(index==(this.props.notificationsLength)-1){
          marginBottom=15
        }
        let styleHighlight={}
        if(!notification.is_read){
            styleHighlight={backgroundColor:'rgba(21, 210, 187, .1)',marginHorizontal:0,paddingHorizontal:15,}
        }
   
 
        return <View   style={[styles.notificationContainer,styleHighlight,{marginBottom}]}>
                     <TouchableOpacity  onPress={()=>this.goProfile(notification)}  style={{flex:.5,justifyContent:'center',alignItems:'flex-start'}}>
                         <CachedImage  style={{width:48,height:48,borderRadius:24,}} source={{uri:getUrlImageCompress(profileTo.profile_image,20)  }}/>
                     </TouchableOpacity>
 
                     <TouchableOpacity onPress={()=>this.goProfile(notification)}  style={styles.notificationContent}>
                                 <Text style={{fontSize:14,color:'black',fontWeight:'bold'}}>{profileTo.profile_name} <Text style={{fontWeight:'normal'}}>ha empezado a seguirte</Text>  </Text>
                                 <Text style={{fontSize:10}}>{notification.notification_time_ago}</Text>
                     </TouchableOpacity>
                   
                  
                   <View style={{flex:.8,justifyContent:'center',alignItems:'flex-end'}}>
                      <ButtonFollower  customTextStyle={{fontSize:13}} customStyle={{width:"90%",height:32}} profileFromId={this.props.profileFrom.profile_id} profileToId={profileTo.profile_id}  isFollowing={isFollowing}/>
                   </View>
               </View>
        
    }


  
    getViewProfileImage(profile){

        return  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                 {(profile.profile_type=='pet')?
                  <View style={{height:96,width:96,alignItems:'flex-start'}}>
                      {(profile.profile_image)?<CachedImage   resizeMethod="resize"  style={{height:48,width:48,borderRadius:24,position:'absolute',borderRadius:24, right:20,top:(96/2)-24}} source={{uri:getUrlImageCompress(profile.profile_image,20)}} />:null}
                  </View>:
                     (profile.profile_image)?<CachedImage  resizeMethod="resize"  style={{height:48,width:48,borderRadius:24,borderRadius:24}} source={{uri:getUrlImageCompress(profile.profile_image,20)}} />:null
                 }       
                </View>
      }
    
      getProfileInfo(profileTo){ 
        placUser=profileTo.plac_user;
        if(profileTo.profile_type=='pet'){
          return  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                      <View style={{flexDirection:'row',alignItems:'center'}}>
                          <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{color:'black',fontSize:14,marginRight:2,}}>{profileTo.profile_name}</Text>
                      </View>
        
           
                  </View>
        }else{
          return <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{color:'black',fontSize:14}}>{ profileTo.profile_name}</Text>
                  <Text   ellipsizeMode={'tail'} numberOfLines={1} style={{color:'#757575',fontSize:12}}>Tienda</Text>
                </View>
    
        }
    }
  
      goProfile(notification){
                      
        if(!notification.is_read){
            this.props.updateNotification(notification);
        }
          let params={
              profileTo:notification.profile_from,
          }
          this.props.navigation.push('ProfileInvite',params);
      }
  

   

      

  }


const styles = StyleSheet.create({
    buttonContinue:{
      justifyContent: 'center',
      alignItems: 'center',
      height: 36,
      backgroundColor: '#00B99B',
      borderRadius: 8
    },
    notificationContainer:{
        height:60,
        flexDirection:'row',
        marginTop:10,
        marginHorizontal:15,
        alignItems:'center'
    },
    notificationContent:{
        paddingHorizontal:5,
        flex:1.7,
        justifyContent:'center',
        alignItems:'flex-start'
    }
  
  });