import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { getUrlImageCompress } from '../../../lib/image';
import { CachedImage } from "react-native-img-cache";
import ButtonContinue from '../../views/ButtonContinue'
import { getIconOrderOnWayToDelivery, getIconOrderDelivered } from '../../../assets/image/store'
import { formatPrice } from '../../../lib/product'
export default class OrderRow extends React.Component {
    constructor(props) {
        super(props);
    }

    _keyExtractor = (item) => item.follower_id;
    render() {
        let { index, item } = this.props.object;
        let notification = item;
        let marginBottom = 0;
        if (index == (this.props.notificationsLength) - 1) {
            marginBottom = 15
        }
        let styleHighlight = {};
        if (!notification.is_read) {
            styleHighlight = styles.highLight;
        }
        let order = notification.order;
        let place = { ...order.place_location, place: null, ...order.place_location.place }
        let orderStatus = JSON.parse(notification.order_status);
        let iconStatus;
        console.warn(order);

        switch (orderStatus.status) {
            case "sent":
                iconStatus = getIconOrderOnWayToDelivery();
                break;
            case "delivered":
                iconStatus = getIconOrderDelivered();
                break;
        }

        return <TouchableOpacity onPress={() => this.goOrderDetail(notification, order)} style={[styles.notificationContainer, styleHighlight, { marginBottom }]}>
            <View style={{ flex: .5, justifyContent: 'center', alignItems: 'flex-start' }}>
                <CachedImage style={styles.imageNotification} source={{ uri: getUrlImageCompress(place.path_image_logo, 20) }} />
            </View>
            <View onPress={() => this.goPost(notification)} style={styles.notificationContent}>
                <Text style={{ fontSize: 14, color: 'black', fontWeight: 'bold' }}>Orden # {order.order_id}
                    <Text style={{ fontSize: 13, color: 'black', fontWeight: 'bold' }}>  ${formatPrice(order.order_payment_full)}</Text>
                </Text>

                <Text ellipsizeMode={'tail'} numberOfLines={2} style={{ fontSize: 12, color: orderStatus.color }}>{orderStatus.header}</Text>
                <Text style={{ fontSize: 10 }}>{notification.notification_time_ago}</Text>
            </View>

        </TouchableOpacity>
    }

    goOrderDetail = (notification, order) => {
        if (!notification.is_read) {
            this.props.updateNotification(notification);
        }
        let params = { order };
        this.props.navigation.navigate('OrderDetail', params);
    }

}



const styles = StyleSheet.create({
    buttonContinue: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 36,
        backgroundColor: '#00B99B',
        borderRadius: 8
    },
    notificationContainer: {
        height: 60,
        flexDirection: 'row',
        marginTop: 10,
        marginHorizontal: 15,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    notificationContent: {
        flex: 2.5,
        paddingHorizontal: 5,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    highLight: {
        backgroundColor: 'rgba(21, 210, 187, .1)',
        marginHorizontal: 0,
        paddingHorizontal: 15,
    },
    imageNotification: {
        width: 48,
        height: 48,
        borderRadius: 24
    }
});