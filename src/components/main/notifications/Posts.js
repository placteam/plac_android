import React from 'react';
import { Text, View,TouchableOpacity} from 'react-native';

// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'
import { getCurrentProfileSelectedByUser } from '../../../app/controllers/ProfileController';
import { getCurrentUser } from '../../../app/controllers/PlacUserController';
import ListViewPagination from '../../views/ListViewPagination';
import {CachedImage}  from "react-native-img-cache";
import { getUrlImageCompress } from '../../../lib/image';


export default class Posts extends React.Component {
  

    constructor(props){
        super(props);
        console.warn(props);
        
        this.state={
            arrayUrls:[],
            posts:[],
            isLoading:true,
            url_query:Routes.URL_GET_NOTIFICATIONS_POSTS,
            loadMore:false,
            paginate:null,


        }
    }
    _keyExtractor = (item) => item.notification_post_id.toString();
    render() {
      return (
        <View style={{ flex: 1,marginHorizontal:15,backgroundColor:'white'}}>
            
              <ListViewPagination
                    initialNumToRender={5}
                    data={this.state.posts}
                    isLoading={this.state.isLoading}
                    loadMore={this.state.loadMore}
                    isSeparatorActive={true}
                    paginate={this.state.paginate}
                    onRefresh={this.onRefresh}
                    keyExtractor={this._keyExtractor}
                    renderRow={this.renderItem}
                    handleLoadMore={this.handleLoadMore}
                    arrayUrls={this.state.arrayUrls}
              />
        </View>
      );
    }


    renderItem=(object)=>{ 
        let notification=object.item;
        let profileFrom=notification.profile_from;
        let placUser=profileFrom.plac_user;
        let post=notification.post;

        var comment=notification.comment;
        viewActionType=null;

        if(!comment){
          viewActionType=<TouchableOpacity onPress={()=>this.goProfile(profileFrom)} style={{flex:1.8,justifyContent:'center',alignItems:'flex-start'}}>
             
                                <Text style={{fontSize:14,color:'black',fontWeight:'bold'}}>{profileFrom.profile_name} <Text style={{fontWeight:'normal'}}>le ha gustado tu publicación</Text>  </Text>
                                <Text style={{fontSize:10}}>{notification.post_time_ago}</Text>
                         </TouchableOpacity>;
        }else{
            if(comment.length>40){
              comment=comment.substring(0, 40)+'...';
             }
            viewActionType=(<TouchableOpacity onPress={()=>this.goProfile(profileFrom)}  style={{flex:1.8,justifyContent:'center',alignItems:'flex-start'}}>
                             <Text style={{fontSize:14,fontWeight:'bold',color:'black'}}>{profileFrom.profile_name} <Text style={{fontWeight:'normal'}}>ha comentado tu publicación</Text>  </Text>
                             <Text style={{fontSize:12,}}>{comment}</Text>
                             <Text style={{fontSize:10}}>{notification.post_time_ago}</Text>
                           </TouchableOpacity>);
        }
        
        return(<View style={{height:60,flexDirection:'row',marginTop:10,alignItems:'center',}}>
                    <TouchableOpacity  onPress={()=>this.goProfile(profileFrom)}  style={{flex:.6,justifyContent:'center',alignItems:'flex-start'}}>
                        <CachedImage  style={{width:48,height:48,borderRadius:24,}} source={{uri:getUrlImageCompress(profileFrom.profile_image,20)  }}/>
                    </TouchableOpacity>
                    {viewActionType}
                  
                    <TouchableOpacity onPress={()=>this.goPost(notification)} style={{flex:.6,justifyContent:'center',alignItems:'flex-end'}}>
                       <CachedImage   style={{width:50,height:50}} source={{uri:post.post_path_image}}/>
                    </TouchableOpacity>
           </View>);

    }

    goProfile(profile){

        let params={
            profileTo:profile,
        }

        this.props.navigation.push('ProfileInvite',params);


    }

    goPost(notification){

        post=notification.post;
        params={
          postId:post.post_id,
          deleteCallBack:this.deleteCallBack
        };
        
        this.props.navigation.push('PostSingle',params);

    }

    deleteCallBack=()=>{

    }


    
    

      onRefresh = () => {
        this.setState({
          url_query: Routes.URL_GET_NOTIFICATIONS_POSTS,
          isLoading: false,
          posts: [],
          arrayUrls:[],
        }, () => {
          this.fetchNotificationsPost()
        });
      }
    
      handleLoadMore = (np,arrayUrls) => {
        this.setState({
          url_query: Routes.URL_GET_NOTIFICATIONS_POSTS+np,
          loadMore: true,
          arrayUrls,
        }, () => {
            this.fetchNotificationsPost()
        });
      }

    fetchNotificationsPost(){

        let  placUser= getCurrentUser();
        let profile=getCurrentProfileSelectedByUser( placUser.plac_user_id);
        var body={
            plac_user_id:placUser.plac_user_id,
            profile_id:profile.profile_id,
        }
            Api.post(this.state.url_query,body)
        .then((response)=>{
          this.manageResponse(response);

        }).catch((ex)=>{
            console.log(ex);
        });
    }

    manageResponse(response){
  
        let {data}=response;
        let paginate=data;
        let posts= this.state.posts;
        data=paginate.data;
        for(i=0;i<data.length;i++){
            posts.push(data[i]);
        }
    
        this.setState({
          posts,
          isSearching:false,
          paginate,
          isLoading:false,
          loadMore:false,
        });
    }

    componentDidMount(){
        this.fetchNotificationsPost();

    }

  }