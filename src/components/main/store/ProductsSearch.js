import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  FlatList,
  RefreshControl,
  Text,
  TouchableOpacity,
} from 'react-native';

// API
import Api from '../../../lib/http_request/';
import * as Routes from '../../../app/routes'
import ListViewPagination from '../../views/ListViewPagination'
import Search from './views/Search'
import ProductsPlaceRow from './views/ProductsPlaceRow';


var params;

export default class ProductsSearch extends React.Component {

  static navigationOptions = ({ navigation }) => {
    params = navigation.state.params;
    return {
      headerTitle: <Search placeholder={"Buscar productos"} onEndEditing={navigation.getParam('onEndEditing')} onChangeText={navigation.getParam('onChangeText')} />
    }
  };

  constructor(props) {
    super(props);
    params = this.props.navigation.state.params;
    this.state = {
      url_query: Routes.URL_GET_PRODUCTS_PLACES_BY_FILTER,
      places: [],
      paginate: null,
      arrayUrls: [],
      filters: {
        city_id: params.citySelected,
        payment_type: "",
        pet_type: params.petTypeSelected
      },
      orderBy: {
        price: "low"
      },
      search: "",
    }
  }
  _keyExtractor = (item) => item.place_id;
  render() {

    return (
      <View style={styles.container}>
        <View style={{ marginHorizontal: 15, marginVertical: 15, marginTop: 0, flex: 1 }}>
          {this.getViewFilters()}
          <ListViewPagination
            initialNumToRender={5}
            data={this.state.places}
            isLoading={this.state.isLoading}
            loadMore={this.state.loadMore}
            paginate={this.state.paginate}
            onRefresh={this.onRefresh}
            keyExtractor={this._keyExtractor}
            urlRequest={Routes.URL_GET_PRODUCTS_PLACES_BY_FILTER}
            renderRow={this.renderPlaceRow}
            handleLoadMore={this.handleLoadMore}
            arrayUrls={this.state.arrayUrls}
          />
        </View>
      </View>
    );
  }

  getViewFilters() {
    let places = this.state.places;
    if (places.length > 0) {
      return <View style={{ borderBottomWidth: .5, borderBottomColor: '#a6a6a6', marginTop: 10, paddingBottom: 10 }} >
        <Text style={{ fontSize: 16, color: 'black', marginVertical: 5 }}>Ordenar por</Text>
        <View style={{ flexDirection: 'row' }}>

          <TouchableOpacity onPress={() => this.manageFilterPrice('price_low')} style={[styles.buttonPriceFilter, (this.state.orderBy == 'price_low') ? styles.buttonPriceFilterSelected : {}]}>
            <Text style={{ fontSize: 12, color: (this.state.orderBy != 'price_low') ? '#00B99B' : 'white' }}>Menor precio</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.manageFilterPrice('price_high')} style={[styles.buttonPriceFilter, { marginLeft: 5 }, (this.state.orderBy == 'price_high') ? styles.buttonPriceFilterSelected : {}]}>
            <Text style={{ fontSize: 12, color: (this.state.orderBy != 'price_high') ? '#00B99B' : 'white' }}>Mayor precio</Text>
          </TouchableOpacity>

        </View>
      </View>
    }

  }

  manageFilterPrice(type) {
    this.setState({
      orderBy: type,
      isLoading: true,
      arrayUrls: [],
      places: []
    }, () => {
      this.searchProducts();
    });

  }

  onRefresh = () => {
    this.setState({
      url_query: Routes.URL_GET_PRODUCTS_PLACES_BY_FILTER,
      isLoading: false,
      places: [],
      arrayUrls: [],
    }, () => {
      this.searchProducts();
    });
  }

  handleLoadMore = (np, arrayUrls) => {
    this.setState({
      url_query: Routes.URL_GET_PRODUCTS_PLACES_BY_FILTER + np,
      loadMore: true,
      arrayUrls
    }, () => {
      this.searchProducts();
    });
  }


  renderPlaceRow = (object) => {
    return <ProductsPlaceRow navigation={this.props.navigation} object={object} />
  }

  setTextInputHeader = () => {
    this.props.navigation.setParams({
      onChangeText: this.onChangeText,
      onEndEditing: this.onEndEditing

    });
  }

  onEndEditing = () => {
    this.setState({ places: [], isLoading: true, url_query: Routes.URL_GET_PRODUCTS_PLACES_BY_FILTER }, () => {
      this.searchProducts();
    });
  }

  onChangeText = (search) => {
    this.setState({ search });
  }

  searchProducts() {
    var body = {
      ...this.state,
    }
    Api.post(this.state.url_query, body)
      .then((response) => {
        this.manageResponse(response);
      }).catch((ex) => {
        console.warn(ex.message);
      });
  }

  manageResponse(response) {
    let { data } = response;
    let paginate = data;
    let places = this.state.places.slice();
    data = paginate.data;
    for (i = 0; i < data.length; i++) {
      places.push(data[i]);
    }
    this.setState({
      places,
      isSearching: false,
      paginate,
      isLoading: false,
      loadMore: false,
    });
  }

  componentDidMount() {
    this.setTextInputHeader();
  }


}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  buttonPriceFilter: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#15d2bb',
    width: '25%',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonPriceFilterSelected: {
    backgroundColor: '#15d2bb'
  }






});
