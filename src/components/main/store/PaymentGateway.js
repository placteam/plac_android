import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  BackHandler,
  Platform,
  WebView,
  View } from 'react-native';

  var params;
  export default class PaymentGateway extends Component {

    static navigationOptions = {
    header: null,
    }
    constructor(props) {
     super(props);
     params=this.props.navigation.state.params;
     this.state={
         order:params.order,
     }

   }


    render(){
        let mercadoPago=this.state.order.mercado_pago;
        let uri= mercadoPago.response.init_point;
        
        return(
                <View style={{flex:1,}}>
                   {(Platform.OS=="ios")?<View style={{backgroundColor:"#15d2bb",flexDirection:"row",height:20}}></View>:null }

                    <View style={{flex:2}}>

                            <WebView
                                source={{uri:uri}}
                                style={{flex:1.93,marginTop:1}}/>
                        
                                <View style={{flexDirection:'row',backgroundColor:'#54BBE8',flex:.07,justifyContent:'center',alignItems:'center'}}>
                                    <TouchableOpacity  style={{justifyContent:'center',alignItems:'center',flex:1,height:30,}} onPress={()=>this.goOrders()}>
                                        <Text allowFontScaling={false} style={{color:'white',justifyContent:'center',fontSize:12}}>Ver orden de compra generada</Text>
                                    </TouchableOpacity>
                                </View>
                    </View>
                </View>
         )
    }

    goOrders(){
      params.flag="neworder";
      this.props.navigation.replace('OrderDetail',params);
    }




    componentDidMount() {
      if(Platform.OS=="android"){
         BackHandler.addEventListener('hardwareBackPress',  this.handleBack);
      }
    }

    componentWillUnmount() {
      if(Platform.OS=="android"){
          BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
       }
    }

    handleBack() {
        return true;
    }
}


  const styles = StyleSheet.create({
    btnNormal:{
      borderWidth:1,
      borderColor:'rgba(255,232,22,1)',
    },
    btnPress:{
        borderWidth:1,
        borderColor:'rgba(255,232,22,1)',
      backgroundColor:'rgba(255,232,22,.5)',
    }
  });


  AppRegistry.registerComponent('PaymentGateway', () => PaymentGateway);
