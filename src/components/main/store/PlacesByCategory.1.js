import React, {PureComponent  } from 'react';

import {
  StyleSheet,
  View,
  TextInput,
  Image,
  Text,
  Alert,
  FlatList,
  TouchableOpacity,
  Dimensions,

  
} from 'react-native';



import { CachedImage } from 'react-native-img-cache';
import { getProportionalImageHeight } from '../../../lib/image';
import PetTypeListView from '../../views/pet/PetTypeListView';
import { search } from '../../../assets/image/svg';

var{width}=Dimensions.get('window');

export default class PlacesByCategory extends PureComponent {

    static navigationOptions = ({ navigation}) => {
        params = navigation.state.params;
        category=params.category;
        return {
            headerStyle: {
                elevation: 0,
                shadowOpacity: 0,
              },
          headerTitle: (
            <View
            style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                marginTop:10,
                marginRight:40
        
      
              }}>
                    <Text style={{fontSize: 16, color: "black"}}>
                        {(category)?category.category_name:"Producto"}
                    </Text>
                    <Text style={{fontSize:12}}>{category.category_additional_text}</Text>
            
        
            </View>
          )
        };
      };


  constructor(props){
    super(props);
    params=this.props.navigation.state.params;
    this.state={
        search:'',
    }
 
  }

  render() {
      let category=params.category;
      console.warn(category);

      let imageHeight=getProportionalImageHeight({width:360,height:134},width-30);
      
   
      return <View style={styles.container}>

               <View style={{flexDirection:'row',}}>
                  <View style={{width:55,height:55,justifyContent:'center',alignItems:'center'}}>
                     <Image style={{width:16,height:16}} source={require('../../../assets/image/general/back/back_icon_black.png') }/>
                  </View>
                  <View style={{flex:1,       justifyContent: "center",
                                alignItems: "center",}}>
                                    <Text style={{fontSize: 16, color: "black"}}>
                                        {(category)?category.category_name:"Producto"}
                                    </Text>
                                    <Text style={{fontSize:12}}>{category.category_additional_text}</Text>
                </View>
                    
               </View>




                  <View style={{marginHorizontal:15,marginTop:10}}>
                     <View style={{ height: 134}}>
                        <View  activeOpacity={.8} style={styles.viewSearch} >
                                    {search()}
                                    <TextInput placeholder={'Buscar productos para max'}  value={this.state.search} onChangeText={(search)=>this.setState({search})} style={{color:'#aaaaaa',marginLeft:5,height:42,borderWidth:0,fontSize:16}}/>
                        </View>
                      
                        <CachedImage style={{top:0,position:'absolute',height:imageHeight,width:width-30,borderRadius:8,}} source={{uri:category.category_img_url_internal_app}}/>
                  
                    </View>


                  </View>
                 
              
                        
            
             </View>
   
          
  }

  petTypeSelected=(type)=>{
     console.warn(type);
  

  }

  onRefresh=()=>{
    this.setState({
      url_query:urlQuery+placUser.plac_user_id,
      isLoading:false,
      products:[],
    },()=>{
      this.getProductQuestions()
    });
  }

  handleLoadMore=(np)=>{
    this.setState({
      url_query:urlQuery+placUser.plac_user_id+np,
      loadMore:true,
    },()=>{
      this.getProductQuestions();
    });
  }

  componentWillMount(){


  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textInputQuestion:{
    height:48,
    paddingLeft: 10,
    borderRadius:8,
    borderWidth:1,
    width:'100%',
    borderColor:'#00b99b'
  },
  viewSearch: {
    marginHorizontal:15,
    height: 42,
    borderRadius: 8,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    marginVertical:15,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 4,
    shadowRadius: 8,
    elevation: 1,
    backgroundColor:'white',
    borderWidth: .6,
    borderColor: '#15d2bb',
  
  },
  

 

});

