import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  Alert,
  FlatList,
  RefreshControl,
  Text,
  ScrollView,
  Picker,
  TouchableOpacity,
} from 'react-native';

import { Rating} from 'react-native-ratings';
// API
import Api from  '../../../lib/http_request/';
import {URL_RESOURCE_PLACES, URL_GET_PRODUCT_SUBCATEGORIES_BY_FILTER, URL_GET_PRODUCTS_BY_FILTER, URL_GET_PRODUCT_BRANDS_BY_FILTER, URL_SHOW_PLACE_NAME } from '../../../app/routes';
import { getUrlImageCompress, getProportionalImageHeight } from '../../../lib/image';



import CardView from   'react-native-cardview'
import ReadMore from '../../../lib/text/ReadMore';
import SubCategoryRow from './views/categories/SubCategoryRow';
import CategoryRow from './views/categories/CategoryRow';
import ProductsRow from './views/ProductRow';

import { CachedImage } from 'react-native-img-cache';
import CartGeneral from './views/CartGeneral';

var{width,height}=Dimensions.get('window');



export default class Place extends React.Component {

    static navigationOptions = {
        header: null,
    }
  

  constructor(props){
    super(props);
    params=props.navigation.state.params;
  
    this.state={
        url_query_products:URL_GET_PRODUCTS_BY_FILTER,
        place:null,
        categories:[],
        subcategories:[],
        products:[],
        placeId:"",
        brands:[],
        isLoading:true,
        brand:"",
        filters:{
          pet_type:params.petType,
          place_id:params.place_id,
        },
        arrayUrls:[],
        isOpenCart:false,
   
     }



              
  }

  _keyExtractorCategory = (item) => item.category_id;
  _keyExtractorSubCategory = (item) => item.subcategory_id;
  _keyExtractorProduct = (item) => item.product_id;

  render() {

    let place=this.state.place;
    if(place==null){
      return null;
    }
      
    return (
      <View style={styles.container}>
          {this.getViewHeader(place)}
          {this.getViewPlaceInfo(place)}
          {this.getViewCartGeneral()}
      </View>
    );
  }

  getViewHeader(place) {
 
    let isOpenCart=this.state.isOpenCart;
    let internalView= <View style={{marginBottom:1,flexDirection: 'row', height: 55}}>
                         <TouchableOpacity  onPress={()=>this.props.navigation.goBack()} style={{ width: 55, height: 55, justifyContent: 'center', alignItems: 'center' }}>
                                   <Image style={{ width: 16, height: 16 }} source={require('../../../assets/image/general/back/back_icon_black.png')} />
                         </TouchableOpacity>
                         <View style={{flex: 1, marginLeft:15,justifyContent: "center",alignItems: "flex-start"}}>
                                     <Text style={{ fontSize: 18, color: "black", fontWeight: 'bold' }}>
                                        {(place) ?  place.place_location_name: "Lugar"}
                                     </Text>
                         </View>        
                      {this.getViewCartGeneral()}
                     </View>;
      if(isOpenCart){
        return <View   style={{height: 55}}>
                  {internalView}
              </View>
      }
        return <CardView
                  style={{ height: 55}}
                  cardElevation={3}
                  cardMaxElevation={3}
                  cornerRadius={1}>
                  {internalView}
              </CardView>
  }

  getViewCartGeneral(){
    return <CartGeneral isOpenCart={this.isOpenCart} navigation={this.props.navigation}/>
          
  }
  isOpenCart=(isOpenCart)=>{
      this.setState({isOpenCart});
  }


getViewPlaceInfo(place){

  let widthProduct=(width/3)-16.5;
  let  heightImage=width-220;
  let heightLogo=heightImage;


 return  <ScrollView    
            scrollEventThrottle={16}
            onScrollEndDrag={(e)=>this.onScrollEnd(e)} 
            style={{flex:1}}>
          <View style={{width,height:heightImage,marginTop:1}} >
               <CachedImage style={{width,height:heightImage,marginTop:1}} source={{uri:getUrlImageCompress(place.path_image1,20)}}/>
               <CachedImage  style={{width:56,height:56,position:'absolute',left:(width/2)-24,top:heightLogo-28,}} source={{uri:getUrlImageCompress(place.path_image_logo,10)}}   />
          </View>
          {this.getInfo1(place)}
          {this.getInfoAboutProducts(place)}
          {this.getCategoriesList()}
          
          {this.fetchSubcategoriesList()}
          {this.getViewBrands()}

           <FlatList 
               data={this.state.products}
               removeClippedSubviews={true}
               showsVerticalScrollIndicator={false}
               refreshControl={
                  <RefreshControl
                      refreshing={this.props.isLoading}
                      onRefresh={()=>this.onRefresh()}
                      title="Cargando..."
                      colors={["#15d2bb","#15d2bb","#15d2bb"]}
                      tintColor="#15d2bb"
                      titleColor="#15d2bb"
                  />
               }
               style={{marginHorizontal:10}}
               numColumns={3}
               initialNumToRender={9}
               onEndReachedThreshold={1}
               keyExtractor={this._keyExtractorProduct}
               renderItem={(object)=>{return <ProductsRow numColumns={3} widthProduct={widthProduct} subcategorySelected={this.subcategorySelected} object={object} navigation={this.props.navigation}/>}}
           />           
                                    
  </ScrollView> 
}

onScrollEnd(e){
  if(this.state.products.length>0){
    let paddingToBottom = 100;
    paddingToBottom += e.nativeEvent.layoutMeasurement.height;
    if(e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom) {
       this.handleLoadMore();
    }
  }
}

getInfo1(place){
return  <View style={{marginHorizontal:15,alignItems:'center'}}>
      {(place.rating!=0)?<TouchableOpacity  onPress={()=>this.goPlaceRating()} style={{flexDirection:'row',marginTop:29,alignItems:'center'}}>
            <Text style={{fontSize:15,color:'grey',marginRight:5}}>{place.rating}</Text>
            <View >
                <Rating 
                  type="star"
                  fractions={1}
                  startingValue={place.rating}
                  readonly
                  imageSize={16}
          
                  style={{alignItems: 'flex-start',justifyContent:'flex-start', paddingVertical: 0 }}
              />
         </View>
         
        </TouchableOpacity>:null}
      
        <View  style={{marginTop:(place.rating!=0)?5:29,alignItems:'flex-start',justifyContent:'flex-start'}}>
          <ReadMore
                  numberOfLines={4}
                  renderTruncatedFooter={this._renderTruncatedFooter}
                  renderRevealedFooter={this._renderRevealedFooter}
                  >
                      <Text style={{textAlign:'center', fontSize:14,color:'black'}}>
                          {place.place_description}
                      </Text>
              </ReadMore>
            {this.getViewOff(place)} 

               

        </View>
      </View>
}

getViewOff(place){

  if(place.promotion!=undefined){
   let  promotion=JSON.parse(place.promotion);
    if(promotion.isActive){
     return  <View style={{marginTop:5,alignItems:'center',justifyContent:'center'}}>
                <View style={{alignItems:'center',flexDirection:'row',justifyContent:'center',height:getProportionalImageHeight({width:1280,height:192},100)+5}}>
                    <Text style={{fontSize:10,color:'black'}} >{promotion.text}</Text>
                    <CachedImage style={{marginLeft:10,height:getProportionalImageHeight({width:1280,height:192},100),width:100}} source={{uri:promotion.image}}/>
                </View>
                <Text style={{fontSize:9,color:'black'}} >{promotion.expired}</Text>
            </View>
    }
  }
  return null;
}



getInfoAboutProducts(place){
  return <View style={{flexDirection:'row',marginHorizontal:15,marginTop:10}}>
          <View style={{borderRightWidth:1,borderRightColor:'black',paddingRight:8,}}>
            <Text style={{fontSize:13,fontWeight:'300',color:'black'}}>Bogotá</Text>
          </View>

              <View style={{borderRightWidth:(place.total_orders_made!=0)?1:0,paddingHorizontal:8,borderRightColor:'black'}}>
              <Text style={{fontSize:13,fontWeight:'300',color:'black'}}>{place.total_products} Articulos</Text>
            </View>
            
            {(place.total_orders_made!=0)?<View style={{paddingHorizontal:8,}}>
              <Text style={{fontSize:13,fontWeight:'300',color:'black'}}  >{place.total_orders_made} ventas</Text>
            </View>:null}
        </View>
}

getCategoriesList(){
return <FlatList
            style={{marginTop:10,backgroundColor:'#15d2bb',height:42}}
            contentContainerStyle={{justifyContent:'center',alignItems:'center'}}
            data={this.state.categories}
            ref={(ref) => { this.categoriesList = ref; }}
            getItemLayout={(data, index) => (
                  {length: 80, offset: 80 * index, index}
                )}

            showsHorizontalScrollIndicator={false}
            horizontal={true}
            keyExtractor={this._keyExtractorCategory}
            renderItem={(object)=><CategoryRow  categorySelected={this.categorySelected} object={object} navigation={this.props.navigation}/>}
        />
}
fetchSubcategoriesList(){
 return <FlatList
    
          contentContainerStyle={{justifyContent:'center',alignItems:'center'}}
          data={this.state.subcategories}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          ref={(ref) => { this.subcategoriesList = ref; }}
          getItemLayout={(data, index) => (
              {length: 120, offset: 120 * index, index}
            )} 
          keyExtractor={this._keyExtractorSubCategory}
          renderItem={(object)=><SubCategoryRow   isAccordingToWindow={false} subcategorySelected={this.subcategorySelected} object={object} navigation={this.props.navigation}/>}
    />
}


categorySelected=(category)=>{
      let categories=this.state.categories;
      for(i=0;i<categories.length;i++){
        if(categories[i].category_id==category.category_id){
          categories[i].isSelected=true;
          this.categoriesList.scrollToIndex({index:i});
        }else{
          categories[i].isSelected=false;
        }
      }

      let filters=this.state.filters;
      filters.category_id=category.category_id;
      filters.subcategory_id="";
      filters.brand="";
      this.setState({
        filters,
        categories,
        products:[],
        brands:[],
        url_query_products:URL_GET_PRODUCTS_BY_FILTER,
        isLoading:true,
        arrayUrls:[],
        brand:"",
      },()=>{
          this.fetchProducts();
          this.fetchSubcategories();
          this.fetchBrands();
      });
}

getViewBrands() {

  let brands = this.state.brands;
  if(brands.length>0){
    return   <View style={{marginHorizontal:15,marginVertical:10,flexDirection:'row', alignItems: 'flex-start',}}>
                <Text style={{ fontSize: 12, marginRight: 10,marginTop:3,color: '#15d2bb' }}>
                  Seleccionar marca
                </Text>
              
                    <Picker
                      itemStyle={{fontSize: 12}}
                  
                      selectedValue={this.state.brand}
                      mode={'dropdown'}      
                      style={{ height: 20,padding: 0,width:width/1.5, borderWidth: 1, justifyContent: 'center', alignItems: 'center',}}
                      onValueChange={this.onChangeBrand}>
                      <Picker.Item itemStyle={{fontSize:12}} label={"Marca"} key={i} value={"Marca"} />
                      {brands.map((element, i) => {
                        return <Picker.Item label={element.brand} key={i} value={element.brand} />
                      })}
                    </Picker>   
          </View>
  }

}

onChangeBrand=(brand, itemIndex)=>{
  let filters=this.state.filters;
  if(brand!="Marca"){
    filters.brand=brand;
    this.setState({brand,filters,products:[],arrayUrls:[]},()=>{
      this.fetchProducts();
    });
   
  }
 
}

subcategorySelected=(subcategory)=>{

 
    let subcategories=this.state.subcategories;
    for(i=0;i<subcategories.length;i++){
      if(subcategories[i].subcategory_id==subcategory.subcategory_id){
        subcategories[i].isSelected=true;
        this.subcategoriesList.scrollToIndex({index:i});
      }else{
        subcategories[i].isSelected=false;
      }
    }

    let filters=this.state.filters;
    filters.subcategory_id=subcategory.subcategory_id;
    filters.brand="";
        this.setState({
          filters,
          brand:"",
          products:[],
          url_query_products:URL_GET_PRODUCTS_BY_FILTER,
          subcategories,
          arrayUrls:[],
          isLoading:true,
        },()=>{
          this.fetchProducts();
          this.fetchBrands();
        });
}







  onRefresh = () => {
    
    this.setState({
      url_query_products:URL_GET_PRODUCTS_BY_FILTER,
      isLoading: true,
      products: [],
      arrayUrls:[],
    }, () => {
      this.fetchProducts();
    });
  }

  handleLoadMore () {
    let paginate=  this.state.paginate;
    let nextPageURL=paginate.next_page_url;

    if(paginate.current_page!=paginate.last_page){
        if(nextPageURL){
          let nextPage=nextPageURL.split("?");
          let arrayUrls=this.state.arrayUrls;
              if(arrayUrls.indexOf(nextPageURL)==-1){
                 arrayUrls.push(nextPageURL);
                      let np="?"+nextPage[1];
                      this.setState({
                        arrayUrls,
                        url_query_products:URL_GET_PRODUCTS_BY_FILTER +np,
                        loadMore: true
                      },()=>{
                        this.fetchProducts()
                      });

                   
              }
        }
   }
  }



goPlaceRating(){
  this.props.navigation.navigate('PlaceRating',params)
}




  fetchSubcategories(){
    let filters=this.state.filters;
    var body={
      filters,
    }

    Api.post(URL_GET_PRODUCT_SUBCATEGORIES_BY_FILTER,body)
     .then((response)=>{
      let {data,status,message}=response;
      if(status=="success"){
          this.setState({
             subcategories:data,
          });

  
      }else{
          Alert.alert("Ha ocurrido un problema",message);
      }
     
     }).catch((ex)=>{
        Alert.alert("Ha ocurrido un problema",ex.message);
        
     });
    
  }


  fetchPlace(){
      let  URL=URL_RESOURCE_PLACES+"/"+this.state.placeId;
      Api.get(URL)
       .then((response)=>{
            let {data,status,message}=response;
            if(status=="success"){
              let place=data;
                this.setState({
                    place,
                    categories:place.categories,
                });
            }else{
                Alert.alert("Ha ocurrido un problema",message);
            }
       }).catch((ex)=>{
          Alert.alert("Ha ocurrido un problema",ex.message);
       });
  }

  formatString(string) {
    string = string.replace(/-/g," ");
    return string;
  }


  fetchPlaceByName(){
    let  URL=URL_SHOW_PLACE_NAME;
    let body={
      place_name:this.formatString(params.place_name),
    }

    console.warn(body);
    

    Api.post(URL,body)
     .then((response)=>{
          let {data,status,message}=response;
          if(status=="success"){
            let place=data;
            let filters=this.state.filters;
            filters.place_id=place.place_id;
              this.setState({
                  filters,
                  place,
                  categories:place.categories,
              },()=>{
                this.fetchBrands();
                this.fetchProducts();
              });
          }else{
              Alert.alert("Ha ocurrido un problema",message);
          }
     }).catch((ex)=>{
        Alert.alert("Ha ocurrido un problema",ex.message);
     });
}

  fetchBrands() {
    let filters=this.state.filters;
    let body = {
      filters
    }

    Api.post(URL_GET_PRODUCT_BRANDS_BY_FILTER, body)
      .then((response) => {
        this.setState({ brands: response });
      }).catch((ex) => {
        console.warn(ex.message);
      });
    }

  fetchProducts(){
    let filters=this.state.filters;
    let pagination={
      per_page:9,
    }
    filters.pagination=pagination;
    let body={
      filters,
    }
    Api.post(this.state.url_query_products,body)
     .then((response)=>{
   


      let {data,status,message}=response;
      if(status=="success"){
        this.manageResponse(data);
  
      }else{
          Alert.alert("Ha ocurrido un problema",message);
      }
     
     }).catch((ex)=>{
        Alert.alert("Ha ocurrido un problema",ex.message);
        
     });
  }


  manageResponse(paginate){
    let products= this.state.products;
    let data=paginate.data;
    for(i=0;i<data.length;i++){
        products.push(data[i]);
    }

    

    this.setState({
      products,
      isSearching:false,
      paginate,
      isLoading:false,
      loadMore:false,
    });
}

  


  componentDidMount(){
      let placeId=  params.place_id;
      
      this.setState({
         placeId
      },()=>{
        if(params.place_name!=undefined){
          this.fetchPlaceByName();
        }else{
          this.fetchPlace();
          this.fetchBrands();
          this.fetchProducts();
        }
 

      });
  }
}


  



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textTitle:{
    fontSize:16,
    color:'black',
    fontWeight:'bold',
    marginVertical:15
  },
  imageUser:{
    width:36,
    height:36,
    borderRadius:18,
  },
  textName:{
    fontSize:14,
    color:'black',
    fontWeight:'bold'
  },
  viewContentAddCart:{
    marginHorizontal:15,
    height:120,
    justifyContent:'flex-start',
    alignItems:'center'
  },
  viewContainerQuestion:{
    flexDirection:'row',marginTop:10,
    justifyContent:'flex-end',
    alignItems:'flex-start'
  },
  viewQuestion:{
    backgroundColor:'#ededed',
    width:'70%',
    padding:5,
    borderRadius:8,
  },
  textQuestion:{
    fontSize:14,
    color:'black'
  },
  viewProductQuantity:{
    marginHorizontal:5,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:8, 
    height:48,
    width:48,
    borderWidth:2,
    borderColor:'#15d2bb'
  },
  viewAddCartAbsolute:{
    borderRadius:1,
    shadowColor: '#000000',
      shadowOffset: {
        width: 20,
        height:20
      },
    shadowRadius: 1,
    shadowOpacity: .3,  
    position:'absolute',
    bottom:0,
    height:120,
    width,
  }
    
  
  
 

  

});
