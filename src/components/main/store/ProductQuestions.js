import React, {PureComponent  } from 'react';

import {
  StyleSheet,
  View,
  TextInput,
  Image,
  Text,
  Alert,

  
} from 'react-native';

import ButtonContinue from '../../views/ButtonContinue';
import ListViewPagination from '../../views/ListViewPagination';
import * as Routes from '../../../app/routes';

import * as PlacUserController from '../../../app/controllers/PlacUserController'
import * as emoticons from 'react-native-emoticons';

//API
import Api from '../../../lib/http_request';
import ProductQuestionRow from './views/ProductQuestionRow';
var params;
var product;
var placUser;

const urlQuery=Routes.URL_GET_PRODUCT_QUESTIONS_BY_PRODUCT;
export default class ProductQuestions extends PureComponent {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    params = navigation.state.params;
    return {
      headerTitle: (
        <View
        style={{
            flex: 1,
            flexDirection:'row',
            justifyContent: "flex-start",
            alignItems: "flex-start"
          }}>

          <Text style={{fontSize: 16, color: "black", }}>
          {(params.product)?params.product.product_name:"Producto"}
          
          </Text>
        </View>
      )
    };
  };

  constructor(props){
    super(props);
    params=this.props.navigation.state.params;
    product=params.product;
    placUser=PlacUserController.getCurrentUser();
    this.state={
      question:'',
      heightTI:48,
      questions:[],
      isLoading:false,
      paginate:null,
      isValidData:true,
      url_query:urlQuery+product.product_id,
      arrayUrls:[],
    
    }

  }
  _keyExtractor = (item, index) => item.question_id;
  render() {

   
      return <View style={styles.container}>
                <View  style={{marginHorizontal:15,flex:1}} >
                    <View style={{marginTop:15,marginBottom:15,flexDirection:'row'}}> 
                          <View style={{flex:2.2}}>
                            <TextInput
                              placeholder={'Ej. ¿Qué colores, sabores hay?'}
                              onContentSizeChange={(event) => {
                                this.setState({heightTI: event.nativeEvent.contentSize.height});
                              }}
                              style={[styles.textInputQuestion, {height: Math.max(48, this.state.heightTI)}]}
                              value={this.state.question}
                              multiline={true}
                              onChangeText={(question)=>this.setState({question})}
                            />

                          </View>
                          <View style={{flex:.8,justifyContent:'center',alignItems:'flex-end'}}>

                            <ButtonContinue onPress={()=>this.makeQuestion()} isValidData={this.state.isValidData}   text={'Preguntar'} customStyle={{width:'95%'}}  />
                        
                          </View>
                    </View>
                  
                      <ListViewPagination
                          isSeparatorActive={false}
                          initialNumToRender={5}
                          methodRequest={"GET"}
                          data={this.state.questions}
                          isLoading={this.state.isLoading}
                          loadMore={this.state.loadMore}
                          paginate={this.state.paginate}
                          keyExtractor={this._keyExtractor}
                          onRefresh={this.onRefresh}
                          urlRequest={urlQuery}
                          renderRow={this.renderRow}
                          handleLoadMore={this.handleLoadMore}
                          arrayUrls={this.state.arrayUrls}
                      />

                

                  
                </View>
             </View>
   
          
  }

  onRefresh=()=>{
    this.setState({
      url_query:urlQuery+product.product_id,
      isLoading:true,
      questions:[],
      arrayUrls:[],
    },()=>{
      this.fetchProductQuestions()
    });
  }

  handleLoadMore=(np,arrayUrls)=>{
  
    this.setState({
      url_query:urlQuery+product.product_id+np,
      loadMore:true,
      arrayUrls
    },()=>{
 
      
      this.fetchProductQuestions();
    });
  }

  renderRow=(object)=>{
    return <ProductQuestionRow placeLocation={product.place_location}  object={object}/>

  }


  makeQuestion(){
    const url=Routes.URL_RESOURCE_PRODUCT_QUESTIONS;
    let body={
      place_id: product.place_id,
      product_id:product.product_id,
      plac_user_id:placUser.plac_user_id,
      question_txt:emoticons.stringify(this.state.question)
    }

    this.setState({isValidData:false,});

    Api.post(url,body)
    .then((response)=>{
      let {data,status,message}=response;
      if(status=="success"){
        let questions= this.state.questions;
        questions.unshift(data);
        params.addQuestion(data);
        this.setState({isValidData:true,question:'',questions});
      }else{
          Alert.alert("Ha ocurrido un problema",message);
      }
    
    }).catch((ex)=>{
       Alert.alert("Ha ocurrido un problema",ex.message);
       this.setState({isLoading:false});
       
    });
   
  }

  fetchProductQuestions(){
    Api.get(this.state.url_query)
    .then((response)=>{
      console.warn(response);
      
     let {data,status,message}=response;
     if(status=="success"){
       this.manageResponse(data);
     }else{
         Alert.alert("Ha ocurrido un problema",message);
     }
     this.setState({isLoading:false});
    
    }).catch((ex)=>{
       Alert.alert("Ha ocurrido un problema",ex.message);
       this.setState({isLoading:false});
       
    });
  }

  manageResponse(response){
    const {data}=response;
    let questions= this.state.questions;
 
    for(i=0;i<data.length;i++){
       questions.push(data[i]);
    }

    this.setState({
      questions,
      isSearching:false,
      paginate:response,
      isLoading:false,
      loadMore:false,
    });
}

  componentDidMount(){
    this.fetchProductQuestions();

  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textInputQuestion:{
    height:48,
    paddingLeft: 10,
    borderRadius:8,
    borderWidth:1,
    width:'100%',
    borderColor:'#00b99b'
  }
  

 

});
