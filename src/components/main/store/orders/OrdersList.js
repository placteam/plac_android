import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    View
} from 'react-native';
import ListViewPagination from '../../../views/ListViewPagination';


// API
import Api from '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'
import { getCurrentUser } from '../../../../app/controllers/PlacUserController';
import CardView from 'react-native-cardview';
import { getUrlImageCompress } from '../../../../lib/image';
import { formatPrice } from '../../../../lib/product';



export default class OrdersList extends Component {

    static navigationOptions = ({ navigation, navigationOptions }) => {
        params = navigation.state.params;
        return {
            title: 'Mis ordenes',

        };
    };
    constructor(props) {
        super(props);
        params = this.props.navigation.state.params;
        this.state = {
            isLoading: true,
            loadMore: false,
            paginate: null,
            orders: [],
            url_query: Routes.URL_GET_ORDER_USERS,
            arrayUrls: [],

        }

    }



    render() {
        return (
            <View style={styles.container} >

                <View style={{ marginHorizontal: 15, flex: 1 }}>
                    <ListViewPagination
                        initialNumToRender={5}
                        numColumns={1}
                        data={this.state.orders}
                        isLoading={this.state.isLoading}
                        loadMore={this.state.loadMore}
                        paginate={this.state.paginate}
                        isSeparatorActive={false}
                        onRefresh={this.onRefresh}
                        keyExtractor={this._keyExtractor}
                        renderRow={this.renderItem}
                        handleLoadMore={this.handleLoadMore}
                        arrayUrls={this.state.arrayUrls}
                    />

                </View>

            </View>
        );
    }


    _keyExtractor = (item, index) => item.order_id 

    renderItem = (object) => {
        let order = object.item;
        let placeLocation = order.place_location;
        let place = placeLocation.place;




        return <CardView
            style={{ margin: 2, marginTop: 10, padding: 10, height: 90 }}
            cardElevation={2}
            cardMaxElevation={2}
            cornerRadius={4}  >
            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.goOrderDetail(object.item)}>
                <View style={{ flex: 1, justifyContent: 'space-between' }}>
                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image style={{ width: 20, height: 20, borderRadius: 10 }} source={{ uri: getUrlImageCompress(place.path_image_logo, 10) }} />
                            <Text style={{ marginLeft: 5, color: 'black', fontSize: 14 }}>{placeLocation.place_location_name}</Text>
                        </View>
                        {this.getPaymentInfo(order)}
                    </View>
                    <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                        <Text style={{ fontSize: 10 }}>{order.created_at}</Text>
                    </View>
                </View>
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-start' }}>
                        <Text style={{ fontSize: 12,color:order.delivery_status.color}}>{this.getTextDeliveryStatus(order)}</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }} >
                        <Text style={{ marginLeft: 5, fontSize: 15, fontWeight: 'bold', color: '#15d2bb' }}>${formatPrice(parseFloat(order.order_resumed.total))}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </CardView>
    }
    getTextDeliveryStatus(order) {
        let orderMP = order.mercado_pago;
        let orderDeliveryStatus = order.delivery_status.status.es;
        if (orderMP != null) {
            if (orderMP.status == "pending") {
                return "Esperando pago...";
            }
            return orderDeliveryStatus;
        } else {
            return orderDeliveryStatus;
        }

    }

    getPaymentInfo(order) {
        if (order.order_payment_type == 'payment_delivery') {
            let paymentStatus = "Pendiente de pago"
            if (order.order_payment_status == "approved") {
                paymentStatus = "Pago efectuado";
            }

            return <View>
                        <Text style={{ color: 'black', fontSize: 12 }}>Pago contra entrega</Text>
                        <Text style={{ fontSize: 12 }}>{paymentStatus}</Text>
                  </View>
        } else {
            let orderMP = order.mercado_pago;
            if (orderMP != null) {
                let brief = orderMP.brief;
                let payment = brief.payment;
                let paymentStatus = brief.payment_status;
                return <View>
                    <Text style={{ color: 'black', fontSize: 13 }}>{payment.payment_type_id} - {payment.payment_method_id} </Text>
                    <Text style={{ fontSize: 12 }}>{paymentStatus}</Text>
                </View>
            } else {
                return <View>
                    <Text style={{ color: 'black', fontSize: 12 }}>Pago en proceso o abandonado</Text>

                </View>
            }
        }
    }

    goOrderDetail(order) {
        let params = { order };
        this.props.navigation.push('OrderDetail', params);
    }

    onRefresh = () => {
        this.setState({
            url_query: Routes.URL_GET_ORDER_USERS,
            isLoading: true,
            orders: [],
            arrayUrls: [],
        }, () => {
            this.fetchOrders();
        });
    }

    handleLoadMore = (np, arrayUrls) => {
        this.setState({
            arrayUrls,
            url_query: Routes.URL_GET_ORDER_USERS + np,
            loadMore: true,
        }, () => {
            this.fetchOrders();
        });
    }

    fetchOrders() {
        let placUser=getCurrentUser();
        console.warn(placUser);
        
        

        let body = {
            plac_user_id: placUser.plac_user_id,
        }
        Api.post(this.state.url_query, body)
            .then((response) => {

                let { data, status, message } = response;
                if (status == "success") {
                    this.manageResponse(response);
                } else {
                    Alert.alert("Ha ocurrido un problema", ex.message);
                }

            }).catch((ex) => {


            });

    }

    manageResponse(response) {
        let { data } = response;
        let paginate = data;
        let orders = this.state.orders;
        data = paginate.data;
        for (i = 0; i < data.length; i++) {
            orders.push(data[i]);
        }

        this.setState({
            orders,
            paginate,
            isLoading: false,
            loadMore: false,
        });
    }

    componentWillMount() {
        this.setState({ arrayUrls: [] });
    }
    componentWillUnmount() {
        this.setState({ arrayUrls: [] });
    }

    componentDidMount() {
        this.fetchOrders();
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },


});


AppRegistry.registerComponent('PickCity', () => PickCity);
