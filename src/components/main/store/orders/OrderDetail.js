import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  Dimensions,

  View
} from 'react-native';


import { formatPrice } from '../../../../lib/product';
import StepIndicator from 'react-native-step-indicator';

// API
import Api from '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'
import { getCurrentUser } from '../../../../app/controllers/PlacUserController';
import firebase from 'react-native-firebase'
import { Rating } from 'react-native-ratings';

var labels = ["Preparando envío", "En camino", "Entregado"];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 40,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#15d2bb',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#15d2bb',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#15d2bb',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#15d2bb',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#15d2bb',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#15d2bb'
}



var { width } = Dimensions.get('window');
navigation = "";
params = "";
var order;
var params;
export default class OrderDetail extends Component {

  static navigationOptions = ({ navigation, navigationOptions }) => {
    params = navigation.state.params;
    return {
      title: 'Detalle de orden',
    };
  };
  constructor(props) {
    super(props);
    navigation = props.navigation;
    params = navigation.state.params;

    this.state = {
      currentPosition: 0,
      labels,
      order: null,
    }

  }

  _keyExtractor = (item, index) => item.order_detail_id;

  render() {
    if (this.state.order == null) {
      return null;
    }
    return <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>

      {(this.state.currentPosition != 3) ?
        <View style={{ marginVertical: 10 }}>
          <StepIndicator
            customStyles={customStyles}
            stepCount={3}
            currentPosition={this.state.currentPosition}
            labels={this.state.labels}
          />
          {(params.flag != undefined) ? <View style={{ marginTop: 10, marginHorizontal: 15, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 16, color: '#15d2bb' }}>Tu orden fue procesada.</Text>
            <Text style={{ fontSize: 14, textAlign: 'center', color: '#15d2bb' }}>En un momento PLAC se comunicará contigo vía telefónica, para confirmar tu compra, Gracias 😃🚚</Text>
          </View> : null}
        </View> :
        <View style={{ height: 40, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: 'rgb(254, 82, 108)' }}>La orden ha sido cancelada</Text>
        </View>
      }

      {this.getViewOrderInfo()}
      {this.getViewAssessment()}
      {this.getViewDetail()}
      {this.getViewTotal()}

    </ScrollView>

  }

  goAssessment(order) {
    params.order = order;
    params.assessmentCallback=this.assessmentCallback;
    this.props.navigation.navigate('Assessment', params);
  }


  assessmentCallback=(assessment)=>{
    let order= this.state.order;
    order.assessment=assessment;
    this.setState({
      order
    });
  
  }

  getViewOrderInfo() {
    let order = this.state.order;
    return (<View style={{ flex: 2, marginTop: 5, flexDirection: 'row', backgroundColor: '#F7F7F7' }}>
      {this.getViewAddress()}
      <View style={[styles.infoBox, { marginLeft: 2.5, flex: .8, padding: 10, }]}>

        <Text style={{ fontSize: 13, color: 'black', 'fontWeight': 'bold', }}>
          Fecha de orden </Text>
        <Text style={styles.textItem}>
          {order.created_at}
        </Text>
        <Text style={{ fontSize: 13, color: 'black', 'fontWeight': 'bold', }}>
          Orden</Text>
        <Text style={styles.textItem}>
          #{order.order_id}
        </Text>

      </View>
    </View>);
  }

  getViewAssessment() {
    let order = this.state.order;
    console.warn(order.assessment);

    if (order.assessment != null) {
      return <View style={{ backgroundColor: '#F7F7F7', }}>
        <View style={[styles.infoBox, { flexDirection: 'row', padding: 10, }]}>
          <View style={{ flex: .5, justifyContent: 'center', alignItems: 'flex-start' }}>
            <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 13, }}>Valoración</Text>
          </View>
          <View style={{ flex: 1.5, justifyContent: 'flex-end', alignItems: 'center', flexDirection: 'row' }}>
            <Text allowFontScaling={false} style={{ color: "#15d2bb", fontWeight: 'bold', fontSize: 13 }}>{order.assessment.assessment_type}  </Text>

            <Rating
              type="star"
              fractions={1}
              startingValue={order.assessment.assessment_quantity}
              readonly
              imageSize={16}
              style={{ alignItems: 'flex-start', justifyContent: 'flex-start', paddingVertical: 0 }} />

          </View>
        </View>
      </View>
    } else {
      if (this.state.currentPosition == 2) {
        return <View style={[styles.infoBox, {flexDirection: 'row', padding: 10, }]}>
          <View style={{ flex: 1 ,justifyContent:'center',alignItems:'center'}}>
            <Text>
              ¿Como estuvo la compra?
            </Text>
          </View>
          <TouchableOpacity onPress={ ()=>this.goAssessment(order)} style={{ width:'40%', borderRadius: 8, backgroundColor: '#f1c40f', height: 32, justifyContent: 'center', alignItems: 'center' }} >
            <Text style={{ fontSize: 14, color: 'white' }}>Valorar</Text>
          </TouchableOpacity>
        </View>
      }


    }

  }

  getViewDetail() {
    let orderDetails = this.state.order.order_details;
    return (<View style={{ flexDirection: 'column', margin: 5, backgroundColor: 'white' }}>
      <View style={{ flex: 4, flexDirection: 'row', borderBottomWidth: .5, borderColor: 'rgba(133,133,133,.5)', paddingBottom: 1, }}>
        <View style={[styles.viewDetail, { flexDirection: 'row', flex: 1.7, }]}><Text style={styles.title}>Producto</Text></View>
        <View style={styles.viewDetail}><Text style={styles.title}>Precio</Text></View>
        <View style={styles.viewDetail}><Text style={styles.title}>Cantidad</Text></View>
        <View style={styles.viewDetail}><Text style={styles.title}>Total</Text></View>
      </View>
      <View style={{ flex: 4, flexDirection: 'row', marginTop: 10 }}>
        <FlatList
          data={orderDetails}
          style={{ marginTop: 10, }}
          extraData={this.state}
          initialNumToRender={6}
          removeClippedSubViews={true}
          keyExtractor={this._keyExtractor}
          renderItem={(object) => this.renderItem(object)}
        />
      </View>
    </View>)
  }


  renderItem(object) {
    let orderDetail = object.item;
    let product = orderDetail.product;


    let productImage = JSON.parse(product.product_images)[0];
    let productPrice = formatPrice(orderDetail.order_price);
    let orderDetailTotal = formatPrice(orderDetail.order_total);
    let orderDetailQuantity = orderDetail.order_quantity;
    return (<View style={{ flexDirection: 'row' }}>

      <View style={[styles.viewDetail, { flexDirection: 'row', flex: 1.7, }]}>
        <View style={{ flex: .5, }}>
          <Image resizeMode={'contain'} style={{ width: 45, height: 45 }} source={{ uri: productImage.url }} />
        </View>
        <View style={{ flex: 1.5, }}>
          <Text style={[styles.titleDetail, { marginLeft: 3, fontSize: 10 }]}>{product.product_name}</Text>
        </View>

      </View>
      <View style={styles.viewDetail}>
        <Text style={styles.titleDetail}>
          ${productPrice}
        </Text>
      </View>
      <View style={styles.viewDetail}>
        <Text style={styles.titleDetail}>
          {orderDetailQuantity}
        </Text>
      </View>
      <View style={styles.viewDetail}>
        <Text style={styles.titleDetail}>
          ${orderDetailTotal}
        </Text>
      </View>
    </View>);

  }

  getViewAddress() {
    let order = this.state.order;
    let shippingAddress = order.shipping_address;

    let address = shippingAddress.address_formatted;
    let telephone = shippingAddress.plac_user_telephone;
    let neighborhood = shippingAddress.plac_user_neighborhood;
    let city = shippingAddress.city.city_name;

    let orderDeliveryStatus = order.delivery_status;
    let productDeliveryStatus = orderDeliveryStatus.status.es;
    let colorDeliveryStatus = orderDeliveryStatus.color;


    return (<View style={[styles.infoBox, { marginRight: 2.5, flex: 1.2, padding: 8 }]}>

      <Text style={{ fontSize: 13, color: 'black', 'fontWeight': 'bold', }}> Entrega </Text>
      <Text style={[styles.textItem, { color: colorDeliveryStatus }]}>
        {productDeliveryStatus}
      </Text>
      <Text style={styles.textItem}>
        {address}
      </Text>
      <Text style={styles.textItem}>
        {telephone}
      </Text>
      <Text style={styles.textItem}>
        {neighborhood}, {city}
      </Text>
    </View>);

  }

  getViewAddressSelected(order) {

    var addressSelected = order.shipping_address;
    if (addressSelected != null) {
      // VERIFY IF THE PRODUCT CAN TO BE  SENT TO CITY
      return (<AddressRow addressSelected={addressSelected}></AddressRow>);
    }
  }
  getCoupon(message, discount) {
    let order = this.state.order;
    let couponDetail = order.coupon_detail;
    if (couponDetail != null) {
      return this.getViewValue('Cupón (' + message + ')', discount, "-");
    }

  }

  getViewTotal() {
    let order = this.state.order;
    //Price order
    let orderResumed = order.order_resumed;
    let total = orderResumed.total;
    let subTotal = orderResumed.subTotal;
    let shippingPrice = orderResumed.shipping_price;
    let coupon = orderResumed.coupon;
    let message = "";
    let discount = 0;
    if (coupon != null) {
      message = coupon.message + "" + coupon.couponCode;
      discount = coupon.discount;
    }
    return (<View style={{ margin: 10, marginTop: 20 }}>
      {this.getViewValue('Subtotal', subTotal)}
      {this.getViewValue('Envío', shippingPrice)}
      {this.getCoupon(message, discount)}
      {this.getViewValue('Total', total)}
    </View>);
  }

  getViewValue(title, value, simbol = "") {
    var fontSize = 13;
    var marginTop = 2;
    var style = {};
    if (title == 'Total') {
      fontSize = 14;
      marginTop = 5;
    }
    return <View style={{ flexDirection: "row", marginTop: marginTop }}>
      <View style={{ flex: .7, marginLeft: 10, alignItems: 'flex-end' }}><Text style={{ fontSize: fontSize, fontWeight: 'bold', color: 'black', fontFamily: 'Roboto-Regular', marginRight: 20 }}>{title}</Text></View>
      <View style={[{ flex: .3, justifyContent: 'center', marginLeft: 5, alignItems: 'flex-end' }, style]}><Text style={{ fontSize: fontSize, fontWeight: 'bold', color: 'black', fontFamily: 'Roboto' }}>{simbol}${formatPrice(value)}</Text></View>
    </View>
  }

  fetchOrderDetail() {
    let placUser = getCurrentUser();
    let placUseId = placUser.plac_user_id;
    let orderId = params.order.order_id;

    let body = {
      order_id: orderId,
      plac_user_id: placUseId,
    }
    Api.post(Routes.URL_GET_ORDER_USERS, body)
      .then((response) => {
        console.warn(response);
        let { data, status, message } = response;
        if (status == "success") {
          let order = data[0];
          let position = order.delivery_status.position;
          this.setState({
            order,
            currentPosition: position
          });
        } else {
          Alert.alert("Ha ocurrido un problema", ex.message);
        }

      }).catch((ex) => {
        console.warn(ex.message);
      });
  }



  componentDidMount() {
    this.fetchOrderDetail();
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingTop: 20,
  },
  infoBox: {
    borderRadius: 2,
    flex: 1,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    margin: 5,
    backgroundColor: 'white'
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto',
    fontWeight: '600',
    color: 'black',
    fontSize: 13,
  },
  titleDetail: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    fontFamily: 'Roboto',
    color: 'black',
    fontWeight: '400',
    fontSize: 12,
  },
  viewDetail: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textItem: {
    fontSize: 11,
    marginTop: 3,
    color: 'black',
    'fontWeight': '400',
  },

});





AppRegistry.registerComponent('OrderDetail', () => OrderDetail);
