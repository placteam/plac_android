import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ListView,
  TouchableOpacity,
  StatusBar,
  Image,
  Dimensions,
  RefreshControl,
  View } from 'react-native';

  //Dimensions device
  var {height, width} = Dimensions.get('window');

  export default class RowCity extends Component {
    constructor(props) {
     super(props);
     this.state={
       buttonCity:styles.radioButtonNormal,
       city:this.props.city,
     }

   }



   render() {

     var city=this.props.city;
      var style=styles.radioButtonNormal;
      if(city.state=='active'){
        style=styles.radioButtonPress;
      }
     return(<TouchableOpacity onPress={()=>this.onPressCity(city)} style={{height:35,flexDirection:'row',width:width-130}}>
                 <View style={{flex:1.2,justifyContent:'center',alignItems:'flex-start',}}>
                   <Text allowFontScaling={false}style={{marginLeft:5,color:'black',fontWeight:'300',fontSize:14,}}>{city.city_name}</Text>
                 </View>
                <View style={{flex:.8,justifyContent:'center',alignItems:'flex-end',marginRight:5,}}>
                   <TouchableOpacity onPress={()=>this.onPressCity(city)} style={style}></TouchableOpacity>
               </View>
           </TouchableOpacity>
           );
  }



  onPressCity(city){
    this.props.citySelected(city);
  }





  }

  const styles = StyleSheet.create({

    radioButtonNormal:{
      width:20,
      height:20,
      borderWidth:.5,
      borderRadius:10,
      marginRight:5,
    },
    radioButtonPress:{
      width:20,
        marginRight:5,
      height:20,
      borderWidth:1,
      borderColor:'#15d2bb',
      backgroundColor:'#15d2bb',
      borderRadius:10,
    }

  });


  AppRegistry.registerComponent('RowCity', () =>RowCity);
