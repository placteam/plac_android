import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
  Dimensions,
  View } from 'react-native';


  import  RowCity from './RowCity'

  var {height, width} = Dimensions.get('window');
  
  var cities=[
    {city_id:2257,city_name:"Bogota",state:'inactive'},
    {city_id:2256,city_name:"Medellin",state:'inactive'}
  ]

  import * as PlacUserController from '../../../../app/controllers/PlacUserController'
  var placUser;

  export default class PickCity extends Component {
    constructor(props) {
     super(props);
     placUser= PlacUserController.getCurrentUser();
     this.state={
       tabSelected:0,
       cities,
       citySelected:null,
     }

   }


   _keyExtractor= (item, index) => item.city_id.toString();
   render() {
     return (
       <View   elevation={2} style={{margin:2,justifyContent:'center',alignItems:'center',width:width-80,borderWidth:.3,borderColor:'grey',borderRadius:4,backgroundColor:'white'}} >
          <Text allowFontScaling={false} style={{fontSize:20,marginTop:15,color:'black',fontWeight:'bold'}}>Selecciona tu ciudad</Text>
          <Text allowFontScaling={false} style={{textAlign:'center',fontSize:12,marginBottom:20,marginTop:10,marginLeft:20,marginRight:20,color:'black',fontWeight:'400'}}>Te mostraremos las tiendas que venden productos y servicios para tu mascota.</Text>

                      <View  style={{height:250,}}>
                          <FlatList
                            data={this.state.cities}
                            enableEmptySections={true}
                            renderItem={this.renderRow}
                            initialNumToRender={5}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={this._keyExtractor}
                            ItemSeparatorComponent={()=><View style={styles.separator}/>}
                        
                          />
                      </View>

               <View>

                   <View style={{height:30,marginTop:20,borderRadius:3,marginBottom:20,}}   elevation={2} >
                      <TouchableOpacity onPress={()=>this.saveCity()} style={styles.continueButton} >
                            <Text allowFontScaling={false}style={styles.continueText}>Continuar</Text>
                      </TouchableOpacity>
                   </View>

               </View>
     </View>
   );
  }


  renderRow=(object)=>{
        city=object.item;
        return (<RowCity citySelected={(city)=>this.citySelected(city)} city={city}/>);
  }

  citySelected(city){
      var cities=this.state.cities;
      for(i=0;i<cities.length;i++){
        if(cities[i].city_id==city.city_id){
          cities[i].state='active';
        }else{
          cities[i].state='inactive';
        }
      }
      this.setState({
        cities,
        citySelected:city,
      });
  }

  componentDidMount(){
    var citySelected=null;
    for(i=0;i<cities.length;i++){
      if(cities[i].state=='active'){
        citySelected=cities[i];
      }
    }
     this.setState({
       cities,
       citySelected,
     });
  }



  saveCity(){

    var citySelected=this.state.citySelected;
    if(citySelected==null){
       return alert('Por favor selecciona una ciudad para continuar.');
    }
    var city=JSON.stringify(citySelected);

    var placUser2={
          ...placUser,
          plac_user_city:city,
    }

    PlacUserController.update(placUser2);
    this.props.citySelected(citySelected);

  }



  }

  const styles = StyleSheet.create({
    container: {
      justifyContent:'center',
      borderRadius:3,
      borderWidth:.4,
      borderColor:'grey',
      alignItems:'center',
      backgroundColor:'white',
      flexDirection:"column",
    },
    continueButton:{
      width:width-130,
      height:30,
      borderRadius:3,
      backgroundColor:'#15d2bb',
      justifyContent:'center',
      alignItems:'center',
    },
    continueText:{
      fontSize:15,
      color:'white',

    },
    separator:{
      flex: 1,
      height: .5,
      marginLeft:5,marginRight:5,
      backgroundColor: '#8E8E8E',
    },

  });


  AppRegistry.registerComponent('PickCity', () =>PickCity);
