import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    FlatList,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Image,
    View
} from 'react-native';
var { width } = Dimensions.get('window');
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import { AirbnbRating } from 'react-native-ratings';
import { Keyboard } from 'react-native'

import Api from '../../../../lib/http_request'
import * as Routes from '../../../../app/routes'

import { formatPrice } from '../../../../lib/product';
import ButtonContinue from '../../../views/ButtonContinue';
var params;
var order;
var categories = [
    { category_name: "Precio", state: 0 },
    { category_name: "Calidad", state: 0 },
    { category_name: "Servicio", state: 0 },
    { category_name: "Tiempo", state: 0 }];
export default class Assessment extends Component {
    static navigationOptions = {
        title: "Valorar compra"
    }

    constructor(props) {
        super(props);
        params = props.navigation.state.params;
        this.state = {

            categories,
            categorySelected: '',
            message: '',
            rating: 0,
            isRated: false,
        };
        order = params.order;
    }

    _keyExtractor = (item, index) => item.category_name;

    render() {

        return (<View style={styles.container}>
            <View style={{ margin: 15 }}>
                <KeyboardAwareScrollView keyboardDismissMode="interactive"
                    keyboardShouldPersistTaps={'always'}>
                    {(!this.state.isRated) ? <View>
                        {this.getViewPlaceInfo()}
                        {this.getViewRatingStar()}
                        {this.getViewAssessmentType()}
                        {this.getViewReview()}
                        <ButtonContinue onPress={this.addAssessment} customStyle={{ marginTop: 20 }} isValidData={true} text={"Valorar"} />
                    </View> : this.getViewSuccess()}
                </KeyboardAwareScrollView>
            </View>

        </View>
        )
    }

    renderItem(object) {
        let category = object.item;
        let styleCategoryPress = {};
        let styleText = {};
        if (category.state == 1) {
            styleCategoryPress = { backgroundColor: '#15d2bb' };
            styleText = { color: 'white' }
        }
        return <TouchableOpacity onPress={() => this.onPressCategory(object)} style={[styles.categoryItem, styleCategoryPress]}>
            <Text style={[styleText]}>{category.category_name}</Text>
        </TouchableOpacity>
    }

    onPressCategory(object) {
        let category = object.item;
        var categories = this.state.categories;
        var categorySelected = "";
        for (i = 0; i < categories.length; i++) {
            if (category.category_name == categories[i].category_name) {
                categorySelected = category;
                categories[i].state = 1;
            } else {
                categories[i].state = 0;
            }
        }
        this.setState({ categories: categories, categorySelected });
    }

    getViewPlaceInfo() {
        let placeLocation = order.place_location;
        let paymentFull = order.order_payment_full;
        return (<View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{}}>
                <Image style={{ width: 40, height: 40 }} source={{ uri: placeLocation.place.path_image_logo }} />
            </View>
            <View style={{ marginLeft: 10 }}>
                <Text style={{ fontSize: 15, color: 'black' }}>{placeLocation.place_location_name}</Text>
                <Text style={{ fontSize: 12, }}>Pedido entregado ${formatPrice(paymentFull)}</Text>
            </View>
        </View>);
    }

    getViewAssessmentType() {
        return (<View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ textAlign: 'center', marginVertical: 20, marginBottom: 10 }}>¿Qué destacarías de esta compra?</Text>
            <FlatList
                data={this.state.categories}
                extraData={this.state}
                initialNumToRender={4}
                numColumns={2}
                removeClippedSubViews={true}
                keyExtractor={this._keyExtractor}
                renderItem={(object) => this.renderItem(object)} />
        </View>);
    }

    getViewRatingStar() {
        return (<View style={{ padding: 10, }}>
            <AirbnbRating
                count={5}
                reviews={["Terrible", "Mala", "Buena", "Muy buena ", "Increible"]}
                defaultRating={0}
                size={30}
                onFinishRating={this.ratingCompleted}
            />
        </View>);
    }

    ratingCompleted = (rating) => {
        this.setState({ rating });
    }

    getViewReview() {
        return (<View style={{
            marginTop: 20,
            borderBottomColor: '#000000',
            backgroundColor: 'rgb(247,247,247)',
            borderRadius: 2,
        }}>
            <TextInput
                ref={(input) => { this.review = input; }}
                autoGrow={true}
                editable={true}
                returnKeyType={'done'}
                onSubmitEditing={() => Keyboard.dismiss()}
                placeholder={'Escribe tu comentario aqui (opcional)'}
                underlineColorAndroid={'rgb(247,247,247)'}
                style={{ textAlignVertical: "top" }}
                multiline={true}
                numberOfLines={5}
                onChangeText={(message) => this.setState({ message })}
                value={this.state.message}
            />
        </View>);
    }

    getViewSuccess() {
        return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', margin: 20, }}>
            <Text style={styles.textMessageShared}>Muchas gracias por compartir tu experiencia con nosotros </Text>
            <TouchableOpacity onPress={() => this.goBack()} style={styles.buttonBack}>
                <Text style={{ fontSize: 13, color: 'white', fontWeight: 'bold' }}>Regresar</Text>
            </TouchableOpacity>
            <View style={{ marginBottom: 80 }} />
        </View>
    }


    goBack() {
        this.props.navigation.goBack();
    }


    addAssessment = () => {
        if (this.state.rating == 0) {
            return alert("Selecciona la valoración");
        }

        if (this.state.categorySelected == "") {
            return alert("Selecciona una categoría destacada");
        }

        var assessment = {
            assessment_type: this.state.categorySelected.category_name,
            assessment_quantity: this.state.rating,
            assessment_message: this.state.message,
            order_id: order.order_id,
            plac_user_id: order.shipping_address.plac_user_id,
        }

        var body = {
            assessment
        }

        Api.post(Routes.URL_ORDER_ASSESSMENT, body)
            .then((response) => {
                params.assessmentCallback(response.data);
                this.setState({ isRated: true, assessment: response.data });
            }).catch((ex) => {
                console.warn(ex);
            });

    }

    componentDidMount() {

    }

}




const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    buttonRatingCompany: {
        borderWidth: 1,
        borderColor: "#fff",
        borderRadius: 4,
        padding: 10,
        height: 30,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#15d2bb"
    },
    categoryItem: {
        justifyContent: "center",
        alignItems: "center",
        height: 28,
        borderRadius: 4,
        borderWidth: 1,
        margin: 5,
        borderColor: "#15d2bb",
        backgroundColor: "white",
        width: width / 2 - 60,
    },
    categoryItemPress: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#15d2bb",
        height: 28,
        margin: 5,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: "#FFD785",
    },
    buttonConfirm: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        borderColor: "#15d2bb"
    },
    buttonSendReview: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        backgroundColor: 'orange',
        borderRadius: 4,
    },
    textMessageShared: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'black',
        textAlign: 'center',
        margin: 10,
    },
    buttonBack: {
        height: 30,
        marginTop: 10,
        width: width / 2.6,
        justifyContent: 'center',
        borderRadius: 4,
        alignItems: 'center',
        backgroundColor: '#15d2bb'
    }

});


AppRegistry.registerComponent('Assessment', () => Assessment);
