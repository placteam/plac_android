import React, { Component } from 'react';
import {

  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Modal,
  FlatList,
  ScrollView
} from 'react-native';

import { CachedImage } from 'react-native-img-cache';

import *  as ProfileController from '../../../app/controllers/ProfileController'
import * as PlacUserController from '../../../app/controllers/PlacUserController'

import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'

import ToolbarStore from '../../views/ToolbarStore'
import PetTypeListView from '../../views/pet/PetTypeListView';
import {search} from '../../../assets/image/svg'

import Swiper from 'react-native-swiper';
import { getProportionalImageHeight} from '../../../lib/image';
import PickCity from './city/PickCity';
import CartGeneral from './views/CartGeneral';
import CardView from 'react-native-cardview';


const { width,height} = Dimensions.get('window')
categoryImg={width:672,height:120};
var params={};

export default class Store extends React.Component {
  static navigationOptions = {
    header:null
  }

  constructor(props){
    super(props);

    placUser=PlacUserController.getCurrentUser();
    profile=ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id)
    this.state={
      swiperIndex:0,
      profile,
      productCategories:[],
      petTypeSelected:profile.pet_type,
      petTypeSelectedObject:{},
      ads:[],
      citySelected:2257,
      modalCityVisible:false,
    }
  }

  render() {
    console.warn(profile.pet_type);
   
    return (
      <View style={styles.container}>
          <ToolbarStore   placUser={placUser} navigation={this.props.navigation} />
  
          {this.getAdSlider()}
          <View    
                style={{   shadowOpacity: 0.75,
                  shadowRadius: 5,
                  shadowColor: 'red',
                  shadowOffset: { height: 1, width: 1 },marginHorizontal:15,marginTop:10,marginVertical:5}}
               >
                <TouchableOpacity  onPress={()=>{
                    params.citySelected=this.state.citySelected;
                    params.petTypeSelected=this.state.petTypeSelected;
                    this.props.navigation.navigate('ProductsSearch',params);
                  }} 
                  style={styles.viewSearch} 
                  activeOpacity={.8}>
                    {search()}
                    <Text style={{color:'#aaaaaa',marginLeft:5,fontSize:16}}>Buscar productos para 
                       {(this.state.petTypeSelectedObject.id==profile.pet_type)?" "+ profile.profile_name:" "+this.state.petTypeSelectedObject.name}
                    </Text>
                </TouchableOpacity>
          </View>
          <ScrollView showsVerticalScrollIndicator={false} style={{marginHorizontal:15,marginBottom:10}}>

             <PetTypeListView
                marginBoxHorizontal={15}
                profile={this.state.profile}
                petTypeSelected={this.petTypeSelected}
              />
            
              {this.getCategories()}
          </ScrollView>


          {this.getViewCartGeneral()}
      
        
      </View>
    );
  }

  getViewCartGeneral(){
    return <CartGeneral  isOpenCart={()=>{}} petType={this.state.petTypeSelected}  city={this.state.citySelected}  navigation={this.props.navigation}/>

  }




 
  

  getAdSlider(){
        if(this.state.ads.length>0){
         let  imageHeight=getProportionalImageHeight({width:1308,height:510},width);

          return <View style={{height:imageHeight}}>
                      <Swiper
                        index={0}
                        dot={<View style={{backgroundColor: 'rgba(255,255,255,.5)', width: 10, height: 10, borderRadius: 5, marginHorizontal:2}} />}
                        activeDot={<View style={{backgroundColor: '#15d2bb', width: 10, height: 10, borderRadius: 5,marginHorizontal:2}} />}
                        paginationStyle={{
                          bottom: 5
                        }}
                        loop={false}>
                            {this.state.ads.map((ad) => {
                              var adImages=JSON.parse(ad.ad_images);
                              var adImage=adImages[0];
                              return <TouchableOpacity activeOpacity={.8} onPress={()=>{this.goDestinationAd(ad)}} key={ad.ad_id} style={{width}}>
                                        <CachedImage    source={{uri:adImage.url}} style={{height:imageHeight,width}}/>
                                      </TouchableOpacity>
                            })}
                      </Swiper>
                </View>
        }
   
  }

  goDestinationAd(ad){
    let action=JSON.parse(ad.ad_action);
    let {route,destination}=action;
    let params={
     place_id:route,
    }
 
    switch(destination){
       case "tienda":
         this.props.navigation.push('Place',params);
       break;
       case "producto":
         let product={
           product_id:route,
         }
         params.product=product;
         this.props.navigation.push('Product',params);
       break;
       case "info":
 
       break;
    }
   
  
    

  }

  _keyExtractor = (item, index) => item.category_id;
  getCategories(){
    return  <FlatList
              style={{flex:1,marginTop:5,}}
              data={this.state.productCategories }
              extraData={this.state}
              keyExtractor={this._keyExtractor}
              renderItem={(object)=>this._renderCategoryItem(object)}
            />
   
  }

  _renderCategoryItem(object){
    let category=object.item;
    let itemHeight=getProportionalImageHeight(categoryImg,(width-30));
    
    return <CardView 
              style={{margin:2,marginBottom:10,}}
              cardElevation={2}
              cardMaxElevation={2}
              cornerRadius={8}> 
              <TouchableOpacity  onPress={()=>this.goPlacesByCategory(category)}  style={[{height:itemHeight}]}>
                      <CachedImage  source={{uri:category.category_img_url_app}} style={{height:itemHeight,width:(width-30),borderRadius:8}}/>
                      <View style={{position:'absolute',height:60,top:10,left:10,width:width-30}}>
                            <Text style={{fontSize:16,color:'black',fontFamily:'bold'}}>{category.category_name}</Text>
                            <Text style={{fontSize:14,color:'#555555'}}>{category.category_additional_text}</Text>
                      </View>
         
                </TouchableOpacity>
          </CardView>

  }

  goPlacesByCategory(category){
    params.category=category;
    params.petTypeSelected=this.state.petTypeSelected;
    params.citySelected=this.state.citySelected;
    this.props.navigation.navigate('PlacesByCategory',params);

  }

  petTypeSelected=(petTypeSelected)=>{
    console.warn(petTypeSelected);
    
    this.setState({
      petTypeSelected:petTypeSelected.name,
      petTypeSelectedObject:petTypeSelected
    },()=>{
      this.fetchPromotionsAds()
    });

  }

  fetchPromotionsAds(){
    URL=Routes.URL_GET_STORE_PROMOTIONS_ADS;
    var body={
        pet_type:this.state.petTypeSelected,
        city_id:this.state.citySelected,
    };
    Api.post(URL,body)
       .then((response)=>{
        var {data,status,message}=response;
        if(status=="success"){
          this.setState({
            ads:data
          });
        }
      
       }).catch((ex)=>{
 
       });
  }

  fetchCategories(){
    URL=Routes.URL_RESOURCE_PRODUCT_CATEGORIES;

    Api.get(URL)
       .then((response)=>{
         var {data,status,message}=response;
         if(status=="success"){
            this.setState({
              productCategories:data
            });
         }
       
       }).catch((ex)=>{
 
       });
  }



  
  
  componentDidMount(){
    this.fetchCategories();
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  viewSearch: {
    height: 42,
    borderRadius: 8,
    flexDirection:'row',
    paddingLeft:10,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor:'white',
    borderWidth: .5,
    borderColor: '#15d2bb',
  
  },
  viewCategoryItem:{
    marginBottom:10,
  },

  modalCenterxy:{
    flex:1,
    height:20,
    justifyContent: 'center',
    alignItems: 'center',
},
  

});
