import React, {PureComponent  } from 'react';

import {
  StyleSheet,
  View,
  TextInput,
  Image,
  Text,
  Alert,
  FlatList,
  TouchableOpacity,

  
} from 'react-native';


import ListViewPagination from '../../views/ListViewPagination';
import * as Routes from '../../../app/routes';
import * as PlacUserController from '../../../app/controllers/PlacUserController'
//API
import Api from '../../../lib/http_request';
import ProductQuestionRow from './views/ProductQuestionRow';

import { CachedImage } from 'react-native-img-cache';
import { getOneProductImage, formatPrice } from '../../../lib/product';
import { getProportionalImageWidth, getUrlImageCompress } from '../../../lib/image';

var placUser;
var params;
var urlQuery=Routes.URL_GET_PRODUCT_QUESTIONS_BY_ME;

export default class ProductQuestionsByMe extends PureComponent {

  static navigationOptions ={
    title:"Preguntas realizadas"
  }


  constructor(props){
    super(props);
    params=this.props.navigation.state.params;
    placUser=PlacUserController.getCurrentUser();
    this.state={
      question:'',
      heightTI:48,
      products:[],
      isLoading:false,
      paginate:null,
      isValidData:true,
      url_query:urlQuery+placUser.plac_user_id,
      arrayUrls:[],
    }

  }
  _keyExtractor = (item, index) => item.product_id;
  render() {

   
      return <View style={styles.container}>
                <View  style={{marginHorizontal:15,flex:1}} >
                      <ListViewPagination
                          isSeparatorActive={false}
                          initialNumToRender={5}
                          methodRequest={"GET"}
                          data={this.state.products}
                          isLoading={this.state.isLoading}
                          loadMore={this.state.loadMore}
                          paginate={this.state.paginate}
                          keyExtractor={this._keyExtractor}
                          onRefresh={this.onRefresh}
                          renderRow={this.renderRow}
                          handleLoadMore={this.handleLoadMore}
                          arrayUrls={this.state.arrayUrls}
                      />

                </View>
             </View>
   
          
  }

  onRefresh=()=>{
    this.setState({
      url_query:urlQuery+placUser.plac_user_id,
      isLoading:true,
      products:[],
      arrayUrls:[],
    },()=>{
      this.getProductQuestions()
    });
  }

  handleLoadMore=(np ,arrayUrls)=>{
    this.setState({
      url_query:urlQuery+placUser.plac_user_id+np,
      loadMore:true,
      arrayUrls,
    },()=>{
      this.getProductQuestions();
    });
  }

  _keyQuestionExtractor = (item, index) => item.question_id;
  renderRow=(object)=>{
    let product=object.item;
    let productImage=getOneProductImage(product);
    let imageWidth= getProportionalImageWidth(productImage.width,productImage.height,36);
    return <View style={{marginVertical:10}}>
             <TouchableOpacity onPress={()=>this.goProduct(product)} style={{alignItems:'center',flexDirection:'row',borderBottomWidth:1 , borderColor:'#aaaaaa', paddingBottom:10}}>
                <View style={{width:36,justifyContent:'center',alignItems:'center'}}>
                
                <CachedImage style={{width:imageWidth,height:36}} source={{uri:getUrlImageCompress(productImage.url,20)}}/>

                </View>
                <View>
                <Text style={{marginLeft:5,fontSize:16,color:'black'}}>{product.product_name}</Text>
                <Text style={{fontSize:16,color:'black'}}>${formatPrice(product.price_final)}</Text>
                </View>
             </TouchableOpacity>

             <FlatList
             data={product.questions}
             renderItem={(object)=>this.renderProductQuestionRow(object,product)}
             initialNumToRender={10}
             keyExtractor={this._keyQuestionExtractor}

  
             />

          
           </View>

  }

  renderProductQuestionRow=(object,product)=>{
   let  placeLocation= product.place_location;
   return  <ProductQuestionRow from={'me'}  placUser={placUser} placeLocation={placeLocation}  object={object}/>
  }


  goProduct(product){
    params.product=product;
    this.props.navigation.push('Product',params);
  }


  

  getProductQuestions(){
    console.warn(this.state.url_query);
    
      Api.get(this.state.url_query)
      .then((response)=>{
      let {data,status,message}=response;
      if(status=="success"){
        this.manageResponse(data);
      }else{
          Alert.alert("Ha ocurrido un problema",message);
      }
      this.setState({isLoading:false});
      
      }).catch((ex)=>{
        Alert.alert("Ha ocurrido un problema",ex.message);
        this.setState({isLoading:false});
        
      });
  }

  componentWillUnmount(){
    this.setState({url_query:urlQuery});
  }

  manageResponse(response){
    const {data}=response;
    let products= this.state.products;
 
    for(i=0;i<data.length;i++){
       products.push(data[i]);
    }

    this.setState({
      products,
      isSearching:false,
      paginate:response,
      isLoading:false,
      loadMore:false,
    });
}

  componentWillMount(){

      this.getProductQuestions();

  

  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textInputQuestion:{
    height:48,
    paddingLeft: 10,
    borderRadius:8,
    borderWidth:1,
    width:'100%',
    borderColor:'#00b99b'
  }
  

 

});
