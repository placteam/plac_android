import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList,
    Alert,
    Image,
    Modal,
    Dimensions,
    ScrollView,
    TextInput,
    View
} from 'react-native';

// API
import Api from '../../../lib/http_request/';
import * as Routes from '../../../app/routes'

var { width, height } = Dimensions.get('window');
import { getResumedCheckoutByPlaceId, deleteOrderDetailsByPlaceId ,  } from '../../../app/controllers/OrderDetailController';


import ProductInCartRow from './views/ProductInCartRow';
import realm from '../../../config/realm';

import ButtonContinue from '../../views/ButtonContinue';
import CardView from 'react-native-cardview';

import { formatPrice } from '../../../lib/product';
import PaymenMethods from './views/PaymentMethods';





var place;

export default class Checkout extends Component {

    static navigationOptions = {
        title: 'Realizar compra'
    }

    constructor(props) {
        super(props);
        params = props.navigation.state.params;

        place = params.place;
        this.state = {
            products: [],
            total: 0,
            place,
            addressSelected: params.addressSelected,
            modalCouponVisible: false,
            isVisiblePaymentMethods: false,
            paymentMethodSelected: null,
            couponCode:"",
            paymentMethodsAvailable:[],
            messageAboutCoupon:null,
            messageStyle:{},
            messageTransaction:"",
            shipping_price:0,
            cityPrice:{},
            indexTransaction:-1,
            isSavingOrder:false,
            subTotal_tmp:0,
            shipping_price_tmp:0,
            total_tmp:0,
            coupon:null,     
        }

    }



    _keyProductExtractor = (item) => item.product_id;
    render() {

        return <View style={styles.container}>
            <ScrollView style={{ marginTop: 1, marginBottom: 200 }}>
                {this.getData()}
                <View
                    style={{ margin: 10, marginTop: 5, backgroundColor: 'white', borderRadius: 4, }} >
                    <View style={[styles.viewItem, { width: width - 4, padding: 10, borderBottomWidth: .2, borderBottomColor: '#COCOCO' }]}>
                        <View style={styles.viewItemTitle}>
                            <Text style={styles.textItemTitle}>Productos</Text>
                        </View>
                        <View style={styles.viewItemValue} />
                    </View>
                    <FlatList
                        style={{ marginTop: 10 }}
                        ItemSeparatorComponent={() => <View style={{ height: .4, width, marginVertical: 5, backgroundColor: '#c0c0c0' }}></View>}
                        data={this.state.products}
                        keyExtractor={this._keyProductExtractor}
                        renderItem={(object) => this.renderProductItem(object)}
                    />
                </View>
            </ScrollView>
            {this.getViewBriefPayment()}
            {this.getModalCoupon()}
        </View>

    }

    renderProductItem = (object) => {
        let productObject = object.item;
        return <ProductInCartRow navigation={this.props.navigation} place={place} productObject={productObject} />
    }


    getData() {

        let item = this.state.addressSelected;


        return <View>
            <CardView
                style={{ marginVertical: 5, marginBottom: 1, padding: 2 }}
                cardElevation={1}
                cardMaxElevation={1}
                cornerRadius={1}
            >
                <View style={[styles.viewItem, { width: width - 30, marginLeft: 15, borderBottomWidth: .2, borderBottomColor: '#COCOCO' }]}>
                    <View style={styles.viewItemTitle}>
                        <Text style={styles.textItemTitle}>Tu dirección de entrega</Text>
                    </View>
                    <TouchableOpacity onPress={() => { this.props.navigation.replace('UserAddress',params) }} style={styles.viewItemValue}>
                        <Text style={{ fontSize: 14, color: '#15d2bb' }}>Cambiar</Text>
                    </TouchableOpacity>
                </View>
                <View style={[{ width: '100%', padding: 15, paddingBottom: 10, paddingTop: 5, }]} >
                    <Text style={styles.textAddressInfo}>{item.plac_user_name}</Text>
                    <Text style={styles.textAddressInfo}>{item.address_full}</Text>
                    <Text style={styles.textAddressInfo}>{item.city.city_name}, Colombia</Text>
                    <Text style={styles.textAddressInfo}>+57 {item.plac_user_telephone}</Text>
                </View>

            </CardView>
            <CardView
                style={{ marginVertical: 1, padding: 2 }}
                cardElevation={1}
                cardMaxElevation={1}
                cornerRadius={1}
            >
                <View style={[styles.viewItem, { width: width - 30, marginLeft: 15 }, (this.state.isVisiblePaymentMethods) ? { borderBottomWidth: .2, borderBottomColor: '#COCOCO' } : {}]}>
                    <View style={styles.viewItemTitle}>
                        <Text style={styles.textItemTitle}>Metodo de pago</Text>
                    </View>

                    {(this.state.paymentMethodSelected == null) ?
                        <TouchableOpacity onPress={() => this.managePaymentMethods()} style={styles.viewItemValue}>
                            <Text style={{ fontSize: 14, color: '#15d2bb' }}>Escoger</Text>
                        </TouchableOpacity> :
                        <TouchableOpacity onPress={() => this.managePaymentMethods()} style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={styles.textMethodSelected}>{this.state.paymentMethodSelected.name}</Text>
                        </TouchableOpacity>}
                       
                </View>
                {this.getViewTransactionMessage(0)}
                {this.state.paymentMethodsAvailable.length>0? <PaymenMethods  paymentMethodsAvailable={this.state.paymentMethodsAvailable}  paymentMethodSelected={this.paymentMethodSelected} isVisiblePaymentMethods={this.state.isVisiblePaymentMethods} />:null}
        
            </CardView>
        </View>
    }

    managePaymentMethods() {
        let isVisiblePaymentMethods = this.state.isVisiblePaymentMethods;
        if (isVisiblePaymentMethods) {
            isVisiblePaymentMethods = false;
        } else {
            isVisiblePaymentMethods = true;
        }
        this.setState({ isVisiblePaymentMethods });
    }

    paymentMethodSelected = (paymentMethodSelected) => {
        this.setState({
            paymentMethodSelected,
            isVisiblePaymentMethods: false,
            indexTransaction:-1 ,
        });
    }

    getViewBriefPayment() {
       let paymentButtonText="Comprar";
       let paymentMethodSelected=this.state.paymentMethodSelected;
       if(paymentMethodSelected!=null){
        if(paymentMethodSelected.payment_method_id=='mercado_pago'){
            paymentButtonText="Pagar en linea";
           }else   if(paymentMethodSelected.payment_method_id=='payment_delivery'){
            paymentButtonText="Ordernar y pagar en casa";
            }
       }
       let coupon=this.state.coupon;
       

        return <View style={{ position: 'absolute', backgroundColor: 'white', width, bottom: 0,  borderRadius: 2, }}>
            <View style={{ flex: 1,paddingVertical:15,paddingVertical:10,justifyContent: 'center', alignItems: 'center' }}>
                <View style={[styles.viewItem, styles.viewBrief]}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.textItemTitle}>Subtotal </Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Text style={{ fontSize: 14, color: 'black' }}>  ${formatPrice(this.state.subTotal)} </Text>
                    </View>
                </View>
           
                
                
                <View style={[styles.viewItem, styles.viewBrief]}>
                    <View style={{ flex: 1.5}}>
                        <Text style={styles.textItemTitle}>Envío </Text>
                        {(coupon!=null)? 
                           (coupon.coupon_type=='free_shipping')? coupon.message:null
                            :null}
                        <Text  style={{fontSize:13}}>{this.state.cityPrice.description}</Text>
                    </View>
                    <View style={{ flex: .5, justifyContent: 'center', alignItems: 'flex-end' }}>

                        <Text style={{ fontSize: 14, color: 'black' }}> 
                            {(coupon==null)?"$"+formatPrice(this.state.shipping_price) :
                            "$"+formatPrice( coupon.resumed.shipping_price)
                            }
                     
                        </Text>

                    </View>
                </View>
                {this.getViewCoupon()}
                {(coupon==null)?<TouchableOpacity onPress={() => this.setState({ modalCouponVisible: true })} style={[styles.viewItem, { marginHorizontal: 40, height: 25, marginTop:5, justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }]}>
                    <Text style={{ fontSize: 13, color: '#15d2bb' }}>¿Tienes un cupo de descuento ?</Text>
                </TouchableOpacity>:
                <TouchableOpacity onPress={() => this.setState({ coupon: null,modalCouponVisible: true   })} style={[styles.viewItem, { marginHorizontal: 40, height: 25, marginTop:5, justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }]}>
                        <Text style={{ fontSize: 13, color: 'red' }}>Remover cupon X </Text>
                 </TouchableOpacity>
              }
            </View>
            <View style={{ flex: 1, paddingVertical:15,borderTopWidth: .2, justifyContent: 'center', alignItems: 'center' }}>
                <View style={[styles.viewItem, styles.viewBrief]}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.textItemTitle}>Total a pagar </Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Text style={{ fontSize: 14, color: 'black' }}>
                         {(coupon==null)?"COP $"+formatPrice(parseFloat(this.state.shipping_price)+ parseFloat(this.state.subTotal)):
                          "COP $"+formatPrice(coupon.resumed.total)
                         }
                         
                         </Text>
                    </View>
                </View>
                {this.getViewTransactionMessage(1,20,15)}
                {this.getViewTransactionMessage(2,20,15)}
                {(this.state.indexTransaction!=1)? <ButtonContinue  onPress={()=>this.store()} customStyle={{ width: width - 30, marginHorizontal: 15, marginTop: 15, }} isValidData={true} title={"Comprar"} text={paymentButtonText} />:null}
            </View>
        </View>
    }


    getViewCoupon(){
    let coupon=this.state.coupon;
       if(coupon!=null){
        return <View style={[styles.viewItem, styles.viewBrief]}>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.textItemTitle}>Cupon</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end'}}>
                        <Text style={{ fontSize: 14, color: 'black' }}>{coupon.coupon_code}</Text>
                        <Text style={{ fontSize: 14, color: 'black' }}>{coupon.resumed.message}</Text>
                    </View>
                </View>
       }
       
    }

    getModalCoupon() {
        return <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalCouponVisible}
                onRequestClose={() => this.closeModalCoupon()}>
                    <TouchableOpacity activeOpacity={1} onPress={()=>this.closeModalCoupon()}   style={{ justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: 'rgba(0,0,0,.5)' }}>
                        <CardView
                            style={{ margin: 5, marginBottom: 1, padding: 2, width: width - 120 }}
                            cardElevation={2}
                            cardMaxElevation={2}
                            cornerRadius={4}
                        >
                            <TouchableOpacity  activeOpacity={1}  disabled={false} style={{ width: width - 120, padding: 15, borderBottomColor: '#COCOCO', justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={{ uri: 'https://trello-attachments.s3.amazonaws.com/5b560097aaca7fd74a05bfe3/5bd9baf8a7c13907fbc88d6f/8c56a923fcd0e89f4b16723244405346/coupon_(3).png' }} style={{ width: 60, height: 60, }} />
                                <Text style={{ fontSize: 16, color: 'black', fontWeight: 'bold' }}>Cupon de descuento</Text>
                                <Text style={{ fontSize: 12, marginTop: 10, }}>Ingresa tu cupon de descuento</Text>
                                <TextInput
                                    placeholder={"Ej. TUCUPON"}
                                    style={{ height: 36, paddingVertical: 10, marginVertical: (this.state.messageAboutCoupon!=null)?0:10, textAlign: 'center', width: '80%', marginTop: 5, borderColor: 'gray', borderWidth: 1, borderRadius: 4, }}
                                    onChangeText={(couponCode) => this.setState({couponCode})}
                                    value={this.state.couponCode}
                                />
                                {this.getViewMessageAboutCoupon()}
                                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                    <ButtonContinue onPress={() => this.checkCoupon()} customStyleText={{ fontSize: 14 }} isValidData={true} customStyle={{ width: "60%", height: 32 }} text={"Aplicar"} />
                                </View>
                            </TouchableOpacity>
                        </CardView>
                    </TouchableOpacity>
              </Modal>
    }

    closeModalCoupon() {
        this.setState({ modalCouponVisible: false });
    }
    
    getViewMessageAboutCoupon(){
        if(this.state.messageAboutCoupon!=null){
            return <View style={{marginBottom:10,marginTop:5}}>

                        <Text style={this.state.messageStyle}>{this.state.messageAboutCoupon}</Text>
                  </View>
        }
    

    }

    getViewTransactionMessage(indexInput,margin=15,fontSize=12){
        let indexTransaction= this.state.indexTransaction;
        if(indexInput==indexTransaction){
            return <View style={{margin,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                    {(indexTransaction==1)?<Image  style={{width: 70,height: 26,}}   source={require('../../../assets/image/general/loader.gif')}/>:null}
                     <Text style={[this.state.messageStyle,{fontSize}]}>{this.state.messageTransaction}</Text>
                  </View>
        }
              
      }


    getResumedCheckout() {
        let  resumedCheckout = getResumedCheckoutByPlaceId(place.place_id);
        this.setState({
            products: resumedCheckout[0],
            subTotal:resumedCheckout[1],
        });
    }

   
    updateUI = (ad) => {
        setTimeout(() => {
            this.getResumedCheckout();
        }, 100);
    }

    checkCoupon() {
        let addressSelected=params.addressSelected;
        let {subTotal,shipping_price}=this.state;
        let coupon_code=this.state.couponCode;
        var body = {
            coupon_code:coupon_code.toUpperCase(),
            plac_user_id: addressSelected.plac_user_id,
            place_id: place.place_id,
            total:parseFloat(subTotal)+parseFloat(shipping_price),
            subTotal,
            shipping_price
        } 

     
      
        
        Api.post(Routes.URL_CHECK_COUPON, body)
            .then((response) => {
                let { data, status, message } = response;
                if (status == "success") {
                    this.updateOrderFromCoupon(data);
                } else {
                   this.setState({
                       messageStyle:styles.messageWarning,
                       messageAboutCoupon:message,
                   });
                }
            }).catch((ex) => {
                message=ex.message;
                if(ex.message=="Network request failed"){
                message="No tienes conexión a internet.";
                }
                this.setState({
                    messageStyle:styles.messageError,
                    messageAboutCoupon:message,
                });
               
            });
    }
    updateOrderFromCoupon(data){
        this.setState({coupon:data,modalCouponVisible:false,});
    }



    fetchStoreConfigurationByPlace(){
        let body={
            place_id: place.place_id,
            plac_user_city_id:params.addressSelected.city_id,
            product_type:(params.comingFromService==undefined)?"product":"service",
        }
        Api.post(Routes.URL_GET_STORE_CONFIGURATION, body)
        .then((response) => {
            let { data, status, message } = response;
            if (status == "success") {
               let cityPrice= data.city_price;
               let paymentMethodsAvailable= data.payment_methods_available;
               let shipping_price=cityPrice.price;
               this.setState({paymentMethodsAvailable,shipping_price,cityPrice});
            } else {
               this.setState({
                   messageStyle:styles.messageWarning,
                   messageTransaction:message,
               });
            }
        }).catch((ex) => {
            let message=ex.message;
            if(ex.message=="Network request failed"){
             message="No tienes conexión a internet.";
            }
            this.setState({
                messageStyle:styles.messageError,
                messageTransaction:message,
            });
           
        });
    }

    store(){
       let {products,subTotal,shipping_price,paymentMethodSelected,coupon}=this.state;
       if(paymentMethodSelected==null){
                this.setState({
                    indexTransaction:0,
                    messageTransaction:'Selecciona un metodo de pago para continuar la compra',
                    messageStyle:styles.messageWarning
                });
          return;
       }

       let payment_method_id=paymentMethodSelected.payment_method_id;
       let total=parseFloat(subTotal)+parseFloat(shipping_price);
       let subTotal_tmp=subTotal;
       let total_tmp=total;
       let shipping_price_tmp=shipping_price;
       if(coupon!=null){
            couponResumed=  coupon.resumed;
            total_tmp=couponResumed.total;
            subTotal_tmp=couponResumed.subTotal;
            shipping_price_tmp=couponResumed.shipping_price;
       }

       
       
        let body={
            order:{
                coupon,
                order_details:JSON.parse(JSON.stringify(products)),
                order_resumed:{
                    subTotal,
                    shipping_price,
                    total,
                    subTotal_tmp,
                    shipping_price_tmp,
                    total_tmp,
                },
                plac_user_shipping_address:params.addressSelected,
                payment_method_id,
                place_location:place,
                products_type:"products",
            }
        }

 

        this.setState({indexTransaction:1,messageTransaction:"Este pedido esta siendo procesado...",messageStyle:styles.messageSuccess});
        setTimeout(() => {
            Api.post(Routes.URL_RESOURCE_ORDERS, body)
            .then((response) => {
                let { data, status, message } = response;
                if (status == "success") {
                    deleteOrderDetailsByPlaceId(place.place_id);
                    setTimeout(() => {
                        let order=data;
                        params.order=order;
                        if(payment_method_id=='mercado_pago'){
                            this.props.navigation.replace('PaymentGateway',params);
                        }else{
                            params.flag="neworder";
                            this.props.navigation.replace('OrderDetail',params);
                        }
                    }, 200);
                  
                } else {
                    this.setState({indexTransaction:2,messageTransaction:message});
                }
            }).catch((ex) => {
                let message=ex.message;
                console.warn(message);
                if(ex.message=="Network request failed"){
                  message="No tienes conexión a internet.";
                }
                this.setState({
                    messageStyle:styles.messageError,
                    messageTransaction:message,
                    indexTransaction:2,
                });
               
            });
            
        }, 10000);
     


    }




    componentWillMount() {
        // Observe Realm Notifications
        realm.addListener('change', this.updateUI);
    }

    componentWillUnmount() {
        // ..later remove the listener
        realm.removeListener('change', this.updateUI);
    }

    componentDidMount() {
        this.getResumedCheckout();
        this.fetchStoreConfigurationByPlace();

    }


  

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
       


    },
    viewItem: {
        height: 40,
        flexDirection: 'row',
    },
    viewItemTitle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    viewItemValue: {
        flex: 1,

        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    textItemTitle: {
        fontSize: 14,
        color: 'black',
        fontWeight: '400',
    },
    textAddressInfo: {
        fontSize: 12,
        color: 'rgba(0,0,0,.6)'
    },
    textMethodSelected: {
        borderWidth: 1,
        paddingVertical: 6,
        paddingHorizontal: 6,
        borderRadius: 4,
        borderColor: '#15d2bb',
        fontSize: 14,
        color: '#15d2bb'
    },
    viewBrief: {
        marginHorizontal: 30,
        height: null,
        justifyContent: 'center',
        alignItems: 'center'
    },
    messageSuccess:{
        fontSize: 12,
        color: '#15d2bb',
      },
      messageWarning:{
        fontSize: 12,
        color: '#f0ad4e',
      },
      messageError:{
        fontSize: 12,
        color: '#d2152b',
      }
      
  




});





AppRegistry.registerComponent('Cart', () => Cart);
