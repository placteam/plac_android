import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  Picker,
  ScrollView,
  Image,
  View } from 'react-native';

  // API
  import Api from  '../../../../lib/http_request';
  import * as Routes from '../../../../app/routes'
  import { getCurrentUser } from '../../../../app/controllers/PlacUserController';

import ButtonContinue from '../../../views/ButtonContinue';
import { validateEmail } from '../../../../lib/validations';

  var nomenclatures=['Calle','Carrera','Avenida','Avenida Carrera','Diagonal','Circular','Avenida Calle','Circunvalar','Transversal','Via']

 var params;

  export default class CreateAddress extends Component {

  


    static navigationOptions = ({ navigation, navigationOptions }) => {
        params = navigation.state.params;
        return {
            title:'Nueva dirección',
        };
      };

    constructor(props) {
     super(props);
     params=this.props.navigation.state.params;
 

     this.state={
       plac_user_name:'',
       plac_user_address:'',
       plac_user_email:'',
       plac_user_telephone:'',
       plac_user_address:{
         mainWay:"",
         address1:"",
         address2:"",
         address3:""
       },
       plac_user_neighborhood:'',
       plac_user_additional_info:'',
       cities:[],
       citySelected:null,
       indexError:-1,
       message:"Creando nueva dirección",
     };
   

   }


   _keyShippingAddressExtractor=(item)=>item.plac_user_shipping_address_id;
    render(){
      return (
        <View style={{flex:1,backgroundColor:'#F6F6F6'}}>
        
                <ScrollView showsVerticalScrollIndicator={false} style={{margin:15,marginTop:5}}>

             


                    <View >
                        <Text  style={styles.textTitle}>Nombre</Text>
                        <TextInput 
                            ref={(input) => { this.plac_user_name = input; }}
                            returnKeyType={"next"}
                            placeholder={"Tu nombre"}
                            onSubmitEditing={()=>{
               
                              this.plac_user_email.focus()
                            }}
                            onEndEditing={()=>{
                              this.plac_user_email.focus()
                            }}

                            style={styles.textInput}
                            onChangeText={(plac_user_name) => this.setState({plac_user_name})}
                            value={this.state.plac_user_name}
                        />
                               {this.getViewErrorMessage(0)}

                    </View>
                    <View>
                        <Text  style={styles.textTitle}>Email</Text>
                        <TextInput 
                            ref={(input) => { this.plac_user_email = input; }}
                            returnKeyType={"next"}
                            onSubmitEditing={()=>{
                              this.plac_user_telephone.focus()
                            }}
                            onEndEditing={()=>{
                              this.checkEmail();

                            }}
                            placeholder={"Tu correo"}
                            style={styles.textInput}
                            onChangeText={(plac_user_email) => this.setState({plac_user_email})}
                            value={this.state.plac_user_email}
                        />
                               {this.getViewErrorMessage(1)}

                    </View>

                    <View>
                        <Text style={styles.textTitle}>Celular</Text>
                        <TextInput 
                            ref={(input) => { this.plac_user_telephone = input; }}
                         
                            keyboardType='numeric'
                            placeholder={"Número de celular"}
                          
                            style={styles.textInput}
                            onChangeText={(plac_user_telephone) => this.setState({ plac_user_telephone})}
                            value={this.state. plac_user_telephone}
                        />
                               {this.getViewErrorMessage(2)}

                    </View>
             

                     <View style={{borderBottomWidth:.5,paddingVertical:5,borderBottomColor:'#c0cc0c0'}}>
                        <Text style={{fontSize:16,color:'black'}}>Contacto</Text>
                     </View> 


                    <View>
                      <Text  style={styles.textTitle}>Ciudad</Text>
                      <View style={[styles.viewContainerAddress,{height:36,alignItems:'center',}]}>
                          <Picker
                              itemStyle={{fontSize: 12}}
                              selectedValue={this.state.citySelected}
                              mode={'dropdown'}      
                              style={{width:120, height: 20,padding: 0,flex:1, borderWidth: 1, justifyContent: 'center', alignItems: 'center',}}
                              onValueChange={(citySelected,i)=>{
                                this.setState({citySelectedI:this.state.cities[i-1],citySelected});
                                if(i!=0){
                                  this.plac_user_neighborhood.focus();
                                }
                               
                            }}>
                              <Picker.Item itemStyle={{fontSize:12}} label={"Ciudad"} key={i} value={"Ciudad"} />
                              {this.state.cities.map((element, i) => {
                                return <Picker.Item label={element.city_name} key={i} value={element.city_name} />
                              })}
                            </Picker>
                        </View>
                        {this.getViewErrorMessage(3)}
                        
                    </View>
                    <View >
                        <Text style={styles.textTitle}>Barrio</Text>
                        <TextInput 
                            ref={(input) => { this.plac_user_neighborhood = input; }}
                           
                            placeholder={"Ej. Cedritos"}
                            style={styles.textInput}
                            onChangeText={(plac_user_neighborhood) => this.setState({plac_user_neighborhood})}
                            value={this.state.plac_user_neighborhood}
                        />
                               {this.getViewErrorMessage(5)}

                    </View>


                    <View>
                      <Text style={{fontSize:12,color:'#080808',marginTop:10}}>Dirección</Text>
                      <View style={styles.viewContainerAddress}>
                        <View style={{marginRight:2,justifyContent:'center'}}>
                            <Picker
                            itemStyle={{fontSize: 12}}
                        
                            selectedValue={this.state.plac_user_address.mainWay}
                            mode={'dropdown'}      
                            style={{width:120, height: 20,padding: 0,flex:1, borderWidth: 1, justifyContent: 'center', alignItems: 'center',}}
                             
                            onValueChange={(mainWay) => {
                                let plac_user_address=this.state.plac_user_address;
                                plac_user_address.mainWay=mainWay;
                                this.setState({plac_user_address})}

                              }>
                            <Picker.Item itemStyle={{fontSize:12}} label={"Nomenclatura"} key={"empty"} value={"Calle"} />
                            {nomenclatures.map((element, i) => {
                              return <Picker.Item label={element} key={i} value={element} />
                            })}
                          </Picker>
                        </View>
                        <View style={{flex:1}}>
                           <TextInput
                              returnKeyType={'next'}
                              onSubmitEditing={()=>{
                                this.address2.focus()

                              }}  
                              placeholder={'12'}
                              value={this.state.plac_user_address.address1}
                              onChangeText={(address1) => {
                                let plac_user_address=this.state.plac_user_address;
                                plac_user_address.address1=address1;
                                this.setState({plac_user_address})}
                              }
                              style={styles.textInputAddress}
                              >
                            </TextInput>
                        </View>
                        <View style={styles.viewSimbols}>
                           <Text style={{fontWeight:'bold',color:'black',fontSize:15,}}>#</Text>
                        </View>
                        <View style={{flex:1,}}>
                          <TextInput
                            ref={(input) => { this.address2 = input; }}
                            returnKeyType={'next'}
                            onSubmitEditing={()=>{
                              this.address3.focus()
                            }}
                      
                           
                            placeholder={'74 f'}
                            value={this.state.plac_user_address.address2}
                              onChangeText={(address2) => {
                                let plac_user_address=this.state.plac_user_address;
                                plac_user_address.address2=address2;
                                this.setState({plac_user_address})}
                              }
                       
                            style={styles.textInputAddress}
                            >
                            </TextInput>
                        </View>
                        <View style={styles.viewSimbols}>
                           <Text style={{fontWeight:'bold',color:'black',fontSize:15,}}>-</Text>
                        </View>

                        <View style={{flex:1}}>
                          <TextInput
                             ref={(input) => { this.address3 = input; }}
                             returnKeyType={'next'}
                             onSubmitEditing={()=>{
                              this.validateData();
                      
                            }}

                            onEndEditing={()=>{
                              this.validateData();
                    
                            }}
                            
                             
                            placeholder={'55 bis'}
                            value={this.state.plac_user_address.address3}
                            onChangeText={(address3) => {
                              let plac_user_address=this.state.plac_user_address;
                              plac_user_address.address3=address3;
                              this.setState({plac_user_address})}
                            }
                            style={styles.textInputAddress}
                            >
                          </TextInput>
                        </View>
                    </View>
                    {this.getViewErrorMessage(4)}
                    </View>
             
              

                    <View>
                        <Text style={styles.textTitle}>Información adicional (opcional)</Text>
                        <TextInput 
                            ref={(input) => { this.plac_user_additional_info= input; }}
                            returnKeyType={'next'}
                            placeholder={"Ej. Apartamento / Casa / Dpto"}
                            style={styles.textInput}
                            onChangeText={(plac_user_additional_info) => this.setState({plac_user_additional_info})}
                            value={this.state.plac_user_additional_info}
                        />
                               {this.getViewErrorMessage(6)}

                    </View>

                    {(this.state.isLoading)?<View style={{justifyContent: 'center',paddingVertical:20,alignItems: 'center'}}>
                               <Image  style={{width: 70,height: 26,}}   source={require('../../../../assets/image/general/loader.gif')}/>
                               <Text style={{fontSize: 12}}>{this.state.message}</Text>
                    </View>:null}
                    <ButtonContinue onPress={()=>this.createAddress()} text={"Crear dirección"}  isValidData={true} customStyle={{marginVertical:20}}/>
             

              
              
                </ScrollView>

        </View>)
      
    }


  getViewErrorMessage(indexInput){
    indexError= this.state.indexError;
    if(indexInput==indexError){
      return <Text style={[this.state.messageStyle,{width:'100%'}]}>{this.state.message}</Text>;
    }
  }

  checkEmail(){
    let plac_user_email=this.state.plac_user_email;
    if(!validateEmail(plac_user_email)){
      this.setState({indexError:1,messageStyle:styles.messageWarning,message:"Upps el correo debe tener esta forma correo@proveedor.com"});

    }else{
      this.setState({indexError:-1});
    }
  }
  
  validateData(){
    let {plac_user_name,plac_user_email,plac_user_telephone,plac_user_address,citySelected,plac_user_neighborhood}=this.state;
    let {mainWay,address1,address2,address3}=plac_user_address;
    if(!plac_user_name){
      this.setState({indexError:0,messageStyle:styles.messageWarning,message:"Upps, debes completar tu nombre"});
      return false;
    }else{
      this.setState({indexError:-1});
    }
    
    if(!plac_user_email){
      this.setState({indexError:1,messageStyle:styles.messageWarning,message:"Upps, debes completar tu correo"});
      return false;
    }else{
      this.setState({indexError:-1});
    }

    if(!plac_user_telephone){
      this.setState({indexError:2,messageStyle:styles.messageWarning,message:"Upps, debes completar tu télefono"});
      return false;
    }else{
      this.setState({indexError:-1});
    }
 
    if(citySelected=="Ciudad"){
    
      
      this.setState({indexError:3,messageStyle:styles.messageWarning,message:"Upps, debes seleccionar una ciudad"});
      return false;
    }else{
      this.setState({indexError:-1});
    }
    
    if(!mainWay || !address1|| !address2|| !address3){
      this.setState({indexError:4,messageStyle:styles.messageWarning,message:"Upps, debes agregar tu dirección"});
      return false;
    }else{
      this.setState({indexError:-1});
    }

    if(!plac_user_neighborhood){
      this.setState({indexError:5,messageStyle:styles.messageWarning,message:"Upps, debes agregar tu barrio"});
      return false;
    }else{
      this.setState({indexError:-1});
    }

    return true;

 



  }

  setCurrentUser(){
    let placUser=getCurrentUser();
    this.setState({
      ...placUser,
    }); 
  }

  createAddress(){
    if(this.validateData()){
        let {citySelectedI}=this.state;
        var body={
          ...this.state,
          city_id:citySelectedI.city_id,
          cities:[],
          citySelectedI:null,
        }
        this.setState({
          isLoading:true,
        });

        Api.post(Routes.URL_RESOURCE_PLACUSER_SHIPPING_ADDRESS,body)
        .then((response)=>{
            var {status,data}=response;
            if(status=='success'){
              this.setState({
                isLoading:false,
              },()=>{
                params.addressSelected=data;
                params.callBackNew=data;
                this.props.navigation.replace('Checkout',params);
              });
            }
        }).catch((ex)=>{
          console.warn(ex.message);
        });
    }  
  }


  fetchCities(){
                 
    Api.get(Routes.URL_RESOURCE_CITIES)
       .then((response)=>{
           var {status,data,message}=response;
           if(status=='success'){
              this.setState({cities:data});
           }
       
       }).catch((ex)=>{
         console.warn(ex.message);
       });
  }

  componentDidMount(){
      let headerRight=<TouchableOpacity  onPress={()=>{this.createAdddress()}} style={{marginRight:15,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize:15,color:'#15d2bb'}}>Crear nueva</Text>
                        </TouchableOpacity>
    this.props.navigation.setParams({headerRight});
    this.fetchCities();
    this.setCurrentUser();
  }      




  }


  const styles = StyleSheet.create({
    textInput:{
      height: 36,
      backgroundColor:'white',
      color:'#080808',
      paddingVertical:10,
      marginVertical:10,
      marginBottom: 0,
      width:'100%',
      marginTop:5,
      borderColor: '#E7E7E7',
      borderWidth: .5,
      borderRadius:4,
      paddingHorizontal:5,
    },
    textInputAddress:{
      height: 36,
      backgroundColor:'white',
      color:'#080808',
      paddingVertical:10,
      width:'100%',
      borderColor: '#E7E7E7',
      borderLeftWidth:.5,
      borderRightWidth:.5,
      borderRadius:4,
      textAlign:'center'
       
    },
    textTitle:{
      fontSize:12,
      color:'#080808',
      marginTop:10,
    },
    viewContainerAddress:{
      backgroundColor:'white',
      marginTop:5,
      borderRadius:4,
      flexDirection:'row',
      borderColor: '#E7E7E7',borderWidth: .5
    },
    viewContainerAddress:{
      backgroundColor:'white',
      marginTop:5,
      borderRadius:4,
      flexDirection:'row',
      borderColor: '#E7E7E7',borderWidth: .5
    },
    viewSimbols:{
      marginHorizontal:10,
      justifyContent:'center',
      alignItems:'center',
    },
    messageSuccess:{
      fontSize: 12,
      color: 'green',
    },
    messageWarning:{
      fontSize: 12,
      color: '#f0ad4e',
    },
    messageError:{
      fontSize: 12,
      color: '#d2152b',
    }
    

  
   

  });


  AppRegistry.registerComponent('Contact', () => Contact);
