import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  FlatList,
  Dimensions,
  Text,
  TouchableOpacity,
  View } from 'react-native';

  // API
  import Api from  '../../../../lib/http_request';
  import * as Routes from '../../../../app/routes'
  import { getCurrentUser } from '../../../../app/controllers/PlacUserController';
  import CardView from 'react-native-cardview';
import ButtonContinue from '../../../views/ButtonContinue';

  


  var {height, width} = Dimensions.get('window');

  //VARS
 var placUser;
 var params;
 var addressSelected=null;

  export default class Contact extends Component {

  


    static navigationOptions = ({ navigation, navigationOptions }) => {
        params = navigation.state.params;
        return {
            title:'Direcciones',
            headerRight:params.headerRight
        };
      };

    constructor(props) {
     super(props);
     placUser=  getCurrentUser();
     params=props.navigation.state.params;

     this.state={
       title:'Direcciones',
       addresses:[],
       isValidData:false,
       
      
     };
   
    this.fetchShippingAddress();
   }


   _keyShippingAddressExtractor=(item)=>item.plac_user_shipping_address_id;
    render(){
      return (
        <View style={{flex:1,backgroundColor:'white'}}>
        
                <View style={{marginHorizontal:15}}>

               

                <Text style={{marginTop:14,color:'black'}}>Selecciona  una  dirección de entrega para continuar</Text>
                    <FlatList
                    style={{marginVertical:5}}
                   
                       showsVerticalScrollIndicator={false}
                       data={this.state.addresses}
                       keyExtractor={this._keyShippingAddressExtractor}
                       renderItem={(object)=>this.renderItem(object)}                      
                   />                    
                </View>
         

        </View>)
      
    }

    renderItem=(object)=>{
      let item=  object.item;
      let style={borderWidth:0};

        return <CardView     
                 style={{margin:2,marginTop:10}}
                cardElevation={2}
                cardMaxElevation={2}
                cornerRadius={4} 
                >
                <TouchableOpacity 
                  activeOpacity={.6}  
                  style={[{width:'100%',padding:10},style]} 
                  onPress={()=>this.onPressAddress(object.item)}>
                      <Text style={{fontSize:14,color:'black',fontWeight:'bold'}}>{item.plac_user_name}</Text>
                      <Text style={{fontSize:12,marginTop:5,color:'black'}}>{item.address_full}</Text>
                      <Text style={{fontSize:12,marginBottom:5,color:'black',}}>{item.city.city_name}, Colombia</Text>
                      <Text style={{fontSize:12,color:'black'}}>+57 {item.plac_user_telephone}</Text>                                           
                </TouchableOpacity>
                {(item.isSelected)?
                <TouchableOpacity onPress={()=>this.goCheckout(object.item)} 
                    style={styles.buttonContinue}>
                    <Text  style={{fontSize:14,color:'white'}} >Continuar</Text>
                </TouchableOpacity>:null}
              </CardView>
               
              
               
     


    }

    onPressAddress(address){
     let addresses=this.state.addresses;
      for(i=0;i<addresses.length;i++){
        if(addresses[i].plac_user_shipping_address_id==address.plac_user_shipping_address_id){
          addresses[i].isSelected=true;
        }else{
          addresses[i].isSelected=false;
        }
      }
      this.setState({addresses,addressSelected:address,isValidData:true});
    }


    createAdddress(){
        params.placUser=placUser;
        params.callBackNew=this.callBackNew;
        this.props.navigation.navigate('CreateAddress',params);
    }

    callBackNew=(item)=>{
        let addresses=this.state.addresses;
        item.isSelected=true;
        addresses.unshift(item);
        this.setState({
          addresses
        });
    }

    goCheckout(addressSelected){
      params.addressSelected=addressSelected;
  
      
      this.props.navigation.replace('Checkout',params);
    }

     


     fetchShippingAddress(){
       
         
        
        var url=Routes.URL_GET_PLACUSER_SHIPPING_ADDRESS+placUser.plac_user_id;
        Api.get(url)
           .then((response)=>{
               var {status,data}=response;
               if(status=='success'){
                 if(data.length>0){
                    this.setState({isLoading:false,addresses:data});
                 }else{
                   this.createAdddress();
                 }
               }
           
           }).catch((ex)=>{
             console.warn(ex.message);
           });
      }

      componentDidMount(){
          let headerRight=<TouchableOpacity  onPress={()=>{this.createAdddress()}} style={{marginRight:15,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:15,color:'#15d2bb'}}>Crear nueva</Text>
                            </TouchableOpacity>
        this.props.navigation.setParams({headerRight});
      }




  }


  const styles = StyleSheet.create({

    modalStyle:{
      flex:1,
      backgroundColor:'white',
    },
    btnAddAddress:{
      height:40,
      backgroundColor:'grey',
      borderRadius:2,
      justifyContent:'center',
      alignItems:'center'
    },
    title:{
      marginBottom:1,
      fontSize:15,
      fontWeight:'bold'
    },
    btnAction:{

    height:28,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    margin:2,
    borderRadius:5,
    marginTop:0,
    borderColor:'#15d2bb',
    width:width/6.5,
  },
  textAction:{
    fontSize:13,
  },
  textInput:{
    fontSize:13,
    height:40,
  },
  rowAddress:{
    backgroundColor:'white',
    borderRadius:5,
    marginTop:8,
    borderWidth:1,
    borderColor:'white',
  },
  floatView:{
    height:40,
    marginTop:20,
    justifyContent:'center',
    alignItems:'center',
  },
  btnNew:{
    height:35,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderRadius:5,
    borderColor:"#15d2bb",
    backgroundColor:'white',
    width:width/2
  },
  buttonContinue:{
    justifyContent:'center',
    alignItems:'center',
    borderBottomLeftRadius:4,
    backgroundColor:'#15d2bb',
    borderBottomRightRadius:4,
    height:36
  }

  });


  AppRegistry.registerComponent('Contact', () => Contact);
