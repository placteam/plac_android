import React, {PureComponent  } from 'react';

import {
  StyleSheet,
  View,
  TextInput,
  Image,
  Text,
  Alert,
  FlatList,
  TouchableOpacity,
  Dimensions,

  
} from 'react-native';


import icons from '../../../../assets/image/tabs';

import { getOneProductImage, formatPrice } from '../../../../lib/product';
import { getProportionalImageHeight, getUrlImageCompress } from '../../../../lib/image';
import { manageQuantities } from '../../../../app/controllers/OrderDetailController';
import CardView from 'react-native-cardview';

export default class ProductInCartRow extends PureComponent {


  

  constructor(props){
    super(props);
    params=this.props.navigation.state.params;
    this.state={
        search:'',
    }
 
  }

  render() {
    let productObject=this.props.productObject;
    let product=JSON.parse(productObject.productJSON);
    let place=this.props.place;
    let productImage=getOneProductImage(product);
    let imageHeight=getProportionalImageHeight(productImage,35);
     return <View

              style={{height:80,padding:7,backgroundColor:'white',flexDirection:'row'}}
             > 
                <TouchableOpacity  style={{justifyContent:'center',alignItems:'center',width:45,}}   onPress={()=>this.goProduct(product)}>
                   <Image style={{width:35,height:imageHeight}} source={{uri:getUrlImageCompress(productImage.url,20)}} />
                </TouchableOpacity>
                <View style={{flex:1,marginLeft:10,justifyContent:'flex-start',alignItems:'flex-start'}}> 
                <View style={{flex:1}}>
                <Text ellipsizeMode={'tail'} numberOfLines={1} style={{fontSize:14,color:'black'}}>{product.product_name}</Text>
                  <Text style={{fontSize:12,color:'grey'}}>${formatPrice(product.product_price)}</Text>

                </View>
                 
                  <View style={{flexDirection:'row',justifyContent:'center',alignItems:'flex-end'}}>
                            {this.getViewManageQuantities(productObject,product,place)}
                            <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-end'}}>
                              <Text style={{fontSize:13,color:'black'}} >Total: ${formatPrice(productObject.total)}</Text>

                            </View>
                  </View>
                
              
                </View>
               
            </View>
      
  }

  getViewManageQuantities(productObject,product,place){
    let quantity=productObject.quantity;
     return <View style={{flexDirection:'row',alignItems:'center'}}>
              <TouchableOpacity onPress={()=>this.manageProductInCart('MINUS',quantity,product,place)} style={{backgroundColor:'#00b99b',width:16,height:16,borderRadius:8,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:16,color:'white'}}> - </Text>
              </TouchableOpacity>
              <View style={styles.viewProductQuantity}>
                    <Text style={{color:'black',fontSize:12}}>{quantity}</Text>
              </View>                
              <TouchableOpacity  onPress={()=>this.manageProductInCart('ADD',quantity,product,place)}  style={{backgroundColor:'#00b99b',width:16,height:16,borderRadius:8,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:16,color:'white'}}> + </Text>
              </TouchableOpacity>
                            
             </View>

  }

  manageProductInCart(actionType,quantity,product,place){
    let  quantityOrder=0;
     if(actionType=='ADD'){
       if(quantity<40){
           quantityOrder=quantity+1
       }
   }else{
       if(quantity>=0){
         quantityOrder=quantity-1;
       }
   }

   manageQuantities(product,place,quantityOrder);
 
 }

 goProduct(productIn){
    let product={
      product_id:productIn.product_id,
    }
    let params={
      product
    }
    this.props.navigation.push('Product',params);
  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  viewProductQuantity:{
    marginHorizontal:5,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:4, 
    height:20,
    width:20,
    borderWidth:1,
    borderColor:'#15d2bb'
  }

});

