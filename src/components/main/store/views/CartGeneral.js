import React, {PureComponent  } from 'react';

import {
  StyleSheet,
  View,
  TextInput,
  Image,
  Text,
  Alert,
  FlatList,
  Animated,
  TouchableOpacity,
  Dimensions,

  
} from 'react-native';

import icons from '../../../../assets/image/tabs';

import { getOrderDetailsByPlace,getQuantityProductsInOrder } from '../../../../app/controllers/OrderDetailController';

import Button  from '../../../views/ButtonContinue'
import { formatPrice } from '../../../../lib/product';
import {  getUrlImageCompress } from '../../../../lib/image';
import realm from '../../../../config/realm';
import ProductInCartRow from './ProductInCartRow';


const { width,height} = Dimensions.get('window');

export default class CartGeneral extends PureComponent {

  

  constructor(props){
    super(props);
    this.state={
      animationSideMenu:new Animated.Value(0),
      animationOpacity:new Animated.Value(0),
      places: getOrderDetailsByPlace(),
      isOpenCart:false,
      quantityInCart:getQuantityProductsInOrder(),
    }

  }

  startAnimation=()=>{
    let isOpenCart=this.state.isOpenCart;
    let opacity=0;
    let widthSideMenu=0;
   
    if(isOpenCart){
      opacity=0;
      widthSideMenu=0;
      isOpenCart=false;
    }else{
      opacity=1;
      widthSideMenu=width;
      isOpenCart=true;
    }

    this.setState({isOpenCart});
    this.props.isOpenCart(isOpenCart);
    Animated.parallel([

      Animated.timing(this.state.animationSideMenu,{
        toValue: widthSideMenu,
        duration:100,
      }),

     Animated.timing(this.state.animationOpacity,{
       toValue: opacity,
       duration:200,
     }),

     ]).start()
}

  _keyPlaceExtractor=(item)=>item.place_id;
  _keyProductExtractor=(item)=>item.product_id;
  render() {
        const animatedSideMenu={
          width:this.state.animationSideMenu,
          opacity:this.state.animationOpacity
        }

        const animatedSideMenu2={
          opacity:this.state.animationOpacity
        }

    
        return <View style={{position:'absolute',width:50,height:50,top:0,right:0}}>
                <TouchableOpacity style={{position:'absolute',width:36,height:36,top:7,right:0}} onPress={()=>this.startAnimation()}>
                            <Image  style={{width:36,height:36,marginRight:5}}  source={{uri:icons.getStoreIconPress()}} />
                </TouchableOpacity>
               {(this.state.quantityInCart>0)?    <TouchableOpacity onPress={()=>this.startAnimation()} style={{position:'absolute',width:15,borderRadius:7.5,backgroundColor:'orange',height: 15,bottom:5,right:8.5,justifyContent:'center',alignItems:'center'}}>
                          <Text style={{color:'white',fontSize:10 }} >{this.state.quantityInCart}</Text>
                </TouchableOpacity>:null}
            


                <Animated.View style={[styles.viewSideMenu,animatedSideMenu]}>
                                  <View style={{flex:.3,backgroundColor:'black',opacity:(this.state.isOpenCart)?.4:0}}>
                                  <TouchableOpacity style={{flex:1}}  activeOpacity={1}  onPress={()=>this.startAnimation()} >

                                  </TouchableOpacity>
                                  </View>
                                  <View  style={{flex:1.7,backgroundColor:'white',borderWidth:.5,borderColor:'grey'}}>
                                      <View style={{height:120,justifyContent:'center',paddingHorizontal:15}}>
                                          <Text style={{fontSize:18,fontWeight:'500',color:'black'}}>
                                              Carrito de compras
                                          </Text>
                                          <Text  style={{fontSize:12,marginTop:10,color:'black'}}>
                                          Aquí se encuentran tus productos, estas a un paso de adquirirlos
                                          </Text>
                                          <Text     style={{fontSize:12,marginTop:10,color:'#15d2bb'}}>
                                          Se crearan {this.state.places.length} pedidos
                                          </Text>
                                      </View>
                                      <FlatList
                                      style={{marginBottom:140}}
                                        data={this.state.places}
                                        keyExtractor={this._keyPlaceExtractor}
                                        renderItem={this.renderPlaceItem}                         
                                      />
                                  </View>
                  </Animated.View >
            </View>
    
  }

  renderPlaceItem=(object)=>{
   let place=object.item;
   let placeImage=place.place.path_image_logo;
   let products=place.products;
   let params={place}


   return <View style={{marginTop:10}}>
              <View style={{backgroundColor:'#F7F7F7',height:56,flexDirection:'row'}}>
                  <TouchableOpacity  onPress={()=>this.goPlace(place)} style={{marginLeft:10,flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                      <Image  style={{height:36,width:36,borderRadius:18}} source={{uri: getUrlImageCompress(placeImage,20)}}/>
                      <Text style={{marginLeft:10,color:'black'}}>{place.place_location_name}</Text>
                  </TouchableOpacity>
                  <View style={{flex:1,justifyContent:'flex-end',flexDirection:'row',alignItems:'center',marginRight:10,}}>
                    <Text style={{marginRight:10,color:'black'}}>${formatPrice(place.total)}</Text>
                    <Button  onPress={()=>{this.props.navigation.navigate('UserAddress',params)}} text={'Comprar'} isValidData={true} customStyleText={{fontSize:12}}  customStyle={{width:'50%',height:'50%'}} title={'Comprar'} />
                  </View>

              </View>
              <View style={{flex:1,marginHorizontal:10,marginVertical:10}}>
                  <FlatList
               
                    data={products}
                    ItemSeparatorComponent={()=><View style={{height:.5,width,marginVertical:5,backgroundColor:'#c0c0c0'}}></View>}


                    keyExtractor={this._keyProductExtractor}
                    renderItem={(object)=>this.renderProductItem(object,place)}                      
                  />      

              </View>

           </View>
    
  }

  renderProductItem=(object,place)=>{
    let productObject=object.item;
    return <ProductInCartRow  navigation={this.props.navigation} place={place} productObject={productObject}/>


  }


  goPlace(place){
    let params={
        ...this.props,
        place_id:place.place_id,
    }
    this.props.navigation.push('Place',params);
  }



  componentWillMount(){
    // Observe Realm Notifications
    realm.addListener('change', this.updateUI);

  }

  componentWillUnmount(){
    // ..later remove the listener
    realm.removeListener('change', this.updateUI);
 
  }

  updateUI=(ad)=>{
    setTimeout(()=>{
      this.setState({
        quantityInCart:getQuantityProductsInOrder(),
        places:getOrderDetailsByPlace()
      });
    },100);
  }



 

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
   },


  viewProductQuantity:{
    marginHorizontal:5,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:4, 
    height:22,
    width:22,
    borderWidth:1,
    borderColor:'#15d2bb'
  },
  viewSideMenu:{
    position:'absolute',
    flexDirection:'row',
    top:50,
    right:0,
    height,
    width:width
  }
  

 

});

