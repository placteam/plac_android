import React, {PureComponent  } from 'react';

import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';



import { getUrlImageCompress, getProportionalImageHeight } from '../../../../lib/image';

import { CachedImage } from 'react-native-img-cache';
import ProductsRow from './ProductRow';

const { width } = Dimensions.get('window')
var widthProduct=(width/3);
export default class ProductsPlaceRow extends PureComponent {

  constructor(props){
    super(props);
  }

  _keyExtractorProduct = (item, index) => item.product_id;
  render() {
    let place=this.props.object.item;
    let products=place.products.data;

  
    return <View style={{marginVertical:  10}}>
                    <TouchableOpacity  onPress={()=>this.goPlace()}   style={{flexDirection:'row'}}>
                    {(place.path_image_logo)?<CachedImage  style={{width:36,height:36}} source={{uri:getUrlImageCompress(place.path_image_logo,10)}}/>:null}
                        <View style={{flex:1.3,marginLeft:10,marginRight:20}}>
                            <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{fontSize:14,color:'black',fontWeight:'bold'}}>{place.place_location_name}</Text>
                            <Text style={{fontSize:12,color:'#555555'}}>{place.total_products} productos</Text>
                        
                        </View>
                        <View style={{flex:.7,justifyContent:'center',alignItems:'flex-end'}} >
                            <Text  style={{fontSize:12,color:'#00B99B'}}>Ver tienda</Text>
                        </View>
                    </TouchableOpacity>
                    {this.getViewOff(place)}
                 
                    
                     <FlatList
                              style={{marginTop:10,}}
                              data={products}
                              initialNumToRender={10}
                              removeClippedSubviews={false}
                              showsHorizontalScrollIndicator={false}
                              horizontal={true}
                              keyExtractor={this._keyExtractorProduct}
                              renderItem={this.renderProductRow}
                          />        
          </View>
          
  }

  renderProductRow=(object)=>{
 
    return <ProductsRow  navigation={this.props.navigation} widthProduct={widthProduct}  object={object}/>
  }

  getViewOff(place){

    if(place.promotion!=undefined){
     let  promotion=JSON.parse(place.promotion);
      if(promotion.isActive){
       return  <View style={{marginTop:5,alignItems:'center',justifyContent:'center'}}>
                  <View style={{alignItems:'center',flexDirection:'row',justifyContent:'center',height:getProportionalImageHeight({width:1280,height:192},100)+5}}>
                      <Text style={{fontSize:10,color:'black'}} >{promotion.text}</Text>
                      <CachedImage style={{marginLeft:10,height:getProportionalImageHeight({width:1280,height:192},100),width:100}} source={{uri:promotion.image}}/>
                  </View>
                  <Text style={{fontSize:9,color:'black'}} >{promotion.expired}</Text>
              </View>
      }
    }
    return null;
  }
  

  goPlace(){
    let place=this.props.object.item;
    let params=this.props.navigation.state.params;
    params.place_id=place.place_id;
    this.props.navigation.navigate("Place",params);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});