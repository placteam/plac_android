import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions,
  View } from 'react-native';

var {width,height} =Dimensions.get('window');  
const ICON_PAYMENT_CASH=require('../../../../assets/image/general/cash.png');
const ICON_EFECTY=require('../../../../assets/image/general/efecty.png');
const ICON_PSE=require('../../../../assets/image/general/pse.png');
const ICON_CREDIT_CARD=require('../../../../assets/image/general/credit-card.png');


  export default class PaymenMethods extends Component {
      

    constructor(props) {
       super(props);
     
     
       this.state={
           isVisiblePaymentMethods:false,
           paymentMethods:[
             {payment_method_id:'payment_delivery',name:"Contra entrega",urlImage:'',isSelected:0,isActive:0},
             {payment_method_id:'mercado_pago',name:"Pago en linea",urlImage:'',isSelected:0,isActive:0},
           ],
       }
    
   }

 


 render() {
    return this.getViewPaymentMethodsOptions();
  }


_keyExtractorPaymentMethods=(item, index) => item.payment_method_id;
 getViewPaymentMethodsOptions(){
    let isVisiblePaymentMethods=this.props.isVisiblePaymentMethods;
    let paymentMethods= this.state.paymentMethods;
    let optionsAvailables=0;
    for(i=0;i<paymentMethods.length;i++){
      if(paymentMethods[i].isActive){
         optionsAvailables++;
      }
    }


    if(isVisiblePaymentMethods){
      return <View style={{marginHorizontal:20,height: 45*optionsAvailables,marginVertical:5}}>
                <FlatList
                   data={this.state.paymentMethods}
                   extraData={this.state}
                   style={{flex:1,}}
                   keyExtractor={this._keyExtractorPaymentMethods}
                   renderItem={({item})=>this.renderRowPaymentMethod(item)}
                />
             </View>
    }else {
        return null;
    }
 }

 renderRowPaymentMethod(item){

    if(item.isActive){
        let stylePaymentMethodItem=styles.viewPaymentMethodItem;
        if(item.isSelected){
            stylePaymentMethodItem=styles.viewPaymentMethodItemPress;
        }
        return (<TouchableOpacity onPress={()=>this.selectPaymentMethod(item)}  style={{flexDirection: 'row',height: 45}}>
                    <View style={{flex:1.2,flexDirection: 'row',justifyContent: 'flex-start',alignItems: 'center'}}>
                       <Text style={{fontSize: 14,color: 'black',fontWeight:'400',marginRight: 5}}>{item.name}</Text>
                       {this.getViewPaymentMethodImage(item)}

                    </View>
                    <TouchableOpacity  onPress={()=>this.selectPaymentMethod(item)}  style={styles.buttonPaymentMethod}>
                        <View style={stylePaymentMethodItem}/>
                    </TouchableOpacity>
                </TouchableOpacity>);
    }
  }


 getViewPaymentMethodImage(item){
    if(item.payment_method_id=='payment_delivery'){
      return   <Image   style={{width:20,height:20,marginTop:2,marginBottom: 2}}  source={ICON_PAYMENT_CASH}/>
    }else{
      return (<View style={{marginTop: 2,justifyContent: 'center',alignItems: 'center'}}>
                <View style={{flexDirection: 'row'}}>
                  <Image   style={{width:50,height:17}}  source={ICON_EFECTY}/>
                  <Image   style={{marginLeft:5,width:35,height:17}}  source={ICON_PSE}/>
                </View>
                 <Image   style={{marginLeft:5,width:81,height:17}}  source={ICON_CREDIT_CARD}/>
             </View>);
    }
}

  selectPaymentMethod(item){
        let paymentMethods= this.state.paymentMethods;
        for(i=0;i<paymentMethods.length;i++){
            if(paymentMethods[i].payment_method_id==item.payment_method_id){
            paymentMethods[i].isSelected=true;
            }else{
            paymentMethods[i].isSelected=false;
            }
        }
        this.setState({paymentMethods,paymentMethodSelected:item});

        this.props.paymentMethodSelected(item);
   }

   componentWillUnmount(){

    let paymentMethods= this.state.paymentMethods;
    for(i=0;i<paymentMethods.length;i++){
      paymentMethods[i].isActive=0;
    }
     this.setState({paymentMethods});
   }

   componentWillMount(){
    let paymentMethodsAvailable=this.props.paymentMethodsAvailable;
    let paymentMethods=this.state.paymentMethods;
     for(i=0;i<paymentMethods.length;i++){
           for(y=0;y<paymentMethodsAvailable.length;y++){
               if(paymentMethods[i].payment_method_id==paymentMethodsAvailable[y].payment_method_id){
                 paymentMethods[i].isActive=1;
               }
           }
     }
     this.setState({paymentMethods});
     
   }


 


  }


  

  const styles = StyleSheet.create({
    container:{
        flex:1
    },
    buttonPaymentMethod:{
        flex:.8,
        borderColor:'#15d2bb',
        justifyContent:'center',
        alignItems: 'flex-end'
      },
    viewPaymentMethodItem:{
        width:20,
        height: 20,
        borderRadius: 10,
        borderColor:'#15d2bb',
        borderWidth: 1,
      },
      viewPaymentMethodItemPress:{
        width:20,
        height: 20,
        borderRadius: 10,
        backgroundColor:'#15d2bb',
      },
      messageSuccess:{
        fontSize: 12,
        color: 'green',
      },
      messageWarning:{
        fontSize: 12,
        color: '#f0ad4e',
      },
      messageError:{
        fontSize: 12,
        color: '#d2152b',
      }
      
  
  });





  AppRegistry.registerComponent('Cart', () => Cart);
