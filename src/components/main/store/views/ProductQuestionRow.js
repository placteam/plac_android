import React, { Component,PureComponent  } from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import ReadMore from '../../../../lib/text/ReadMore';

import { CachedImage } from 'react-native-img-cache';
import { getUrlImageCompress } from '../../../../lib/image';
import * as Emoticons from 'react-native-emoticons';


export default class ProductQuestionRow extends PureComponent {

  constructor(props){
    super(props);

  }

  render() {
    let question=this.props.object.item;
    let placUser;
    if(this.props.from==undefined){
      placUser=question.plac_user;
    }else{
      placUser= this.props.placUser;
    }
  
    return <View style={{marginVertical:10}}>
              <View style={{flexDirection:'row',}}>
                    <CachedImage  style={styles.imageUser} source={{uri:getUrlImageCompress(placUser.plac_user_image,20)}}/>
                    <View style={[styles.viewQuestion,{marginLeft:5,}]}>
                        <Text  style={styles.textName}>{placUser.plac_user_name}</Text> 
                        <ReadMore
                            numberOfLines={5}
                            renderTruncatedFooter={this._renderTruncatedFooter}
                            renderRevealedFooter={this._renderRevealedFooter}
                            >
                                    <Text style={styles.textQuestion}>{Emoticons.parse(question.question_txt)}</Text> 
                        </ReadMore>
                    </View>
              </View>
              {this.getViewQuestionAnswer(question)}
           </View>
  }

  getViewQuestionAnswer(question){
    let placeLocation=this.props.placeLocation;
        if(question.answer_txt){
            return <View style={styles.viewContainerQuestion}>
                    
            <View style={[styles.viewQuestion,{marginRight:5,}]}>
                <Text style={styles.textName}>{placeLocation.place_location_name}</Text>
                <ReadMore
                numberOfLines={5}
                renderTruncatedFooter={this._renderTruncatedFooter}
                renderRevealedFooter={this._renderRevealedFooter}
                >
                    <Text  style={styles.textQuestion} >{Emoticons.parse(question.answer_txt)}</Text> 
                </ReadMore>
            </View>
            <CachedImage  style={styles.imageUser} source={{uri:getUrlImageCompress(placeLocation.place.path_image_logo,20)}}/>
            </View>
        }

  }

 

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  imageUser:{
    width:36,
    height:36,
    borderRadius:18,
  },
  viewContainerQuestion:{
    flexDirection:'row',marginTop:10,
    justifyContent:'flex-end',
    alignItems:'flex-start'
  },
  viewQuestion:{
    backgroundColor:'#ededed',
    width:'70%',
    padding:5,
    borderRadius:8,
  },
  textQuestion:{
    fontSize:14,
    color:'black'
  },
  textName:{
    fontSize:14,
    color:'black',
    fontWeight:'bold'
  },


 

});
