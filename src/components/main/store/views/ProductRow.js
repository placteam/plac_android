import React, { Component,PureComponent  } from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import realm from '../../../../config/realm';
import { getOneProductImage, formatPrice } from '../../../../lib/product';
import { getUrlImageCompress, getProportionalImageWidth } from '../../../../lib/image';
import {CachedImage}  from "react-native-img-cache";
import CardView from   'react-native-cardview'
import { getOrderDetail } from '../../../../app/controllers/OrderDetailController';

export default class ProductsRow extends PureComponent {

  constructor(props){
    super(props);
    this.state={
      quantityInCart:0,
    }

  }

  render() {
    let object=this.props.object;
    let product=object.item;
    let widthProduct=this.props.widthProduct;
    let productImage=getOneProductImage(product);
    let widthProductImage= getProportionalImageWidth(productImage.width,productImage.height,130);
  

   return  <CardView   
              style={{width:widthProduct,margin:5}}
              cardElevation={2}
              cardMaxElevation={2}
              cornerRadius={2}>     
                     <TouchableOpacity style={{alignItems:'center'}}  onPress={()=>this.goProduct(product)} >
                            <CachedImage source={{uri:getUrlImageCompress(productImage.url,20)}} style={{width:widthProductImage,height:130,marginVertical:7}}/>
                            <View style={{padding:5,marginTop:5,}}>
                                  <Text ellipsizeMode={'tail'} numberOfLines={1} style={{fontSize:12,color:'#aaaaaa',fontWeight:'bold'}}>{product.product_name}</Text>
                                  <Text style={{fontSize:14,color:'#555555',fontWeight:'bold'}}>${formatPrice(product.price_final)}</Text>
                             </View>
                          {(this.state.quantityInCart!=0)?<View style={{position:'absolute',top:5,right:5, width:20,height:20,borderRadius:10,backgroundColor:'orange',justifyContent:'center',alignItems:'center'}}>
                          <Text style={{fontSize:12,color:'white'}}>{this.state.quantityInCart}</Text>

                          </View>:null}
                      </TouchableOpacity>
           </CardView>
          
  }

  goProduct(product){
    params=this.props.navigation.state.params;
    console.warn(params);
    
    params.product=product;
    this.props.navigation.navigate('Product',params);
  }



  componentWillMount(){
    //this.updateUI();
    realm.addListener('change', this.updateUI);

  }

  componentWillUnmount(){
    // ..later remove the listener
    realm.removeListener('change', this.updateUI);
 
  }

  getQuantityProducts(){
    let object=this.props.object;
    let product=object.item;
    let orderDetails= getOrderDetail(product.place_id,product.product_id);
    if(orderDetails.length>0){
      orderDetail=orderDetails[0];
      console.warn(orderDetail.quantity);
      this.setState({
        quantityInCart:orderDetail.quantity,
      });
    }else{
      this.setState({
        quantityInCart:0,
      });
    }

  }



updateUI=(ad)=>{
  setTimeout(()=>{
    this.getQuantityProducts();
  },100);
}


  componentDidMount(){
    this.getQuantityProducts();
  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

 

});
