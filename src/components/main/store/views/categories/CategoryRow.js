import React, { Component,PureComponent  } from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { getProportionalImageHeight, getProportionalImageWidth } from '../../../../../lib/image';

var {width}=Dimensions.get('window');

export default class CategoryRow extends React.Component {

  constructor(props){
    super(props);

  }

  render() {
      let object=this.props.object;
      let category=object.item;
      let viewStyleCategory=styles.viewCategoryNormal;
      let textStyleCategory=styles.textCategoryNormal;
      if(category.isSelected==1){
          viewStyleCategory=styles.viewCategoryPress;
          textStyleCategory=styles.textCategoryPress;
      }

      let marginLeft=20;
      if(object.index==0){
        marginLeft=15;

      }

      return <TouchableOpacity onPress={()=>this.categorySelected(this.props.object)}  style={[viewStyleCategory,{marginLeft}]}>
                 <Text style={textStyleCategory}>{category.category_name}</Text>
           </TouchableOpacity>
    
   

  }


  categorySelected(object){
      this.props.categorySelected(object.item);
  }









 

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  viewCategoryNormal:{
    backgroundColor:'#15d2bb',
    justifyContent:'center',
    alignItems:'center',
    height:42,
  },
  viewCategoryPress:{
    backgroundColor:'#15d2bb',
    borderBottomWidth:2.5,
    marginBottom:.5,
    borderColor:'white',
    justifyContent:'center',
    alignItems:'center',
    height:42,
  },
  textCategoryNormal:{
    color:'rgba(242, 242, 242,.8)',
    fontSize:15,
  },
  textCategoryPress:{
    color:'white',
    fontWeight:'bold',
    fontSize:15,
    margin:10,
  },
 


 

});
