import React, { Component,PureComponent  } from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import SubCategoryRow from './SubCategoryRow';

export default class SubCategoriesList extends React.Component {

  constructor(props){
    super(props);
    this.state={
      subcategories:this.props.subcategories,
    }


  }

  _keyExtractor = (item, index) => item.subcategory_id;

  render() {
   
   return    <FlatList
                data={this.state.subcategories}
                showsHorizontalScrollIndicator={false}
                numColumns={3}
      
                keyExtractor={this._keyExtractor}
                renderItem={this._renderSubcategoryItem} />
           

  }

  _renderSubcategoryItem=(object)=>{

    return <SubCategoryRow  isAccordingToWindow={true} subcategorySelected={this.subcategorySelected} object={object}/>



  }

  subcategorySelected=(subcategoryIn)=>{
    var  subcategories=this.state.subcategories;
    for(i=0;i<subcategories.length;i++){
        if(subcategoryIn.subcategory_id==subcategories[i].subcategory_id){
          if(subcategoryIn.isSelected){
              subcategories[i].isSelected=false;
          }else{
              subcategories[i].isSelected=true;
          }
        }else{
          subcategories[i].isSelected=false;
        }
    }
    this.setState({subcategories:[]},()=>{
      this.setState({subcategories});
    });
    this.props.subcategorySelected(subcategoryIn);

  }






 

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  imageUser:{
    width:36,
    height:36,
    borderRadius:18,
  },
  viewContainerQuestion:{
    flexDirection:'row',marginTop:10,
    justifyContent:'flex-end',
    alignItems:'flex-start'
  },
  viewQuestion:{
    backgroundColor:'#ededed',
    width:'70%',
    padding:5,
    borderRadius:8,
  },
  textQuestion:{
    fontSize:14,
    color:'black'
  },
  textName:{
    fontSize:14,
    color:'black',
    fontWeight:'bold'
  },


 

});
