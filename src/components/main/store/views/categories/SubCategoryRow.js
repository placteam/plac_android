import React, { Component,PureComponent  } from 'react';

import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';


import { CachedImage } from 'react-native-img-cache';
var {width}=Dimensions.get('window');

export default class SubCategoryRow extends React.Component {

  constructor(props){
    super(props);

  }

  render() {
      let object=this.props.object;
      let subcategory=object.item;

      let viewSubcategoryStyle=styles.buttonViewSubcategoryItem;
      let textSubcategoryStyle=styles.textSubcategoryNameNormal;
      let imageSubcategoryStyle=styles.imageSubcategoryNormal;
  
      if(subcategory.isSelected){
        textSubcategoryStyle=styles.textSubcategoryNameSelected;
        viewSubcategoryStyle=styles.buttonViewSubcategoryItemPress;
        imageSubcategoryStyle=styles.imageSubcategoryPress;
      }
      
     let marginLeft=15;
 
  
     let  isAccordingToWindow= this.props.isAccordingToWindow;
      let itemStyle={};
      if(isAccordingToWindow){
        if(object.index%3==0){
          marginLeft=0;
        }
        itemStyle.width= (((width-30))-30)/3;
      }


      return <TouchableOpacity onPress={()=>this.subcategorySelected(this.props.object)}  style={[viewSubcategoryStyle,itemStyle,{marginLeft,marginTop:10,justifyContent:'center',alignItems:'center'}]}>
               <CachedImage  style={{width:20,height:20,borderRadius:10}} source={{uri:subcategory.subcategory_img_url}}/>
                <Text   numberOfLines={1} ellipsizeMode={'tail'} style={[textSubcategoryStyle,{textAlign:'center',marginLeft:5,marginRight:5,}]}>{subcategory.subcategory_name}</Text>
           </TouchableOpacity>
    
   
    return <TouchableOpacity  onPress={()=>this.subcategorySelected(this.props.object)} style={[viewSubcategoryStyle,{height:36,marginLeft}]}>
                <View style={imageSubcategoryStyle}>
                <Image  style={{width:25,height:25,borderRadius:12.5,marginLeft:2,}} source={{uri:subcategory.subcategory_img_url}}/>
                </View>
                <View style={{flex:1.3,alignItems:'center',justifyContent:'center'}}>
                    <Text   numberOfLines={1} ellipsizeMode={'tail'} style={[textSubcategoryStyle,{textAlign:'center',marginLeft:5,marginRight:5,}]}>{subcategory.subcategory_name}</Text>
                </View>

            </TouchableOpacity>
  }


  subcategorySelected=(object)=>{
      let  subcategoryIn=object.item;
      this.props.subcategorySelected(subcategoryIn);
  }






 

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  buttonViewSubcategoryItem:{
    borderWidth:.8,
    borderRadius:4,
    marginTop:10,
    borderColor:'#15d2bb',
    flexDirection:'row',

    padding:5,

    alignItems:'center'
  },
  buttonViewSubcategoryItemPress:{

    borderRadius:4,
    marginTop:10,
    backgroundColor:'#15d2bb',
    flexDirection:'row',
    padding:5,
    alignItems:'center'
  },

  textSubcategoryNameNormal:{
    fontSize:13,
    fontWeight:'200',
    color:'black',
  },
  textSubcategoryNameSelected:{
    fontSize:14,
    color:'white',
    fontWeight:'300',
  },
  imageSubcategoryNormal:{
    flex:.7,
    alignItems:'center',
    justifyContent:'center'
  },
  imageSubcategoryPress:{
    flex:.1,
    alignItems:'center',
    justifyContent:'center'
  }
 


 

});
