import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  Dimensions,

} from 'react-native';



const { width } = Dimensions.get('window')


export default class HeaderSearch extends React.Component {

  constructor(props){
    super(props);
  
     this.state={
         value:"",
     }
  }

  render() {
   
    return (
        <TextInput  
        ref={index=>this.searchTI=index}
        returnKeyType={"search"}
        onEndEditing={this.props.onEndEditing}
        style={{height:45,paddingLeft:5,borderRadius:8,marginRight:15,width:'100%',borderColor:'#15D2BB',fontSize:16,}}
        onChangeText={this.onChangeText}
        placeholder={this.props.placeholder}
        placeholderTextColor="#C0C0C0"
        value={this.state.value}/>
    );
  }

  onChangeText=(value)=>{
      this.setState({
          value
      });
      this.props.onChangeText(value);
  }




  componentDidMount(){
      this.searchTI.focus();
   
  
  }


}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  viewSearch: {
    marginHorizontal:15,
    height: 48,
    borderRadius: 8,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    marginVertical:15,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 4,
    shadowRadius: 8,
    elevation: 1,
    backgroundColor:'white',
    borderWidth: 1,
    borderColor: '#15d2bb',
  
  },
  viewCategoryItem:{
 
    marginBottom:10,
  },
  viewCategory:{
    height:60,
    width:width-30,
    borderRadius:8,
    shadowColor: '#000000',
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 2,
    shadowOpacity: .3
  }
  

});
