import React, { PureComponent } from 'react';

import {
  StyleSheet,
  View,
  TextInput,
  Image,
  Text,
  Alert,
  FlatList,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Picker,


} from 'react-native';

//API REQUEST 
import Api from '../../../lib/http_request';
import { URL_GET_PRODUCT_SUBCATEGORIES_BY_FILTER, URL_GET_PRODUCT_BRANDS_BY_FILTER, URL_GET_PRODUCTS_PLACES_BY_FILTER } from '../../../app/routes';

import SubCategoriesList from './views/categories/SubCategoriesList';
import ProductsPlaceRow from './views/ProductsPlaceRow';
import ListViewPagination from '../../views/ListViewPagination';
import CartGeneral from './views/CartGeneral';
import CardView from 'react-native-cardview';

import { CachedImage } from 'react-native-img-cache';



var { width } = Dimensions.get('window');
var category;

export default class PlacesByCategory extends React.Component {

  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props);
    params = this.props.navigation.state.params;


    category = params.category;
    this.state = {
      filters: {
        category_id: category.category_id,
        pet_type: params.petTypeSelected,
        subcategory_id: '',
      },
      subcategories: [],
      search: '',

      brands: [],
      brand: 'marca',
      places:[],
      isLoading:true,
      loadMore:false,
      paginate:null,
      url_query:URL_GET_PRODUCTS_PLACES_BY_FILTER ,
      arrayUrls:[],
      isOpenCart:false,
    }

  }

  _keyExtractor = (item) => item.place_id;
  render() {
    return <View style={styles.container}>
        
   
      
      {this.getViewHeader()}
    
      {this.getViewFilters()}
 
      <View style={{marginHorizontal:15,flex:1}}>
         <ListViewPagination
            initialNumToRender={5}
            numColumns={1}
            data={this.state.places}
            isLoading={this.state.isLoading}
            loadMore={this.state.loadMore}
            paginate={this.state.paginate}
            isSeparatorActive={false}
            onRefresh={this.onRefresh}
            keyExtractor={this._keyExtractor}
            renderRow={this.renderPlaceRow}
            handleLoadMore={this.handleLoadMore}
            arrayUrls={this.state.arrayUrls}
          />

      </View>
      {this.getViewCartGeneral()}
   
     

        
    </View>
  }


  getViewCartGeneral(){
    return <CartGeneral isOpenCart={this.isOpenCart} navigation={this.props.navigation}/>
          
  }
  isOpenCart=(isOpenCart)=>{

      this.setState({isOpenCart});

  }

  renderPlaceRow=(object)=>{
    return <ProductsPlaceRow  hasPagination={true} navigation={this.props.navigation}  object={object}/>
   }


  getViewHeader() {
 
     let isOpenCart=this.state.isOpenCart;
     let internalView= <View style={{marginBottom:1,flexDirection: 'row', height: 55}}>
                          <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{ width: 55, height: 55, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ width: 16, height: 16 }} source={require('../../../assets/image/general/back/back_icon_black.png')} />
                          </TouchableOpacity>
                          <View style={{flex: 1, marginLeft:15,justifyContent: "center",alignItems: "flex-start"}}>
                                      <Text style={{ fontSize: 18, color: "black", fontWeight: 'bold' }}>
                                        {(category) ? category.category_name : "Producto"}
                                      </Text>
                          </View>        
                       {this.getViewCartGeneral()}
                      </View>;
        if(isOpenCart){
          return <View   style={{height: 55}}>
                  {internalView}
                 </View>
        }
          return <CardView 
                    style={{height: 55}}
                    cardElevation={3}
                    cardMaxElevation={3}
                    cornerRadius={1}>
                    {internalView}
                </CardView>
  }


  getViewFilters() {
    if (this.state.subcategories.length > 0) {
      return <View style={{ borderBottomWidth: .5,marginHorizontal:15, borderColor: 'rgba(192,192,192,.5)' }}>

                <SubCategoriesList subcategorySelected={this.subcategorySelected} subcategories={this.state.subcategories} />
                  {this.getViewBrands()}
       
            </View>
    }else{
      return null;
    }
  }

  getViewBrands() {

    let brands = this.state.brands;
    if(brands.length>0){
      return   <View style={{marginVertical:10,flexDirection:'row', justifyContent: 'flex-start',}}>
                  <Text style={{ fontSize: 12, marginTop:3,color: '#15d2bb' }}>
                    Seleccionar marca
                  </Text>
                
                      <Picker
                        itemStyle={{fontSize: 12}}
                    
                        selectedValue={this.state.brand}
                        mode={'dropdown'}      
                        style={{ height: 20,padding: 0,flex:1, borderWidth: 1, justifyContent: 'center', alignItems: 'center',}}
                        onValueChange={this.onChangeBrand}>
                        <Picker.Item itemStyle={{fontSize:12}} label={"Marca"} key={i} value={"Marca"} />
                        {brands.map((element, i) => {
                          return <Picker.Item label={element.brand} key={i} value={element.brand} />
                        })}
                      </Picker>   
            </View>
    }

  }

  onChangeBrand=(brand, itemIndex)=>{
    let filters=this.state.filters;
    if(brand!="Marca"){
      filters.brand=brand;
      this.fetchAgainProducts(filters);
    }
    this.setState({brand});
  }

  subcategorySelected = (subcategorySelected) => {
    let filters=this.state.filters;
    filters.subcategory_id=subcategorySelected.subcategory_id;
    filters.brand="";
    this.fetchAgainProducts(filters);
    setTimeout(()=>{
      this.fetchBrands();
    },500);
  }

  fetchAgainProducts(filters){
    this.setState({ 
        filters,
        isLoading:true,
        places:[],
        url_query:URL_GET_PRODUCTS_PLACES_BY_FILTER, },()=>{
        this.fetchPlacesByFilter();
      });
}


  fetchSubcategories() {
    let filters=this.state.filters;
    let body = {
      filters
    }

    Api.post(URL_GET_PRODUCT_SUBCATEGORIES_BY_FILTER, body)
      .then((response) => {
        console.warn(response)
        var { status, data, message } = response;
        let subcategories = data;
        let i = 0;
        subcategories.forEach(subcategory => {
          subcategory.isSelected = false;
          subcategories[i] = subcategory;
          i++;

        });
        this.setState({ subcategories });
      }).catch((ex) => {
        console.warn(ex.message)

      });
  }

  fetchBrands() {
    let filters=this.state.filters;
    let body = {
      filters
    }

    Api.post(URL_GET_PRODUCT_BRANDS_BY_FILTER, body)
      .then((response) => {
        this.setState({ brands: response });
      }).catch((ex) => {
        console.warn(ex.message);
      });
  }

  fetchPlacesByFilter() {
    let filters=this.state.filters;
    let body = {
      filters
    }

     Api.post(this.state.url_query, body)
     .then((response) => {
 
       this.manageResponse(response);
     }).catch((ex) => {

     });

  }

  manageResponse(response){
  
    let {data}=response;
    let paginate=data;
    let places= this.state.places;
    data=paginate.data;
    for(i=0;i<data.length;i++){
        places.push(data[i]);
    }

    this.setState({
      places,
      isSearching:false,
      paginate,
      isLoading:false,
      loadMore:false,
    });
}




  onRefresh = () => {
    this.setState({
      url_query: URL_GET_PRODUCTS_PLACES_BY_FILTER,
      isLoading: false,
      places: [],
      arrayUrls:[],
    }, () => {
      this.fetchPlacesByFilter();
    });
  }

  handleLoadMore = (np,arrayUrls) => {
    this.setState({
      url_query: URL_GET_PRODUCTS_PLACES_BY_FILTER +np,
      loadMore: true,
      arrayUrls,
    }, () => {
      this.fetchPlacesByFilter()
    });
  }

  componentWillMount() {
    this.fetchSubcategories();
    this.fetchBrands();
    this.fetchPlacesByFilter();
  }

  

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textInputQuestion: {
    height: 48,
    paddingLeft: 10,
    borderRadius: 8,
    borderWidth: 1,
    width: '100%',
    borderColor: '#00b99b'
  },
  viewSearch: {
    marginHorizontal: 15,
    height: 42,
    borderRadius: 8,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    marginVertical: 15,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 4,
    shadowRadius: 8,
    elevation: 1,
    backgroundColor: 'white',
    borderWidth: .6,
    borderColor: '#15d2bb',

  },




});

