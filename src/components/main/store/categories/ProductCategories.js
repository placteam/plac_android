import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  Dimensions,
  View } from 'react-native';

  // INFO DEVICE
  var {height, width} = Dimensions.get('window');
  //ElevatedView
  



  export default class ProductsCategories extends React.Component {
    constructor(props) {
     super(props);
     this.state={
       productsCategories:[],
       productsSubcategories:[],
     }

   }


   _keyExtractorCategories = (item, index) =>item.category_id;

   render() {
     return ( <View style={{backgroundColor:'white',marginTop: 40}}>
                       <FlatList
                         horizontal={true}
                         ref={(ref) => { this.categoriesList = ref; }}
                         getItemLayout={(data, index) => (
                               {length: 80, offset: 80 * index, index}
                             )}
                         data={this.state.productsCategories}
                         extraData={this.state}
                         keyExtractor={this._keyExtractorCategories}
                         renderItem={(object)=>this.renderItemCategory(object)}
                         showsHorizontalScrollIndicator={false}
                         style={{backgroundColor:'#15d2bb'}} />

                         {this.getViewProductsSubCategories()}
                </View>
              );
  }

  renderItemCategory(object){
    category=object.item;
    viewStyleCategory=styles.viewCategoryNormal;
    textStyleCategory=styles.textCategoryNormal;
    if(category.isSelected==1){
        viewStyleCategory=styles.viewCategoryPress;
        textStyleCategory=styles.textCategoryPress;
    }
      return (<CardView   elevation={2}  style={viewStyleCategory}   key={category.category_id} style={viewStyleCategory} >
                      <TouchableOpacity style={{height:42,flex:1,justifyContent:'center',alignItems:'center',padding:10,paddingLeft:25,paddingRight:25,}} onPress={()=>this.filterProductsByCategory(object.item)}>
                          <Text style={textStyleCategory}>{category.category_name}</Text>
                      </TouchableOpacity>
                </CardView>
             );
  }



  _keyExtractorSubCategories = (item, index) =>item.subcategory_id;

  getViewProductsSubCategories(){

    return  <View style={{backgroundColor:'white',marginTop:5,marginRight:15}}>
              <FlatList
                horizontal={true}
                     ref={(ref) => { this.subcategoriesList = ref; }}
                data={this.state.productsSubcategories}
                getItemLayout={(data, index) => (
                      {length: 80, offset: 80 * index, index}
                    )}
                extraData={this.state}
                keyExtractor={this._keyExtractorSubCategories }
                renderItem={(object)=>this.renderItemSubcategory(object)}
                showsHorizontalScrollIndicator={false}
                style={{marginTop:5,}} />
            </View>
  }


  renderItemSubcategory(object){
      subcategory=object.item;
      viewSubcategoryStyle=styles.buttonViewSubcategoryItem;
      textSubcategoryStyle=styles.textSubcategoryNameNormal;
      imageSubcategoryStyle=styles.imageSubcategoryNormal;

      if(subcategory.isSelected){
        textSubcategoryStyle=styles.textSubcategoryNameSelected;
        viewSubcategoryStyle=styles.buttonViewSubcategoryItemPress;
        imageSubcategoryStyle=styles.imageSubcategoryPress;
      }

      return <TouchableOpacity  onPress={()=>this.filterProductsBySubCategory(object.item)} style={[viewSubcategoryStyle,{marginLeft:15,width:width/3.2,height:35}]}>
                <View style={imageSubcategoryStyle}>
                  <Image  style={{width:30,height:30,borderRadius:15,marginLeft:2,}} source={subcategory.subcategory_image}/>
                </View>
                <View style={{flex:1.3,alignItems:'center',justifyContent:'center'}}>
                    <Text   numberOfLines={1} ellipsizeMode={'tail'} style={[textSubcategoryStyle,{textAlign:'center',marginLeft:5,marginRight:5,}]}>{subcategory.subcategory_name}</Text>
                </View>

            </TouchableOpacity>

  }



  managePlaceCategories(place){
      let productsCategories= this.props.place.categories;
      let categoryIdBefore=this.props.categorySelected.category_id;
      let posCategory=0;
      for(i=0;i<productsCategories.length;i++){
          if(productsCategories[i].category_id==categoryIdBefore){
            productsCategories[i].isSelected=1;
            posCategory=i;
          }else{
            productsCategories[i].isSelected=0;
          }
      }

      this.setState({productsCategories});
      this.managePlaceSubcategories(productsCategories[posCategory].category_id)

      setTimeout(()=>{
        if(this.categoriesList!=null){
            this.categoriesList.scrollToIndex({index:posCategory});
        }

      },400);
  }

  managePlaceSubcategories(categoryId){
    let subCategoriesObject= getSubcategoriesByCategoryId(categoryId);
    let subCategoryIdBefore=this.props.categorySelected.subcategory.subcategory_id;
    var subcategories=[];
    if(categoryId!=''){
        let productsCategories= this.props.place.categories;
        let pos=0;
        for(i=0;i<productsCategories.length;i++){
          if(productsCategories[i].category_id==categoryId){
              pos=i;
              break;
          }
        }

        subcategories= productsCategories[pos].subcategories;
        posSubcategory=0;
        for(i=0;i<subcategories.length;i++){
            if(subCategoryIdBefore==subcategories[i].subcategory_id){
                posSubcategory=i;
                subcategories[i].isSelected=1;
            }else{
                    subcategories[i].isSelected=0;
            }
            for(y=0;y<subCategoriesObject.length;y++){
                if(subCategoriesObject[y].id==subcategories[i].subcategory_id){
                      subcategories[i].subcategory_image=subCategoriesObject[y].image.url;
                }
            }

        }
    }
    this.setState({productsSubcategories: subcategories});
    if(subcategories.length>1){
      setTimeout(()=>{
        if(this.subcategoriesList!=null){
            this.subcategoriesList.scrollToIndex({index:posSubcategory});
        }
      },400);
    }


  }


  filterProductsByCategory(category){
      productsCategories=this.state.productsCategories;
       for(i=0;i<productsCategories.length;i++){
          if(category.category_id==productsCategories[i].category_id){
             productsCategories[i].isSelected=1;
               this.categoriesList.scrollToIndex({index:i});
          }else{
             productsCategories[i].isSelected=0;
          }
       }

       this.managePlaceSubcategories(category.category_id);
       this.setState({productsCategories});
       this.props.filterProductsByCategory(category);
  }

  filterProductsBySubCategory(subcategory){
    let  productsSubcategories=this.state.productsSubcategories;
     for(i=0;i<productsSubcategories.length;i++){
        if(subcategory.subcategory_id==productsSubcategories[i].subcategory_id){
           productsSubcategories[i].isSelected=1;
           this.subcategoriesList.scrollToIndex({index:i});
        }else{
           productsSubcategories[i].isSelected=0;
        }
     }

    this.setState({productsSubcategories});
    this.props.filterProductsBySubCategory(subcategory);
  }

  manageProductsCategoriesAndSubcategoriesSelected(){
     category= this.props.categorySelected;
     if(category.category_id!=''){
         subcategory=category.subcategory;
         if(subcategory.subcategory_id!=''){
           subcategory.category_id=category.category_id;
           this.props.filterProductsBySubCategory(subcategory);
         }else{

           this.props.filterProductsByCategory(category);
         }
      }
  }

  componentDidMount(){
    this.managePlaceCategories();
    this.manageProductsCategoriesAndSubcategoriesSelected();
  }



  }

  const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: 'white',
    },
    header: {
      position: 'absolute',
      width:width,
      top: 0,
      left: 0,
      zIndex: 9999
    },
    viewCategoryNormal:{
      backgroundColor:'#15d2bb',
      justifyContent:'center',
      alignItems:'center',
      height:42,
    },
    viewCategoryPress:{
      backgroundColor:'#15d2bb',
      borderBottomWidth:2.5,
      marginBottom:.5,
      borderColor:'white',
      justifyContent:'center',
      alignItems:'center',
      height:42,
    },
    textCategoryNormal:{
      color:'rgba(242, 242, 242,.8)',
      fontSize:15,
    },
    textCategoryPress:{
      color:'white',
      fontWeight:'bold',
      fontSize:15,
    },
    buttonViewSubcategoryItem:{

      borderWidth:.8,
      borderRadius:4,
      borderColor:'#15d2bb',
      flexDirection:'row',
      justifyContent:'flex-start',
      alignItems:'center'
    },
    buttonViewSubcategoryItemPress:{

      borderRadius:4,
      backgroundColor:'#15d2bb',
      flexDirection:'row',
      justifyContent:'flex-start',
      alignItems:'center'
    },

    textSubcategoryNameNormal:{
      fontSize:13,
      fontWeight:'400',

      color:'black',
    },
    textSubcategoryNameSelected:{
      fontSize:14,
      color:'white',
      fontWeight:'500',
    },
    imageSubcategoryNormal:{
      flex:.7,
      alignItems:'center',
      justifyContent:'center'
    },
    imageSubcategoryPress:{
      flex:.1,
      alignItems:'center',
      justifyContent:'center'
    }
  });


  AppRegistry.registerComponent('ProductsCategories', () =>ProductsCategories);
