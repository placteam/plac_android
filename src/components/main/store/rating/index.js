import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Dimensions,
  Image,
Alert,
  FlatList,
  View } from 'react-native';

  // API

  import Api from  '../../../../lib/http_request';
  import * as Routes from '../../../../app/routes'


  import { Rating} from 'react-native-ratings';
import ListViewPagination from '../../../views/ListViewPagination';
  const { width,height} = Dimensions.get('window')
  var viewRatingbarWidth=width-40;
  var heightView=height-80;
  var ratingTotal;
  var params;
  export default class PlaceRating extends Component {
    constructor(props) {
     super(props);
     params=props.navigation.state.params;
     console.warn( params.place_id)
        this.state={
            url_query:Routes.URL_GET_PLACES_RATING_REVIEWS,
          ratingReviews:[],
          ratingBrief:null,
          ratingBars:[],
          paginate:null,
          arrayUrls:[],
        }

   }


   // KEYS FOR FLATLIST
   _keyExtractorRB = (item, index) =>index.toString();
   _keyExtractorRew = (item, index) =>item.assessment_id;

    render(){
     let{ ratingBrief,paginate}= this.state;
     if(ratingBrief==null || paginate==null){
         return null;
     }

     let ratingBarsType= ratingBrief.ratingBarsType.sort(this.compare);





      return <View style={{flex:1,backgroundColor:'white'}}>
                <View style={{margin:15,flex:1}}>
                      <View style={{justifyContent: 'flex-start',alignItems: 'flex-end',flexDirection: 'row'}}>
                        <Text style={{fontSize: 45}}>{ratingBrief.ratingAverage.toFixed(1)}</Text>
                        <Text style={{marginBottom: 7,marginLeft: 10}}>{this.state.paginate.total} valoraciones</Text>
                      </View>
                      <View style={{position: 'absolute',right: 0,top:0,justifyContent: 'center',alignItems: 'center'}}>
                          <View style={{backgroundColor:'#15d2bb',width: 40,height:40,borderRadius: 20,justifyContent: 'center',alignItems: 'center'}}>
                              <Text  style={{color:'white',fontSize: 18}}>{ratingBarsType[0].assessType.toFixed(1)}</Text>
                          </View>
                          <View style={{marginTop: 2}}>
                            <Text style={{color: '#15d2bb',fontSize: 14}}>{ratingBarsType[0].type}</Text>

                          </View>

                      </View>

                      <View style={{flex:.65}}>

                        <FlatList
                            data={this.state.ratingBars}
                            style={{marginTop: 10,}}
                            scrollEnabled={false}
                            initialNumToRender={10}
                            removeClippedSubviews={true}
                            enableEmptySections={true}
                            keyExtractor={this._keyExtractorRB}
                            extraData={this.state}
                            renderItem={(object)=>this._renderRowBar(object)}
                        />
                      </View>
                      <View style={{flex:1.4}}>
                      <ListViewPagination
                                initialNumToRender={5}
                                numColumns={1}
                                isSeparatorActive={false}
                                data={this.state.ratingReviews}
                                isLoading={this.state.isLoading}
                                loadMore={this.state.loadMore}
                                paginate={this.state.paginate}
                                onRefresh={this.onRefresh}
                                keyExtractor={this._keyExtractorRew}
                                handleLoadMore={this.handleLoadMore}
                                renderRow={this._renderRowReview}
                                arrayUrls={this.state.arrayUrls}
                             
                            />
                       
                     </View>




                </View>
            </View>

  }



  _renderRowBar(object){
  let item=object.item;
  let people=item.qty;
  let viewBar=viewRatingbarWidth/1.8;
  let ratingTotal=this.state.ratingBrief.ratingTotal;
  let widthBar=people* viewBar/ratingTotal;
  let percentStart= people*100/ratingTotal;
   let viewStar=viewRatingbarWidth- viewBar;
    return <View style={{flexDirection: 'row',width: viewRatingbarWidth,marginTop: 8}}>
                <View style={{width:viewBar,height: 20,backgroundColor: '#F6F6F6'}}>
                <View style={{backgroundColor: 'grey',position: 'absolute',top:0,left: 0,width:  widthBar,height: 20}}></View>
              </View>
              <View style={{flexDirection: 'row',width: viewStar,justifyContent: 'flex-start',alignItems: 'center',}}>
                <View style={{marginLeft: 10,width: viewStar-60}}>
               
                < Rating
                     type="star"
                     fractions={1}
                     startingValue={item.start}
                     readonly
                     imageSize={15}
                    />
              
                  </View>
                  <View style={{marginLeft: 10}}><Text>{percentStart.toFixed(0)}%</Text></View>
              </View>
            </View>
  }


  _renderRowReview=(object)=>{
    let item=object.item;
    return <View style={{marginTop: 15,}}>
                <View style={{flexDirection: 'row',alignItems: 'center'}}>
                    <Text style={{fontWeight:'bold',color: 'black'}}>{item.plac_user_name}</Text>
                    <View style={{marginLeft: 5}}>

                    < Rating
                     type="star"
                     fractions={1}
                     startingValue={item.assessment_quantity}
                     readonly
                     imageSize={15}
                    />
                
                      </View>
               </View>
               {(item.assessment_message)?<Text style={{marginTop: 3,color: 'black'}}>{item.assessment_message}</Text>:null}
               <Text style={{marginTop:1,fontSize: 12}}>{item.created_ago}</Text>
          </View>
  }




  compare(a, b) {
    const genreA = a.assessType;
    const genreB = b.assessType;

    let comparison = 0;
    if (genreA > genreB) {
      comparison = 1;
    } else if (genreA < genreB) {
      comparison = -1;
    }
    return comparison*-1;
  }

  compareBars(a, b) {
    const genreA = a.start;
    const genreB = b.start;

    let comparison = 0;
    if (genreA > genreB) {
      comparison = 1;
    } else if (genreA < genreB) {
      comparison = -1;
    }
    return comparison*-1;
  }

  onRefresh = () => {
    this.setState({
      url_query: Routes.URL_GET_PLACES_RATING_REVIEWS,
      isLoading: false,
      places: [],
      arrayUrls:[],
    }, () => {
      this.fetchRatingReviews()
    });
  }

  handleLoadMore = (np,arrayUrls) => {
    this.setState({
      url_query: Routes.URL_GET_PLACES_RATING_REVIEWS+np,
      loadMore: true,
      arrayUrls
    }, () => {
        this.fetchRatingReviews()
    });
  }

 

  fetchRatingReviews(){

    var body={place_id:params.place_id};
       Api.post(this.state.url_query,body)
          .then((response)=>{
            let {data,status,message}=response;
            if(status=="success"){
                this.manageResponse(response);
            }else{
                Alert.alert("Ha ocurrido un problema",message);
            }   
    
          }).catch((ex)=>{
            Alert.alert("Ha ocurrido un problema",ex.message);
          });
  }

  manageResponse(response){
  
    let {data}=response;
    let paginate=data;
    let ratingReviews= this.state.ratingReviews;
    data=paginate.data;
    for(i=0;i<data.length;i++){
        ratingReviews.push(data[i]);
    }

    this.setState({
      ratingReviews,
      isSearching:false,
      paginate,
      isLoading:false,
      loadMore:false,
    });
}


  fetchRatingBrief(){

    let body = {
       place_id:params.place_id,
    }

    Api.post(Routes.URL_GET_PLACES_RATING_BRIEF, body)
      .then((response) => {
         
          let {data,status,message}=response;
          if(status=="success"){
              this.setState({
                  ratingBrief:data,
                  ratingBars:data.ratingBars.sort(this.compareBars),
              });
          }else{
              Alert.alert("Ha ocurrido un problema",message);
          }
      }).catch((ex) => {
        Alert.alert("Ha ocurrido un problema catch",ex.message);
      });
  }

  componentDidMount(){
    this.fetchRatingBrief();
    this.fetchRatingReviews();
}

}


  AppRegistry.registerComponent('Rating', () => PlaceRating);
