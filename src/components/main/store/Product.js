import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Alert,
  FlatList,
  Text,
  ScrollView,
  TouchableOpacity,
  Linking,
  Image,
} from 'react-native';

var{width,height}=Dimensions.get('window');

import ButtonContinue from '../../views/ButtonContinue';
import {CachedImage}  from "react-native-img-cache";
import { getOneProductImage, formatPrice } from '../../../lib/product';
import { getProportionalImageHeight, getUrlImageCompress } from '../../../lib/image';
import ReadMore from '../../../lib/text/ReadMore';

// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'
import ProductQuestionRow from './views/ProductQuestionRow';
import { manageQuantities, getOrderDetail } from '../../../app/controllers/OrderDetailController';
import { TEXT_UPDATE_QUANTITIES, TEXT_IN_CART, TEXT_REMOVE_PRODUCT, TEXT_ADD_QUANTITIES } from '../../../app/constants';

import Share from 'react-native-share';
import firebase from 'react-native-firebase';
var params;
var placeLocation;
export default class Product extends React.Component {

    static navigationOptions = ({ navigation, navigationOptions }) => {
        params = navigation.state.params;
        return {
          headerTitle: (
            <View
              style={{
                  flex: 1,
                  flexDirection:'row'
              }}>

              <View style={{flex:1.5,justifyContent:'center',alignItems:'flex-start'}}> 
                  <Text style={{fontSize: 14, color: "black", }}>
                     {(params.product)?params.product.product_name:"Producto"}
                  </Text>
              </View>
              {params.headerRight}
           
            </View>
          )
        };
      };
  

  constructor(props){
    super(props);
    params=props.navigation.state.params;
    this.state={
        product:null,
        total:0,
        quantity:1,
        questions:[],
        quantityOrder:1,
        textActionCart:'Agregar al carrito',
     }
  }

  render() {
      
      let product=this.state.product;
      if(product==null){
        return null;
       }
      let productImage=getOneProductImage(product);
      let imageWidth=width/1.7;
      let imageHeigth= getProportionalImageHeight(productImage,imageWidth);
      placeLocation=product.place_location;


   
    return (
      <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false} style={{marginHorizontal:15,marginVertical:15,marginBottom:125}}>
                  <View style={{justifyContent:'flex-start',alignItems:'center'}}>
                  <CachedImage  source={{uri:getUrlImageCompress(productImage.url,80)}} style={{width:imageWidth,height:imageHeigth}}/>
                  <Text style={{fontSize:24,color:'black',marginTop:10,}}>${formatPrice(product.price_final)}</Text>
                  </View>
                  <Text style={styles.textTitle}>Descripción</Text>
                  <ReadMore
                      numberOfLines={6}
                      renderTruncatedFooter={this._renderTruncatedFooter}
                      renderRevealedFooter={this._renderRevealedFooter}
                      >
                          <Text style={{fontSize:16,color:'#555555'}}>
                              {product.product_description}
                          </Text>
                  </ReadMore>
                  <TouchableOpacity  onPress={()=>this.goQuestions()} style={{marginVertical:15,flexDirection:'row',height:48}}>
                    <View style={{flex:1,justifyContent:'center',alignItems:'flex-start'}}>
                        <Text style={[styles.textTitle,{marginVertical:0}]}>Preguntas al vendedor</Text>
                        <Text style={{fontSize:16,color:'#aaaaaa'}}>{product.total_questions} pregunta{(product.total_questions==1)?"":"s"}</Text>
                      </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}> 
                        <ButtonContinue onPress={()=>this.goQuestions()}  isValidData={true} customStyleText={{fontSize:14,color:'#00b99b'}}  customStyle={{height:36,width:'80%',borderWidth:1,borderColor:'#00b99b',backgroundColor:'white'}}  text={"PREGUNTAR"}/>
                      </View>
                  </TouchableOpacity>
                  <FlatList
                                style={{marginTop:10,}}
                                data={this.state.questions}
                                initialNumToRender={5}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={this._keyExtractor}
                                renderItem={this.renderItem}
                            />   
          </ScrollView >

        {this.getViewAddProductToCart()}

      </View>
    );
  }


  getViewAddProductToCart(){
   return   <View elevation={1} style={styles.viewAddCartAbsolute}>
                  <View style={styles.viewContentAddCart}>
                      <View style={{height:30,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize:20,color:'black'}}>Total: $ {this.state.total}</Text>
                      </View>
                    
                      <View style={{flexDirection:'row',height:100,justifyContent:'center',alignItems:'center'}}>
                          <View style={{flex:1,justifyContent:'center',alignItems:'center',flexDirection:'row' }}>
        
                              <TouchableOpacity onPress={()=>this.manageProductInCart('MINUS')} style={{backgroundColor:'#00b99b',width:24,height:24,borderRadius:12,justifyContent:'center',alignItems:'center'}}>
                                 <Text style={{fontSize:20,color:'white'}}> - </Text>
                              </TouchableOpacity>
                              <View style={styles.viewProductQuantity}>
                                <Text style={{color:'black',fontSize:20}}>{this.state.quantityOrder}</Text>
                              </View>

                              <TouchableOpacity  onPress={()=>this.manageProductInCart('ADD')}  style={{backgroundColor:'#00b99b',width:24,height:24,borderRadius:12,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontSize:20,color:'white'}}> + </Text>
                              </TouchableOpacity>

                          </View> 
                          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}> 
                               <ButtonContinue  onPress={()=>this.addQuantitiesToCart()} customStyleText={{fontSize:12}}  isValidData={true} customStyleText={{textAlign:'center'}} customStyle={{width:'100%',height:32,backgroundColor:this.state.viewColorCart}} text={this.state.textActionCart}/>
                               <ButtonContinue  onPress={()=>this.goToBuyProduct()} customStyleText={{fontSize:12}}  isValidData={true} customStyleText={{textAlign:'center'}} customStyle={{width:'100%',height:32,marginTop: 5,backgroundColor:'orange'}} text={"Comprar ya"}/>
                          </View>  
                        </View>
                     </View> 
                </View>
  }


  _keyExtractor= (item, index) => item.question_id;
  renderItem=(object)=>{
    return <ProductQuestionRow placeLocation={placeLocation} object={object}/>
  }

  manageProductInCart(actionType){
    let {quantityInit,quantityOrder}=this.state;
    if(actionType=='ADD'){
       if(quantityOrder<40){
           quantityOrder=quantityOrder+1
           this.setState({
             quantityOrder,
           });
           this.setPriceFinal(quantityOrder);
       }
    }else{
        if(quantityOrder!=0){
          quantityOrder=quantityOrder-1;
          if(quantityInit!=0){
           
                this.setState({
                  quantityOrder,
                });
          
          }else{
             if(quantityOrder>0){
               this.setState({
                 quantityOrder,
               });
             }
          }
          this.setPriceFinal(quantityOrder);
        }
    }
      if(quantityInit>0){

              if(quantityOrder==quantityInit){
                  this.setState({
                    textActionCart:TEXT_IN_CART,
                    viewColorCart:'#ffbc40',
                    buttonActionDisable:false,
                  });
                }else{
                  this.setState({
                    textActionCart:TEXT_UPDATE_QUANTITIES,
                    viewColorCart:'#15d2bb',
                    buttonActionDisable:false,
                  });
                }

                if(quantityOrder==0){
                    this.setState({
                      textActionCart:TEXT_REMOVE_PRODUCT,
                      viewColorCart:'red',
                      buttonActionDisable:false,
                    });
                }
      }
  }

  addQuantitiesToCart(){
    var {product,quantityOrder}=this.state;
    let place=product.place_location;
    manageQuantities(product,place,quantityOrder);
    this.props.navigation.goBack();
  }

  setPriceFinal(quantity){
    let product=this.state.product;
    let total=formatPrice(parseFloat(quantity*product.price_final));
    this.setState({
      total,
    });
  }

  getQuantityInOrderDetail(){
    var {product}=this.state;
    let place=product.place_location;
    let orderDetails= getOrderDetail(place.place_id,product.product_id);
    if(orderDetails.length>0){
        orderDetail=orderDetails[0];
        let quantity=orderDetail.quantity;
        this.setPriceFinal(quantity);
        this.setState({
          textActionCart:TEXT_IN_CART,
          viewColorCart:"#15d2bb",
          quantityOrder: quantity,
          quantityInit:quantity
        });
    }else{
      this.setState({
        textActionCart:TEXT_ADD_QUANTITIES,
        viewColorCart:"#ffbc40",
        quantityOrder:1,
        total:product.price_final,
      });
    }


  }

  goToBuyProduct(){
    var {product,quantityOrder}=this.state;
    let place=product.place_location;
    manageQuantities(product,place,quantityOrder);
    params.place=place;
    this.props.navigation.navigate('UserAddress',params)
   
  }





  goQuestions(){
    params.product=this.state.product;
    params.addQuestion=this.addQuestionCallBack;
    this.props.navigation.navigate('ProductQuestions',params);
  }

  addQuestionCallBack=(question)=>{
    questions=this.state.questions;
    questions.unshift(question);
    this.setState({questions});

  }

  shareProduct(product){

    let productName=product.product_name;
    let productDescription=product.product_description;
    let shareOptions = {
      title: productName,
      url:product.product_dynamic_link,
      message:productDescription,
      subject:"Mira esto que me encontre en PLAC"
    };
     Share.open(shareOptions);
   
  
  }

  updateProduct(dynamicLink){
    let  URL=Routes.URL_RESOURCE_PRODUCT+"/"+params.product.product_id;
    let body={
      _method:"PUT",
      request_type:"update_dynamic_link",
      dynamic_link:dynamicLink
    }
    
    Api.post(URL,body)
     .then((response)=>{

      let {data,status,message}=response;
      if(status=="success"){
    
      }else{
          Alert.alert("Ha ocurrido un problema",message);
      }
     
     }).catch((ex)=>{
        Alert.alert("Ha ocurrido un problema",ex.message);
        
     });
  }




  fetchProduct(){
      let  URL=Routes.URL_RESOURCE_PRODUCT+"/"+params.product.product_id;
      console.warn(URL);
      
      Api.get(URL)
       .then((response)=>{

        let {data,status,message}=response;
        if(status=="success"){
            this.setState({
                product:data
            })
            this.setState({questions:data.questions});
          
            this.props.navigation.setParams({product:data,headerRight:this.getViewHeaderRight(data)});
            this.getQuantityInOrderDetail();
        }else{
            Alert.alert("Ha ocurrido un problema",message);
        }
       
       }).catch((ex)=>{
          Alert.alert("Ha ocurrido un problema",ex.message);
          
       });
      
  }
  getViewHeaderRight(product){
    return  <TouchableOpacity  onPress={()=>this.shareProduct(product)} style={{flex:.5,justifyContent:'center',alignItems:'flex-end'}}> 
              <Image style={{width:20,height:20,marginRight:20}} source={require('../../../assets/image/general/share_color2.png')}></Image>
            </TouchableOpacity>
  }



  componentDidMount() {
    Linking.addEventListener('url', this.handleOpenURL);
  }
  
  componentWillUnmount() {
    Linking.removeEventListener('url', this.handleOpenURL);
  }
  
  handleOpenURL(event) {
    console.warn(event.url);

    // do something with the url, in our case navigate(route)
  }

  


  componentDidMount(){
      this.fetchProduct();
    

  }
}


  



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textTitle:{
    fontSize:16,
    color:'black',
    fontWeight:'bold',
    marginVertical:15
  },
  imageUser:{
    width:36,
    height:36,
    borderRadius:18,
  },
  textName:{
    fontSize:14,
    color:'black',
    fontWeight:'bold'
  },
  viewContentAddCart:{
    marginHorizontal:15,
    justifyContent:'flex-start',
    alignItems:'center'
  },
  viewContainerQuestion:{
    flexDirection:'row',marginTop:10,
    justifyContent:'flex-end',
    alignItems:'flex-start'
  },
  viewQuestion:{
    backgroundColor:'#ededed',
    width:'70%',
    padding:5,
    borderRadius:8,
  },
  textQuestion:{
    fontSize:14,
    color:'black'
  },
  viewProductQuantity:{
    marginHorizontal:5,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:8, 
    height:48,
    width:48,
    borderWidth:2,
    borderColor:'#15d2bb'
  },
  viewAddCartAbsolute:{
    borderRadius:1,
    backgroundColor:'white',
    shadowColor: '#000000',
      shadowOffset: {
        width: 20,
        height:20
      },
    shadowRadius: 1,
    shadowOpacity: .3,  
    position:'absolute',
    bottom:0,
    width,
  }
    
  
  
 

  

});
