import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, Text, View,FlatList,SectionList} from 'react-native';

import Api from '../../../lib/http_request/';

import * as PlacUserController from '../../../app/controllers/PlacUserController'
import * as ProfileController from '../../../app/controllers/ProfileController'

import ListRow from '../../views/profile/ListRow';

var placUserDevice;
var params;
export default class ShowContactsSuggestInPlac extends Component {


  static navigationOptions = {
    title:"Me gustas",
    header:null,
  };

  constructor(props) {
    super(props);
    params=props.navigation.state.params;
    this.state = {
        placUsers:[],

    }
    placUserDevice=PlacUserController.getCurrentUser();

  }

  render() {

    return (
      <View style={styles.container}>
          <View style={styles.content}>
          
                <View style={{marginTop: 15,justifyContent: 'center', alignItems: "center",}}>
                    <Text style={{ fontSize: 18, textAlign: "center",color:'black'}}> Tienes {this.state.placUsers.length} amigos en PLAC </Text>
                    <View style={{borderBottomWidth:1,width:'40%',borderBottomColor:'#15d2bb',marginTop:10,}}></View>
                </View>

                <SectionList
                  stickySectionHeadersEnabled={true}
                  renderItem={this.renderItemProfiles}
                  renderSectionHeader={this.renderSectionHeader}
                  sections={this.state.placUsers}
                  keyExtractor={(item, index) => item + index}
                />
                {this. getViewContactsList()}
          </View>
      </View>
    );
  }

  renderSectionHeader=(object)=>{
     let placUser=object.section;
     let profilesNumber=placUser.data.length;
    
     return   <View style={{height:40,flexDirection:'row',width:'100%',backgroundColor:'white'}}>
                  <Image style={{width:40,height:40,borderRadius:20}} source={{uri:placUser.plac_user_image}}/>
                  <View style={{marginLeft:10}}>
                      <Text style={{color:'black'}}>{placUser.plac_user_name}</Text>
                      <Text  style={{color:'black'}}> {profilesNumber}  mascota{(profilesNumber>1)?"s":""}</Text>
                  </View>
              </View>
  }



  _keyExtractorUser = (item, index) => index.toString();
  getViewContactsList() {
      

      return <View style={{ flex: 1 }}>
          
                <FlatList
                    style={{ flex: 1, marginTop: 10, }}
                    data={this.state.placUsers}
                    extraData={this.state}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={this._keyExtractorUser}
                    renderItem={this.renderItemUser}
                />
            </View>

  }


  _keyExtractorProfile = (item, index) => item.profile_id;
  renderItemUser = (object) => {
    let placUser = object.item;
    let profiles = placUser.profiles;
    return <FlatList
                style={{ flex: 1, marginTop: 10, }}
                data={profiles}
                extraData={this.state}
                showsVerticalScrollIndicator={false}
                keyExtractor={this._keyExtractorProfile}
                renderItem={(object) => this.renderItemProfiles(object, placUser)}
          />

  }

  renderItemProfiles = (object) => {
      let profile = object.item;
      let placUser=object.section;
      profile.plac_user = placUser;
      return <ListRow isFollowing={false} navigation={{
        state: {
          params: {profileFromId:ProfileController.getCurrentProfileSelectedByUser(placUserDevice.plac_user_id).profile_id,...params}
        }
      }} profile={profile} />
  }

  componentDidMount(){
      let placUsersFromContacts=params.placUsersFromContacts;
      for(i=0;i<placUsersFromContacts.length;i++){
            let placUser=placUsersFromContacts[i];
            let profiles=placUser.profiles;
            placUser.data=profiles;
            placUser.profiles=null;
            placUsersFromContacts[i]=placUser;
        
      }
      console.warn(placUsersFromContacts);
    
    this.setState({
        placUsers:params.placUsersFromContacts
    }); 
  }





}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
  },
  content:{
   marginHorizontal:15,
  },
  viewButtonsAbsolute: {
    position: 'absolute',
    height: 80,
    bottom:20,
    width: '100%',
    justifyContent: 'center'
  },
  buttonContinue: {
    borderRadius: 8,
    backgroundColor: '#15d2bb',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },
  buttonSkip: {
    marginTop: 10,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },



});



 

  