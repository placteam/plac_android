import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, BackHandler, ToastAndroid } from 'react-native';

import Api from '../../../lib/http_request/';
import * as Routes from '../../../app/routes'
import * as PlacUserController from '../../../app/controllers/PlacUserController'

import ListRow from '../../views/profile/ListRow';
import ListViewPagination from '../../views/ListViewPagination';


const placUserDevice = PlacUserController.getCurrentUser();

export default class ShowProfilesSuggest extends Component {
  static navigationOptions = {
    title: "Me gustas",
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      profiles: [],
      arrayUrls: [],
      profiles: [],
      isLoading: true,
      url_query: Routes.URL_GET_PROFILES_SUGGEST,
      loadMore: false,
      paginate: null,
    }


  }

  render() {


    return (
      <View style={styles.container}>
        <View style={{ marginTop: 15, marginLeft: 15, justifyContent: 'center', }}>
          <Text style={{ fontSize: 16, color: 'black' }}>Perfiles sugeridos</Text>
          <View style={{ borderBottomWidth: 1, width: '50%', borderBottomColor: '#15d2bb', marginTop: 10, }} />
        </View>

        {this.getViewContactsList()}

        {(this.props.navigation.state.params.skip || this.props.skip) ? <View style={styles.viewButtonsAbsolute}>
          <TouchableOpacity onPress={this.onPressContinue} style={styles.buttonContinue}>
            <Text style={styles.textButtonContinue}> Continuar </Text>
          </TouchableOpacity >
        </View> : null}

      </View>
    );
  }

  getViewContactsList() {

    return <View style={{ flex: 1 }}>
      <ListViewPagination
        initialNumToRender={5}
        data={this.state.profiles}
        isLoading={this.state.isLoading}
        loadMore={this.state.loadMore}
        isSeparatorActive={true}
        paginate={this.state.paginate}
        onRefresh={this.onRefresh}
        keyExtractor={this._keyExtractorProfile}
        renderRow={this.renderItemProfile}
        handleLoadMore={this.handleLoadMore}
        arrayUrls={this.state.arrayUrls} />
    </View>
  }

  _keyExtractorProfile = (item, index) => item.profile_id;

  renderItemProfile = (object) => {
    let profile = object.item;
    return <ListRow isFollowing={false} navigation={this.props.navigation} profile={profile} />
  }

  onPressContinue = () => {
    this.props.navigation.navigate('AuthLoading', null);
  }

  onRefresh = () => {
    this.setState({
      url_query: Routes.URL_GET_PROFILES_SUGGEST,
      isLoading: false,
      profiles: [],
      arrayUrls: [],
    }, () => {
      this.fetchSuggestProfiles();
    });
  }

  handleLoadMore = (np, arrayUrls) => {
    console.warn(Routes.URL_GET_PROFILES_SUGGEST + np);

    this.setState({
      url_query: Routes.URL_GET_PROFILES_SUGGEST + np,
      loadMore: true,
      arrayUrls,
    }, () => {
      this.fetchSuggestProfiles();
    });
  }


  fetchSuggestProfiles() {
    let profileFrom = this.props.profileFrom || this.props.navigation.state.params.profileFrom;
    var body = {
      filters: {
        pet_type: profileFrom.pet_type,
        breed_id: profileFrom.breed_id,
        profile_type: profileFrom.profile_type,
      },
      profile_from_id: profileFrom.profile_id

    }


    Api.post(this.state.url_query, body)
      .then((response) => {
        var { data, status, message } = response;
        if (status == "success") {
          this.manageResponse(response);
        }

      }).catch((ex) => {
      });
  }

  manageResponse(response) {

    let { data } = response;
    let paginate = data;
    let profiles = this.state.profiles;
    data = paginate.data;
    for (i = 0; i < data.length; i++) {
      profiles.push(data[i]);
    }
    let nProfilesToFollow = 0;
    let nProfiles = profiles.length;
    if (nProfiles > 0) {
      if (nProfiles < 10) {
        nProfilesToFollow = Math.ceil(nProfiles / 2);
      } else if (nProfiles >= 10) {
        nProfilesToFollow = 8;
      }
    }

    this.setState({
      profiles,
      nProfilesToFollow,
      isSearching: false,
      paginate,
      isLoading: false,
      loadMore: false,
    });
  }


  componentDidMount() {
    this.fetchSuggestProfiles();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    let isFocused = this.props.navigation.isFocused();

    return isFocused
  }





}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    marginHorizontal: 15,
  },
  viewButtonsAbsolute: {
    position: 'absolute',
    height: 100,
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    backgroundColor: 'white',

  },
  buttonContinue: {
    marginHorizontal: 20,
    borderRadius: 8,
    backgroundColor: '#15d2bb',
    justifyContent: 'center',
    alignItems: 'center',


  },
  textButtonContinue: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    fontSize: 14,
    color: 'white'
  },
  textButtonSkip: {
    fontSize: 12,
    color: '#c0c0c0'
  },
  buttonSkip: {
    marginTop: 10,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },
  boxContinue: {
    position: 'absolute',
    height: 100,
    backgroundColor: 'white',
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: '#15d2bb',
    bottom: 0,
    alignItems: 'center'
  }



});





