import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, Text, View, FlatList, ScrollView } from 'react-native';
import Contacts from 'react-native-contacts';
import Api from '../../../lib/http_request/';

import * as PlacUserController from '../../../app/controllers/PlacUserController'

import ListRow from '../../views/profile/ListRow';
import ShowProfilesSuggest from './ShowProfilesSuggest';

var placUserDevice;
var params;
export default class ShowContactsSuggestInPlac extends Component {


  static navigationOptions = {
    title: "Me gustas",
    header: null,
  };

  constructor(props) {
    super(props);
    params = props.navigation.state.params;
    this.state = {
      placUsersFromContacts: [],
      nProfilesToFollow: 5,
      isActiveContinue: false,
    }
  }

  render() {


    return (
      <View style={styles.container}>
        <ScrollView style={{ flex: 1 }}>

          {this.getViewContactsList()}
          {this.getViewSuggestProfiles()}
        </ScrollView>
        <View style={styles.viewButtonsAbsolute}>
          <TouchableOpacity onPress={()=>this.goInviteContacts()} style={styles.buttonContinue}>
            <Text style={styles.textButtonContinue}> Continuar </Text>
          </TouchableOpacity >

        </View>
      </View>
    );
  }



  getViewContactsList() {
    let skip = params.skip;
    let nPlacUserFromContacts = this.state.placUsersFromContacts.length;
    if (!skip) {
      if (nPlacUserFromContacts > 0) {
        return <View style={{ flex: 1 }}>
                    <View style={{ marginTop: 15, marginLeft: 15, justifyContent: 'center', }}>
                        <Text style={{ fontSize: 16, color: 'black' }}>Tus amigos en plac</Text>
                        <View style={{ borderBottomWidth: 1, width: '50%', borderBottomColor: '#15d2bb', marginTop: 10, }} />
                      </View>
                      <FlatList
                        style={{ marginTop: 10 }}
                        data={this.state.placUsersFromContacts}
                        extraData={this.state}
                        ItemSeparatorComponent={() => <View style={{ height: .6, backgroundColor: '#c0c0c0' }}></View>}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={this._keyExtractorUser}
                        renderItem={this.renderItemUser}
                      />
                 </View>
      } else {
        return <Text style={{ marginHorizontal: 15, marginTop: 15, fontSize: 15, alignItems: 'center' }}>Ningún de tus amigos esta en plac</Text>
      }

    }

  }

  getViewSuggestProfiles() {

    return <ShowProfilesSuggest  skip={false}  profileFrom={params.profileFrom} navigation={this.props.navigation} />

  }

  _keyExtractorProfile = (item, index) => item.profile_id;
  renderItemUser = (object) => {

    let placUser = object.item;
    let profiles = placUser.profiles;

    return <FlatList
      style={{ flex: 1 }}
      data={profiles}
      extraData={this.state}
      showsVerticalScrollIndicator={false}
      keyExtractor={this._keyExtractorProfile}
      renderItem={(object) => this.renderItemProfiles(object, placUser)}
    />

  }

  renderItemProfiles = (object, placUser) => {
    let profile = object.item;
    profile.plac_user = placUser;
    return <ListRow  isFollowing={false} navigation={this.props.navigation} profile={profile} />
  }

  goInviteContacts() {
    params.contactsUnMatch=this.state.contactsUnMatch;
    this.props.navigation.navigate('ShowContactsToInvite', params);
  }

  getContactsPhone() {
    Contacts.getAll((err, contacts) => {
      console.warn(contacts);
      
      if (contacts.length > 0) {
        let contactEmails = [];
        let contactCellphones = [];

        for (i = 0; i < contacts.length; i++) {
          let contact = contacts[i];
          if (contact.phoneNumbers.length > 0) {
            contactCellphones.push(contact.phoneNumbers[0].number);
          }
          if (contact.emailAddresses.length > 0) {
            contactEmails.push(contact.emailAddresses[0].email);
          }
        }

        this.setState({
          contacts,
          contactCellphones,
          contactEmails,
        }, () => {
          this.checkPlacUserContactsInPLAC();
        });
      } else throw err;

    })
  }



  checkPlacUserContactsInPLAC() {

    let URL = 'placusers/contactdata';
    var body = {
      placUserId: params.placUserFrom.plac_user_id,
      ...this.state
    };
    console.warn(body);

    Api.post(URL, body)
      .then((response) => {
        var { data, status, message } = response;
        if (status == "success") {
          this.setState({
            placUsersFromContacts: data,
          });

          this.checkContactsUnMatch(data);
        }

      }).catch((ex) => {

      });
  }


  checkContactsUnMatch(placUsers) {
    let contactsDevice = this.state.contacts;
    let contactsUnMatch = [];
    for (let i = 0; i < contactsDevice.length; i++) {
      let contact = contactsDevice[i];
      contact.isAdded = 1;
      let email = (contact.emailAddresses.length > 0) ? contact.emailAddresses[0].email : "";
      if (email != "") {
        contactsUnMatch.push(contact);
        for (let y = 0; y < placUsers.length; y++) {
          let placUser = placUsers[y];
          if (email == placUser.plac_user_email) {
            contactsUnMatch.pop();
          }
        }
      }
    }
    this.setState({ contactsUnMatch });
  }





  componentDidMount() {
    this.getContactsPhone();
  }





}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    marginHorizontal: 15,
  },
  viewButtonsAbsolute: {
    position: 'absolute',
    height: 100,
    bottom: 0,
    width: '100%',
    justifyContent: 'center',
    backgroundColor: 'white',

  },
  buttonContinue: {
    marginHorizontal: 20,
    borderRadius: 8,
    backgroundColor: '#15d2bb',
    justifyContent: 'center',
    alignItems: 'center',


  },
  textButtonContinue: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    fontSize: 14,
    color: 'white'
  },
  textButtonSkip: {
    fontSize: 12,
    color: '#c0c0c0'
  },
  buttonSkip: {
    marginTop: 10,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },
  textCountDown: {
    fontSize: 14,
    color: 'black',
    textAlign: 'center'
  },
  boxContinue: {
    position: 'absolute',
    height: 100,
    backgroundColor: 'white',
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: '#15d2bb',
    bottom: 0,
    alignItems: 'center'
  }



});





