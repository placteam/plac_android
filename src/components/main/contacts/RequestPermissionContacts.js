import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity,PermissionsAndroid,  BackHandler,ToastAndroid} from 'react-native';

import * as PlacUserController from '../../../app/controllers/PlacUserController'
import * as ProfileController from '../../../app/controllers/ProfileController'
var params;
var profileFrom;
var placUserFrom;

export default class RequestPermissionContacts extends Component {

  static navigationOptions = {
    title:"Me gustas",
    header:null,
  };
  constructor(props) {
    super(props);
    params=this.props.navigation.state.params;
    placUserFrom= PlacUserController.getCurrentUser();
    profileFrom=ProfileController.getCurrentProfileSelectedByUser(placUserFrom.plac_user_id);
    params.placUserFrom=placUserFrom;
    params.profileFrom=profileFrom;
    params.profileFromId=profileFrom.profile_id;
    params.skip=false;

  }
  render() {
    return (
      <View style={styles.container}>
        {this.getViewAboutContactPermissions()}
      </View>
    );
  }


  getViewAboutContactPermissions() {

    return <View style={styles.content}>
                    <View style={styles.viewBoxtHeader}>
                        <Text style={styles.textTitle}>Averigua cuál de tus contactos ya está en plac</Text>
                        <Image style={{ width: 90, height: 90, marginTop: 60 }} source={{ uri: 'https://image.flaticon.com/icons/png/512/1312/1312151.png' }} />
                    </View>
                    <View style={styles.viewButtonsAbsolute}>
                            <TouchableOpacity onPress={this.onPressContinue} style={styles.buttonContinue}>
                               <Text style={styles.textButtonContinue}> Continuar </Text>
                            </TouchableOpacity >
                            <TouchableOpacity  onPress={()=>this.goSuggestProfilesByBreed()} style={styles.buttonSkip}>
                                <Text style={styles.textButtonSkip}>Saltar</Text>
                            </TouchableOpacity >
                     </View>
             </View>
  }

  onPressContinue = () => {
      this.requestContactPermission();
  }

  goSuggestProfilesByBreed(){
    params.skip=true;
    this.props.navigation.navigate('ShowProfilesSuggest',params);
  }

  async requestContactPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          'title': 'Acceso a tus contactos',
          'message': 'Necesitamos acceso a tus contactos' +
                     'para mostrarte cual de tus amigos esta en PLAC '
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.props.navigation.navigate('ShowContactsSuggestInPlac',params);
      } else {
        console.log("Accesso denegado");
      }
    } catch (err) {
      console.warn(err)
    }
  }

    checkContactsUnMatch(placUsers){
        let contactsDevice= this.state.contacts;
        let contactsUnMatch=[];
        for(let i=0;i<contactsDevice.length;i++){
          let contact = contactsDevice[i];
          contact.isAdded=1;
          let email = (contact.emailAddresses.length > 0) ? contact.emailAddresses[0].email : "";
          if(email!=""){
              contactsUnMatch.push(contact);
              for(let y=0;y<placUsers.length;y++){
                let placUser=placUsers[y];
                if(email==placUser.plac_user_email){
                  contactsUnMatch.pop();
                }
              }
          }
        }
        this.setState({contactsUnMatch});
  }

  componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton=()=> {
      let isFocused = this.props.navigation.isFocused();
      return isFocused;
  }




}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    
  },
  content:{
    marginHorizontal:15,
    flex:1
  },
  viewBoxtHeader:{ 
    marginHorizontal: 15, 
    marginTop: 15, 
    justifyContent: 'center', 
    alignItems: "center" 
  },
  textTitle:{ 
    fontSize: 18,
    color:'black', 
    textAlign: "center" 
  },
  textButtonContinue:{ 
    paddingHorizontal: 10, 
    paddingVertical: 8, 
    fontSize: 14, 
    color: 'white' 
  },
  textButtonSkip:{ 
    fontSize: 12, 
    color: '#c0c0c0' 
  },
  viewButtonsAbsolute: {
    position:'absolute',
    height:100,
    backgroundColor:'white',
    width:'100%',
    bottom:0,
  },
  buttonContinue: {
    borderRadius: 8,
    backgroundColor: '#15d2bb',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },
  buttonSkip: {
    marginTop: 10,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },


});



 

  