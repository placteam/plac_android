import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, Text, View, FlatList } from 'react-native';

import Api from '../../../lib/http_request/';
var params;
export default class ShowContactsToInvite extends Component {


  static navigationOptions = {
    title: "Me gustas",
    header: null,
  };

  constructor(props) {
    super(props);
    params = this.props.navigation.state.params;
    this.state = {
      contactsUnMatch: [],
    }

  }

  render() {
    let contactsUnMatch = this.state.contactsUnMatch;
    let countAdded = 0;
    for (i = 0; i < contactsUnMatch.length; i++) {
      if (contactsUnMatch[i].isAdded) {
        countAdded++;
      }
    }

    return (
      <View style={styles.container}>

        <View style={styles.content}>
          <View style={{ marginTop: 15, justifyContent: 'center', alignItems: "center" }}>
            <Text style={{ fontSize: 16, textAlign: "center", color: 'black' }}>Invitar amigos a PLAC</Text>
            <Text style={{ fontSize: 12, marginTop: 10 }}>
              Ayúdanos a construir la comunidad de  mascotas más grande  del mundo invitando a otros usuarios  </Text>
          </View>
          <FlatList
            style={{ marginTop: 10, marginBottom: 110 }}
            data={this.state.contactsUnMatch}
            extraData={this.state}
            renderItem={this.renderItemSimple}
            keyExtractor={(item, index) => item.recordID} />
        </View>

        <View style={styles.viewButtonsAbsolute}>

          {(countAdded != 0) ? <TouchableOpacity onPress={() => this.inviteContacts()} style={styles.buttonContinue}>
            <Text style={{ paddingHorizontal: 10, paddingVertical: 8, fontSize: 14, color: 'white' }}>
              Invitar a {countAdded} contacto{(countAdded != 1) ? "s" : ""}
            </Text>
          </TouchableOpacity > : null}

          <TouchableOpacity onPress={() => this.goAuthLoadingToMain()} style={styles.buttonSkip}>
            <Text style={{ fontSize: 14, color: '#c0c0c0' }}> Saltar</Text>
          </TouchableOpacity >
        </View>
      </View>
    );
  }

  renderItemSimple = (object) => {
    let contact = object.item;
    let styleCheck = (contact.isAdded) ? { backgroundColor: '#15d2bb', width: "100%", height: '100%' } : {};

    let name = contact.givenName + " " + contact.familyName;
    let email = (contact.emailAddresses.length > 0) ? contact.emailAddresses[0].email : "";
    let hasThumbnail = contact.hasThumbnail;
    let thumbnailPath = contact.thumbnailPath;


    return <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }}>
      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
        {(!hasThumbnail) ?
          <View style={{ width: 40, height: 40, borderRadius: 20, backgroundColor: '#15d2bb', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: 'white' }}>{name.substring(0, 1)}</Text>
          </View> : <Image style={{ width: 40, height: 40, borderRadius: 20 }} source={{ uri: thumbnailPath }} />
        }

        <View style={{ marginLeft: 10 }}>
          <Text style={{ color: 'black', fontSize: 14 }}>{name}</Text>
          <Text style={{ fontSize: 12 }}>{email}</Text>
        </View>
      </View>
      <TouchableOpacity onPress={() => this.addContact(contact)} style={[{ borderWidth: 1, borderColor: '#15d2bb', width: 20, height: 20 }]}>
        <View style={[styleCheck]} />
      </TouchableOpacity>
    </View>
  }

  addContact(item) {
    let list = this.state.contactsUnMatch;
    for (i = 0; i < list.length; i++) {
      let itemList = list[i];
      if (itemList.recordID == item.recordID) {
        if (itemList.isAdded) {
          itemList.isAdded = false;
        } else {
          itemList.isAdded = true;
        }
        list[i] = itemList;
      }
    }
    this.setState({ contactsUnMatch: list });
  }

  goAuthLoadingToMain() {
    this.props.navigation.navigate('AuthLoading', {});
  }


  inviteContacts() {
    URL = 'placusers/contactdata/invite';
    var body = {
      contacts: this.state.contactsUnMatch,
      plac_user_id: params.placUserFrom.plac_user_id,
    }
    Api.post(URL, body)
      .then((response) => {
        var { data, status, message } = response;
        if (status == "success") {
          alert('Tus contactos han sido invitados');
          setTimeout(() => {
            this.goAuthLoadingToMain()
          }, 2000);

        }

      }).catch((ex) => {

      });

  }

  componentDidMount() {
    let contactsUnMatch = params.contactsUnMatch;
    (contactsUnMatch.length > 0) ? this.setState({ contactsUnMatch }) : this.goAuthLoadingToMain();
  }



}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    marginHorizontal: 15,
  },
  viewButtonsAbsolute: {
    position: 'absolute',
    height: 100,
    bottom: 0,
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  buttonContinue: {
    borderRadius: 8,
    backgroundColor: '#15d2bb',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },
  textButtonContinue: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    fontSize: 14,
    color: 'white'
  },
  textButtonSkip: {
    fontSize: 12,
    color: '#c0c0c0'
  },
  buttonSkip: {
    marginTop: 10,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },



});





