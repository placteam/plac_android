import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Dimensions,
    FlatList,
    RefreshControl,
    Text,
    TouchableOpacity,
} from 'react-native';

// API
import Api from '../../../lib/http_request/';
import * as Routes from '../../../app/routes'
import ListViewPagination from '../../views/ListViewPagination'
import Search from '../store/views/Search'

import *  as ProfileController from '../../../app/controllers/ProfileController'
import * as PlacUserController from '../../../app/controllers/PlacUserController'
import ListRow from '../../views/profile/ListRow';

var placUser;
var profileDevice;
export default class ProductsSearch extends React.Component {

    static navigationOptions = ({ navigation }) => {
        params = navigation.state.params;
        return {
            headerTitle: <Search placeholder={"Buscar perfiles"} onEndEditing={navigation.getParam('onEndEditing')} onChangeText={navigation.getParam('onChangeText')} />
        }
    };

    constructor(props) {
        super(props);
        params = this.props.navigation.state.params;
        this.state = {
            url_query: Routes.URL_SEARCH_PROFILES,
            profiles: [],
            isLoading: false,
            loadMore: false,
            arrayUrls: [],
            profiles:[],
        }
        placUser = PlacUserController.getCurrentUser();
        console.warn(placUser.plac_user_id);
        
        profileDevice = ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id);
        console.warn(profileDevice);
        
    }

    _keyExtractor = (item) => item.profile_id;
    render() {
        return (
            <View style={styles.container}>
       
                    <ListViewPagination
                        initialNumToRender={5}
                        data={this.state.profiles}
                        isLoading={this.state.isLoading}
                        loadMore={this.state.loadMore}
                        paginate={this.state.paginate}
                        onRefresh={this.onRefresh}
                        keyExtractor={this._keyExtractor}
                        renderRow={this.renderPlaceRow}
                        handleLoadMore={this.handleLoadMore}
                        arrayUrls={this.state.arrayUrls}
                    />
          
            </View>
        );
    }





    onRefresh = () => {
        this.setState({
            url_query: Routes.URL_SEARCH_PROFILES,
            isLoading: false,
            profiles: [],
            arrayUrls: [],
        }, () => {
            this.searchProfiles();
        });
    }

    handleLoadMore = (np, arrayUrls) => {
        this.setState({
            url_query: Routes.URL_SEARCH_PROFILES + np,
            loadMore: true,
            arrayUrls
        }, () => {
            this.searchProfiles();
        });
    }

    renderPlaceRow = (object) => {
       let profile=object.item;
       let isFollowing=profile.isFollowing;
       return  <ListRow  profileFromId={profileDevice.profile_id} isFollowing={isFollowing}  navigation={this.props.navigation} profile={profile} />
    }

    setTextInputHeader(){
        this.props.navigation.setParams({
            onChangeText: this.onChangeText,
            onEndEditing: this.onEndEditing

        });
    }

    onEndEditing = () => {
     
    }

    onChangeText = (search) => {
        this.setState({search, profiles: [], isLoading: true, url_query: Routes.URL_SEARCH_PROFILES }, () => {
            this.searchProfiles();
        });
    }

    searchProfiles() {
        var body = {
            profile_id: profileDevice.profile_id,
            search: this.state.search,
        }
        console.warn(body);
    
        Api.post(this.state.url_query, body)
            .then((response) => {
                this.manageResponse(response);
            }).catch((ex) => {
                console.warn(ex.message);
            });
    }

    manageResponse(response) {
        let { data } = response;
        let paginate = data;
        let profiles = this.state.profiles.slice();
        data = paginate.data;
        for (i = 0; i < data.length; i++) {
            profiles.push(data[i]);
        }
        this.setState({
            profiles,
            isSearching: false,
            paginate,
            isLoading: false,
            loadMore: false,
        });
    }
    
    componentWillMount(){
        this.setTextInputHeader();
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    buttonPriceFilter: {
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#15d2bb',
        width: '25%',
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonPriceFilterSelected: {
        backgroundColor: '#15d2bb'
    }

});
