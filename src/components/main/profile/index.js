import React, { Component } from 'react';
import {
   AppRegistry,
  Platform,
  StyleSheet,
  Text,
  Image,
  View
} from 'react-native';


// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'

import ToolbarMain from '../../views/ToolbarMain'
import Button from '../../views/ButtonContinue'


import {getFemaleGender,getMaleGender} from '../../../assets/image/svg'


import {getAge,getBirthdayDate} from  '../../../lib/date';


import ProfilePosts from '../../views/posts/ProfilePosts';

import *  as ProfileController from '../../../app/controllers/ProfileController'
import * as PlacUserController from '../../../app/controllers/PlacUserController'
import { getUrlImageCompress } from '../../../lib/image';
var params;
export default class Store extends React.Component {

  static navigationOptions = ({ navigation }) => {
    params=navigation.state.params;
    headerTitle=null;
    if(params.plac_user!=undefined){
      placUser=params.plac_user;
      headerTitle=(<View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center',marginRight:40}}>
                   {(placUser.plac_user_image)? <Image style={{width:36,height:36,borderRadius:18}} source={{uri:getUrlImageCompress(placUser.plac_user_image,20)}}/>:null}
                    <Text style={{marginLeft:10,fontSize:14,color:'black'}}>{placUser.plac_user_name}</Text>
                </View>);
    }
    return {
      headerTitle
    };
  };




  constructor(props){
    super(props);
    params=props.navigation.state.params;
    this.state={
      countFollowers:0,
      countFollowing:0,
      postsNumber:0,
      profile:{},
    }
   
    
  }


  render() {
    profile=this.state.profile;
    return (
      <View style={styles.container}>
        <View  style={{flex:1,marginLeft: 15,marginRight: 15}}>
       
            <ProfilePosts viewHeader={ <View>
              <View style={{alignItems: 'center'}}>
                    <Image style={styles.imageProfile} source={{uri:profile.profile_image}}/>
                    <View style={{flexDirection: 'row',marginTop: 10}}>
                        <View style={{flex:1}}></View>
                        <View style={styles.viewProfileName}>
                          <Text style={{fontSize: 20,color:'black'}}>{profile.profile_name}</Text>
                          <Text style={{fontSize: 14}}>{profile.profile_unique_name}</Text>
                        </View>


                          <View style={{flex:1,justifyContent: 'flex-end',alignItems: 'flex-end'}}>
                        
                        </View>
                    </View>
            </View>
            <View style={styles.viewInfoAboutProfile}>
                <View style={styles.viewInfoBox}>
                     <Text style={styles.textInfo}>Historias <Text style={{color: "black"}}>{this.state.postsNumber }</Text></Text>
                </View>
                 <View style={styles.viewInfoBox}>
                     <Text style={styles.textInfo}>Seguidores <Text style={{color: "black"}}>{this.state.countFollowers}</Text></Text>
                 </View>
                 <View style={styles.viewInfoBox}>
                     <Text style={styles.textInfo}>Siguiendo <Text style={{color: "black"}}>{this.state.countFollowing}</Text></Text>
                 </View>
            </View>

            {(profile.profile_type=='pet')?this.getViewProfilePet(profile):this.getViewProfileCompany()}
    </View>}  postsNumber={(postsNumber)=>this.setState({postsNumber})} profileFrom={params.profileTo} profileTo={params.profileTo} />
        </View>

      </View>
    );
  }



  getViewProfilePet(profile){
    return <View style={{height: 48,flexDirection: 'row',marginTop: 10,}}>
                 <View style={{flex:.5,justifyContent: 'center',alignItems: 'center'}}>
                 <View style={{width:48,height:48,justifyContent: 'center',alignItems: 'center',}}>
                   {this.getGenderView()}
                 </View>
                 
                 </View>
                 <View style={{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                    <Text style={{fontSize: 12,color:'black'}}> 
                      {(profile.breed)?profile.breed.breed_name:null}
                 
                    </Text>
                     <Text style={{fontSize: 12,color:'black'}}>Edad:{getAge((profile.pet_birthday)?profile.pet_birthday:"NA")}</Text>
                     <Text style={{fontSize: 12,color:'black'}}>Cumpleaños : {getBirthdayDate(profile.pet_birthday)} </Text>
                 </View>
                 <View style={{flex:.5}}/>
            </View>
  }

  getGenderView(){
      if(profile.pet_gender){
          if(profile.pet_gender=='MALE'){
          return  getMaleGender();
          }else{
            return  getFemaleGender();
          }
      }  
  }

  getViewProfileCompany(){

    return null;

  }


  getViewPlacUserDevice(){
    return <View style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center'}}>
              <Image style={{width: 36,height: 36,borderRadius: 18}} source={{uri:placUser.plac_user_image}}/>
              <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{fontSize: 14,marginLeft: 5,color:'black'}}>
                {placUser.plac_user_name}
              </Text>
          </View>
  }



  getCountFollowers(profileTo){
    url=Routes.URL_PROFILES_FOLLOWING_COUNT+profileTo.profile_id;
    Api.get(url)
     .then((response)=>{
       var {data,status} =response;
       if(status=='success'){
         this.setState({
           countFollowers:data.count_followers,
           countFollowing:data.count_following
         });
       }

     }).catch((ex)=>{
        console.warn(ex);
     });

  }
  
  getProfile(profileTo){
    
    url=Routes.URL_PROFILES_RESOURCE+"/"+params.profileTo.profile_id;

    
    Api.get(url)
     .then((response)=>{
       var {data,status} =response;
       if(status=='success'){
         this.setState({
           profile:data,
         },()=>{
            params.plac_user= this.state.profile.plac_user;
            this.props.navigation.setParams(params);
         });
       
      
       }
     }).catch((ex)=>{
        console.warn(ex);
     });
  }




  componentDidMount(){
         profileTo=params.profileTo;
         this.getProfile();
         this.getCountFollowers(profileTo);

  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'

  },
  imageProfile:{
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 20,
  },
  viewProfileName:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  buttonEditProfile:{
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor:"#00b99b",
    width: "80%"
  },
  textEditProfile:{
    color:'#00b99b',
    fontSize:14
  },
  viewInfoAboutProfile:{
    height: 36,
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'space-around',
    marginTop: 20,
    backgroundColor: "#ededed"
  },
  textInfo:{
    fontSize: 14,
    color:'#555555'
  },
  viewInfoBox:{
    justifyContent: 'center',
    alignItems: 'center'
  }

});
