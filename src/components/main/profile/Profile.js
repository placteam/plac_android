import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';


// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'

import ToolbarMain from '../../views/ToolbarMain'
import Button from '../../views/ButtonContinue'

import {getFemaleGender,getMaleGender} from '../../../assets/image/svg'
import {getAge,getBirthdayDate} from  '../../../lib/date';


import ProfilePosts from '../../views/posts/ProfilePosts';

import *  as ProfileController from '../../../app/controllers/ProfileController'
import * as PlacUserController from '../../../app/controllers/PlacUserController'
import { CachedImage } from 'react-native-img-cache';
import { getUrlImageCompress } from '../../../lib/image';
isProfileDevice=true;
export default class Profile extends React.Component {

  static navigationOptions = {
    header:null
  }

  constructor(props){
    super(props);
    placUser=PlacUserController.getCurrentUser();

    this.state={
      countFollowers:0,
      countFollowing:0,
      postsNumber:0,
      placUser,
      profile:{},
    }




  }


  render() {
    profile=this.state.profile;
    return (
      <View style={styles.container}>
        {(isProfileDevice)?<ToolbarMain btnLeftDisabled={true}  onPressMiddle={()=>{this.goPlacUser()}}  viewMiddleStyle={{flex:1.5,justifyContent: 'center'}} viewMiddle={this.getViewPlacUserDevice()} navigation={this.props.navigation}/>:null}
        <View  style={{flex:1,marginLeft: 15,marginRight: 15}}>
            <ProfilePosts  navigation={this.props.navigation} viewHeader={ <View>
                      <View style={{alignItems: 'center'}}>
                            <CachedImage style={styles.imageProfile} source={{uri:getUrlImageCompress(profile.profile_image,20)}}/>
                            <View style={{flexDirection: 'row',marginTop: 10}}>
                                <View style={{flex:1}}></View>
                                <View style={styles.viewProfileName}>
                                  <Text style={{fontSize: 20,color:'black'}}>{profile.profile_name}</Text>
                                  <Text style={{fontSize: 14}}>{profile.profile_unique_name}</Text>
                                </View>

                                {(isProfileDevice)?
                                  <View style={{flex:1,justifyContent: 'flex-end',alignItems: 'flex-end'}}>
                                  <Button  onPress={this.goProfileEdit} customStyleText={styles.textEditProfile} customStyle={styles.buttonEditProfile} text={"Editar"} isValidData={true} />
                                </View>:null}
                            </View>
                    </View>
                    <View style={styles.viewInfoAboutProfile}>
                        <View style={styles.viewInfoBox}>
                             <Text style={styles.textInfo}>Historias <Text style={{color: "black"}}>{this.state.postsNumber }</Text></Text>
                        </View>
                         <TouchableOpacity  onPress={()=>this.goFollowingTo()}  style={styles.viewInfoBox}>
                             <Text style={styles.textInfo}>Seguidores  <Text style={{color: "black"}}>{this.state.countFollowers}</Text></Text>
                         </TouchableOpacity>
                         <TouchableOpacity onPress={()=>this.goFollowingFrom()} style={styles.viewInfoBox}>
                             <Text style={styles.textInfo}>Siguiendo <Text style={{color: "black"}}>{this.state.countFollowing}</Text></Text>
                         </TouchableOpacity>
                    </View>

                    {(profile.profile_type=='pet')?this.getViewProfilePet(profile):this.getViewProfileCompany()}



            </View> }  postsNumber={(postsNumber)=>this.setState({postsNumber})} profileFrom={ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id)} profileTo={ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id)} />


        </View>



      </View>
    );
  }


  goFollowingFrom(){
    params={
      profileFromId:this.state.profile.profile_id,
      profileToId:this.state.profile.profile_id,
    }
    this.props.navigation.push('FollowingsFrom',params);
  }

  goFollowingTo(){
    params={
      profileFromId:this.state.profile.profile_id,
      profileToId:this.state.profile.profile_id,
    }
    this.props.navigation.push('FollowingsTo',params);
  }



  getViewProfilePet(profile){
    return <View style={{height: 48,flexDirection: 'row',marginTop: 10,}}>
                 <View style={{flex:.5,justifyContent: 'center',alignItems: 'center'}}>
                 <View style={{width:48,height:48,justifyContent: 'center',alignItems: 'center',}}>
                   {this.getGenderView()}
                 </View>
                 
                   
                 </View>
                 <View style={{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                    <Text style={{fontSize: 12,color:'black'}} >{profile.breed_name}</Text>
                     <Text style={{fontSize: 12,color:'black'}}>Edad:{getAge((profile.pet_birthday)?profile.pet_birthday:"NA")}</Text>
                     <Text style={{fontSize: 12,color:'black'}}>Cumpleaños : {getBirthdayDate(profile.pet_birthday)} </Text>
                 </View>
                 <View style={{flex:.5}}/>
            </View>
  }

  getGenderView(){
    if(profile.pet_gender){
      if(profile.pet_gender=='MALE'){
       return  getMaleGender();
      }else{
         return  getFemaleGender();
      }
      
    }
          
  }

  getViewProfileCompany(){

    return null;

  }

  goProfileEdit=()=>{
      var {profile}=this.state;
      profileType=profile.profile_type;
      if(profileType=="pet"){
        params={
          editCallBack:this.editCallBack
        }
        this.props.navigation.navigate('ProfileEdit',params);
      }
  }

  editCallBack=()=>{
    setTimeout(() => {
        placUser=PlacUserController.getCurrentUser();
        profile=ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id);
        this.setState({
          profile
        });
    }, 200);
  
  }

  goPlacUser(){
       params={
          editUserCallBack:this.editUserCallBack
        }
      this.props.navigation.navigate('PlacUserMe',params);
  }

  editUserCallBack=()=>{
        setTimeout(() => {
            placUser=PlacUserController.getCurrentUser();
            this.setState({
              placUser
            });
        }, 200);
  }


  getViewPlacUserDevice(){
    placUser=this.state.placUser;
    return <View style={{flexDirection: 'row',justifyContent: 'center',alignItems: 'center'}}>
              <View style={{flex:.6,}}>
                 <Image style={{width: 36,height: 36,borderRadius: 18}} source={{uri:placUser.plac_user_image}}/>
              </View>
              <View style={{flex:2}}>
                <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{fontSize: 14,marginLeft: 5,color:'black'}}>
                  {placUser.plac_user_name}
                </Text>
              </View>
            
            
          </View>
  }





  getCountFollowers(){
     url=Routes.URL_PROFILES_FOLLOWING_COUNT+this.state.profile.profile_id;
    Api.get(url)
     .then((response)=>{
       var {data,status} =response;
       if(status=='success'){
         this.setState({
           countFollowers:data.count_followers,
           countFollowing:data.count_following
         });
       }

     }).catch((ex)=>{
        console.warn(ex);
     });


  }
  



  
componentWillMount(){
  profile=ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id);
  this.setState({
    profile,
  },()=>{
    this.getCountFollowers();
  });

}

  componentDidMount(){
 
 

  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'

  },
  imageProfile:{
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 20,
  },
  viewProfileName:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  buttonEditProfile:{
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor:"#00b99b",
    width: "80%"
  },
  textEditProfile:{
    color:'#00b99b',
    fontSize:14
  },
  viewInfoAboutProfile:{
    height: 36,
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'space-around',
    marginTop: 20,
    backgroundColor: "#ededed"
  },
  textInfo:{
    fontSize: 14,
    color:'#555555'
  },
  viewInfoBox:{
    justifyContent: 'center',
    alignItems: 'center'
  }

});
