import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  Dimensions,
  FlatList,
  TextInput,
  ScrollView,
  Picker,
  TouchableOpacity,
  ToastAndroid
} from "react-native";

//COMPONENTS
import PetTypeListView from "../../views/pet/PetTypeListView";
import DatePicker from "../../views/DatePicker";
import ButtonContinue from "../../views/ButtonContinue";

import {
  validateSpecialCharacters,
  cleanString
} from "../../../lib/validations";
import { getBirthdayDate } from "../../../lib/date";
import * as ProfileController from "../../../app/controllers/ProfileController";
import * as PlacUserController from "../../../app/controllers/PlacUserController";

import PickImage from "../../views/pet/PickImage";

// API
import Api from "../../../lib/http_request/";
import * as Routes from "../../../app/routes";

var params;
export default class ProfileEdit extends React.Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    params = navigation.state.params;
    return {
      headerRight: params.viewRight,
      headerTitle: (
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}>
          <Text style={{ fontSize:16, color: "black", marginRight: 40 }}>
            Editar perfil
          </Text>
        </View>
      )
    };
  };

  constructor(props) {
    super(props);
    params = this.props.navigation.state.params;
    placUser = PlacUserController.getCurrentUser();
    profile = ProfileController.getCurrentProfileSelectedByUser(
      placUser.plac_user_id
    );

    this.state = {
      textValueSearch: "",
      breeds: [],
      isDisabledPickBreed: false,
      isVisibleModalPickBreed: true,
      isValidPetUniqueName: true,
      isEditablePetUniqueName: false,
      indexError: -1,
      isLoading: false,
      isPickBirthday: true,
      ...profile,
      pet_birthday: getBirthdayDate(profile.pet_birthday),
      pet_birthday_c: profile.pet_birthday,
      breedSelected: {
        breed_id: profile.breed_id,
        breed_name: profile.breed_name
      },
      isValidData: true,
      profile_image_new: "empty"
    };
  }

  render() {
    isDisabledPickBreed = this.state.isDisabledPickBreed;
    breedSelected = this.state.breedSelected;
    var buttonStyleNotPress = {};
    if (!isDisabledPickBreed) {
      buttonStyleNotPress.backgroundColor = "#ededed";
    }

    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ flex: 1, marginLeft: 24, marginRight: 24, marginBottom: 20 }}
        >
          <PickImage
            pickerImageConfig={{ width: 600, height: 600 }}
            onImageSelected={this.onImageSelected}
            image={{ uri: this.state.profile_image }}
            styleContainer={{ marginTop: 20 }}
          />

          <View>
            <Text style={styles.textLabel}>Nombre de tu mascota</Text>
            <TextInput
              ref={input => {
                this.profile_name = input;
              }}
              returnKeyType={"next"}
              style={styles.textInputData}
              onChangeText={this.onChangePetName}
              placeholder="Ingresa el nombre de tu mascota"
              placeholderTextColor="#C0C0C0"
              value={this.state.profile_name}
            />
            {this.getViewErrorMessage(0)}
          </View>

          <View style={{ marginTop: 20 }}>
            <Text style={styles.textLabel}>Nombre unico para tu mascota</Text>
            <View style={{ flexDirection: "row" }}>
              <View style={{ width: "70%" }}>
                <TextInput
                  ref={input => {
                    this.profile_unique_name = input;
                  }}
                  returnKeyType={"next"}
                  editable={this.state.isEditablePetUniqueName}
                  onEndEditing={this.onSubmitUniqueName}
                  onSubmitEditing={this.onSubmitUniqueName}
                  style={[styles.textInputData, { width: "100%" }]}
                  onChangeText={profile_unique_name =>
                    this.setState({
                      profile_unique_name: cleanString(profile_unique_name)
                    })
                  }
                  placeholder="Ingresa el nombre de usuario"
                  placeholderTextColor="#C0C0C0"
                  value={this.state.profile_unique_name}
                />
                {this.getViewErrorMessage(1)}
              </View>

              <View
                style={{
                  width: "30%",
                  justifyContent: "center",
                  alignItems: "flex-end"
                }}
              >
                <TouchableOpacity
                  onPress={this.manageButtonVerifyPetUniqueName}
                  style={[
                    styles.buttonVerifyPetUniqueName,
                    this.state.isEditablePetUniqueName
                      ? { backgroundColor: "orange", borderWidth: 0 }
                      : {}
                  ]}
                >
                  <Text
                    style={{
                      color: this.state.isEditablePetUniqueName
                        ? "white"
                        : "#00B99B",
                      fontSize: 14
                    }}
                  >
                    {this.state.isEditablePetUniqueName
                      ? "Verificar"
                      : "Modificar"}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={styles.textLabel}>Sexo</Text>
            <Picker
              selectedValue={this.state.pet_gender}
              mode={"dropdown"}
              style={{ height: 50, width: "100%" }}
              onValueChange={(pet_gender, itemIndex) =>
                this.setState({ pet_gender })
              }
            >
              <Picker.Item label="Macho" value="MALE" />
              <Picker.Item label="Hembra" value="FEMALE" />
            </Picker>
            {this.getViewErrorMessage(2)}
          </View>

          <View style={{ marginTop: 10 }}>
            <Text style={styles.textLabel}>¿Qué tipo de mascota es?</Text>
            <View style={{ width: "100%" }}>
              <PetTypeListView
                profile={this.state}
                petTypeSelected={this.petTypeSelected}
              />
            </View>

            {this.getViewErrorMessage(3)}
          </View>
          <View style={{ marginTop: 20 }}>
            <Text style={styles.textLabel}>¿Cuál es su raza?</Text>
            {breedSelected == null ? (
              <TouchableOpacity
                disabled={!isDisabledPickBreed}
                onPress={this.goPickBreed}
                style={[styles.buttonPickBreed, buttonStyleNotPress]}
              >
                <Text style={{ color: "white", fontSize: 16 }}>
                  Seleccionar raza
                </Text>
              </TouchableOpacity>
            ) : (
                <View style={styles.viewBreedName}>
                  <View style={styles.viewTextBreedName}>
                    <Text style={{ fontSize: 16, color: "black" }}>
                      {breedSelected.breed_name}
                    </Text>
                  </View>
                  <View style={styles.viewButtonChangeBreed}>
                    <TouchableOpacity
                      onPress={this.goPickBreed}
                      style={styles.buttonChangeBreed}
                    >
                      <Text style={{ color: "#00B99B", fontSize: 14 }}>
                        Cambiar
                    </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )}
            {this.getViewErrorMessage(4)}
          </View>

          <View style={{ marginTop: 20 }}>
            <Text style={[styles.textLabel]}>Cumpleaños</Text>
            <DatePicker
              onDateChange={this.onDateChange}
              customComponent={
                <View style={styles.viewContainerDate}>
                  <Text
                    style={{
                      fontSize: 16,
                      color: this.state.isPickBirthday ? "black" : "#C0C0C0"
                    }}
                  >
                    {this.state.pet_birthday}
                  </Text>
                </View>
              }
            />

            {this.getViewErrorMessage(5)}
          </View>

          {this.getViewErrorMessage(6)}
        </ScrollView>
      </View>
    );
  }

  getViewErrorMessage(indexInput) {
    indexError = this.state.indexError;
    if (indexInput == indexError) {
      return (
        <Text style={[this.state.messageStyle, { marginTop: 10 }]}>
          {this.state.message}
        </Text>
      );
    }
  }

  onImageSelected = image => {
    this.setState({
      profile_image: image.imageView,
      profile_image_new: image.imageSelected
    });
  };

  onChangePetName = petName => {
    if (!validateSpecialCharacters(petName)) {
      this.setState({
        indexError: 0,
        message: "Nombre no valido " + petName,
        messageStyle: styles.messageWarning,
        profile_name: petName
      });
    } else {
      this.setState({
        indexError: -1,
        profile_name: petName
      });
    }
  };

  onDateChange = date => {
    this.setState({
      pet_birthday: getBirthdayDate(date),
      pet_birthday_c: date,
      isPickBirthday: true
    });
  };

  onSubmitUniqueName = () => {
    this.verifyUniqueNameBefore(this.state.profile_unique_name);
  };

  manageButtonVerifyPetUniqueName = () => {
    if (this.state.isEditablePetUniqueName) {
      this.onSubmitUniqueName();
    } else {
      this.profile_unique_name.focus();
      this.setState({
        isEditablePetUniqueName: true,
        isValidPetUniqueName: false
      });
    }
  };

  verifyUniqueNameBefore(profile_name) {
    profile_unique_name = this.getPetUniqueName(profile_name);

    if (profile_unique_name.length >= 3 && profile_unique_name.length <= 19) {
      this.verifyUniqueName(profile_unique_name);
    } else {
      this.setState({
        indexError: 1,
        isValidPetUniqueName: false,
        isEditablePetUniqueName: true,
        message: "Nombre de perfil unico muy largo, debe ser menor a 20",
        messageStyle: styles.messageError
      });
    }
  }

  getPetUniqueName(petName) {
    return cleanString(petName.toLowerCase().trim());
  }

  petTypeSelected = petType => {
    params.petType = petType;
    if (petType.id == profile.pet_type) {
      var breedSelected = {
        breed_id: profile.breed_id,
        breed_name: profile.breed_name
      };
      this.setState({ breedSelected });
    } else {
      this.setState({ breedSelected: null });
    }

    this.setState({ isDisabledPickBreed: true, pet_type: petType.id });
  };
  goPickBreed = () => {
    params.breedSelectedCallBack = this.breedSelected;
    this.props.navigation.navigate("PickBreed", params);
  };

  breedSelected = breed => {
    params.breed = breed;
    this.setState({ breedSelected: breed, indexError: -1 });
  };

  update = () => {
    var {
      profile_name,
      profile_unique_name,
      pet_birthday_c,
      isValidPetUniqueName,
      pet_birthday,
      pet_type,
      breedSelected,
      pet_gender,
      profile_image,
      profile_image_new
    } = this.state;
    if (!profile_name) {
      return this.setState({
        indexError: 0,
        message: "Te hace falta el nombre",
        messageStyle: styles.messageError
      });
    }

    if (!isValidPetUniqueName) {
      return this.setState({
        indexError: 1,
        message: "El usuario unico no es valido",
        messageStyle: styles.messageError
      });
    }

    if (!pet_gender) {
      return this.setState({
        indexError: 2,
        message: "Selecciona el sexo",
        messageStyle: styles.messageError
      });
    }

    if (!pet_type) {
      return this.setState({
        indexError: 3,
        message: "Selecciona tipo de  mascota",
        messageStyle: styles.messageError
      });
    }

    if (!breedSelected) {
      return this.setState({
        indexError: 4,
        message: "Selecciona la raza de tu mascota",
        messageStyle: styles.messageError
      });
    }

    if (!pet_birthday_c) {
      return this.setState({
        indexError: 5,
        message: "Ingresa el cumpleaños",
        messageStyle: styles.messageError
      });
    }

    if (profile_image_new == "empty") {
      profile_image = "not_modified";
    } else {
      profile_image = profile_image_new;
      profile_image_new = "";
    }

    this.setState({
      isLoading: true
    });

    var body = {
      _method: "PUT",
      profile: {
        ...this.state,
        pet_birthday: pet_birthday_c,
        breed_id: breedSelected.breed_id,
        profile_image
      }
    };

    this.showLoading();
    url = Routes.URL_PROFILES_RESOURCE + "/" + this.state.profile_id;
    Api.post(url, body)
      .then(response => {
        var { status, data, message } = response;

        if (status == "success") {
          profile = {
            ...body.profile,
            profile_image: data.profile_image,
            breed_name: breedSelected.breed_name
          };

          ProfileController.update(profile);
          ToastAndroid.show("Perfil actualizado", ToastAndroid.SHORT);
          this.goBack();
        } else {
          this.showSave();
          this.setState({
            indexError: 6,
            message: "Ha ocurrido el siguiente error: " + message,
            messageStyle: styles.messageError
          });
        }
      })
      .catch(ex => {
        this.showSave();
        this.setState({
          indexError: 6,
          message:
            "Ha ocurrido el siguiente error: " +
            ex.message +
            " .Intenta otra vez",
          messageStyle: styles.messageError
        });
      });
  };

  verifyUniqueName(profile_unique_name) {
    Api.get(Routes.URL_PROFILE_VERIFY_UNIQUE_NAME + profile_unique_name)
      .then(response => {
        if (response.status == "error") {
          this.setState({
            indexError: 1,
            isEditablePetUniqueName: true,
            isValidPetUniqueName: false,
            message: response.message,
            messageStyle: styles.messageError
          });
        } else {
          this.setState({
            indexError: 1,
            isValidPetUniqueName: true,
            isEditablePetUniqueName: false,
            message: response.message,
            messageStyle: styles.messageSuccess
          });
        }
      })
      .catch(ex => {
        console.log(ex);
      });
  }

  showLoading() {
    params.viewRight = (
      <Image
        style={{ width: 45, height: 17, marginRight: 20 }}
        source={require("../../../assets/image/general/loader.gif")}
      />
    );
    this.props.navigation.setParams(params);
  }

  showSave() {
    params.viewRight = (
      <TouchableOpacity
        onPress={this.update}
        style={{ fontSize: 20, color: "#15d2bb", paddingRight: 20 }}
      >
        <Text style={{ fontSize: 14, color: "#15d2bb" }}>Guardar</Text>
      </TouchableOpacity>
    );
    this.props.navigation.setParams(params);
  }

  goBack() {
    params = this.props.navigation.state.params;
    params.editCallBack();
    this.props.navigation.goBack();
  }

  componentDidMount() {
    this.showSave();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  textLabel: {
    fontWeight: "500",
    color: "#555555",
    fontSize: 12
  },
  textInputData: {
    height: 40,
    borderColor: "#00B99B",
    borderBottomWidth: 1,
    fontSize: 16,
    color: "#212121",
    padding: 0
  },
  buttonVerifyPetUniqueName: {
    borderWidth: 1,
    width: "85%",
    borderColor: "#00B99B",
    justifyContent: "center",
    alignItems: "center",
    height: 36,
    borderRadius: 8
  },
  viewBreedName: {
    height: 48,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  viewTextBreedName: {
    width: "70%",
    padding: 5,
    paddingLeft: 0,
    borderBottomWidth: 1,
    borderBottomColor: "#00B99B"
  },
  viewButtonChangeBreed: {
    width: "30%",
    justifyContent: "center",
    alignItems: "center"
  },
  buttonChangeBreed: {
    borderWidth: 1,
    width: "80%",
    borderColor: "#00B99B",
    justifyContent: "center",
    alignItems: "center",
    height: 36,
    borderRadius: 8
  },
  buttonPickBreed: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    marginTop: 10,
    height: 36,
    backgroundColor: "#00B99B"
  },
  messageSuccess: {
    fontSize: 12,
    color: "green"
  },
  messageWarning: {
    fontSize: 12,
    color: "#f0ad4e"
  },
  messageError: {
    fontSize: 12,
    color: "#d2152b"
  },

});
