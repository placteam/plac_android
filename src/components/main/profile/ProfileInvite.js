import React, { Component } from 'react';
import {
   AppRegistry,
  Platform,
  StyleSheet,
  Text,
  Image,
  View,
  Linking,
  TouchableOpacity,
} from 'react-native';

import { CachedImage } from 'react-native-img-cache';
// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'
import {getUrlImageCompress} from  '../../../lib/image';
import {getFemaleGender,getMaleGender} from '../../../assets/image/svg'
import {getAge,getBirthdayDate} from  '../../../lib/date';
import ProfilePosts from '../../views/posts/ProfilePosts';

import ReadMore from '../../../lib/text/ReadMore'

import ButtonFollower from "../../views/profile/ButtonFollower";


import *  as ProfileController from '../../../app/controllers/ProfileController'
import * as PlacUserController from '../../../app/controllers/PlacUserController'



export default class ProfileInvite extends React.PureComponent{

    static navigationOptions = ({ navigation }) => {
        params=navigation.state.params;
        return {
          headerTitle:params.headerTitle,
        };
      };


  constructor(props){
    super(props);
    this.state={
        countFollowers:0,
        countFollowing:0,
        postsNumber:0,
        profile:null,
        profileId:"",
    }
   
    
  }


  render() {
    profile=this.state.profile;
    if(profile==null){
        return null;
    }

    return (
        <View style={styles.container}>
        <View  style={{flex:1,marginLeft: 15,marginRight: 15}}>
            <ProfilePosts navigation={this.props.navigation} viewHeader={ <View>
              <View style={{alignItems: 'center'}}>
                    <CachedImage style={styles.imageProfile} source={{uri:getUrlImageCompress(profile.profile_image,20)}}/>
                    <View style={{flexDirection: 'row',marginTop: 10}}>
                        <View style={{flex:1}}></View>
                        <View style={styles.viewProfileName}>
                          <Text style={{fontSize: 20,textAlign:'center',color:'black'}}>{profile.profile_name}</Text>
                          <Text style={{fontSize: 14,color:'#757575'}}>@{profile.profile_unique_name}</Text>
                        </View>
                          <View style={{flex:1,justifyContent:'center',alignItems:"flex-end"}}>
                          {this.getButtonFollowing()}
                        
                        </View>
                    </View>
            </View>
            <View style={styles.viewInfoAboutProfile}>
                <View style={styles.viewInfoBox}>
                     <Text style={styles.textInfo}>Historias <Text style={{color: "black"}}>{this.state.postsNumber }</Text></Text>
                </View>
                 <TouchableOpacity  onPress={()=>this.goFollowingTo()}  style={styles.viewInfoBox}>
                     <Text style={styles.textInfo}>Seguidores <Text style={{color: "black"}}>{this.state.countFollowers}</Text></Text>
                 </TouchableOpacity>
                 
                 <TouchableOpacity onPress={()=>this.goFollowingFrom()} style={styles.viewInfoBox}>
                     <Text style={styles.textInfo}>Siguiendo <Text style={{color: "black"}}>{this.state.countFollowing}</Text></Text>
                 </TouchableOpacity>
            </View>

            {(profile.profile_type=='pet')
            ?this.getViewProfilePet(profile):this.getViewProfileCompany(profile)}
    </View>}  postsNumber={(postsNumber)=>this.setState({postsNumber})} profileFrom={this.getProfileFrom()} profileTo={this.state.profile} />
        </View>

      </View>
    );
  }

  goFollowingFrom(){
    params={
      profileFromId:this.getProfileFrom().profile_id,
      profileToId:this.state.profile.profile_id,
    }
    this.props.navigation.push('FollowingsFrom',params);
  }

  goFollowingTo(){
    params={
      profileFromId:this.getProfileFrom().profile_id,
      profileToId:this.state.profile.profile_id,
    }
    this.props.navigation.push('FollowingsTo',params);
  }

  getButtonFollowing(){
    profileFrom=this.getProfileFrom();
    profile=this.state.profile;
    return <ButtonFollower  manageFollower={this.manageFollower}   customStyle={{width:"80%"}} profileFromId={profileFrom.profile_id} profileToId={profile.profile_id}  isFollowing={profile.isFollowing}/>
  }

  manageFollower=(action)=>{
    countFollowers=this.state.countFollowers;
    if(action=="add"){
      countFollowers++;
    }else{
      countFollowers--;
    }
      this.setState({countFollowers});

  }

  

  


  getViewProfilePet(profile){
    return <View style={{height: 48,flexDirection: 'row',marginTop: 10,}}>
                 <View style={{flex:.5,justifyContent: 'center',alignItems: 'center'}}>
                 <View style={{width:48,height:48,justifyContent: 'center',alignItems: 'center',}}>
                   {this.getGenderView()}
                 </View>
                 
                 </View>
                 <View style={{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                    <Text style={{fontSize: 12,color:'black'}}> 
                      {(profile.breed)?profile.breed.breed_name:null}
                 
                    </Text>
                     <Text style={{fontSize: 12,color:'black'}}>Edad:{getAge((profile.pet_birthday)?profile.pet_birthday:"NA")}</Text>
                     <Text style={{fontSize: 12,color:'black'}}>Cumpleaños : {getBirthdayDate(profile.pet_birthday)} </Text>
                 </View>
                 <View style={{flex:.5}}/>
            </View>
  }

  getGenderView(){
      if(profile.pet_gender){
          if(profile.pet_gender=='MALE'){
          return  getMaleGender();
          }else{
            return  getFemaleGender();
          }
      }  
  }

  getViewProfileCompany(profile){

  
    return <View style={{marginTop: 10,marginBottom:10}}>
             <ReadMore
              numberOfLines={4}
              renderTruncatedFooter={this._renderTruncatedFooter}
              renderRevealedFooter={this._renderRevealedFooter}
             >
                   <Text style={{fontSize:14,color:'black'}}>
                      {profile.company_description}
                   </Text>
            </ReadMore>
          
          </View>

  }

  _renderTruncatedFooter = (handlePress) => {
    return (
      <Text style={{marginTop: 2,fontSize:12,color:'#555555'}} onPress={handlePress}>
        Leer mas
      </Text>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text style={{marginTop: 2,fontSize:12,color:'#555555'}} onPress={handlePress}>
        Menos
      </Text>
    );
  }





  getCountFollowers(){
    url=Routes.URL_PROFILES_FOLLOWING_COUNT+this.state.profileId;
    console.warn(url);
    
    Api.get(url)
     .then((response)=>{
       var {data,status} =response;
       if(status=='success'){
         this.setState({
           countFollowers:data.count_followers,
           countFollowing:data.count_following
         });
       }

     }).catch((ex)=>{
        console.warn(ex);
     });

  }


  getProfileFrom(){
    placUser=PlacUserController.getCurrentUser();
    profileToId=this.state.profileId;
    profileFrom=null;
    if(placUser!=null){
       profileFrom= ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id);
    }

    return profileFrom;
  }


  
  
  getProfile(){

   let profileFrom=this.getProfileFrom();
   let profileToId=this.state.profileId;
   let profileFromId=profileToId;
   if(profileFrom!=null){
    profileFromId=profileFrom.profile_id;
   }
    

  let url=Routes.URL_GET_PROFILE_DATA+profileFromId+"/"+profileToId;

  
    Api.get(url)
     .then((response)=>{
       var {data,status} =response;
       if(status=='success'){
           
         this.setState({
           profile:data,
         },()=>{
             if(profile){
               this.setPlacUser();
             }
         });
       }
     }).catch((ex)=>{
        console.warn(ex);
     });
  }

  setPlacUser(){
      headerTitle=null;
      profile=this.state.profile;
      placUser=profile.plac_user;
      if(profile.profile_type!='company'){
        headerTitle=(<TouchableOpacity  onPress={()=>this.goPlacUser()} style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center',marginRight:40}}>
                        {(placUser.plac_user_image)?<Image style={{width:36,height:36,borderRadius:18}} source={{uri:getUrlImageCompress(placUser.plac_user_image,10)}}/>:null}
                        <Text style={{marginLeft:10,fontSize:14,color:'black'}}>{placUser.plac_user_name}</Text>
                    </TouchableOpacity>);
      }

    params.headerTitle=headerTitle;
    this.props.navigation.setParams(params);
  }

  goPlacUser(){
    params.placUser= this.state.profile.plac_user;
    this.props.navigation.push('PlacUserInvite',params);
  }


  componentWillUnmount() {
    this.props.navigation.setParams({headerTitle:null});
    this.setState({profile:null});
  } 
  


  componentDidMount() {
      if(this.props.navigation.state.params.profileTo!=undefined){
          let profileTo=this.props.navigation.state.params.profileTo;
          this.setState({
            profile:null,
            profileId:profileTo.profile_id,
          },()=>{
            this.getProfile();
            this.getCountFollowers();
          });
      }
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'

  },
  imageProfile:{
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 20,
  },
  viewProfileName:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  buttonEditProfile:{
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor:"#00b99b",
    width: "80%"
  },
  textEditProfile:{
    color:'#00b99b',
    fontSize:14
  },
  viewInfoAboutProfile:{
    height: 36,
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'space-around',
    marginTop: 20,
    backgroundColor: "#ededed"
  },
  textInfo:{
    fontSize: 14,
    color:'#555555'
  },
  viewInfoBox:{
    justifyContent: 'center',
    alignItems: 'center'
  }

});
