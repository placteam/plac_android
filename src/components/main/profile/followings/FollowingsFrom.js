import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';


// API
import Api from  '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'


import ListRow from '../../../views/profile/ListRow';

var route;
var params;

export default class FollowingsFrom extends React.Component {

  static navigationOptions = {
    title:"Siguiendo"
  }
  constructor(props){
    super(props);
    params=props.navigation.state.params;
    route=Routes.URL_GET_PROFILE_FOLLOWING_FROM+params.profileFromId+"/"+params.profileToId;
    this.state={
      profiles:[],
      isLoading:true,
      arrayUrls:[],
      url_get_followings:route,
    }
  
  }


  _keyExtractor = (item, index) => item.follower_id;
  render() {
    profile=this.state.profile;
    return (
      <View style={styles.container}>
        <View  style={{flex:1,}}>
                      <FlatList
                             style={{marginTop: 5}}
                             data={this.state.profiles}
                             initialNumToRender={15}

                             refreshControl={
                               <RefreshControl
                                   refreshing={this.state.isLoading}
                                   onRefresh={()=>this.onRefresh()}
                                   title="Cargando..."
                                   colors={["#15d2bb","#15d2bb","#15d2bb"]}
                                   tintColor="#15d2bb"
                                   titleColor="#15d2bb"
                                />
                             }
                             ItemSeparatorComponent={()=>{
                              return <View  style={{height:1,backgroundColor:'#a6a6a6',width:'100%'}} />
                            }}
                             removeClippedSubviews={true}
                      
                             showsVerticalScrollIndicator={false}
                             onEndReached={()=>this.handleLoadMore()}
                             onEndReachedThreshold={.5}
                             keyExtractor={this._keyExtractor}
                             renderItem={this.renderRow}
                          />
        </View>
      
      </View>
    );
  }

  onRefresh(){
    this.setState({
      isLoading:true,
      url_get_followings:route,
    });
    this.getFollowings();
  }

  renderRow=(object)=>{
    follower=object.item;
    profileTo=follower.profile_to;
   return  <ListRow  isFollowing={follower.isFollowing}  navigation={this.props.navigation} profile={profileTo} />
   
  }




  handleLoadMore () {
    paginate=  this.state.paginate;
    nextPageURL=paginate.next_page_url;
    if(paginate.current_page!=paginate.last_page){
        if(nextPageURL){
          nextPage=nextPageURL.split("?");
          arrayUrls=this.state.arrayUrls;
              if(arrayUrls.indexOf(nextPageURL)==-1){
                 arrayUrls.push(nextPageURL);
                      this.setState({
                        arrayUrls,
                        url_get_followings:route+"?"+nextPage[1]
                      }, () => {
                            this.getFollowings();
                      });
              }
        }
   }
};


  
  getFollowings(){
    url=this.state.url_get_followings;
    Api.get(url)
     .then((response)=>{
       this.manageResponse(response);
     }).catch((ex)=>{
        console.warn(ex);
     });
  }

  manageResponse(response){
    const {data}=response;
    let profiles= this.state.profiles;
    for(i=0;i<data.length;i++){
        profiles.push(data[i]);
    }
    this.setState({
      profiles,
      paginate:response,
      isLoading:false,
    });
}

  componentDidMount(){
    this.getFollowings();
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  imageProfile:{
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 20,
  },
  viewProfileName:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  buttonEditProfile:{
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor:"#00b99b",
    width: "80%"
  },
  textEditProfile:{
    color:'#00b99b',
    fontSize:14
  },
  viewInfoAboutProfile:{
    height: 36,
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'space-around',
    marginTop: 20,
    backgroundColor: "#ededed"
  },
  textInfo:{
    fontSize: 14,
    color:'#555555'
  },
  viewInfoBox:{
    justifyContent: 'center',
    alignItems: 'center'
  }

});
