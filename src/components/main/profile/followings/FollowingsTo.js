import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';


import { CachedImage } from 'react-native-img-cache';

// API
import Api from  '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'

import {getMaleGender,getFemaleGender} from '../../../../assets/image/svg'
import {getUrlImageCompress} from '../../../../lib/image'
import ButtonFollower from '../../../views/profile/ButtonFollower'
import ListRow from '../../../views/profile/ListRow';

var route;
var params;
export default class FollowingsTo extends React.Component {

  static navigationOptions = {
    title:"Siguiendo"
  }
  constructor(props){
    super(props);
    params=props.navigation.state.params;
    route=Routes.URL_GET_PROFILE_FOLLOWING_TO+params.profileFromId+"/"+params.profileToId;
    
    this.state={
      profiles:[],
      isLoading:true,
      arrayUrls:[],
      url_get_followings:route,
    }
  
  }


  _keyExtractor = (item, index) => item.follower_id;
  render() {
    profile=this.state.profile;
    return (
      <View style={styles.container}>
        <View  style={{flex:1,}}>
                      <FlatList
                             style={{marginTop: 5}}
                             data={this.state.profiles}
                             initialNumToRender={15}

                             refreshControl={
                               <RefreshControl
                                   refreshing={this.state.isLoading}
                                   onRefresh={()=>this.onRefresh()}
                                   title="Cargando..."
                                   colors={["#15d2bb","#15d2bb","#15d2bb"]}
                                   tintColor="#15d2bb"
                                   titleColor="#15d2bb"
                                />
                             }
                             ItemSeparatorComponent={()=>{
                              return <View  style={{height:1,backgroundColor:'#a6a6a6',width:'100%'}} />
                            }}
                             removeClippedSubviews={true}
                      
                             showsVerticalScrollIndicator={false}
                             onEndReached={()=>this.handleLoadMore()}
                             onEndReachedThreshold={.5}
                             keyExtractor={this._keyExtractor}
                             renderItem={this.renderRow}
                          />
        </View>
      
      </View>
    );
  }

  onRefresh(){
    this.setState({
      isLoading:true,
      url_get_followings:route,
    });
    this.getFollowings();
  }

  renderRow=(object)=>{
    follower=object.item;
    profileFrom=follower.profile_from;
    return <ListRow  isFollowing={follower.isFollowing} navigation={this.props.navigation} profile={profileFrom}/>
    
  }

  goProfile(object){
  
    
    follower=object.item;
    profileFrom=follower.profile_from;
    params.profileTo=profileFrom;
    if(profileFrom.profile_id!=params.profileFromId){
        this.props.navigation.push('ProfileInvite',params);
      }else{
        this.props.navigation.push('ProfileMe',params);
      }
  }

  getViewProfileImage(profileTo,placUser){

    return  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
             {(profileTo.profile_type=='pet')?
              <View style={{height:96,width:96,alignItems:'flex-start'}}>
                  {(placUser.plac_user_image)?<CachedImage  resizeMethod="resize" style={{height:48,width:48,borderRadius:24,position:'absolute',borderRadius:24, left:0,top:(96/2)-24}} source={{uri:getUrlImageCompress(placUser.plac_user_image,20)}} />:null}
                  {(profileTo.profile_image)?<CachedImage   resizeMethod="resize"  style={{height:48,width:48,borderRadius:24,position:'absolute',borderRadius:24, right:20,top:(96/2)-24}} source={{uri:getUrlImageCompress(profileTo.profile_image,20)}} />:null}
              </View>:
                 (profileTo.profile_image)?<CachedImage  resizeMethod="resize"  style={{height:48,width:48,borderRadius:24,borderRadius:24}} source={{uri:getUrlImageCompress(profileTo.profile_image,20)}} />:null
            }       
        </View>
  }

  getProfileInfo(profileFrom){
    placUser=profileFrom.plac_user;
    if(profileFrom.profile_type=='pet'){
      return  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
      <View style={{flexDirection:'row',alignItems:'center'}}>
         <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{color:'black',fontSize:14,marginRight:2,}}>{ profileFrom.profile_name}</Text>
         {this.getPetGender(profileFrom)}
        
      </View>
                  <Text   ellipsizeMode={'tail'} numberOfLines={1} style={{color:'#757575',fontSize:12}}>{profileFrom.breed.breed_name}</Text>
                  <View style={{height:1,backgroundColor:'#a6a6a6',marginTop:1,marginBottom:1,width:'60%'}}/>
                  <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{textAlign:'center',color:'black',fontSize:12}}>{placUser.plac_user_name}</Text>
              </View>
    }else{
      return <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
               <Text  ellipsizeMode={'tail'} numberOfLines={1} style={{color:'black',fontSize:14}}>{ profileFrom.profile_name}</Text>
               <Text   ellipsizeMode={'tail'} numberOfLines={1} style={{color:'#757575',fontSize:12}}>Tienda</Text>
             </View>

    }
    

  }

  getPetGender(profileFrom){
    petGender=profileFrom.pet_gender;
    if(petGender){
        if(petGender=="MALE"){
          return getMaleGender(12,12);
        }else{
          return getFemaleGender(12,12);
        }
    }
  }



  handleLoadMore () {
    paginate=  this.state.paginate;
    nextPageURL=paginate.next_page_url;
    if(paginate.current_page!=paginate.last_page){
        if(nextPageURL){
          console.warn(nextPageURL);
          
          nextPage=nextPageURL.split("?");
          arrayUrls=this.state.arrayUrls;
              if(arrayUrls.indexOf(nextPageURL)==-1){
                 arrayUrls.push(nextPageURL);
                      this.setState({
                        arrayUrls,
                        url_get_followings:route+"?"+nextPage[1]
                      }, () => {
                            this.getFollowings();
                      });
              }
        }
   }
};


  
  getFollowings(){
    url=this.state.url_get_followings;
    Api.get(url)
     .then((response)=>{
       this.manageResponse(response);
     }).catch((ex)=>{
        console.warn(ex);
     });
  }

  manageResponse(response){
    const {data}=response;
    let profiles= this.state.profiles;

    for(i=0;i<data.length;i++){
        profiles.push(data[i]);
    }
    this.setState({
      profiles,
      paginate:response,
      isLoading:false,
    });
}

  componentDidMount(){
    this.getFollowings();
   

  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'

  },
  imageProfile:{
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 20,
  },
  viewProfileName:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  buttonEditProfile:{
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor:"#00b99b",
    width: "80%"
  },
  textEditProfile:{
    color:'#00b99b',
    fontSize:14
  },
  viewInfoAboutProfile:{
    height: 36,
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'space-around',
    marginTop: 20,
    backgroundColor: "#ededed"
  },
  textInfo:{
    fontSize: 14,
    color:'#555555'
  },
  viewInfoBox:{
    justifyContent: 'center',
    alignItems: 'center'
  }

});
