
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  Dimensions,

  View } from 'react-native';

export default class Root extends Component {
    constructor(props){
        super(props);

    }

    render(){
        return <View></View>
    }

    componentWillMount(){
        //OneSignal.addEventListener('received', this.onReceived);
         OneSignal.addEventListener('opened', this.onOpened);
       }
       
      
       componentWillUnmount() {
         //OneSignal.removeEventListener('received', this.onReceived);
         OneSignal.removeEventListener('opened', this.onOpened);
      
       }
     
    
       onOpened=(openResult)=> {
         let additionalData=openResult.notification.payload.additionalData;
         let notificationType=additionalData.notification_type;
          switch(notificationType){
              case "store-order":
              let params={
                order:{
                  order_id:
                  additionalData.order.order_id
                 },
              }
              console.warn(params);
              
              this.props.navigation.navigate('OrderDetail',params);
              break;
          }
     
       }
    

}