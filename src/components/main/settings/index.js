import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,

} from 'react-native';


import * as PlacUserController from '../../../app/controllers/PlacUserController'
import * as ProfileController from '../../../app/controllers/ProfileController'
import * as PlacUserConfigurationController from '../../../app/controllers/PlacUserConfigurationController'
import * as AppConfigurationController from '../../../app/controllers/AppConfigurationController'

import CardView from 'react-native-cardview';

import firebase from 'react-native-firebase';
import { GoogleSignin } from 'react-native-google-signin';
const FBSDK = require('react-native-fbsdk');
const {
  LoginManager,
} = FBSDK;


var { width, height } = Dimensions.get('window');
export default class Settings extends React.Component {
  static navigationOptions = {
   title:"Configuración"
  }

  constructor(props){
    super(props);

  }

  render() {
   
    return (
      <View style={styles.container}>
    
          { /*CardView
                style={{ marginVertical: 5, marginBottom: 1, padding: 2 }}
                cardElevation={1}
                cardMaxElevation={1}
                cornerRadius={1}
            >
                <View style={styles.viewItem}>
                    <View >
                        <Text style={styles.textItemTitle}>Cuenta</Text>
                    </View>
                    <View style={{marginTop:15,}}>
                        <Text style={{fontSize:14,color:'black'}}>Perfil privado</Text>

                    </View>
                </View>
             
          </CardView>*/}

              <CardView
                style={{ marginVertical: 5, marginBottom: 1, padding: 2 }}
                cardElevation={1}
                cardMaxElevation={1}
                cornerRadius={1}
            >
                <View style={styles.viewItem}>
                    <View  >
                        <Text style={styles.textItemTitle}>Notificaciones</Text>
                    </View>
                    <TouchableOpacity onPress={()=>this.goNotificationsPush()} style={{marginTop:15,}}>
                        <Text style={{fontSize:14,color:'black'}}>Notificaciones push</Text>
                    </TouchableOpacity>
                      <TouchableOpacity onPress={()=>this.goNotificationsMail()}   style={{marginTop:15,}}>
                        <Text style={{fontSize:14,color:'black'}}>Correo electrónico</Text>
                    </TouchableOpacity>
                </View>
             
            </CardView>

              <CardView
                style={{ marginVertical: 5, marginBottom: 1, padding: 2 }}
                cardElevation={1}
                cardMaxElevation={1}
                cornerRadius={1}
            >
                <View style={styles.viewItem}>
                    <View >
                        <Text style={styles.textItemTitle}>Cuenta</Text>
                    </View>

                     <TouchableOpacity  onPress={()=>this.logOut()} style={{marginTop:15,}}>
                        <Text style={{fontSize:14,color:'black'}}>Cerrar sesión</Text>
                    </TouchableOpacity>

                </View>
             
            </CardView>
      </View>
    );
  }

  goNotificationsPush(){
      this.props.navigation.navigate('NotificationsPush',{});
  }

  goNotificationsMail(){
    this.props.navigation.navigate('NotificationsMail',{});
 }


  logOut(){
        AppConfigurationController .destroy();
        PlacUserController.destroy();
        ProfileController.destroy();
        PlacUserConfigurationController.destroy();
        firebase.auth().signOut();
        LoginManager.logOut();
        this.signOutGoogle();
        setTimeout(() => { this.props.navigation.navigate('AuthLoading', null) }, 500);
  }
  
  signOutGoogle = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
  
    } catch (error) {
      console.warn(error);
    }
  };




}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content:{
      marginHorizontal:15,
      marginVertical: 15,
  },
  viewItem: {
    width: width - 30,
    marginLeft: 15,
    paddingVertical: 15,


},
  viewItemTitle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
},
textItemTitle: {
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold',
},
});
