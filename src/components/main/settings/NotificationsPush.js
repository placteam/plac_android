import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Switch,

} from 'react-native';


import *  as ProfileController from '../../../app/controllers/ProfileController'
import * as PlacUserController from '../../../app/controllers/PlacUserController'

import Api from '../../../lib/http_request/';
import * as Routes from '../../../app/routes'

import CardView from 'react-native-cardview';
var { width } = Dimensions.get('window');

export default class NotificationsPush extends React.Component {
    static navigationOptions = {
        title: "Push"
    }

    constructor(props) {
        super(props);
        placUser = PlacUserController.getCurrentUser();
        profile = ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id);
        this.state = {

        }

    }

    render() {
        let { post_likes_push_status, post_comments_push_status, followers_new_follower_push_status } = this.state;
        let likesStatus = (post_likes_push_status == "active") ? true : false;
        let commentsStatus = (post_comments_push_status == "active") ? true : false;
        let newFollowerStatus = (followers_new_follower_push_status == "active") ? true : false;

        return (
            <View style={styles.container}>

                <CardView
                    style={styles.cardItem}
                    cardElevation={1}
                    cardMaxElevation={1}
                    cornerRadius={1}
                >
                    <View style={styles.viewItem}>
                        <View  >
                            <Text style={styles.textItemTitle}>Publicaciones</Text>
                        </View>

                        <View style={styles.viewItemOption}>
                            <View style={{ flex: 1.5 }}>
                                <Text style={styles.textTitleOption}>Me gustas</Text>
                                <Text style={styles.textSubtitleOption}>Me gustas de tu seguidores en tus publicaciones</Text>
                            </View>
                            <View style={{ flex: .5, justifyContent: 'center' }}>
                                <Switch
                                     thumbTintColor={(likesStatus)?"#15d2bb":"white"}
                                    onValueChange={this.onLikeChange}
                                    value={likesStatus} />
                            </View>
                        </View>

                        <View style={styles.viewItemOption}>
                            <View style={{ flex: 1.5 }}>
                                <Text style={styles.textTitleOption}>Comentarios</Text>
                                <Text style={styles.textSubtitleOption}>Comentarios de  tu seguidores en las publicaciones</Text>
                            </View>
                            <View style={{ flex: .5, justifyContent: 'center' }}>
                                <Switch
                                    thumbTintColor={(commentsStatus)?"#15d2bb":"white"}
                                    onValueChange={this.onCommentChange}
                                    value={commentsStatus} />
                            </View>
                        </View>

                    </View>

                </CardView>
                <CardView
                    style={styles.cardItem}
                    cardElevation={1}
                    cardMaxElevation={1}
                    cornerRadius={1}
                >
                    <View style={styles.viewItem}>
                        <View >
                            <Text style={styles.textItemTitle}>Seguidores</Text>
                        </View>
                        <View style={styles.viewItemOption}>
                            <View style={{ flex: 1.5 }}>
                                <Text style={styles.textTitleOption}>Nuevo seguidor</Text>
                                <Text style={styles.textSubtitleOption}>Perfiles que empezaron a seguir a <Text style={{ color: 'black' }}>{profile.profile_name}</Text> y tus otros perfiles</Text>
                            </View>

                            <View style={{ flex: .5, justifyContent: 'center' }}>
                                <Switch
                                    thumbTintColor={(newFollowerStatus)?"#15d2bb":"white"}
                                    onValueChange={this.onNewFollowerChange}
                                    value={newFollowerStatus} />
                            </View>
                        </View>
                    </View>
                </CardView>
            </View>
        );
    }

    onLikeChange = (value) => {
        let status = (value) ? "active" : "inactive";
        this.setState({
            post_likes_push_status: status,
        }, () => {
            this.updateNotificationSettigns();
        });
    }

    onCommentChange = (value) => {
        let status = (value) ? "active" : "inactive";
        this.setState({
            post_comments_push_status: status,
        }, () => {
            this.updateNotificationSettigns();
        });
    }

    onNewFollowerChange = (value) => {
        let status = (value) ? "active" : "inactive";
        this.setState({
            followers_new_follower_push_status: status,
        }, () => {
            this.updateNotificationSettigns();
        });
    }



    updateNotificationSettigns() {
        let URL = Routes.URL_RESOURCE_NOTIFICATION_SETTINGS + "/" + this.state.notification_setting_id;
        console.warn(URL);

        var body = {
            notification_settings: this.state,
            _method: "PUT",
        }

        Api.post(URL, body)
            .then((response) => {
                var { data, status, message } = response;
                if (status == "success") {
                } else {
                    console.warn(message);
                }
            }).catch((ex) => {
                console.warn(ex.message);
            });


    }

    fetchNotificationSettings() {
        let URL = Routes.URL_GET_NOTIFICATION_SETTINGS_USER + placUser.plac_user_id;

        Api.get(URL)
            .then((response) => {
                var { data, status, message } = response;
                if (status == "success") {
                    this.setState({ ...data });

                }
            }).catch((ex) => {
                console.warn(ex.message);
            });
    }

    componentDidMount() {
        this.fetchNotificationSettings();
    }


}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    content: {
        marginHorizontal: 15,
        marginVertical: 15,
    },
    cardItem: {
        marginVertical: 5,
        marginBottom: 1,
        padding: 2
    },
    viewItem: {
        width: width - 30,
        marginLeft: 15,
        paddingVertical: 15,
    },
    viewItemTitle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    viewItemOption: {
        marginTop: 15,
        flexDirection: 'row'
    },
    textItemTitle: {
        fontSize: 16,
        color: 'black',
        fontWeight: 'bold',
    },
    textTitleOption: {
        fontSize: 14,
        color: 'black'
    },
    textSubtitleOption: {
        fontSize: 10
    }
});
