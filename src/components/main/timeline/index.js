import React, { Component } from 'react';
import {
  AppRegistry,
  AsyncStorage,
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  RefreshControl,
  Animated,
} from 'react-native';

import firebase from 'react-native-firebase';

const { width, height } = Dimensions.get('window');
// API
import Api from '../../../lib/http_request/';
import * as Routes from '../../../app/routes'

import *  as ProfileController from '../../../app/controllers/ProfileController'
import * as PlacUserController from '../../../app/controllers/PlacUserController'

import { getIconCamera, getIconGallery, getIconAdd } from '../../../assets/image/svg'

import ToolbarMain from '../../views/ToolbarMain'
import Button from '../../views/ButtonContinue'

import TimeLineHeader from './header/TimeLineHeader'

import PostRow from './PostRow'

import ImagePicker from 'react-native-image-crop-picker';
import OneSignal from 'react-native-onesignal';

var profile;
var placUser;

var widthView = width - 30;
var btnPickPositionRight = 15;
var btnPickPositionBottom = 15;
var increase = 70;


const pickerImageConfig = {
  width: 600,
  height: 600,
  cropping: true,
  includeBase64: true,
  compressImageQuality: .7,
  multiple: false,
}

var unsubscribe;


export default class TimeLine extends React.Component {

  static navigationOptions = {
    header: null
  }


  constructor(props) {
    super(props);
    placUser = PlacUserController.getCurrentUser();
    profile = ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id)

    this.state = {
      posts: [],
      url_get_post_following: Routes.URL_GET_POST_FOLLOWING,
      isLoading: true,
      arrayUrls: [],
      animationPickImageGallery: new Animated.Value(btnPickPositionRight),
      animationPickImageCamera: new Animated.Value(btnPickPositionBottom),
      animationOpacity: new Animated.Value(0),
      animationRotate: new Animated.Value(0),
      isOpenPickImage: false,
    }
  }



  _keyExtractor = (item, index) => item.post_id;
  render() {

    const animatedGallery = {
      right: this.state.animationPickImageGallery,
      opacity: this.state.animationOpacity
    }

    const animatedCamera = {
      bottom: this.state.animationPickImageCamera,
      opacity: this.state.animationOpacity
    }

    const rotation = this.state.animationRotate.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '45deg']
    });

    return (
      <View style={styles.container}>
        <ToolbarMain btnLeftDisabled={false} isDisabled={true} navigation={this.props.navigation} />
        <FlatList
          style={{ marginTop: 5 }}
          data={this.state.posts}
          refreshControl={
            <RefreshControl
              refreshing={this.state.isLoading}
              onRefresh={() => this.onRefresh()}
              title="Cargando..."
              colors={["#15d2bb", "#15d2bb", "#15d2bb"]}
              tintColor="#15d2bb"
              titleColor="#15d2bb"
            />
          }
          removeClippedSubviews={true}
          ListEmptyComponent={() => this.postsEmpty()}
          ListHeaderComponent={<TimeLineHeader navigation={this.props.navigation} profile={profile} />}

          ListFooterComponent={() => this.footerComponent()}
          showsVerticalScrollIndicator={false}
          onEndReached={() => this.handleLoadMore()}
          onEndReachedThreshold={1}

          keyExtractor={this._keyExtractor}
          renderItem={this.renderPostRow}
        />


        <Animated.View style={[styles.buttonPickImage, animatedCamera]}>
          <TouchableOpacity onPress={this.takePhoto} style={{ width: 48, height: 48, borderRadius: 24, justifyContent: 'center', alignItems: 'center' }}>
            {getIconCamera()}
          </TouchableOpacity>
        </Animated.View>

        <Animated.View style={[styles.buttonPickImage, animatedGallery]}>
          <TouchableOpacity onPress={this.pickGallery} style={{ width: 48, height: 48, borderRadius: 24, justifyContent: 'center', alignItems: 'center' }}>
            {getIconGallery()}
          </TouchableOpacity>
        </Animated.View>

        <Animated.View style={[styles.buttonPickImage, { transform: [{ rotate: rotation }] }]}>
          <TouchableOpacity onPress={this.openPickImage} style={{ width: 48, height: 48, borderRadius: 24, justifyContent: 'center', alignItems: 'center' }}>
            {getIconAdd()}
          </TouchableOpacity>
        </Animated.View>

      </View>
    );
  }

  takePhoto = () => {
    ImagePicker.openCamera(pickerImageConfig).then(this.manageImagePicked);
  }

  pickGallery = () => {
    ImagePicker.openPicker(pickerImageConfig).then(this.manageImagePicked);
  }

  manageImagePicked = (image) => {
    this.openPickImage();
    var imageBase64 = 'data:image/png;base64,' + image.data;
    var image = { imageView: { uri: imageBase64 }, imageSelected: imageBase64 }

    var params = {
      placUser,
      profile,
      image,
      action: "new",
    }

    params.newPostCallBack = ((post) => this.newPostCallBack(post));
    this.props.navigation.navigate("NewPost", params);

  }

  newPostCallBack(post) {
    var posts = this.state.posts;
    posts.unshift(post);
    this.setState({ isLoading: true, post }, () => {
      this.setState({ isLoading: false });

    });

  }


  openPickImage = () => {
    isOpenPickImage = this.state.isOpenPickImage;
    if (!isOpenPickImage) {
      isOpenPickImage = true;
    } else {
      isOpenPickImage = false;
    }
    this.setState({ isOpenPickImage })
    this.startAnimation(isOpenPickImage)
  }

  startAnimation = (state) => {
    valuePickGallery = btnPickPositionRight;
    valuePickCamera = btnPickPositionBottom;
    console.warn();
    opacity = 0;
    rotate = 0;
    if (state) {
      valuePickGallery = btnPickPositionRight + increase;
      valuePickCamera = btnPickPositionBottom + increase;
      opacity = 1;
      rotate = 1;
    }
    Animated.parallel([
      Animated.timing(this.state.animationRotate, {
        toValue: rotate,
        duration: 200
      }),
      Animated.timing(this.state.animationPickImageCamera, {
        toValue: valuePickCamera,
        duration: 300,
      }),
      Animated.timing(this.state.animationPickImageGallery, {
        toValue: valuePickGallery,
        duration: 300,
      }),
      Animated.timing(this.state.animationOpacity, {
        toValue: opacity,
        duration: 500,
      }),

    ]).start()
  }



  postsEmpty() {
    return (!this.state.isLoading) ? <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, height }}>
      <Text>No hay publicaciones </Text>
      <Button onPress={this.openPickImage} customStyle={{ width: "50%", marginTop: 30, }} isValidData={true} text={"Publicar"} />
    </View> : null
  }

  footerComponent() {
    return (this.state.loadMore) ? <View style={{ justifyContent: 'center', height: 70, alignItems: 'center' }}>
      <Image style={{ width: 60, height: 23 }} source={require('../../../assets/image/general/loader.gif')} />
    </View> : null
  }



  onRefresh() {
    this.setState({
      arrayUrls: [],
      loadMore: false,
      isLoading: true,
      posts: [],
      url_get_post_following: Routes.URL_GET_POST_FOLLOWING
    }, () => {
      this.fetchPostFollowing();
    });
  }

  handleLoadMore() {
    paginate = this.state.paginate;
    nextPageURL = paginate.next_page_url;

    if (paginate.current_page != paginate.last_page) {
      if (nextPageURL) {

        nextPage = nextPageURL.split("?");
        arrayUrls = this.state.arrayUrls;

        if (arrayUrls.indexOf(nextPageURL) == -1) {
          console.warn(nextPageURL);
          arrayUrls.push(nextPageURL);

          this.setState({
            loadMore: true,
            arrayUrls,
            url_get_post_following: Routes.URL_GET_POST_FOLLOWING + "?" + nextPage[1]
          }, () => {
            this.fetchPostFollowing();
          });
        }
      }
    }
  };

  renderPostRow = (object) => {
    return <PostRow deletePost={this.deletePost} navigation={this.props.navigation} manageLike={this.manageLike} profileFrom={profile} placUser={placUser} object={object} />
  }

  deletePost = (post) => {
    var posts = this.state.posts;
    var position = posts.indexOf(post);
    posts.splice(position, 1);
    this.setState({
      posts,
    });
  }


  fetchPostFollowing() {
    var url = this.state.url_get_post_following;
    var body = {
      profile: {
        profile_id: profile.profile_id,
      }
    }
    Api.post(url, body)
      .then((response) => {
        this.managePostResponse(response);
      }).catch((ex) => {
        console.warn(ex);
      });
  }

  managePostResponse(response) {
    const { data } = response;
    let posts = this.state.posts.slice();
    for (i = 0; i < data.length; i++) {
      posts.push(data[i]);
    }
    this.setState({
      posts,
      paginate: response,
      isLoading: false,
      loadMore: false,
    });
  }

  goDeepLink(url) {
    let str = url.replace("https://", "");
    var indexProducto = str.indexOf("producto");
    var indexStore = str.indexOf("tienda");
    let urlArray = str.split("/");
    if (indexProducto != -1 && indexStore != -1) {
      let placeName = urlArray[2];
      let productId = urlArray[4];
      let params = {
        place_name: placeName,
        product: { product_id: productId }
      }

      this.props.navigation.push('Place', params);
      setTimeout(() => {
        this.props.navigation.push('Product', params);
      }, 1000);
    } else if (indexStore != -1) {

      let placeName = urlArray[2];
      let params = {
        place_name: placeName,
      }
      this.props.navigation.push('Place', params);
    }
  }




  componentWillMount() {
    unsubscribe = firebase.links().onLink((url) => {
      this.goDeepLink(url);
    });

  }

  componentWillUnmount() {
    OneSignal.removeEventListener('opened', this.onOpened);
    unsubscribe();
  }

  componentDidMount() {
    this.fetchPostFollowing();
    OneSignal.addEventListener('opened', this.onOpened);
    firebase.links()
      .getInitialLink()
      .then((url) => {
        if (url) {
          this.goDeepLink(url);
        } else {
          console.warn("not open from url");
        }
      });

  }

  onOpened = (openResult) => {
    let additionalData = openResult.notification.payload.additionalData;
    let params = {
      additionalData,
    }
    this.props.navigation.navigate('Notifications', params);
  }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  buttonPickImage: {
    position: 'absolute',
    bottom: 15,
    right: 15,
    backgroundColor: '#00B99B',
    width: 48,
    height: 48,
    borderRadius: 24,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 8,
    elevation: 2,
  },
});
