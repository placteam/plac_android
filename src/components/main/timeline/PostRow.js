import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Animated,

} from 'react-native';

// API
import Api from  '../../../lib/http_request/';
import * as Routes from '../../../app/routes'

import {CachedImage}  from "react-native-img-cache";
//Image
import * as Emoticons from 'react-native-emoticons';
import {getIconComment,getIconCommentPress,getMenuDots} from '../../../assets/image/post'
import {getUrlImageCompress} from '../../../lib/image'

import ActionSheet from 'react-native-actionsheet'

const ICON_LIKE_NORMAL=require('../../../assets/image/post/ic_action_like.png');
const ICON_LIKE_PRESS=require('../../../assets/image/post/ic_action_like_press.png');
const { width,height} = Dimensions.get('window');


var postOptionsProfileTo=["Reportar historia","Cancelar"];
var postOptionsProfileFrom=["Editar historia","Eliminar historia","Cancelar"];


export default class PostRow extends React.Component{

   constructor(props){
     super(props);
     post=this.props.object.item;
     this.state={
       post,
       animationBound:new Animated.Value(1),
       message:Emoticons.parse(post.message),
     }



   }


  render() {

    post=this.state.post;
    profile=post.profile;
    placUser=profile.plac_user;

    return (
      <View style={styles.container}>

                <View style={styles.header}>
                      <TouchableOpacity onPress={()=>this.goProfile()} style={{flexDirection: 'row'}}>
                            <View>
                              <CachedImage
                               style={styles.imageRounded}
                               source={{
                                 uri: getUrlImageCompress(profile.profile_image,20),
                               }} />
                            </View>
                            <View style={styles.viewBoxProfile}>
                              <Text style={styles.textProfileName}>{profile.profile_name}</Text>
                              <Text style={styles.textTimeAgo}>{post.post_time_ago}</Text>
                            </View>
                       </TouchableOpacity>
                       <View>
                           <TouchableOpacity onPress={()=>this.goPlacUser()}>
                          {(placUser.plac_user_image)?<CachedImage style={{width: 36,height: 36,borderRadius: 18}} source={{uri:getUrlImageCompress(placUser.plac_user_image,20) }}/>:null}

                           </TouchableOpacity>
                       </View>
                 </View>
                 <CachedImage source={{uri:getUrlImageCompress(post.post_path_image,100)}} style={styles.imagePost} />
                 <View style={styles.viewBoxActions}>
                        {this.renderViewLikes(post)}
                        {this.renderViewComments(post)}

                       <TouchableOpacity  onPress={this.showActionSheet}  activeOpacity={.5} >
                            {getMenuDots()}
                       </TouchableOpacity>
                 </View>
                 {(post.message)?<View style={styles.viewPostMessage}>
                                   <Text style={{fontSize: 14,fontWeight: 'bold',color:'black'}}>{profile.profile_name} <Text style={{fontSize: 12,fontWeight: 'normal'}}>{this.state.message} </Text></Text>
                                 </View>:null}

                    <ActionSheet
                      ref={o => this.ActionSheetOptions = o}
                      title={'Escoge una acción'}
                      options={(post.profile.profile_id==this.props.profileFrom.profile_id)?postOptionsProfileFrom:postOptionsProfileTo}
                      cancelButtonIndex={(post.profile.profile_id==this.props.profileFrom.profile_id)?2:1}
                      destructiveButtonIndex={1}
                      onPress={this.manageOptions}


                    />


                    <ActionSheet
                      ref={o => this.ActionSheetDeletePost = o}
                      title={'¿Estas seguro de eliminar esta publicación?'}
                      options={["Eliminar","Cancelar"]}
                      cancelButtonIndex={1}
                      destructiveButtonIndex={1}
                      onPress={(index) => {
                        if(index==0){
                          this.deletePost();
                         }}}
                    />
      </View>
    );
  }
  manageOptions=(index)=>{
      post=this.props.object.item;
      if(post.profile.profile_id==this.props.profileFrom.profile_id){
            if(index==0){
              this.goEditPost();
            }
            if(index==1){
              this.ActionSheetDeletePost.show();
            }
      }else{
        console.warn("entra aqui");
          if(index==0){
            this.goComplaints();
          }
      }
  }

  showActionSheet = () => {
    this.ActionSheetOptions.show()
  }




  renderViewLikes(post){
    const animatedStyle={
      transform:[
        {scale:this.state.animationBound}
      ]
    }

    return  <View   style={styles.buttonAction}>
               <Text onPress={()=>this.goPostActionsViews(post,"PostLikes")} style={[styles.textInfoPost,{marginRight: 2}]}>{post.count_likes}</Text>
               <TouchableOpacity onPress={()=>this.manageLike(post)}>
                     <Animated.Image  source={(post.has_liked)?ICON_LIKE_PRESS:ICON_LIKE_NORMAL} style={[{width:35,height:35,},animatedStyle]}  />
                </TouchableOpacity>
           </View>

  }

  renderViewComments(post){
    const animatedStyle={
      transform:[
        {scale:this.state.animationBound}
      ]
    }
    return  <View    style={styles.buttonAction}>
                  <TouchableOpacity onPress={()=>this.goPostActionsViews(post,"PostComments")}   activeOpacity={.5} style={styles.buttonAction}>
                      <Text style={[styles.textInfoPost,{marginRight: 8}]}>{post.count_comments}</Text>
                      {(post.has_commented)?getIconCommentPress():getIconComment()}
                  </TouchableOpacity>
           </View>

  }

  goPostActionsViews(post,type){
      params={
        post,
        profileFrom:this.props.profileFrom,
        placUserFrom:this.props.placUser,
 
      }
      params.commentPostCallBack=(()=>this.commentPostCallBack());
      this.props.navigation.push(type,params);
  }

  goEditPost(){
    params={
      post:this.props.object.item,
      profile:this.props.profileFrom,
      action:"edit",
    }

   params.updatePostCallBack=((post)=>this.updatePostCallBack(post));
   this.props.navigation.navigate("NewPost",params);

  }

  commentPostCallBack(){
    console.warn("intro here");
    
     let post=this.state.post;
     let countComments= parseInt(post.count_comments) + 1
     post.count_comments=countComments;
     post.has_commented=true;
     this.setState({post});

  }

  updatePostCallBack(post){
    this.setState({message:post.message});
  }

  goComplaints(){
    params={
      post:this.props.object.item,
      profile:this.props.profileFrom,
      placUser:this.props.placUser,
    }

     this.props.navigation.navigate("PostComplaint",params);
  }


  handlePress=()=>{
    Animated.spring(this.state.animationBound,{
      toValue:1.2 ,
      friction:2,
      tension:160
    }).start(()=>{
      Animated.timing(this.state.animationBound,{
        toValue:1,
        duration:10,
      }).start();

    });
  }

  manageLike(post){
    this.handlePress();

    profileFrom=this.props.profileFrom;
    url=Routes.URL_MANAGE_POST_LIKES;
    var body={
        	profile_from_id:profileFrom.profile_id,
        	profile_to_id:post.profile.profile_id,
        	post_id:post.post_id
     }
     if(post.has_liked==1){
       post.has_liked=0;
       post.count_likes= post.count_likes-1;
     }else{
       post.has_liked=1;
       post.count_likes= post.count_likes+1;
     }

       this.setState({post},()=>{
         Api.post(url,body)
            .then((response)=>{

            }).catch((ex)=>{
               console.warn(ex);
            });
       });


  }

    deletePost(){
      post=this.props.object.item;

      var url= Routes.URL_POST_RESOURCE+"/"+post.post_id;
      var body={
        _method:"DELETE"
      }
      Api.post(url,body)
         .then((response)=>{
           console.warn("consumed service deleted post");
           console.warn(response);
            this.props.deletePost(post);
         }).catch((ex)=>{
            console.warn(ex.message);
         });
     }

     goProfile(){
        post=this.props.object.item,
        profileTo=post.profile;
        params={profileTo};
        if(profileTo.profile_id!=this.props.profileFrom.profile_id){
         /// url="profile://profile/main/tabs/home/profile/"+profileTo.profile_id;
         /// Linking.openURL(url);
          
          this.props.navigation.push("ProfileInvite",params);
        }else{
          this.props.navigation.push("ProfileMe",params);
        }
     }

     goPlacUser(){
      post=this.props.object.item,
      placUser=post.profile.plac_user;
      params={placUser};
      if(placUser.plac_user_id!=this.props.profileFrom.plac_user_id){
        this.props.navigation.push("PlacUserInvite",params);
      }else{
        this.props.navigation.navigate("PlacUserMe",params);
      }
   }


}

/*<View style={{height: 120,borderBottomWidth: 1,justifyContent: 'center',borderColor: '#EDEDED'}}>
  <View style={{alignItems: 'center',width: 100}}>
      <View style={{width:60,height:60,backgroundColor:'blue',borderRadius: 30}}/>
      <Text  style={{fontSize: 14,color: 'black'}}>Paco</Text>
      <Text  style={{fontSize: 12,color: '#555555'}}>Criollo</Text>
      <View style={{position: 'absolute',width: 24,height: 24,top:0,right: 10,borderRadius: 12,borderWidth: 1,justifyContent: 'center',alignItems: 'center',borderColor: "#15d2bb"}}>
        <Text>X</Text>

      </View>
  </View>


</View>*/



const styles = StyleSheet.create({
  container: {
    flex: 1,
     backgroundColor: "white",
     marginLeft: 15,marginRight: 15
  },
  header:{
    height: 48,
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  imageRounded:{
    width: 36,
    height: 36,
    borderRadius: 18
  },
  textProfileName:{
    fontSize: 14,
    color: 'black'
  },
  textTimeAgo:{
    fontSize: 12,
    color: '#555555'
  },
  viewBoxProfile:{
    marginLeft: 9,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  imagePost:{
    marginTop: 1,
    width:width-30,
    height: width-30,
    borderRadius: 8
  },
  viewBoxActions:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 48
  },
  buttonAction:{
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 5
  },
  textInfoPost:{
    fontSize: 16,
    color:"#555555"
  },
  viewPostMessage:{
    justifyContent:  'center',
    borderBottomWidth: 1,
    borderColor: "#EDEDED",
    paddingTop: 5,
    paddingBottom: 5,
    marginBottom: 20,
  }

});
