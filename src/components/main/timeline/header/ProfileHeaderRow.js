import React, { Component } from 'react';
import {
  AppRegistry,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,

} from 'react-native';
import { CachedImage } from 'react-native-img-cache';
const { width } = Dimensions.get('window');
import { getUrlImageCompress } from '../../../../lib/image'
import ButtonFollower from '../../../views/profile/ButtonFollower';



export default class ProfileHeaderRow extends React.Component {

  constructor(props) {
    super(props);
  }


  render() {
    var { item,index} = this.props.object;
    profile = item;
    let  marginLeft=0;
    if(index==0){
      marginLeft=15;
    }

    return (
      <View style={styles.container}>
        <View style={{ width: width / 5,marginHorizontal:5,justifyContent: 'center', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => this.goProfile(this.props.object)} style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center' }}>
            <CachedImage source={{ uri: getUrlImageCompress(profile.profile_image, 20) }} style={{ borderRadius: 24, margin: 12, width: 48, height: 48 }} />
          </TouchableOpacity >
        
          <Text ellipsizeMode={'tail'} numberOfLines={1} style={{ fontSize: 14,marginHorizontal:5,color: 'black' }}>{profile.profile_name}</Text>
          <Text ellipsizeMode={'tail'} numberOfLines={1}  style={{ fontSize: 12,marginHorizontal:5, color: '#AAAAAA' }}>@{profile.profile_unique_name}</Text>
          <ButtonFollower onFollowProfile={this.onFollowProfile} customTextStyle={{fontSize:12}} customStyle={{width:"80%",height:24,marginTop: 5,}} profileFromId={this.props.profileFrom.profile_id} profileToId={profile.profile_id}  isFollowing={false}/>
        </View>
      </View>
    );
  }

  onFollowProfile=(profileId)=>{
    this.props.onFollowProfile(profileId);
   
    

  }

  goProfile = (object) => {
    profileTo = object.item;
    params = {profileTo};
    this.props.navigation.push('ProfileInvite', params);
  }




}


const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
  },

});
