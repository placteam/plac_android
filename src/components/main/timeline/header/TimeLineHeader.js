import React, { Component } from 'react';
import {
   AppRegistry,
   AsyncStorage,
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  RefreshControl,
  Animated,
} from 'react-native';


const { width,height} = Dimensions.get('window');
// API
import Api from  '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'

import ProfileHeaderRow  from  './ProfileHeaderRow';

export default class TimeLineHeader extends React.Component{



   constructor(props){
     super(props);

     this.state={
       profiles:[],
       isLoading:true,
       arrayUrls:[]
     }
   }



 _keyExtractor = (item, index) => item.profile_id;
  render() {

    return (  <View style={styles.container}>

                   <FlatList
                     style={{marginTop: 15}}
                     data={this.state.profiles}
                     horizontal={true}
                     showsHorizontalScrollIndicator={false}
                     keyExtractor={this._keyExtractor}
                     renderItem={this.renderRow}
                  />
      </View>
    );
  }



  handleLoadMore () {
      paginate=  this.state.paginate;
      nextPageURL=paginate.next_page_url;
      if(paginate.current_page!=paginate.last_page){
          if(nextPageURL){
            nextPage=nextPageURL.split("?");
            arrayUrls=this.state.arrayUrls;
                if(arrayUrls.indexOf(nextPageURL)==-1){
                        this.setState({
                          loadMore:true,
                          arrayUrls,
                          url_get_post_following:Routes.URL_GET_POST_FOLLOWING+"?"+nextPage[1]
                        }, () => {
                              this.getPostFollowing();
                        });
                }
          }
     }
  };

  renderRow=(object)=>{
    return  <ProfileHeaderRow  onFollowProfile={this.onFollowProfile}  profileFrom={this.props.profile}  navigation={this.props.navigation}  object={object}  />
  }

  onFollowProfile=(profileId)=>{
    this.deleteProfile(profileId);
  }

  deleteProfile(profileId){
    let profiles=this.state.profiles;
    for(i=0;i<profiles.length;i++){
        if(profiles[i].profile_id==profileId){
            profiles.splice(i, 1);
        }
    }
    this.setState({profiles});  
  }


  getProfileRecents(){
        var url=Routes.URL_GET_PROFILES_RECENTS+this.props.profile.profile_id;
        this.setState({profiles:[]},()=>{
          Api.get(url)
           .then((response)=>{
             var {status,data}=response;

             this.setState({
               profiles:data,
               isLoading:false,
             });

           }).catch((ex)=>{
              console.warn(ex);
           });
        })

  }

  componentDidMount(){
  this.getProfileRecents();
  }


}



const styles = StyleSheet.create({
  container: {
     backgroundColor: "white",
     borderBottomWidth: 1,
     borderBottomColor: "#ededed",
     paddingBottom: 10,
  },

});
