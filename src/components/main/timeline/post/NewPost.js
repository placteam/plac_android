import React, { Component } from 'react';
import {
   AppRegistry,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  ScrollView,
} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
// API
import Api from  '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'

import * as emoticons from 'react-native-emoticons';
const { width,height} = Dimensions.get('window');

var params;
var image;
var action;

export default class NewPost extends React.Component{
  static navigationOptions = ({ navigation }) => {
      const {state} = navigation;
      return {
        title: `${state.params.title}`,
      };
    };

   constructor(props){
     super(props);
     params=this.props.navigation.state.params;
     image=params.image;
     action=params.action;
     this.state={
       disabled:false,
       isLoading:false,
       comment:(action=="edit")?params.post.message:"",
     }
   }




  render() {
    return (
        <View style={styles.container}>

        <KeyboardAwareScrollView  keyboardDismissMode="interactive"
                                 keyboardShouldPersistTaps={'always'}>

            <View style={{marginLeft: 15,marginRight: 15,marginTop: 15,}}>
              <Image
                style={{height:width-30,width:width-30,borderRadius: 8,}}
                source={(action=="new")?image.imageView:{uri:params.post.post_path_image}}>
              </Image>
           </View>



            <View style={styles.viewBoxComment}>
               <View style={{flexDirection: 'row',flex:1,marginLeft: 15,marginRight: 15,paddingTop:6,paddingBottom: 6}}>
                     <Image style={{width: 36,height: 36,borderRadius: 18}} source={{uri:params.profile.profile_image}}/>
                     <View style={{marginLeft:10,marginRight: 10,flex:1,borderRadius: 8,}}>
                         <TextInput
                          ref={(input) => { this.comment = input; }}
                          returnKeyType={"send"}
                          onContentSizeChange={(event) => {
                           this.setState({height: event.nativeEvent.contentSize.height});
                          }}
                          onSubmitEditing={this.onSubmitEditing}
                          multiline={true}
                          style={[styles.textInputData, {height: Math.max(36, this.state.height)}]}
                          onChangeText={this.onChangeText}
                          placeholder='Escribe tu comentario'
                          value={this.state.comment}
                      />

                     </View>
                     <TouchableOpacity  disabled={this.state.disabled} onPress={this.onSubmitEditing}  style={{height: 36,justifyContent: 'center',alignItems: 'center'}}>
                          {(this.state.isLoading)?<View style={{marginBottom: 0,justifyContent: 'center',alignItems: 'center'}}>
                               <Image style={{width: 60,height:23}} source={require('../../../../assets/image/general/loader.gif')}/>
                          </View>:<Text  style={{fontSize: 14,color: '#15d2bb'}}>{(action=="new")?"Publicar":"Editar"}</Text>}
                     </TouchableOpacity>

               </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
    );
  }

  onSubmitEditing=()=>{
    if(action=="new"){
        this.createPost();
    }else{
        this.updatePost();
    }

  }


  onChangeText=(comment)=>{
    comment=emoticons.parse(emoticons.stringify(comment));
    this.setState({comment});
  }

  createPost(){
    this.setState({disabled:true,isLoading:true,message:'Publicando...'});
      var comment= this.state.comment;
      if(comment!=""){
        comment = emoticons.stringify(comment);
      }

    var body ={
      profile_id:params.profile.profile_id,
      post_message:comment,
      post_image:params.image.imageSelected,
    }

    Api.post(Routes.URL_POST_RESOURCE,body)
                 .then((response)=>{
                    this.newPostCallBack(response.data);
                 }).catch((ex)=>{
                   console.warn(ex);
                 });
  }

  updatePost(){
    this.setState({disabled:true,isLoading:true,message:'Publicando...'});
    var comment= this.state.comment;
    if(comment!=""){
       comment = emoticons.stringify(comment);
    }

    var body ={
      message:comment,
      _method:"PUT"
    }

    Api.post(Routes.URL_POST_RESOURCE+"/"+params.post.post_id,body)
                 .then((response)=>{
                     this.updatePostCallBack(response.data);
                 }).catch((ex)=>{
                   console.warn(ex);
                 });

  }


  newPostCallBack(post){
      this.setState({
        isLoading:false,
        message:"Publicación hecha"
      },()=>{
        params.newPostCallBack(post);
        this.props.navigation.goBack();
      });
  }

  updatePostCallBack(post){
      this.setState({
        isLoading:false,
        message:"Publicación editada"
      },()=>{
        params.updatePostCallBack(post);
        this.props.navigation.goBack();
      });
  }



  componentDidMount(){
    title="Nueva publicación";
    if(action=="edit"){
      title="Editar publicación";
    }
    this.props.navigation.setParams({title});
  }



}



const styles = StyleSheet.create({
  container: {
    flex: 1,
     backgroundColor: "white",
  },
  postButton:{
    borderRadius:5,
    borderWidth:1,
    borderColor:"#15d2bb",
    justifyContent:"center",
    alignItems:"center",
    marginLeft:100,
    marginRight:100,
    backgroundColor:"white",
    height:45,
  },
  comment:{
    fontSize:17,
    marginTop:5,
    padding:4,
    marginLeft:10,
    marginRight:10
  },
  viewBoxComment:{
    alignItems: 'center',
    width,
    flexDirection: 'row',
    backgroundColor: 'white',
    marginTop: 10,

    marginBottom: 20,
  },
  textInputData:{
    height: 36,
    width: '100%',
    fontSize: 14
  },



});
