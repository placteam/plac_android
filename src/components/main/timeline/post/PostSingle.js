import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';

// API
import Api from  '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'

import *  as ProfileController from '../../../../app/controllers/ProfileController'
import * as PlacUserController from '../../../../app/controllers/PlacUserController'
import PostRow from '../../timeline/PostRow'



var placUser;
var profile;
export default class PostSingle  extends React.Component {

  static navigationOptions = {
    title:"Publicación"
  }
  constructor(props){
    super(props);
    params=this.props.navigation.state.params;
    placUser=PlacUserController.getCurrentUser();
    profile=ProfileController.getCurrentProfileSelectedByUser(placUser.plac_user_id)
    this.state={
        object:null,
    }
  
  }

  render() {
      object=this.state.object;
      if(object==null){
          return null;
      }
 
    return (
      <View style={styles.container}>
       <ScrollView style={{flex:1}}>
         <PostRow  deletePost={this.deletePost} navigation={this.props.navigation} manageLike={this.manageLike} profileFrom={profile} placUser={placUser} object={object} />

      </ScrollView>
      </View>
    );
  }
  manageNotification(notification){
    if(notification.type=='COMMENT'){
        let  params={
          post:notification.post,
          profileFrom:profile,
          placUserFrom:placUser
        }
        this.props.navigation.push('PostComments',params);
    }
   
  }
  
  fetchPostSingle(){
    url=Routes.URL_GET_POST_SINGLE;
    var body={
      profile_from_id:profile.profile_id,
      post_id:params.postId,
    }

    Api.post(url,body)
     .then((response)=>{
         console.warn(response);
        var {status,data}=response;
        var object={
            item:data,
        }
        
        this.setState({
            object
        });

        if(params.notification!=undefined){
          this.manageNotification(params.notification);
        }

     }).catch((ex)=>{
        console.warn(ex.message);
     });
  }

  

  componentDidMount(){
    this.fetchPostSingle();
   

  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'

  },
  imageProfile:{
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 20,
  },
  viewProfileName:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  buttonEditProfile:{
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor:"#00b99b",
    width: "80%"
  },
  textEditProfile:{
    color:'#00b99b',
    fontSize:14
  },
  viewInfoAboutProfile:{
    height: 36,
    flexDirection: 'row',
    borderRadius: 20,
    justifyContent: 'space-around',
    marginTop: 20,
    backgroundColor: "#ededed"
  },
  textInfo:{
    fontSize: 14,
    color:'#555555'
  },
  viewInfoBox:{
    justifyContent: 'center',
    alignItems: 'center'
  }

});
