import React, { Component } from 'react';
import {
   AppRegistry,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  RefreshControl,
  Dimensions,
  FlatList,
  TextInput,
} from 'react-native';


import * as Emoticons from 'react-native-emoticons';
// API
import Api from  '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'
import * as emoticons from 'react-native-emoticons';

import {getUrlImageCompress} from '../../../../lib/image'
import { CachedImage } from 'react-native-img-cache';

const { width,} = Dimensions.get('window');
var params;
var post;
var profileFrom;
var placUserFrom;

export default class Comments extends React.Component{
  static navigationOptions = {
    title:"Comentarios",
  };



   constructor(props){
     super(props);
     params=this.props.navigation.state.params;
     post=params.post;
     profileFrom=params.profileFrom;
     placUserFrom=params.placUserFrom;
     this.state={
       comments:[],
       isLoading:true,
       isStoring:false,
       url_get_post_comments:Routes.URL_GET_POST_COMMENTS,
       arrayUrls:[],
     }
   }


 _keyExtractor = (item, index) => index.toString();

  render() {
    return (
      <View style={styles.container}>
        <View style={{marginLeft: 15,marginRight: 15}}>
                <FlatList
                  ref={(list) => { this.commentsFL = list; }}
                  style={{marginTop: 5,marginBottom: 60}}
                  data={this.state.comments}
                  refreshControl={
                    <RefreshControl
                        refreshing={this.state.isLoading}
                        onRefresh={()=>this.onRefresh()}
                        title="Cargando..."
                        colors={["#15d2bb","#15d2bb","#15d2bb"]}
                        tintColor="#15d2bb"
                        titleColor="#15d2bb"
                     />
                  }


                  showsVerticalScrollIndicator={false}
                  onEndReached={()=>this.handleLoadMore()}
                  onEndThreshold={0}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.renderCommentRow}
                />
        </View>
        <View style={styles.viewBoxComment}>
           <View style={styles.viewBoxCommentContent}>
           {this.getViewProfile(profileFrom,placUserFrom)}
               
                 <View style={styles.viewTextInputMessage}>
                     <TextInput
                      ref={(input) => { this.comment = input; }}
                      returnKeyType={"send"}
                      onContentSizeChange={(event) => {
                       this.setState({height: event.nativeEvent.contentSize.height});
                      }}
                      onSubmitEditing={()=>this.onSubmitEditing()}
                      multiline={true}
                      style={[styles.textInputData, {height: Math.max(36, this.state.height)}]}
                      onChangeText={this.onChangeText}
                      placeholder='Escribe tu comentario'
                      value={this.state.comment}
                  />

                 </View>
                 <TouchableOpacity  disabled={this.state.disabled} onPress={()=>this.onSubmitEditing()}  style={{height: 36,justifyContent: 'center',alignItems: 'center'}}>
                          {(this.state.isStoring)?<View style={{marginBottom: 0,justifyContent: 'center',alignItems: 'center'}}>
                               <Image style={{width: 60,height:23}} source={require('../../../../assets/image/general/loader.gif')}/>
                          </View>:<Text  style={{fontSize: 14,color: '#15d2bb'}}>Publicar</Text>}
                 </TouchableOpacity>
               
           </View>
        </View>
      </View>
    );
  }


    onChangeText=(comment)=>{
      comment=emoticons.parse(emoticons.stringify(comment));
      this.setState({comment});
    }

    onSubmitEditing(){
      this.commentPost();
    }


    onRefresh(){
          this.setState({
            url_get_post_comments:Routes.URL_GET_POST_COMMENTS
          },()=>{
            this.getFetchComments();
          });
    }

    handleLoadMore () {
          paginate=  this.state.paginate;
          nextPageURL=paginate.next_page_url;
          if(paginate.current_page!=paginate.last_page){
              if(nextPageURL){
                    nextPage=nextPageURL.split("?");
                    arrayUrls=this.state.arrayUrls;
                    if(arrayUrls.indexOf(nextPageURL)==-1){
                          arrayUrls.push(nextPageURL);
                            this.setState({
                             loadMore:true,
                              arrayUrls,
                              url_get_post_comments:Routes.URL_GET_POST_COMMENTS+"?"+nextPage[1]
                            }, () => {
                                  this.getFetchComments();
                            });
                    }
              }
        }

    };


  renderCommentRow=(object)=>{
    postComment=object.item;
    profile=  postComment.profile;
    placUser=profile.plac_user;
    return <View  style={{flexDirection: 'row',marginTop: 10,marginBottom: 10,width: "100%"}}>
                {this.getViewProfile(profile,placUser)}
                <View style={{marginLeft: 8,flex:1,}}>
                     <View style={{padding: 8,flex:1,backgroundColor: '#EDEDED',borderRadius: 8}}>
                        <Text style={styles.textPlacUserName}>{profile.profile_name}</Text>
                        <Text  style={styles.textComment}>{Emoticons.parse(postComment.message)}</Text>
                     </View>
                     <View  style={{paddingLeft:  8,paddingTop: 2}}>
                        <Text style={{fontSize: 12,color:'#717171'}}>{postComment.post_time_ago}</Text>
                     </View>
                 </View>
         </View>
  }


 getViewProfile(profile,placUser){
   
  return  <TouchableOpacity onPress={()=>this.goProfile(profile)} style={{height:40,width:40}}>
              <CachedImage style={styles.imageProfile} source={{uri:getUrlImageCompress(profile.profile_image,20)}}/>
             {(profile.profile_type=='pet')? <CachedImage source={{uri:getUrlImageCompress(placUser.plac_user_image,20)}} style={styles.imagePlacUser} />:null}
          </TouchableOpacity>

}


goProfile(profile){
  params.profileTo=profile;
  this.props.navigation.push('ProfileInvite',params);
}




  getFetchComments(){
    url=this.state.url_get_post_comments;
    var body={
      post_id:post.post_id,
      profile_id:profileFrom.profile_id
    }

    Api.post(url,body)
       .then((response)=>{
             var {data} =response;
             var comments= this.state.comments;
             for(i=0;i<data.length;i++){
                   comments.push(data[i]);
             }
             this.setState({
               comments,
               paginate:response,
               isLoading:false,
               loadMore:false,
             });


       }).catch((ex)=>{
          console.warn(ex);
       });
  }


  commentPost(){
    url=Routes.URL_POST_COMMENTS_RESOURCE;

    if(this.state.comment==""){
      return alert("Te hace falta el comentario");
    }

    var body={
      post_id:post.post_id,
      profile_from_id:profileFrom.profile_id,
      profile_to_id:post.profile.profile_id,
      comment:Emoticons.stringify(this.state.comment),
    }
    
    this.setState({
      isStoring:true,
      disabled:true,
    });


    Api.post(url,body)
       .then((response)=>{
         var {data,status} = response;
         var comments=this.state.comments;
         comments.unshift(data);
         this.setState({
           isLoading:false,
           isStoring:false,
           disabled:false,
           comments,
           comment:"",
         },()=>{
           this.comment.clear();
           this.commentsFL.scrollToOffset({ animated: true, offset: 0 });
           params.commentPostCallBack();
         });


       }).catch((ex)=>{
          console.warn(ex);
       });
  }



  componentDidMount(){
    this.getFetchComments();
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
     backgroundColor: "white",
  },

  textPlacUserName:{
    fontSize: 12,
    color: 'black',
    fontWeight: 'bold'
  },
  textComment:{
    fontSize: 14,
    color: 'black'
  },
  viewBoxComment:{
    alignItems: 'center',
    borderTopWidth: 1,
    borderColor: '#D1D1D1',
    width,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'white',
  },
  viewBoxCommentContent:{
    flexDirection: 'row',
    flex:1,marginLeft: 15,
    marginRight: 15,
    paddingTop:6,paddingBottom: 6
  },
  viewTextInputMessage:{
    marginLeft:10,
    marginRight: 10,
    flex:1,
    borderRadius: 8,
    borderWidth: 1,
    borderColor:'#00B99B'
  },
  
  textInputData:{
    height: 36,
    width: '100%',
    fontSize: 14
  },
  imageProfile:{
    width: 36,
    height: 36,
    borderRadius: 18
  },
  imagePlacUser:{
    width:20,
    height:20,
    borderRadius:10,
    position:'absolute',
    right:0,
    bottom:0
  }



});
