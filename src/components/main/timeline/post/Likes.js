import React, { Component } from 'react';
import {
   AppRegistry,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  RefreshControl,
  Dimensions,
  FlatList,
} from 'react-native';

// API
import Api from  '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'


import ListRow from '../../../views/profile/ListRow';

var params;
var post;

export default class Likes extends React.Component{
  static navigationOptions = {
    title:"Me gustas",
  };



   constructor(props){
     super(props);
     params=this.props.navigation.state.params;
     post=params.post;
     this.state={
       likes:[],
       isLoading:true,
       arrayUrls:[],
       url_get_post_likes:Routes.URL_GET_POST_LIKES
     }
   }


 _keyExtractor = (item, index) => index.toString();

  render() {
    return (
      <View style={styles.container}>
        <View style={{}}>
                <FlatList
                  style={{marginTop: 5}}
                  data={this.state.likes}
                  refreshControl={
                    <RefreshControl
                        refreshing={this.state.isLoading}
                        onRefresh={()=>this.onRefresh()}
                        title="Cargando..."
                        colors={["#15d2bb","#15d2bb","#15d2bb"]}
                        tintColor="#15d2bb"
                        titleColor="#15d2bb"
                     />
                  }
                  ItemSeparatorComponent={()=> <View
                         style={{
                           height: 1,
                           width: "100%",
                           backgroundColor: "#CED0CE",
                         }}
                       />}

                  showsVerticalScrollIndicator={false}
                  onEndReached={()=>this.handleLoadMore()}
                  onEndReachedThreshold={.5}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.renderRow}
                />
        </View>
      </View>
    );
  }


    onRefresh(){
        this.setState({
          url_get_post_likes:Routes.URL_GET_POST_LIKES
        },()=>{
          this.getPostLikes();
        });
    }

    handleLoadMore () {
          paginate=  this.state.paginate;
          nextPageURL=paginate.next_page_url;
          if(paginate.current_page!=paginate.last_page){
              if(nextPageURL){
                    nextPage=nextPageURL.split("?");
                    arrayUrls=this.state.arrayUrls;
                    if(arrayUrls.indexOf(nextPageURL)==-1){
                          arrayUrls.push(nextPageURL);
                            this.setState({
                              loadMore:true,
                              arrayUrls,
                              url_get_post_likes:Routes.URL_GET_POST_LIKES+"?"+nextPage[1]
                            }, () => {
                                  this.getPostLikes();
                            });
                    }
              }
        }

    };

    renderRow=(object)=>{
      postLike=object.item;
      profile=postLike.profile;
      params=this.props.navigation.state.params;
      params.profileFromId=params.profileFrom.profile_id;
      params.profileToId=profile.profile_id;
      this.props.navigation.state.params=params;
      return <ListRow  isFollowing={profile.isFollowing} navigation={this.props.navigation} profile={profile}/>
     
    }

  







  goProfile(object){
    postLike=object.item;
    profile=postLike.profile;
    params.profileTo=profile;
    this.props.navigation.push('ProfileInvite',params);
  }


  getPostLikes(){

    url=this.state.url_get_post_likes;

    var body={
      post_id:post.post_id,
      profile_id:params.profileFrom.profile_id
    }
    console.warn(url);

    Api.post(url,body)
       .then((response)=>{
         console.warn(response);

         var {status,data,message} =response;
         var likes= this.state.likes;
         for(i=0;i<data.length;i++){
               likes.push(data[i]);
         }

         this.setState({
           likes,
           paginate:response,
           isLoading:false,
           loadMore:false,
         });


       }).catch((ex)=>{
          console.warn(ex);
       });

  }

  componentDidMount(){
    this.getPostLikes();
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
     backgroundColor: "white",
  },


});
