import React, { Component } from 'react';
import {
   AppRegistry,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  RefreshControl,
  Dimensions,
  FlatList,
} from 'react-native';

// API
import Api from  '../../../../lib/http_request/';
import * as Routes from '../../../../app/routes'

import ButtonContinue from '../../../views/ButtonContinue'

const { width,height} = Dimensions.get('window');
var params;
var post;

export default class Report extends React.Component{
  static navigationOptions = {
    title:"Reportar",
  };



   constructor(props){
     super(props);
     params=this.props.navigation.state.params;

     this.state={
       complaints:[],
       isLoading:true,
       optionSelected:null,
       isReported:false,

     }
   }


 _keyExtractor = (item, index) => item.complaint_id;

  render() {
    return (
      <View style={styles.container}>
        <View style={{marginLeft: 15,marginRight: 15}}>

           {(!this.state.isReported)?
             <View>
               <View style={{borderBottomWidth: 1,borderBottomColor: "#EDEDED",paddingBottom: 10,marginTop: 15}}>
                  <Text style={{fontSize: 16,color:'#555555'}}>Selecciona el motivo por el cual reportas esta publicación. </Text>
               </View>

                <FlatList
                  style={{marginTop: 5}}
                  data={this.state.complaints}
                  showsVerticalScrollIndicator={false}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.renderRow}
                />
                <View style={{marginTop:50}}>
                  <ButtonContinue onPress={()=>this.reportPost()}  isValidData={this.state.isValidData} text={"Reportar"}/>
                </View>
              </View>:<View style={{flex:1,marginTop: 15}}>
                             <Text allowFontScaling={false} style={{marginTop:5,fontWeight:'bold',fontSize:16,color:'black',}}>Gracias por denunciar esta publicación</Text>
                             <Text allowFontScaling={false} style={{marginTop:10,fontSize:13,}}>Te agradecemos por la ayuda que nos das al denunciar este tipo de contenido, con esto PLAC puede seguir siendo un lugar seguro y confiable para todos. </Text>
                             <Text allowFontScaling={false} style={{fontSize:13,marginTop:15,marginBottom: 50}}>Recuerda, PLAC no comunicará quien ha denunciado la publicación</Text>
                             <ButtonContinue text={"Regresar"} isValidData={true} onPress={()=>this.props.navigation.goBack()}/>

                     </View>}

        </View>
      </View>
    );
  }

  renderRow=(object)=>{
    item=object.item;
    stylePress={};
    if(item.isSelected){
      stylePress={backgroundColor:"#15d2bb",borderColor:"#15d2bb"}
    }

    return <TouchableOpacity  onPress={()=>this.optionSelected(object.item)} style={{flexDirection: 'row',height: 48,width:width-30,alignItems: 'center'}}>
              <View style={{width: 48,height: 48,justifyContent: 'center',alignItems: 'center'}}>
                  <View  style={[styles.buttonOption,stylePress]}/>
              </View>
              <View style={{marginRight: 50,}}>
                <Text style={{fontSize: 14,color:'#555555',}}>{item.complaint_name}</Text>
              </View>
          </TouchableOpacity>
  }

  optionSelected(item){
        complaints=this.state.complaints;
        for(i=0;i<complaints.length;i++){
          if(complaints[i].complaint_id==item.complaint_id){
            complaints[i].isSelected=1;

          }else{
              complaints[i].isSelected=0;
          }
        }
        this.setState({
          complaints,
          isValidData:true,
          optionSelected:item
     });
  }

  reportPost(){

    var body={
      complaint:this.state.optionSelected,
      post:params.post,
      plac_user_id:params.placUser.plac_user_id,
    }
    url=Routes.URL_POST_COMPLAINTS_RESOURCE;
    Api.post(url,body)
       .then((response)=>{
         var {status, data}=response;
         if(status=="success"){
           this.setState({isReported:true});

         }


       }).catch((ex)=>{
          console.warn(ex);
       });
  }





  getPostComplaints(){
    url=Routes.URL_GET_POST_COMPLAINTS;
    Api.get(url)
       .then((response)=>{
         var {data} =response;
         complaints=data;
         for(i=0;i<complaints.length;i++){
           complaints[i].isSelected=0;
         }



         this.setState({
           complaints,
         });

       }).catch((ex)=>{
          console.warn(ex);
       });
  }

  componentDidMount(){
    this.getPostComplaints();
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
     backgroundColor: "white",
  },
  buttonOption:{
    width: 24,
    height: 24,
    borderRadius: 12,
    borderWidth: 1,
  }


});
