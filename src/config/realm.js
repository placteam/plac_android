'use strict';
import Realm from 'realm';
import PlacUser from '../app/models/PlacUser';
import Profile from '../app/models/Profile';
import UserConfiguration from '../app/models/UserConfiguration'
import AppConfiguration from '../app/models/AppConfiguration'
import PlacUserConfiguration from '../app/models/PlacUserConfiguration'
import OrderDetail from '../app/models/OrderDetail'



export default new Realm({schema: [PlacUser,Profile,UserConfiguration,AppConfiguration,PlacUserConfiguration,OrderDetail],schemaVersion:29});
