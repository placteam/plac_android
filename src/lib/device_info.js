import DeviceInfo from 'react-native-device-info';

export function getDeviceInfo(){
    return {
            app_version:DeviceInfo.getVersion(),
            so_name:DeviceInfo.getSystemName(),
            so_version:DeviceInfo.getSystemVersion(),
            so_version_release:DeviceInfo.getSystemVersion(),
            device_locale:DeviceInfo.getDeviceLocale(),
            device_country:DeviceInfo.getDeviceCountry(),
        }

}
