import Enviroment from "../../../config/environment"

class Api {
  static headers() {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'dataType': 'json',
    }
  }

  static post(route, params) {
    return this.xhr(route,params,'init', 'POST')
  }


  static get(route) {
    return this.xhr(route,null,'init', 'GET');
  }


  static put(route, params) {
    return this.xhr(route, params,'init', 'PUT')
  }


  static delete(route, params) {
    return this.xhr(route, params,'init', 'DELETE')
  }

  static xhr(route, params, urlFinal,verb) {
    const host = Enviroment.getUrlEnviroment();
  
    if(urlFinal=="init"){
       urlFinal = `${host}${route}`
    }
    console.log('URL');
    console.log(urlFinal);



    let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );

    options.headers = Api.headers()
    return fetch(urlFinal, options).then( resp => {
      let json = resp.json();
      if (resp.ok) {
        return json
      }
      return json.then(err =>

        { console.log(err);
          throw err
        }
      );
    }).then( json => json );
  }

}



export default Api
