

export function validateEmail(email) {
  let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function validateSpecialCharacters(string){
  var re = /^[a-zA-Z0-9\sa-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/;
  return re.test(string);
}

export function cleanString(string){

   return string.replace(/[^a-zA-Z0-9]/g, "") ;

}
