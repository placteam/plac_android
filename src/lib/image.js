export function getProportionalImageHeight(image,widthDevice){
    return (image.height / image.width) * widthDevice;
}


export function getProportionalImageWidth(w1: number,h1:  number,h2: number){
    return (h2  * w1)/h1;
}


export function getUrlImageCompress(urlImage,compress=50){
    if(urlImage!=null){
        var split=urlImage.split("/upload");
        var style="/q_"+compress;
        return split[0]+'/upload'+style+split[1];
    }else{
        return "";
    }
     
}



