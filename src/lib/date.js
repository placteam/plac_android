export function getCurrentDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
     if(dd<10) {
          dd = '0'+dd
      }
      if(mm<10) {
          mm = '0'+mm
      }
    today = yyyy+'-'+ mm + '-' + dd;
    return today;
}

export function getAge(date){
  // Si la fecha es correcta, calculamos la edad
  var values=date.split("-");
  var dia = values[2];
  var mes = values[1];
  var ano = values[0];


  // cogemos los valores actuales
  var fecha_hoy = new Date();
  var ahora_ano = fecha_hoy.getYear();
  var ahora_mes = fecha_hoy.getMonth()+1;
  var ahora_dia = fecha_hoy.getDate();

  var ageFinal="No definido";

  // realizamos el calculo
  var edad = (ahora_ano + 1900) - ano;
  if ( ahora_mes < mes ){
      edad--;
  }
  if ((mes == ahora_mes) && (ahora_dia < dia)){
      edad--;
  }
  if (edad > 1900){
      edad -= 1900;
  }
  // calculamos los meses
  var meses=0;
  if(ahora_mes>mes){
      meses=ahora_mes-mes;
  }
  if(ahora_mes<mes){
      meses=12-(mes-ahora_mes);
   }
  if(ahora_mes==mes && dia>ahora_dia){
      meses=11;
  }
  // calculamos los dias
  var dias=0;
  if(ahora_dia>dia){
      dias=ahora_dia-dia;
  }
  if(ahora_dia<dia){
      ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
      dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
  }

   if(edad ===0&& meses===0){
      textoDias=" días";
    if(dias==1){
        textoDias=" día"
      }
     ageFinal=    dias+textoDias;
  }else{
      var textoAnos=" años ";
      var textoMeses=" meses"
    if(edad==1){
        textoAnos=" año ";
      }
    if(meses==1){
      textoMeses=" mes ";
      }
    ageFinal= edad+textoAnos+meses+textoMeses;
  }

  return ageFinal;
}


export function getBirthdayDate(birthday){
  let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
  if(birthday){
     birthday=(birthday.replace('00:00:00',"")).trim(); 
     birthdayArray= birthday.split("-");
    return (birthdayArray[2] + " de " + meses[birthdayArray[1]-1]);
  }else{
    return "NA";
  }


}


