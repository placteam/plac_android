import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList,PermissionsAndroid,Animated } from 'react-native';
import { SafeAreaView } from 'react-navigation'
import Contacts from 'react-native-contacts';

import Api from './src/lib/http_request';
import * as Routes from './src/app/routes'
import ListRow from './src/components/views/profile/ListRow';


export default class HelloWorldApp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      placUsers: [],
      isActiveContinue: false,
      contactListToSM:[],
      contactsUnMatch:[],
    }

  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1,marginHorizontal:15}}>
        <View>
          <Text style={{ fontSize: 20 }}>Contactos</Text>
        </View>
        {this.getViewAboutContactPermissions()}
   

      
      </SafeAreaView>
    );
  }

  renderItemSimple=(object)=>{
   let contact=object.item;
   let styleCheck=(contact.isAdded)?{backgroundColor:'green',width:"100%",height:'100%'}:{};

   let name=  contact.givenName +" "+ contact.familyName;
   let email = (contact.emailAddresses.length > 0) ? contact.emailAddresses[0].email : "";
  
   return <View style={{flexDirection:'row',marginTop:10,alignItems:'center'}}>
              <View style={{flex:1,flexDirection:'row',alignItems:'center'}}>
                  <View style={{width:40,height:40,borderRadius:20,backgroundColor:'#15d2bb',justifyContent:'center',alignItems:'center'}}>
                    <Text style={{color:'white'}}>{name.substring(0, 1)}</Text>
                  </View>
                  <View style={{marginLeft:10}}>
                    <Text style={{color:'black',fontSize:14}}>{name}</Text>
                    <Text style={{fontSize:12}}>{email}</Text>
                
                  </View>
              </View>
              <TouchableOpacity onPress={()=>this.addContact(contact)} style={[{borderWidth:1,width:20,height:20}]}>
                <View style={[styleCheck]}>

                </View>

              </TouchableOpacity>
        </View>
  }

  addContact(item){
  
    let list=this.state.contactsUnMatch;
    for(i=0;i<list.length;i++){
      let itemList=list[i];
      if(itemList.recordID==item.recordID){
        if(itemList.isAdded){
          itemList.isAdded=false;
        }else{
          itemList.isAdded=true;
        }
        list[i]=itemList;
      }
      
    }
    console.warn(item);

    this.setState({contactsUnMatch:list});
    
  }

  getViewAboutContactPermissions() {
    if (this.state.isActiveContinue) {
      return null;
    }
    return <View style={{ flex: 1 }}>
      <View style={{ marginHorizontal: 15, marginTop: 15, justifyContent: 'center', alignItems: "center" }}>
        <Text style={{ fontSize: 18, textAlign: "center" }}>Averigua  quien de tus contactos ya esta en PLAC</Text>

        <Image style={{ width: 80, height: 80, marginTop: 60 }} source={{ uri: 'https://image.flaticon.com/icons/png/512/1312/1312151.png' }} />

      </View>

      <View style={styles.viewButtonsAbsolute}>
        <TouchableOpacity onPress={this.onPressContinue} style={styles.buttonContinue}>
          <Text style={{ paddingHorizontal: 10, paddingVertical: 8, fontSize: 14, color: 'white' }}>
            Continuar                                                                                                                  
              </Text>
        </TouchableOpacity >
        <TouchableOpacity style={styles.buttonSkip}>
          <Text style={{ fontSize: 12, color: '#c0c0c0' }}>
            Saltar
              </Text>
        </TouchableOpacity >


      </View>
    </View>
  }

  onPressContinue = () => {
    this.setState({
      isActiveContinue: true
    });

    this.requestContactPermission();

   
  }

  _keyExtractorUser = (item, index) => index.toString();
  getViewContactsList() {
    if (this.state.isActiveContinue) {
      return <View style={{ flex: 1 }}>
              <Text>Tienes {this.state.placUsers.length} amigos en PLAC</Text>
        <FlatList
          style={{ flex: 1, marginTop: 10, }}
          data={this.state.placUsers}
          extraData={this.state}
          showsVerticalScrollIndicator={false}
          keyExtractor={this._keyExtractorUser}
          renderItem={this.renderItemUser}
        />


      </View>
    }
  }

  _keyExtractorProfile = (item, index) => item.profile_id;
  renderItemUser = (object) => {

    let placUser = object.item;
    let profiles = placUser.profiles;
    return <FlatList
      style={{ flex: 1, marginTop: 10, }}
      data={profiles}
      extraData={this.state}
      showsVerticalScrollIndicator={false}
      keyExtractor={this._keyExtractorProfile}
      renderItem={(object) => this.renderItemProfiles(object, placUser)}
    />

  }

  renderItemProfiles = (object, placUser) => {
    let profile = object.item;
    profile.plac_user = placUser;
    return <ListRow isFollowing={false} navigation={{
      state: {
        params: {
          profileFromId: ""
        }
      }
    }} profile={profile} />
  }

  async requestContactPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          'title': 'Cool Photo App Camera Permission',
          'message': 'Cool Photo App needs access to your camera ' +
                     'so you can take awesome pictures.'
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {

      } else {
        console.log("Camera permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }

  checkContactsUnMatch(placUsers){
    let contactsDevice= this.state.contacts;
    let contactsUnMatch=[];
    for(let i=0;i<contactsDevice.length;i++){
      let contact = contactsDevice[i];
      let email = (contact.emailAddresses.length > 0) ? contact.emailAddresses[0].email : "";
      if(email!=""){
          contactsUnMatch.push(contact);
          for(let y=0;y<placUsers.length;y++){
            let placUser=placUsers[y];
            if(email==placUser.plac_user_email){
              contactsUnMatch.pop();
            }
          }
      }
    }
    //console.warn(contactsUnMatch);
    this.setState({contactsUnMatch});
   
  }

  checkPlacUserContactsInPLAC() {

    URL = 'placusers/contactdata';
    var body = {
      ...this.state
    };
    Api.post(URL, body)
      .then((response) => {
        var { data, status, message } = response;
        if (status == "success") {
          this.setState({
            placUsers: data,
          });

          this.checkContactsUnMatch(data);
        }

      }).catch((ex) => {

      });
  }


}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  viewButtonsAbsolute: {
    position: 'absolute',
    bottom: 40,
    height: 80,
    width: '100%',
    justifyContent: 'center'
  },
  buttonContinue: {
    borderRadius: 8,
    backgroundColor: '#15d2bb',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },
  buttonSkip: {
    marginTop: 10,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },


});



 

  