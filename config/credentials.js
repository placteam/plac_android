
var enviromentType='development';


export function getGoogleClientIdByEnvironment(){
  switch (enviromentType) {
    case "production":
     return getUrlsRelease();
    break;
    case "development":
    return getUrlsDebug();
    break;
  }
}


export function getUrlsRelease(){
  return {
    web_client_id:"839585325028-nq8m5pqciff007dfnpcmsrcki18aacmt.apps.googleusercontent.com",
    ios_client_id:"839585325028-6c4r8q3fivphj5ppk7q5kfshifiqvbrb.apps.googleusercontent.com",
    android_client_id:"839585325028-ojk00gjcvrdsjb5bukh6mr8v5d21up79.apps.googleusercontent.com"
  }
}

export function  getUrlsDebug(){
      return {
        web_client_id:"839585325028-oiu37hjimlqn1fq3523kiud458gjlk96.apps.googleusercontent.com",
        ios_client_id:"839585325028-52udda8m0jk934fkjqc0g1jruomt0l5d.apps.googleusercontent.com",
        android_client_id:"839585325028-0ps7i6d3h5g9a6rfbtjvg6se8hhdp8jg.apps.googleusercontent.com"
      }
}
