import {
  Platform,
 } from 'react-native';

module.exports={
    getCompressQuality(){
      var compressImageQuality=.9;
      if(Platform.OS=="ios"){
        compressImageQuality=.7;
      }
      return compressImageQuality;
    },

}
